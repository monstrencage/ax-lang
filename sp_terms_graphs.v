(** This module is devoted to the translation from series-parallel
terms to graphs. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export graph sp_terms.

Section s.

  (** * Definitions and basic facts *)
  (** We fix a decidable alphabet [X]. *)
  Variable X : Set.
  Variable Λ : decidable_set X.

  Notation grph := (graph nat X).
  Notation edg := (@edge nat X).

  (** The length of a term is defined inductively as follows. *)
  Fixpoint ln (u : 𝐒𝐏 X) :=
    match u with
    | 𝐒𝐏_var _  => S O
    | u ⨾ v | u ∥ v => ln u + ln v
    end.

  (** In fact, it coincides with the length of the list of variables. *)
  Lemma size_ln u : ln u = #(𝒱 u).
  Proof.
    unfold 𝒱;induction u;simpl in *;try rewrite app_length; lia.
  Qed.

  (** As such, it is smaller that the size of the term. *)
  Lemma ln_smaller_size u : ln u <= |u|.
  Proof. rewrite 𝐒𝐏_size_𝐒𝐏_variables_eq, size_ln;lia. Qed.

  (** A term always has strictly positive length. *)
  Lemma ln_non_zero u : 0 < ln u.
  Proof. induction u;simpl;auto with arith. Qed.

  (** We associate to every term a graph as follows: *)  
  Fixpoint 𝕲 (u : 𝐒𝐏 X) : grph :=
    match u with
    | 𝐒𝐏_var a => (0,[(0,a,S 0)],S 0)
    | u⨾v => 𝕲 u ⋄ graph_map (⨥ (ln u)) (𝕲 v)
    | u∥v =>
      (graph_map {|ln u\ln u+ln v|} (𝕲 u))
        ⋄ graph_map ({|ln u \ 0|} ∘ ⨥ (ln u)) (𝕲 v)
    end.

  (** The list of labels of the graph of a term is exactly its list of
  variables. *)
  Lemma graph_variables u : 𝒱 u = 𝒱 (𝕲 u).
  Proof.
    induction u;simpl;try reflexivity.
    - rewrite variables_graph_union,variables_graph_map.
      rewrite <- IHu1,<-IHu2;reflexivity.
    - rewrite variables_graph_union,variables_graph_map,
      variables_graph_map.
      rewrite <- IHu1,<-IHu2;reflexivity.
  Qed.
  
  (** The input of the graph of a term is always [0], and its output is
  equal to the length of the term.*)
  Lemma graph_input u : input (𝕲 u) = 0.
  Proof.
    induction u;simpl;auto.
    rewrite input_graph_union.
    destruct (𝕲 u1) as ((i&e)&o);simpl.
    unfold graph_map;autounfold in *;simpl in *.
    rewrite IHu1;unfold replace.
    destruct_eqX 0 (ln u1);auto.
    pose proof (ln_non_zero u1);lia.
  Qed.

  Lemma graph_output u : output (𝕲 u) = ln u .
  Proof.
    induction u;simpl;auto.
    rewrite IHu2;unfold Basics.compose,replace;autounfold.
    destruct_eqX (ln u1 + ln u2) (ln u1);auto.
    pose proof (ln_non_zero u2);lia.
  Qed.

  (** If the edge [(i,α,j)] appears in the graph of [u], then we can
  infer the following inequalities.*)
  Lemma 𝕲_bounded u i α j : (i,α,j) ∈ (edges (𝕲 u)) -> i < j <= ln u.
  Proof.
    revert i α j;induction u;simpl;auto.
    - intros i α j;intros [h|F];[|tauto];inversion h;lia.
    - intros i α j;destruct (𝕲 u1) as ((i1&e1)&o1);
        destruct (𝕲 u2) as ((i2&e2)&o2);
        rewrite edges_graph_union,in_app_iff;
        unfold graph_map;autounfold;simpl.
      rewrite graph.in_map;intros [I|(?&?&I&<-&<-)].
      -- apply IHu1 in I;lia.
      -- apply IHu2 in I;lia.
    - intros i α j;destruct (𝕲 u1) as ((i1&e1)&o1);
        destruct (𝕲 u2) as ((i2&e2)&o2);
        rewrite edges_graph_union,in_app_iff;
        unfold graph_map;autounfold;simpl.
      repeat rewrite graph.in_map;intros [(?&?&I&<-&<-)|(?&?&I&<-&<-)].
      -- apply IHu1 in I;unfold replace.
         destruct_eqX x (ln u1);[lia|].
         destruct_eqX x0 (ln u1);lia.
      -- apply IHu2 in I;unfold replace.
         destruct_eqX (ln u1 + x) (ln u1).
         --- destruct_eqX (ln u1 + x0) (ln u1);lia.
         --- destruct_eqX (ln u1 + x0) (ln u1);lia.
  Qed.

  (** The length of a term is also its greatest vertex. *)
  Lemma vertices_bounded u i : i ∈ ⌊𝕲 u⌋ -> i <= ln u.
  Proof.
    intro I;apply in_graph_vertices in I as [<-|[<-|[(?&?&I)|(?&?&I)]]].
    - rewrite graph_input;lia.
    - rewrite graph_output;lia.
    - apply 𝕲_bounded in I;lia.
    - apply 𝕲_bounded in I;lia.
  Qed.

  (** We give an inductive definition of the set of vertices of the
  graph of a term, which will we show to be equivalent to the real
  thing. *)
  Fixpoint vertices u :=
    match u with
    | 𝐒𝐏_var _ => (S O)::O::[]
    | u⨾v => vertices u++(List.map (⨥ (ln u)) (vertices v))
    | u∥v => rm (ln u) (vertices u++(List.map (⨥ (ln u)) (vertices v)))
    end.
  (* begin hide *)
  Lemma injective_par_map_left u v :
    injective {|ln u \ ln u + ln v|} ⌊𝕲 u⌋.
  Proof.
    apply injective_replace.
    intros h;apply vertices_bounded in h.
    pose proof (ln_non_zero v);lia.
  Qed.
  
  Lemma injective_par_map_right u v :
    injective ({| ln u \ 0 |} ∘ ⨥ (ln u)) ⌊𝕲 v⌋.
  Proof.
    apply injective_compose;[apply injective_add_left
                            |apply injective_replace].
    intros h;apply in_map_iff in h as (?&E&I).
    autounfold in *;pose proof (ln_non_zero u);lia.
  Qed.
  
  Lemma inverse_par_map_left u v :
    inverse {| ln u \ ln u+ln v |} {| ln u+ln v \ ln u |} ⌊ 𝕲 u⌋.
  Proof.
    apply inverse_replace.
    intros h;apply vertices_bounded in h.
    pose proof (ln_non_zero v);lia.
  Qed.
  
  Lemma inverse_par_map_right u v :
    inverse ({|ln u\ 0|}∘⨥ (ln u)) (⨪ (ln u)∘{|0\ln u|}) ⌊𝕲 v⌋.
  Proof.
    apply inverse_composition;[apply inverse_add_left
                              |apply inverse_replace].
    intros h;apply in_map_iff in h as (?&E&I).
    autounfold in *;pose proof (ln_non_zero u);lia.
  Qed.
  Hint Resolve injective_par_map_right injective_par_map_left
       injective_add_left inverse_add_left NatNum
       inverse_par_map_right inverse_par_map_left.
  (* end hide *)

  (** The graph of [u⨾v] is the sequential product of the graphs of
  [u] and [v] (modulo a translation of the graph of [v] to avoid
  vertex capture). *)
  Lemma good_for_seq_𝕲 u v:
    good_for_seq (𝕲 u) (graph_map (⨥ (ln u)) (𝕲 v)).
  Proof.
    split.
    - rewrite graph_output,input_graph_map,graph_input;autounfold;lia.
    - intro x;rewrite graph_vertices_map,input_graph_map,graph_input.
      intros I J;apply in_map_iff in J as (?&<-&J).
      apply vertices_bounded in I;apply vertices_bounded in J.
      autounfold in *;lia.
  Qed.

  (** The graph of [u∥v] is the parallel product of the graphs of [u]
  and [v] (modulo two translations to avoid vertex capture). *)
  Lemma good_for_par_𝕲 u v:
    good_for_par (graph_map {|ln u\ln u+ln v|} (𝕲 u))
                 (graph_map ({|ln u\0|}∘⨥(ln u)) (𝕲 v)). 
  Proof.
    split;[|split].
    - rewrite input_graph_map,input_graph_map,graph_input,graph_input.
      unfold replace, Basics.compose. 
      destruct_eqX 0 (ln u);[pose proof (ln_non_zero u);lia|].
      simpl_nat;rewrite eqX_refl;auto.
    - rewrite output_graph_map,output_graph_map,graph_output,
      graph_output;unfold replace, Basics.compose;rewrite eqX_refl;auto.
      autounfold in *;destruct_eqX (ln u + ln v) (ln u);
        [pose proof (ln_non_zero v)|];lia.
    - intro x;rewrite graph_vertices_map,input_graph_map,graph_input.
      rewrite output_graph_map,graph_output,graph_vertices_map.
      rewrite in_map_iff,in_map_iff.
      intros (?&E1&I) (?&E2&J);rewrite<-E1;rewrite<- E2 in E1;revert E1.
      apply vertices_bounded in I;apply vertices_bounded in J.
      unfold replace,Basics.compose;simpl_nat.
      rewrite eqX_refl;autounfold in *;
        destruct_eqX (ln u + ln v) (ln u);
        [pose proof (ln_non_zero v);lia|];auto.
      destruct_eqX (x0) (ln u);auto.
      destruct_eqX (ln u + x1) (ln u);lia. 
  Qed.
  (* begin hide *)
  Hint Resolve good_for_par_𝕲 good_for_seq_𝕲.
  (* end hide *)

  (** The path relation in term-graphs is contained in the
  _smaller-or-equal_ relation of natural numbers. *)
  Lemma path_𝕲 u i j : i -[𝕲 u]→ j -> i <= j.
  Proof.
    intro P;induction P;try lia.
    apply 𝕲_bounded in H;lia.
  Qed.

  (** Hence we get that the path relation is a partial order, thus
  proving that the graph of a term is always acyclic.*)
  Lemma 𝕲_acyclic u : acyclic (𝕲 u).
  Proof.
    intros x y;split;[intros ->;split;reflexivity|].
    intros (h1&h2);apply path_𝕲 in h1;apply path_𝕲 in h2;
      lia.
  Qed.

  (** Is it not hard to show that it is also connected, thus making it
  a _good_ graph.*)
  Lemma 𝕲_good u : good (𝕲 u).
  Proof.
    induction u.
    - split.
      + simpl;intros i [<-|[<-|[<-|[<-|F]]]];
          autounfold;simpl in *;split;
            tauto || reflexivity || (eapply path_edge;now left).
      + apply 𝕲_acyclic.
    - simpl; apply good_seq_graph_union;auto.
      apply graph_map_injective_good;auto.
    - simpl; apply good_par_graph_union;auto;
        apply graph_map_injective_good;auto.
  Qed.
  
  Lemma 𝕲_connected u : connected (𝕲 u).
  Proof. apply 𝕲_good. Qed.
  (* begin hide *)
  Hint Resolve 𝕲_connected ln_non_zero 𝕲_good 𝕲_acyclic.
  (* end hide *)

  (** The length of a term and [0] are vertices. *)
  Lemma vertex_ln u : ln u ∈ vertices u.
  Proof.
    induction u;simpl;auto.
    - apply in_app_iff;right;apply in_map_iff;exists (ln u2);
        split;autounfold in *;[lia|apply IHu2].
    - apply in_rm;split;[pose proof (ln_non_zero u2);lia|].
      apply in_app_iff;right;apply in_map_iff;exists (ln u2);autounfold in *;
        split;[lia|apply IHu2].
  Qed.

  Lemma vertex_0 u : 0 ∈ (vertices u).
  Proof.
    induction u;simpl;auto.
    - apply in_app_iff;left;apply IHu1.
    - apply in_rm;split;[pose proof (ln_non_zero u1);lia|].
      apply in_app_iff;left;apply IHu1.
  Qed.

  (** As promised, the set of vertices of the graph of [u] is
  [vertices u]. *)
  Lemma vertices_graph_vertices u :
    vertices u ≈ ⌊𝕲 u⌋.
  Proof.
    induction u;simpl.
    - unfold graph_vertices;autounfold;simpl;intro;simpl;firstorder.
    - rewrite seq_vertices_connected_graph_union;auto.
      rewrite graph_vertices_map,IHu1,IHu2;reflexivity.
    - rewrite par_vertices_graph_union;auto.
      rewrite graph_vertices_map,graph_vertices_map,rm_app,
      <-IHu1,<-IHu2;clear IHu1 IHu2.
      intro x;repeat rewrite in_rm || rewrite in_app_iff
              || rewrite in_map_iff.
      split;[intros [(N&I)|(N&y&<-&I)]
            |intros [(y&E&I)|(y&E&I)]];
      unfold replace,Basics.compose in *;simpl.
      -- left;exists x;split;auto.
         destruct_eqb x (ln u1);tauto.
      -- right;exists y;split;auto.
         autounfold in *;destruct_eqb (ln u1+y) (ln u1);lia.
      -- revert E;destruct_eqb y (ln u1);auto.
         --- intros <-;right;split;auto.
             ---- pose proof (ln_non_zero u2);lia.
             ---- exists (ln u2);split;autounfold in *;[lia|apply vertex_ln].
         --- destruct_eqX y (ln u1);[tauto|];intros <-;left;auto.
      -- revert E;autounfold in *;destruct_eqX (ln u1 + y) (ln u1);
           intros <-.
         --- left;split;[pose proof(ln_non_zero u1);lia|apply vertex_0].
         --- right;split;auto;exists y;split;[lia|auto].
  Qed.

  (** The extremities of every edge in [𝕲 u] are vertices. *)
  Lemma In_vertices u i α j :
    (i,α,j) ∈ (edges (𝕲 u)) -> i ∈ (vertices u) /\ j ∈ (vertices u).
  Proof.
    intros;split;apply vertices_graph_vertices,in_graph_vertices;
      right;right;eauto.
  Qed.
  (* begin hide *)

  Ltac is_vertex :=
    repeat (match goal with
            | [h: In (_,_,_) (edges (𝕲 _)) |- _ ] =>
              apply In_vertices in h;destruct h
            | [ |- In _ ⌊_⌋ ] =>
              apply vertices_graph_vertices
            end);
    tauto.
  (* end hide *)
  
  (** If there is a path from [i] to [j] in [𝕲 u], then either both of
  them are vertices of [u] or none of them are. *)
  Lemma path_vertices u i j :
    i-[𝕲 u]→ j -> i ∈ (vertices u) <-> j ∈ (vertices u).
  Proof.
    intro P;rewrite vertices_graph_vertices;apply path_graph_vertices,P.
  Qed.

  (** If [φ] is a morphism from the graph of [u] to that of [v], and
  if [i] is a vertex of [u], then its image is a vertex of [v]. *)
  Lemma is_morphism_vertices φ u v:
    [] ⊨ 𝕲 u -{φ}→ 𝕲 v -> forall i, i ∈ (vertices u) -> (φ i) ∈ (vertices v).
  Proof.
    intros (_&_&_&M) i V;
      apply vertices_graph_vertices,M,vertices_graph_vertices,V.
  Qed.
  (* begin hide *)
  Hint Resolve In_vertices vertices_graph_vertices.

  Ltac ln_pos :=
    repeat (match goal with
            | [ u : 𝐒𝐏 _ |- _ ] =>
              match goal with
              | [ _ : 0 < ln u |- _ ] => fail 1
              | _ => let L := fresh "L" in
                    pose proof (ln_non_zero u) as L
              end
            end).
  Ltac show_integers :=
    (match goal with
     | [ h : _ -[ _ ]→ _ |- _ ] =>
       apply path_𝕲 in h
     | [ h : In _ (edges (𝕲 _ )) |- _ ] =>
       apply 𝕲_bounded in h
     | [ h : In _ ⌊ _ ⌋ |- _ ] =>
       apply vertices_bounded in h
     | [ h : In _ (vertices _ ) |- _ ] =>
       apply vertices_graph_vertices,vertices_bounded in h
     end).
  Ltac integers :=
    ln_pos;unfold incr in *;repeat show_integers;simpl in *;lia.
  (* end hide *)

  (** The next few lemma extract from a path in either [𝕲 (u1 ⨾ u2)]
      or [𝕲 (u1 ∥ u2)] paths in either [𝕲 u1] or [𝕲 u2]. *)
  Lemma path_seq_left u1 u2 i k :
    k ∈ (vertices u1) ->  i -[𝕲 (u1 ⨾ u2)]→ k -> i -[𝕲 u1]→ k.
  Proof.
    intros I G;pose proof (path_𝕲 G) as L;simpl in G;
      apply path_seq in G as [->|(N&[(I1&I2&P)|[(I1&I2&P)|(I1&I2)]])];
      auto using 𝕲_good,graph_map_injective_good.
    - reflexivity.
    - exfalso;eapply injective_graph_map_path in P;auto;simpl in P.
      rewrite graph_vertices_map,in_map_iff in I1.
      destruct I1 as (?&<-&I1).
      rewrite graph_vertices_map,in_map_iff in I2.
      destruct I2 as (?&<-&I2).
      simpl_nat;autounfold in *;integers.
    - rewrite graph_vertices_map,in_map_iff in I2.
      destruct I2 as (k'&<-&I2).
      autounfold in *;replace k' with 0 in * by integers.
      simpl_nat;rewrite <- graph_output;apply 𝕲_connected,I1.
  Qed.

  Lemma path_seq_right u1 u2 i k :
    ln u1 <= i -> i -[𝕲 (u1 ⨾ u2)]→ k -> i - ln u1 -[𝕲 u2]→ k - ln u1.
  Proof.
    intros L G;pose proof (path_𝕲 G) as L';simpl in G;
      apply path_seq in G as [->|(N&[(I1&I2&P)|[(I1&I2&P)|(I1&I2)]])];
      auto using 𝕲_good,graph_map_injective_good.
    - reflexivity.
    - replace i with 0 in * by integers.
      replace k with 0 in * by integers.
      reflexivity.
    - eapply injective_graph_map_path in P;auto;simpl in P;auto.
    - replace (i-ln u1) with 0 in * by integers.
      rewrite graph_vertices_map,in_map_iff in I2.
      destruct I2 as (k'&<-&I2);simpl_nat.
      rewrite <- (graph_input u2);apply 𝕲_connected,I2.
  Qed.
  
  Lemma path_par u1 u2 i k :
    i -[𝕲 (u1 ∥ u2)]→ k ->
    {|ln u1 + ln u2\ln u1|} i -[ 𝕲 u1]→ {|ln u1 + ln u2\ln u1|} k
    \/ (⨪ (ln u1)∘{|0\ln u1|}) i -[𝕲 u2 ]→ (⨪ (ln u1)∘{|0\ln u1|}) k.
  Proof.
    intros G; simpl in G.
    apply path_par_union in G;auto;
      try (apply graph_map_injective_good;auto).
    destruct G as [G|G].
    -- eapply injective_graph_map_path in G;eauto;auto.
    -- eapply injective_graph_map_path in G;eauto;auto.
  Qed.

  Lemma path_par_cross u1 u2 i k :
    i < ln u1 < k -> i -[𝕲 (u1∥u2)]→ k -> i = 0 \/ k = ln (u1∥u2).
  Proof.
    intros r I.
    assert (i∈⌊𝕲 (u1∥u2)⌋ /\ k ∈ ⌊𝕲 (u1∥u2)⌋) as (Vi&Vk)
        by (destruct (path_non_trivial_only_for_vertices I) as [E | V];
            [lia|split;apply V;simpl;tauto]).
    apply vertices_graph_vertices in Vi;simpl in Vi.
    rewrite in_rm,in_app_iff,in_map_iff in Vi.
    destruct Vi as (Ni&[Ii|(x&<-&Ii)]);autounfold in *;[|integers].
    apply vertices_graph_vertices in Vk;simpl in Vk.
    rewrite in_rm,in_app_iff,in_map_iff in Vk.
    destruct Vk as (Nj&[Ik|(j&<-&Ij)]);[integers|].
    rewrite vertices_graph_vertices in Ii,Ij.
    simpl in I; apply path_par_union_cross in I;
      auto using 𝕲_good,graph_map_injective_good.
    - rewrite input_graph_union,input_graph_map,graph_input in I.
      rewrite output_graph_union,output_graph_map,graph_output in I.
      unfold replace,Basics.compose in I.
      destruct_eqX 0 (ln u1);[integers|].
      autounfold in *;destruct_eqX (ln u1+ln u2) (ln u1);[integers|].
      simpl in *;tauto.
    - rewrite graph_vertices_map,in_map_iff.
      exists i;split;auto.
      unfold replace; destruct_eqX i (ln u1);lia.
    - rewrite graph_vertices_map,in_map_iff.
      exists j;split;auto.
      autounfold in *;unfold replace,Basics.compose;
        destruct_eqX (ln u1+j) (ln u1);lia.
  Qed.

  Lemma path_par_left u1 u2 i k :
    k < ln u1 -> i -[𝕲 (u1∥u2)]→ k -> i -[𝕲 u1]→ k.
  Proof.
    intros r I;destruct_eqb i k;[reflexivity|].
    assert (i∈⌊𝕲 (u1∥u2)⌋ /\ k ∈ ⌊𝕲 (u1∥u2)⌋) as (Vi&Vk)
        by (destruct (path_non_trivial_only_for_vertices I) as [E | V];
            [lia|split;apply V;simpl;tauto]).
    apply vertices_graph_vertices in Vi;simpl in Vi.
    rewrite in_rm,in_app_iff,in_map_iff in Vi.
    autounfold in *;destruct Vi as (Ni&[Ii|(x&<-&Ii)]);[|integers].
    apply vertices_graph_vertices in Vk;simpl in Vk.
    rewrite in_rm,in_app_iff,in_map_iff in Vk.
    autounfold in *;destruct Vk as (Nj&[Ik|(j&<-&Ij)]);[|integers].
    rewrite vertices_graph_vertices in Ii,Ik.
    simpl in I; apply path_par_union_left in I;
      auto using 𝕲_good,graph_map_injective_good.
    - eapply injective_graph_map_path in I;auto.
      unfold replace in I; destruct_eqX i (ln u1+ln u2);[integers|].
      destruct_eqX k (ln u1+ln u2);[lia|].
      apply I.
    - rewrite graph_vertices_map,in_map_iff.
      exists i;split;auto.
      unfold replace; destruct_eqX i (ln u1);lia.
    - rewrite graph_vertices_map,in_map_iff.
      exists k;split;auto.
      unfold replace; destruct_eqX k (ln u1);lia.
  Qed.

  Lemma path_par_right u1 u2 i k :
    ln u1 < i -> i -[𝕲 (u1∥u2)]→ k -> i - ln u1 -[𝕲 u2]→ k - ln u1.
  Proof.
    intros r I;destruct_eqb i k;[reflexivity|].
    pose proof (path_𝕲 I) as L.
    assert (i∈⌊𝕲 (u1∥u2)⌋ /\ k ∈ ⌊𝕲 (u1∥u2)⌋) as (Vi&Vk)
        by (destruct (path_non_trivial_only_for_vertices I) as [E | V];
            [lia|split;apply V;simpl;tauto]).
    apply vertices_graph_vertices in Vi;simpl in Vi.
    rewrite in_rm,in_app_iff,in_map_iff in Vi.
    destruct Vi as (Ni&[Ii|(i'&<-&Ii)]);[integers|].
    apply vertices_graph_vertices in Vk;simpl in Vk.
    rewrite in_rm,in_app_iff,in_map_iff in Vk.
    destruct Vk as (Nj&[Ik|(j&<-&Ij)]);[integers|simpl_nat].
    rewrite vertices_graph_vertices in Ii,Ij.
    simpl in I; apply path_par_union_right in I;
      auto using 𝕲_good,graph_map_injective_good.
    - eapply injective_graph_map_path in I;auto.
      unfold replace,Basics.compose in I.
      autounfold in *;destruct_eqX (ln u1 + i') 0; [integers|].
      destruct_eqX (ln u1+j) 0;[integers|].
      simpl_nat;auto.
    - rewrite graph_vertices_map,in_map_iff.
      exists i';split;auto.
      unfold replace,Basics.compose.
      autounfold in *;destruct_eqX (ln u1+i') (ln u1);lia.
    - rewrite graph_vertices_map,in_map_iff.
      exists j;split;auto.
      unfold replace,Basics.compose.
      autounfold in *;destruct_eqX (ln u1+j) (ln u1);lia.
  Qed.  

  (** * From axiomatic containment to graph ordering *)
  
  (** If [u] and [v] are related by the equality axioms of
  series-parallel terms, then their graphs are equivalent. *)
  Lemma is_morph_𝐒𝐏_ax u v : 𝐒𝐏_ax u v -> 𝕲 u ⋈ 𝕲 v.
  Proof.
    intro w;destruct w.
    - simpl;rewrite graph_map_union,graph_union_assoc,graph_map_map.
      erewrite (@graph_map_ext_in _ _ _ (_∘_));[reflexivity|].
      intros;auto;autounfold in *;unfold Basics.compose;lia.
    - pose proof (ln_non_zero e) as L1;pose proof (ln_non_zero f) as L2.
      split;simpl ;apply par_smaller_hom_order;auto.
      + etransitivity;
          [now eexists;apply par_graph_union_decomp_right;auto
          |etransitivity;apply injective_map_hom_eq;auto].
      + etransitivity.
        * eexists;apply par_graph_union_decomp_left;auto.
        * etransitivity;apply injective_map_hom_eq;auto.
      + etransitivity;
          [now eexists;apply par_graph_union_decomp_right;auto
          |etransitivity;apply injective_map_hom_eq;auto].
      + etransitivity.
        * eexists;apply par_graph_union_decomp_left;auto.
        * etransitivity;apply injective_map_hom_eq;auto.
    - simpl;symmetry.
      transitivity (graph_map {|ln e\ln e+ln e|} (𝕲 e)).
      -- apply injective_map_hom_eq;auto.
      -- split.
         --- apply par_smaller_hom_order;auto.
             ---- reflexivity.
             ---- pose proof (ln_non_zero e);
                    etransitivity;apply injective_map_hom_eq;auto.
         --- apply par_hom_order_weak_left;auto.
  Qed.

  (** The graph of a term [e] is greater than the point graph
  [(0,[],0)] if and only if all of its variables are tests. *)
  Lemma supid_hom_order A e : 𝒱 e ⊆ A <-> A ⊨ (0, [], 0) ⊲ 𝕲 e.
  Proof. rewrite graph_variables,contractible_graph;tauto. Qed.

  (** The parametric ordering of series-parallel terms is contained
  in the graph ordering with the same parameter. *)
  Lemma is_morph_𝐒𝐏_inf A u v : A ⊨ u << v -> A ⊨ 𝕲 u ⊲ 𝕲 v.
  Proof.
    intro E;induction E.
    - reflexivity.
    - etransitivity;eauto.
    - destruct H as [H|H];apply is_morph_𝐒𝐏_ax in H as (L1&L2);
        eapply hom_order_incl;eauto;apply incl_nil.
    - destruct H.
      + eapply hom_order_incl;[apply incl_nil|].
        simpl;etransitivity;[eexists;apply par_graph_union_decomp_left;
                              auto|].
        apply injective_map_hom_eq;auto.
      + transitivity ((0,[],0)⋄𝕲 f).
        -- replace ((0,[],0)⋄(𝕲 f)) with (𝕲 f);
             [reflexivity|].
           pose proof (graph_input f) as h;
             destruct (𝕲 f) as ((?&?)&?);autounfold in *;
               simpl in *;rewrite h;reflexivity.
        -- simpl;apply seq_hom_order;auto.
           --- intros x [<-|[<-|F]];unfold input,output;simpl in *;
                 tauto||(split;reflexivity).
           --- split.
               ---- simpl;rewrite graph_input;auto.
               ---- intros x [<-|[<-|F]] _;
                      [| |simpl in F;tauto];simpl;rewrite graph_input;
                        reflexivity.
           --- apply supid_hom_order;auto.
           --- eapply hom_order_incl;[apply incl_nil|];
                 apply injective_map_hom_eq,injective_add_left.
      + transitivity (𝕲 f⋄(ln f,[],ln f)).
        -- replace ((𝕲 f)⋄(ln f,[],ln f)) with (𝕲 f);
             [reflexivity|].
           pose proof (graph_output f) as h;
             destruct (𝕲 f) as ((?&?)&?);unfold graph_union;
               autounfold in *;simpl in *;rewrite h,app_nil_r;
                 reflexivity.
        -- simpl;apply seq_hom_order;auto.
           --- split.
               ---- simpl;rewrite graph_output;auto.
               ---- intros x _ [<-|[<-|F]];
                      [| |simpl in F;tauto];simpl;reflexivity.
           --- reflexivity.
           --- etransitivity;[|etransitivity;
                               [apply supid_hom_order,H|]].
               ---- replace (ln f,[],ln f)
                    with (graph_map (fun _ => ln f) ((0,[],0) : grph))
                   by reflexivity.
                    eapply hom_order_incl;[apply incl_nil|];
                      eexists;apply graph_map_morphism.
               ---- eapply hom_order_incl;[apply incl_nil|];
                      apply injective_map_hom_eq,injective_add_left.
    - simpl;apply seq_hom_order;auto.
      etransitivity;[|etransitivity;eauto];
        (eapply hom_order_incl;[apply incl_nil|]);eauto;
          apply injective_map_hom_eq;auto.
    - pose proof (ln_non_zero g) as L1;pose proof (ln_non_zero h) as L2;
        pose proof (ln_non_zero e) as L3;
        pose proof (ln_non_zero f) as L4.
      simpl;apply par_hom_order;auto;
        (etransitivity;[|etransitivity]);
        [|apply IHE1| | |apply IHE2| ];
        (eapply hom_order_incl;[apply incl_nil|]);eauto;
          apply injective_map_hom_eq;auto.
  Qed.

  (** * From graph ordering to axiomatic containment *)
  (** ** Variables *)
  (** If the graph of [u] is smaller than the graph of a variable,
      then [u] is provably smaller than this variable. *)
  Lemma morph_var A u a φ :
    A ⊨ (𝕲 (𝐒𝐏_var a))-{φ}→ (𝕲 u) -> [] ⊨ u << 𝐒𝐏_var a.
  Proof.
    revert φ;induction u;simpl.
    - intros φ (hi&ho&he&Int);autounfold in *;simpl in *.
      destruct (he 0 a 1) as [(I&N)|[I|F]].
      -- now left.
      -- rewrite hi,ho in N;discriminate.
      -- inversion I;auto.
      -- simpl in F;tauto.
    - intros φ hφ;exfalso;clear IHu1 IHu2.
      destruct hφ as (h1&h2&h3&h4);
        rewrite input_graph_union,graph_input in h1;
        rewrite output_graph_union,output_graph_map,graph_output in h2;
        autounfold in *;simpl in *.
      destruct (h3 0 a 1) as [(I&N)|I].
      -- now left.
      -- rewrite h1,h2 in N;pose proof (ln_non_zero u1);lia.
      -- rewrite edges_graph_union in I;apply in_app_iff in I.
         rewrite h1,h2 in I;pose proof (ln_non_zero u1) as L1;
           pose proof (ln_non_zero u2) as L2.
         destruct I as [I|I].
         --- apply 𝕲_bounded in I;lia.
         --- rewrite edges_graph_map in I.
             apply graph.in_map in I as (?&?&I&E1&E2).
             lia.
    - intros φ hφ.
      destruct hφ as (h1&h2&h3&h4).
      rewrite input_graph_union,input_graph_map,graph_input in h1;
        rewrite output_graph_union,output_graph_map,graph_output in h2;
        autounfold in *;simpl in *.
      pose proof (ln_non_zero u1) as L1;
        pose proof (ln_non_zero u2) as L2.
      unfold replace in h1,h2.
      destruct_eqX 0 (ln u1);[lia|].
      destruct_eqX (ln u1 + ln u2) (ln u1);[lia|].
      destruct (h3 0 a 1) as [(I&F)|I].
      -- now left.
      -- integers.
      -- rewrite edges_graph_union,edges_graph_map,edges_graph_map in I.
         rewrite h1,h2 in I;apply in_app_iff in I as [I|I];
           apply graph.in_map in I as (?&?&I&E1&E2).
         --- unfold replace in E1, E2.
             destruct_eqX x (ln u1);[lia|].
             destruct_eqX x0 (ln u1);[|apply 𝕲_bounded in I;lia].
             rewrite E1 in *;clear x E1 x0 E.
             etransitivity;[|apply (IHu1 (replace 1 (ln u1)))];auto.
             split;[|split;[|split]];
               [rewrite graph_input|rewrite graph_output| |];
               autounfold;simpl.
             ---- unfold replace;destruct_eqX 0 1;lia.
             ---- unfold replace;rewrite eqX_refl;auto.
             ---- intros ? ? ? [h|F];[|simpl in F;tauto];right.
                  inversion h;unfold replace;destruct_eqX 0 1;[lia|];
                    rewrite <- H1;auto.
             ---- intros x [<-|[<-|[<-|[<-|F]]]];
                    (simpl in F;tauto)||unfold input,output,replace;
                    simpl.
                  ++++ left;apply graph_input.
                  ++++ right;left;apply graph_output.
                  ++++ left;apply graph_input.
                  ++++ right;left;apply graph_output.
         --- unfold replace in E1, E2.
             destruct_eqX (ln u1 + x) (ln u1);[|lia].
             destruct_eqX (ln u1 + x0) (ln u1);[lia|].
             replace x with 0 in * by lia;
               replace x0 with (ln u2) in * by lia;
               clear x E1 x0 E N0 E2 N1.
             etransitivity;[|apply (IHu2 (replace 1 (ln u2)))];eauto.
             split;[|split;[|split]];
               [rewrite graph_input|rewrite graph_output| |];
               autounfold;simpl.
             ---- unfold replace;destruct_eqX 0 1;lia.
             ---- unfold replace;rewrite eqX_refl;auto.
             ---- intros ? ? ? [h|F];[|simpl in F;tauto];right.
                  inversion h;unfold replace;destruct_eqX 0 1;[lia|];
                    rewrite <- H1;auto.
             ---- intros x [<-|[<-|[<-|[<-|F]]]];
                    (simpl in F;tauto)||unfold input,output,replace;
                    simpl.
                  ++++ left;apply graph_input.
                  ++++ right;left;apply graph_output.
                  ++++ left;apply graph_input.
                  ++++ right;left;apply graph_output.
  Qed.                  

  (** ** Sequential product *)
  Section seq.
    (** We fix a term [u], and an integer [k], such that [k] is an
    interior vertex of [𝕲 u] (that is a vertex different from both [0]
    and [ln u]. *)
    Context { u : 𝐒𝐏 X } { k : nat }.
    Context {Rk: 0 < k < ln u } {Vk: k ∈ (vertices u)}.

    (** We'll split [u] according to [k]:
        - [pref u k] is a term whose graph is the part of the [𝕲 u]
          that can reach [k];
        - [suf u k] is a term whose graph is the part of the [𝕲 u]
          that is reachable from [k];
        - [moustache u k] is a term whose graph is the part of [𝕲 u]
          that is visible from [k].  *)
    Fixpoint pref u k :=
      match u with
      | u ⨾ v =>
        if eqX k (ln u)
        then u
        else
          if Nat.ltb k (ln u)
          then pref u k
          else u ⨾ (pref v (k - ln u))
      | u ∥ v =>
        if Nat.ltb k (ln u)
        then pref u k
        else (pref v (k - ln u))
      | _ => u
      end.

    Fixpoint suf u k :=
      match u with
      | u ⨾ v =>
        if eqX k (ln u)
        then v
        else
          if Nat.ltb k (ln u)
          then (suf u k) ⨾ v
          else suf v (k - ln u)
      | u ∥ v =>
        if Nat.ltb k (ln u)
        then suf u k
        else (suf v (k - ln u))
      | _ => u
      end.
    Fixpoint moustache u k :=
      match u with
      | u ⨾ v =>
        if eqX k (ln u)
        then u ⨾ v
        else
          if Nat.ltb k (ln u)
          then moustache u k ⨾ v
          else u ⨾ (moustache v (k - ln u))
      | u ∥ v =>
        if Nat.ltb k (ln u)
        then moustache u k
        else moustache v (k - ln u)
      | _ => u
      end.

    (** [split_point u k] is the vertex in the graph of [moustache u
    k] that corresponds to [k]. *)
    Fixpoint split_point u k :=
      match u with
      | u ⨾ v => 
        if eqX k (ln u)
        then k
        else
          if Nat.ltb k (ln u)
          then split_point u k
          else ln u + split_point v (k - ln u)
      | u ∥ v =>
        if Nat.ltb k (ln u)
        then split_point u k
        else split_point v (k - ln u)
      | _ => k
      end.

    (** We can relate [u] with [pref u k ⨾ suf u k] using axiomatic
    containment. *)
    Lemma 𝐒𝐏_inf_pref_suf : [] ⊨ u << pref u k ⨾ suf u k.
    Proof.
      revert k Rk Vk;induction u as [ |u1 IHu1 u2 IHu2
                                      |u1 IHu1 u2 IHu2 ];intro k;simpl.
      - lia.
      - list_edges_simpl;intros r [I|(n&<-&I)].
        -- destruct_eqb k (ln u1);auto;destruct_ltb k (ln u1);auto.
           --- apply vertices_graph_vertices,vertices_bounded in I;lia.
           --- apply IHu1 in I;[|lia].
               transitivity (pref u1 k ⨾suf u1 k ⨾ u2);auto.
        -- autounfold in *;destruct_eqb (ln u1+n) (ln u1);auto.
           destruct_ltb (ln u1+n) (ln u1);[|lia].
           apply IHu2 in I;[|lia];simpl_nat.
           transitivity (u1 ⨾ (pref u2 n ⨾suf u2 n));auto.
      - rewrite in_rm,in_app_iff,in_map_iff;intros r (N&[I|(n&<-&I)]).
        -- destruct_ltb k (ln u1);auto.
           --- apply vertices_graph_vertices,vertices_bounded in I;lia.
           --- apply IHu1 in I;[|lia];eauto.
        -- autounfold in *;destruct_ltb (ln u1+n) (ln u1);[|lia];
             simpl_nat.
           apply IHu2 in I;[|lia];eauto.
           rewrite <- I;eauto.
    Qed.

    (** [split_point u k] is the length of [pref u k]. *)
    Lemma ln_pref_split_point : ln (pref u k) = split_point u k.
    Proof.
      revert k Rk Vk;induction u as [|u1 IHu1 u2 IHu2|u1 IHu1 u2 IHu2];
      clear u;intros k r V.
      - integers.
      - simpl;destruct_eqb k (ln u1);auto.
        simpl in V;rewrite in_app_iff,in_map_iff in V.
        autounfold in *;destruct V as [V|(k'&<-&V)];
          [destruct_ltb k (ln u1);[integers|]
          |destruct_ltb (ln u1+k') (ln u1);[|integers]].
        -- simpl;rewrite IHu1;[|integers|auto];simpl;auto.
        -- simpl;rewrite IHu2;[|integers|auto];simpl_nat;auto.
      - simpl in *;rewrite in_rm,in_app_iff,in_map_iff in V.
        autounfold in *;destruct V as (N&[V|(k'&<-&V)]);
          [destruct_ltb k (ln u1);[integers|]
          |destruct_ltb (ln u1+k') (ln u1);[|integers]].
        -- simpl;rewrite IHu1;[|integers|auto];simpl;auto.
        -- simpl;rewrite IHu2;[|integers|auto];simpl_nat;auto.
    Qed.

    (** The graphs of [moustache u k] and of [pref u k ⨾ suf u k]
        are equal. *)
    Lemma split_moustache : 𝕲 (moustache u k) = 𝕲 (pref u k ⨾ suf u k).
    Proof.
      revert k Rk Vk;induction u as [|u1 IHu1 u2 IHu2|u1 IHu1 u2 IHu2];
      clear u;intros k r V.
      - exfalso;integers.
      - simpl;destruct_eqb k (ln u1);auto.
        simpl in V;rewrite in_app_iff,in_map_iff in V.
        autounfold in *;destruct V as [V|(k'&<-&V)];
          [destruct_ltb k (ln u1);[integers|]
          |destruct_ltb (ln u1+k') (ln u1);[|integers]].
        -- pose proof V as E;apply IHu1 in E;[|integers];simpl.
           rewrite E;simpl;rewrite <- graph_union_assoc;f_equal;auto.
           rewrite graph_map_union;f_equal;auto.
           rewrite graph_map_map;apply graph_map_ext_in.
           intros x I;unfold Basics.compose;autounfold.
           rewrite <- Plus.plus_assoc_reverse;f_equal.
           rewrite<- graph_output,E,graph_output;simpl;lia.
        -- simpl_nat;pose proof V as E;apply IHu2 in E;
             [|integers];simpl.
           autounfold in *;rewrite E;simpl;
             rewrite <- graph_union_assoc;f_equal;auto.
           rewrite graph_map_union;f_equal;auto.
           rewrite graph_map_map;apply graph_map_ext_in.
           intros x I;autounfold in *;unfold Basics.compose;lia.
      - simpl in *;rewrite in_rm,in_app_iff,in_map_iff in V.
        autounfold in *;destruct V as (N&[V|(k'&<-&V)]);
          [destruct_ltb k (ln u1);[integers|]
          |destruct_ltb (ln u1+k') (ln u1);[|integers]].
        -- simpl;rewrite IHu1;[|integers|auto];simpl;auto.
        -- simpl;rewrite IHu2;[|integers|auto];simpl_nat;auto.
    Qed.

    (** [moustache u k] is provably smaller than [pref u k ⨾ suf u k]. *)
    Lemma split_moustache_inf: [] ⊨ moustache u k << pref u k ⨾ suf u k.
    Proof.
      revert k Rk Vk;induction u as [|u1 IHu1 u2 IHu2|u1 IHu1 u2 IHu2];
      clear u;intros k r V.
      - exfalso;integers.
      - simpl;destruct_eqb k (ln u1);auto.
        simpl in V;rewrite in_app_iff,in_map_iff in V.
        autounfold in *;destruct V as [V|(k'&<-&V)];
          [destruct_ltb k (ln u1);[integers|]
          |destruct_ltb (ln u1+k') (ln u1);[|integers]].
        -- pose proof V as E;rewrite IHu1;auto;integers.
        -- pose proof V as E;rewrite IHu2;auto;simpl_nat;auto;integers.
      - simpl in *;rewrite in_rm,in_app_iff,in_map_iff in V.
        autounfold in *;destruct V as (N&[V|(k'&<-&V)]);
          [destruct_ltb k (ln u1);[integers|]
          |destruct_ltb (ln u1+k') (ln u1);[|integers]].
        -- simpl;rewrite IHu1;[|integers|auto];simpl;auto.
        -- simpl;rewrite IHu2;[|integers|auto];simpl_nat;auto.
    Qed.

    (** [u] is provably smaller than its moustache. *)
    Lemma 𝐒𝐏_inf_moustache : [] ⊨ u << moustache u k.
    Proof.
      clear Rk;revert k Vk;
        induction u as [|u1 IHu1 u2 IHu2|u1 IHu1 u2 IHu2];
        clear u;intros k;simpl.
      - intro;auto.
      - list_edges_simpl;intros [I|(n&<-&I)].
        -- destruct_eqb k (ln u1);auto;destruct_ltb k (ln u1);auto.
           apply vertices_graph_vertices,vertices_bounded in I;lia.
        -- autounfold in *;destruct_eqb (ln u1+n) (ln u1);auto.
           destruct_ltb (ln u1+n) (ln u1);[|lia].
           apply IHu2 in I;simpl_nat;auto.
      - rewrite in_rm,in_app_iff,in_map_iff;intros (N&[I|(n&<-&I)]).
        -- destruct_ltb k (ln u1);auto.
           --- apply vertices_graph_vertices,vertices_bounded in I;lia.
           --- apply IHu1 in I;eauto.
        -- autounfold in *;destruct_ltb (ln u1+n) (ln u1);
             [|lia];simpl_nat.
           apply IHu2 in I as <- ;eauto.
    Qed.

    (** [split_point u k] is a vertex of [moustache u k]. *)
    Lemma split_point_in_moustache :
      split_point u k ∈ vertices (moustache u k).
    Proof.
      clear Rk;revert k Vk;
        induction u as [|u1 IHu1 u2 IHu2|u1 IHu1 u2 IHu2];
        clear u;intros k;simpl;auto.
      - destruct_eqb k (ln u1);simpl;auto.
        destruct_ltb k (ln u1);simpl;auto.
        -- intros I;apply in_app_iff;right;apply in_map_iff;
             exists (split_point u2 (k - ln u1));autounfold;split;[lia|].
           apply IHu2;apply in_app_iff in I as [I|I];[integers|].
           apply in_map_iff in I as (?&<-&?);simpl_nat;auto.
        -- intros I;apply in_app_iff;left.
           apply IHu1;apply in_app_iff in I as [I|I];auto.
           autounfold in *;apply in_map_iff in I as (?&<-&?);integers.
      -intros I;rewrite in_rm,in_app_iff,in_map_iff in I;
         destruct I as (N&[I|(j&<-&I)]);autounfold in *;
         [destruct_ltb k (ln u1);[integers|]
         |destruct_ltb (ln u1+j) (ln u1);[|integers]].
       -- apply IHu1;auto;integers.
       -- apply IHu2; simpl_nat;auto.
    Qed.

    (** If [φ] is a morphism from [u1⨾u2] to [v1⨾v2], and if the image
    of the output of [u1] is the output of [v1], then the graph of [u1]
    is greater than that of [v1] and the graph of [u2] is greater than
    the graph of [v2]. *)
    Lemma morphism_seq_split u1 u2 v1 v2 φ A :
      A ⊨ 𝕲 (u1⨾u2) -{φ}→ 𝕲 (v1⨾v2) -> φ (ln u1) = ln v1 ->
      A ⊨ 𝕲 v1 ⊲ 𝕲 u1 /\ A ⊨ 𝕲 v2 ⊲ 𝕲 u2.
    Proof.
      clear u k Rk Vk;intros (M1&M2&M3&M4) h;split.
      - exists φ;split;[|split;[|split]];repeat rewrite graph_input in *;
          repeat rewrite graph_output in *;auto.
        + intros i α j I;destruct (M3 i α j) as [(?&->)|Ie];auto.
          * simpl;rewrite edges_graph_union;apply in_app_iff;left;auto.
          * assert (Lj : φ j <= ln v1).
            -- rewrite <- h;eapply path_𝕲.
               eapply morphism_is_order_preserving;eauto.
               replace j with (id j) by reflexivity.
               replace (ln u1) with (id (ln u1)) by reflexivity.
               destruct (seq_graph_union_decomp
                           (good_for_seq_𝕲 u1 u2)) as (_&_&M&_).
               eapply morphism_is_order_preserving;
                 [simpl;apply M|clear M].
               rewrite <- graph_output;
                 apply (proj1 (𝕲_good u1));auto.
               is_vertex.
            -- simpl in Ie;rewrite edges_graph_union ,in_app_iff in Ie;
                 destruct Ie as [Ie|Ie];auto.
               exfalso;rewrite edges_graph_map,graph.in_map in Ie.
               unfold incr in *;destruct Ie as (x&y&Ie&Ex&Ey);
               integers.
        + intros j I.
          assert (Lj : φ j <= ln v1).
          * rewrite <- h;eapply path_𝕲.
            eapply morphism_is_order_preserving;eauto.
            replace j with (id j) by reflexivity.
            replace (ln u1) with (id (ln u1)) by reflexivity.
            destruct (seq_graph_union_decomp
                        (good_for_seq_𝕲 u1 u2)) as (_&_&M&_).
            eapply morphism_is_order_preserving;
              [simpl;apply M|clear M].
            rewrite <- graph_output;
              apply (proj1 (𝕲_good u1));auto.
          * cut (φ j ∈ ⌊𝕲 (v1⨾v2)⌋);unfold incr in *.
            -- repeat rewrite <- vertices_graph_vertices;simpl.
               rewrite in_app_iff;intros [?|F];[tauto|].
               revert Lj;apply in_map_iff in F as (i&<-&F).
               unfold incr in *;simpl_nat.
               intro Lj;replace i with 0 in * by lia;simpl_nat.
               apply vertex_ln.
            -- apply M4.
               repeat rewrite <- vertices_graph_vertices in *;simpl.
               apply in_app_iff;tauto.
      - exists (fun n => φ (ln u1 + n) - ln v1);split;[|split;[|split]];
          repeat rewrite graph_input in *;repeat rewrite graph_output in *.
        -- simpl_nat;lia.
        -- simpl in *;rewrite M2;lia.
        -- intros i α j I;destruct (M3 (ln u1+i) α (ln u1+ j))
             as [(?&->)|Ie];auto.
           --- simpl;rewrite edges_graph_union,edges_graph_map,in_app_iff.
               right;apply graph.in_map;exists i;exists j;split;auto;lia.
           --- right;simpl in Ie;rewrite edges_graph_union in Ie;
                 rewrite edges_graph_map,in_app_iff,graph.in_map in Ie.
               destruct Ie as [Ie|(?&?&Ie&<-&<-)];simpl_nat;auto.
               exfalso.
               pose proof (𝕲_bounded Ie) as (P&N).
               apply PeanoNat.Nat.le_ngt in N;apply N.
               eapply PeanoNat.Nat.le_lt_trans;[|eauto].
               rewrite<- h;apply (@path_𝕲 (v1⨾ v2)).
               apply (morphism_is_order_preserving M3).
               replace (ln u1) with (id (⨥ (ln u1) 0)) at 1
                 by (autounfold;unfold id;lia).
               replace (ln u1+i) with  (id (⨥ (ln u1) i))
                 by (autounfold;unfold id;lia). 
               destruct (seq_graph_union_decomp
                           (good_for_seq_𝕲 u1 u2)) as (_&_&_&M).
               eapply morphism_is_order_preserving;
                 [simpl;apply M|clear M].
               apply graph_map_path.
               rewrite <- (graph_input u2).
               apply 𝕲_connected;is_vertex.
        -- intros i I.
           cut ( (ln u1+i) ∈ ⌊𝕲 (u1⨾u2)⌋
                 /\ ln v1 <= φ (ln u1 + i)).
           ++ intros (V&R).
              apply M4 in V.
              rewrite <- vertices_graph_vertices in *.
              simpl in V;apply in_app_iff in V as [V|V].
              ** apply vertices_graph_vertices,vertices_bounded in V.
                 replace (φ (ln u1 + i)) with (ln v1) in * by lia.
                 rewrite PeanoNat.Nat.sub_diag;apply vertex_0.
              ** apply in_map_iff in V as (?&<-&V);simpl_nat;auto.
           ++ split.
              ** rewrite <- vertices_graph_vertices in *.
                 simpl;apply in_app_iff;right.
                 apply in_map_iff;exists i;split;auto;lia.
              ** rewrite <- h at 1.
                 eapply path_𝕲.
                 replace (ln u1 + i) with ((id ∘ ⨥ (ln u1)) i)
                   by (unfold id,Basics.compose,incr;lia).
                 replace (ln u1) with ((id ∘ ⨥(ln u1)) 0) at 1
                   by (unfold id,Basics.compose,incr;lia).
                 eapply morphism_is_order_preserving;[apply M3|].
                 eapply morphism_is_order_preserving;
                   [|rewrite <- (graph_input u2);
                     eapply 𝕲_connected,I].
                 eapply is_premorphism_compose_weak.
                 --- apply graph_map_morphism.
                 --- apply seq_graph_union_decomp;auto.
    Qed.

    (** [μ u k] is a function mapping the vertices of [u] that are
    visible from [k] to vertices in [moustache u k].  *)
    Fixpoint μ u k i :=
      match u with
      | 𝐒𝐏_var _ => 0
      | u ⨾ v =>
        if eqX k (ln u)
        then i
        else
          if Nat.ltb k (ln u)
          then
            if Nat.leb i (ln u)
            then μ u k i
            else ln (moustache u k) + i - ln u
          else 
            if Nat.leb i (ln u)
            then i
            else ln u + μ v (k - ln u)
                          (i - ln u)
      | u ∥ v => 
        if Nat.ltb k (ln u)
        then (μ u k ({|ln (u∥v)\ln u|} i))
        else (μ v (k - ln u) (i - ln u))
      end.

    (** The image of [0] by [µ] is [0].*)
    Lemma μ_0 : μ u k 0 = 0.
    Proof.
      revert k Rk Vk;induction u as [|u1 IHu1 u2 IHu2|u1 IHu1 u2 IHu2];
      clear u.
      - intros k;simpl;lia.
      - intros k Rk Vk;simpl.
        destruct_eqb k (ln u1);[auto|destruct_ltb k (ln u1)];auto.
        apply IHu1;[lia|].
        simpl in Vk;apply in_app_iff in Vk as [Vk|Vk];auto;
          apply in_map_iff in Vk as (?&<-&Vk);
          autounfold in *;integers.
      - intros k Rk;simpl;rewrite in_rm,in_app_iff,in_map_iff.
        destruct_ltb k (ln u1);intros (N&[Vk|(k'&<-&Vk)]).
        + integers.
        + unfold id,replace,Basics.compose;simpl_nat;simpl.
          rewrite IHu2;simpl;auto.
          autounfold in*;simpl in *;lia.
        + unfold id,replace,Basics.compose;simpl_nat;simpl.
          case_eq (ln u1 + ln u2);[integers|].
          rewrite IHu1;simpl;auto;lia.
        + autounfold in *;integers.
    Qed.

    (** The image of the length of [u] by [µ] is the length of
     its moustache.*)
    Lemma μ_ln : μ u k (ln u) = ln (moustache u k).
    Proof.
      revert k Rk Vk;induction u as [|u1 IHu1 u2 IHu2|u1 IHu1 u2 IHu2];
      clear u;simpl.
      - intros k;simpl;lia.
      - intros k Rk Vk;simpl.
        destruct_eqb k (ln u1);[auto|destruct_ltb k (ln u1)];auto.
        * destruct_leb (ln u1 + ln u2) (ln u1);[integers|].
          simpl_nat;simpl;rewrite IHu2;auto.
          -- simpl in *;lia.
          -- simpl in Vk;apply in_app_iff in Vk as [Vk|Vk];
               [apply vertices_graph_vertices,vertices_bounded in Vk;lia
               |apply in_map_iff in Vk as (?&<-&Vk);simpl_nat;auto].
        * simpl;unfold incr in *.
          destruct_leb (ln u1 + ln u2) (ln u1);[|lia].
          integers.
      - intros k Rk;simpl;rewrite in_rm,in_app_iff,in_map_iff.
        destruct_ltb k (ln u1);intros (N&[Vk|(k'&<-&Vk)]).
        + integers.
        + simpl_nat.
          unfold id,replace,Basics.compose;simpl_nat;simpl.
          rewrite IHu2;simpl;auto.
          unfold incr in *;simpl in *;lia.
        + unfold replace;rewrite eqX_refl;apply IHu1;auto;lia.
        + unfold incr in *;integers.
    Qed.

    (** This lemma could be restated as the statement that in the
    graph of [moustache u k], every vertex is either reachable or
    co-reachable from [split_point u k]. *)
    Lemma visible_moustache :
      vertices (moustache u k) ≈ visible (𝕲 (moustache u k))
               (split_point u k).
    Proof.
      apply incl_PartialOrder;split;unfold Basics.flip,visible;
        [|rewrite <- vertices_graph_vertices;unfold incl; 
          setoid_rewrite filter_In;intros i;tauto].
      rewrite vertices_graph_vertices;intro i.
      rewrite filter_In,orb_true_iff;repeat rewrite exists_path_spec;
        repeat rewrite split_moustache.
      intros I;split;auto;revert I;rewrite <- ln_pref_split_point;auto.
      rewrite <- vertices_graph_vertices;intro I;simpl in I.
      apply in_app_iff in I as [I|I].
      - left.
        apply vertices_graph_vertices,𝕲_connected in I as (_&I).
        rewrite graph_output in I.
        eapply (morphism_is_order_preserving) in I;
          [|apply (seq_graph_union_decomp
                     (good_for_seq_𝕲 (pref u k) (suf u k)))].
        unfold id in I;auto.
      - right.
        apply in_map_iff in I as  (j&<-&I).
        apply vertices_graph_vertices,𝕲_connected in I as (I&_).
        rewrite graph_input in I.
        eapply (morphism_is_order_preserving) in I;
          [|eapply graph_map_morphism].
        eapply (morphism_is_order_preserving) in I;
          [|apply (seq_graph_union_decomp
                     (good_for_seq_𝕲 (pref u k) (suf u k)))].
        unfold id,incr in *;simpl_nat;auto.
    Qed.

    
    (** The image of [k] by [μ u k] is [split_point u k]. *)
    Lemma μ_k : μ u k k = split_point u k.
    Proof.
      revert k Rk Vk;induction u as [|u1 IHu1 u2 IHu2|u1 IHu1 u2 IHu2];
        clear u;simpl.
      - intros k;lia.
      - intros k Rk;rewrite in_app_iff,in_map_iff.
        destruct_ltb k (ln u1);
          (destruct_eqb k (ln u1);[|destruct_leb k (ln u1)]);
          try lia;intros [I|(k'&<-&I)];try integers.
        + simpl_nat;rewrite IHu2;auto;integers.
        + simpl_nat;rewrite IHu1;auto;integers.
      - intros k Rk;rewrite in_rm,in_app_iff,in_map_iff.
        unfold replace.
        destruct_eqX k (ln u1 + ln u2);[lia|].
        destruct_ltb k (ln u1);intros (Nk&[I|(k'&<-&I)]);try integers.
        + simpl_nat;apply IHu2;auto;integers.
        + simpl_nat;apply IHu1;auto;integers.
    Qed.

  End seq.

  (** If a vertex in [u] is either reachable or co-reachable from
    [k], then [μ] sends it to a vertex of the moustache. *)
  Lemma μ_internal u k :
    0 < k < ln u -> k ∈ (vertices u) ->
    internal_map (μ u k)
                 (filter_graph (fun i => k -[ 𝕲 u ]?→ i)
                               (fun j => j -[ 𝕲 u ]?→ k)
                               (𝕲 u))
                 (𝕲 (moustache u k)).
  Proof.
    unfold internal_map;intros Rk Vk.
    setoid_rewrite filter_graph_visible;
      [|apply 𝕲_connected|apply vertices_graph_vertices,Vk].
    setoid_rewrite filter_In.
    setoid_rewrite orb_true_iff.
    setoid_rewrite exists_path_spec.
    repeat setoid_rewrite <- vertices_graph_vertices.
    revert k Rk Vk;induction u as [|u1 IHu1 u2 IHu2
                                   |u1 IHu1 u2 IHu2].
    - intros k;simpl;lia.
    - intros k Rk Vk.
      destruct_eqb k (ln u1);[|destruct_ltb k (ln u1)].
      + intros i.
        simpl;rewrite PeanoNat.Nat.eqb_refl;auto.
        simpl;tauto.
      + assert (Rk' : 0 < k - ln u1 < ln u2)
          by (simpl in *;lia).
        assert (Vk' : (k-ln u1)∈ vertices u2)
          by (simpl in Vk;apply in_app_iff in Vk as [Vk|Vk];
              [apply vertices_graph_vertices,vertices_bounded in Vk;lia
              |apply in_map_iff in Vk as (?&<-&Vk);simpl_nat;auto]).
        pose proof (IHu2 _ Rk' Vk') as M.
        intros i.
        simpl;apply PeanoNat.Nat.eqb_neq in N as ->.
        apply PeanoNat.Nat.ltb_ge in L as ->;auto.
        simpl;repeat rewrite in_app_iff.
        repeat rewrite in_map_iff.
        intros ([I|(j&<-&I)]&V).
        * destruct_leb i (ln u1);[|integers];auto.
        * assert (Vj:  j ∈ vertices u2
                       /\ ((j -[ 𝕲 u2 ]→ k - ln u1)
                          \/ k - ln u1 -[ 𝕲 u2 ]→ j)).
          -- split;auto.
             destruct V as [P|P];autounfold in *;
               (apply path_seq_right in P;[|lia]);
               simpl_nat;[left|right];auto.
          -- apply M in Vj;autounfold in *.
             destruct_leb (ln u1+j) (ln u1).
             ++ left;replace j with 0 by lia;simpl_nat;
                  apply vertex_ln.
             ++ right;eexists;split;[|eauto]; simpl_nat;lia.
      + assert (Vk' : k ∈ vertices u1)
          by (simpl in Vk;apply in_app_iff in Vk as [Vk|Vk];auto;
              apply in_map_iff in Vk as (?&<-&Vk);unfold incr in *;
              integers).
        intros i.
        simpl;apply PeanoNat.Nat.eqb_neq in N as ->.
        pose proof L as Lk.
        apply PeanoNat.Nat.ltb_lt in L as ->;auto.
        simpl;repeat rewrite in_app_iff.
        repeat rewrite in_map_iff.
        intros ([I|(j&<-&I)]&V).
        * destruct_leb i (ln u1);[|integers];auto.
          left;apply IHu1;simpl in *;lia||auto.
          split;auto.
          destruct V as [P|P];
            (apply path_seq_left in P;auto).
        * unfold incr in *;destruct_leb (ln u1+j) (ln u1).
          -- replace j with 0 by lia;simpl_nat.
             clear Vk;pose proof Vk' as Vk.
             cut (0 < k < ln u1);[clear Rk;intros Rk|lia].
             rewrite (@μ_ln u1 k Rk Vk).
             left;apply vertex_ln.
          -- right;exists j;split;auto.
             lia.
    - intros k Rk.
      destruct_ltb k (ln u1);intros Ik x (Ix&V);simpl in Ik,Ix;
        repeat setoid_rewrite in_rm in Ik;
        repeat setoid_rewrite in_app_iff in Ik;
        repeat setoid_rewrite in_map_iff in Ik;
        repeat setoid_rewrite in_rm in Ix;
        repeat setoid_rewrite in_app_iff in Ix;
        repeat setoid_rewrite in_map_iff in Ix;
        destruct Ik as (N&[Ik|(k'&<-&Ik)]);
        destruct Ix as (Nx&[Ix|(y&<-&Iy)]);
        try (unfold incr in *;integers).
      + destruct V as [V|V].
        * apply path_par_cross in V as [->|F];[|lia|integers].
          cut (⨥ (ln u1) k'∈ vertices (u1∥u2));
            [intros Vk;rewrite (@μ_0 _ _ Rk Vk);apply vertex_0|].
          simpl;rewrite in_rm,in_app_iff,in_map_iff.
          split;auto;right;exists k';split;auto.
        * apply path_𝕲 in V.
          integers.
      + simpl;apply PeanoNat.Nat.ltb_ge in L as ->;auto; simpl_nat.
        apply IHu2; unfold incr in *;simpl in *;lia||auto;split;auto.
        destruct V as [V|V];apply path_par_right in V;simpl_nat;
          unfold visible;tauto||lia.
      + pose proof L as Lk.
        simpl;apply PeanoNat.Nat.ltb_lt in L as ->;auto; simpl_nat.
        unfold replace;destruct_eqX x (ln u1+ln u2);[integers|].
        apply IHu1; simpl in *;lia||auto;split;auto.
        destruct V as [V|V];apply path_par_left in V;simpl_nat;
          unfold visible;tauto||lia||integers.
      + unfold incr in *;destruct V as [V|V].
        * apply path_𝕲 in V.
          integers.
        * apply path_par_cross in V as [->|F];[lia| |integers].
          simpl in *;replace y with (ln u2) in * by lia.
          unfold replace;
            destruct_eqX (ln u1 + ln u2) (ln u2 + ln u1);[|lia].
          destruct_ltb k (ln u1);[integers|].
          apply IHu1;try tauto.
          split;[apply vertex_ln|].
          right;rewrite<- graph_output;apply 𝕲_connected;
            is_vertex.
  Qed.

  (** [μ u k] is a morphism from the [k]-visible part of the graph of
  [u] to the moustache graph. *)
  Lemma μ_morph u k :
    0 < k < ln u -> k ∈ (vertices u) ->
    ([] ⊨ (filter_graph (fun i : nat => k -[ 𝕲 u ]?→ i)
                        (fun j : nat => j -[ 𝕲 u ]?→ k) 
                        (𝕲 u)) 
     -{μ u k}→ 𝕲 (moustache u k)).
  Proof.
    intros Rk Vk;repeat split.
    - rewrite input_filter_graph;repeat rewrite graph_input.
      apply (@μ_0 u k);auto.
    - rewrite output_filter_graph;repeat rewrite graph_output;
        apply (@μ_ln u k);auto.
    - apply proper_morphism_diff.
      unfold incl;intros ((i,a),j).
      intro I;apply graph.in_map in I.
      revert I; rewrite edges_filter_graph.
      setoid_rewrite filter_In.
      setoid_rewrite orb_true_iff.
      setoid_rewrite exists_path_spec.
      intros (x&y&(Ie&V)&<-&<-).
      revert k Rk Vk a x y Ie V;induction u.
      + intros k;simpl;lia.
      + intros k Rk Vk.
        destruct_eqb k (ln u1);[|destruct_ltb k (ln u1)].
        * simpl;repeat rewrite PeanoNat.Nat.eqb_refl.
          simpl;tauto.
        * intros a x y Ie V.
          simpl in Vk;rewrite in_app_iff,in_map_iff in Vk.
          destruct Vk as [Vk|(k'&<-&Vk)];[integers|].
          assert (Lk : 0 < k') by (unfold incr in *;lia);simpl.
          apply PeanoNat.Nat.eqb_neq in N as ->.
          apply PeanoNat.Nat.ltb_ge in L as ->.
          simpl in Ie;simpl;rewrite edges_graph_union,in_app_iff,
                            edges_graph_map,graph.in_map in *;simpl_nat.
          destruct Ie as [Ie|(i&j&Ie&<-&<-)];simpl_nat.
          -- destruct_leb x (ln u1);[|integers].
             destruct_leb y (ln u1);[|integers].
             tauto.
          -- unfold incr in *;
               destruct_leb (ln u1 + j) (ln u1);[integers|].
             destruct_leb (ln u1 + i) (ln u1).
             ++ replace i with 0 in * by lia.
                right;apply graph.in_map;simpl;unfold incr in *.
                eexists;eexists;split;[eapply IHu2;eauto|].
                ** simpl in *;lia.
                ** destruct V as [V|V];[integers|]; right.
                   apply path_seq_right in V;[|integers].
                   simpl_nat;auto.
                ** rewrite @μ_0;simpl in *;try split;
                     lia||auto.
             ++ right;apply graph.in_map;simpl.
                eexists;eexists;split;[eapply IHu2;eauto|].
                ** simpl in *;lia.
                ** destruct V as [V|V];apply path_seq_right in V;
                     simpl_nat;tauto||lia.
                ** lia.
        * intros a x y Ie V.
          simpl in Vk;rewrite in_app_iff,in_map_iff in Vk.
          destruct Vk as [Vk|(k'&<-&Vk)];[|integers].
          pose proof L as Lk;simpl.
          apply PeanoNat.Nat.eqb_neq in N as ->.
          apply PeanoNat.Nat.ltb_lt in L as ->.
          simpl in Ie;simpl;rewrite edges_graph_union,in_app_iff,
                            edges_graph_map,graph.in_map in *;simpl_nat.
          destruct Ie as [Ie|(i&j&Ie&<-&<-)];simpl_nat.
          -- destruct_leb x (ln u1);[|integers].
             destruct_leb y (ln u1);[|integers].
             left;apply IHu1;try tauto.
             clear IHu1 IHu2.
             destruct V as [V|V];apply path_seq_left in V;
               tauto||is_vertex.
          -- unfold incr in *;
               destruct_leb (ln u1+j) (ln u1);[integers|]; simpl_nat.
             destruct_leb (ln u1+i) (ln u1);simpl_nat.
             ++ replace i with 0 in * by lia.
                right;apply graph.in_map;simpl.
                eexists;eexists;split;[eauto|].
                simpl_nat;rewrite @μ_ln;lia||auto.
             ++ right;apply graph.in_map;simpl.
                eexists;eexists;split;[eauto|].
                split;lia.
      + intros k Rk.
        destruct_ltb k (ln u1);intros Ik x;
          simpl in Ik;
          repeat setoid_rewrite in_rm in Ik;
          repeat setoid_rewrite in_app_iff in Ik;
          repeat setoid_rewrite in_map_iff in Ik;
          destruct Ik as (N&[Ik|(k'&<-&Ik)]);try integers.
        * intros i0 j0 Ie.
          simpl in Ie;
            rewrite edges_graph_union,in_app_iff,edges_graph_map,
            edges_graph_map,graph.in_map,graph.in_map in Ie.
          destruct Ie as [Ie|Ie]; destruct Ie as (i&j&Ie&<-&<-).
          -- intros V;simpl;simpl_nat.
             apply PeanoNat.Nat.ltb_ge in L as ->.
             revert V; unfold replace,Basics.compose.
             destruct_eqX i (ln u1);[integers|].
             destruct_eqX j (ln u1); simpl_nat.
             ++ intros [V|V];integers.
             ++ intros [V|V];[integers|].
                apply path_par_cross in V as [->|Ei];integers.
          -- unfold replace,Basics.compose,incr in*.
             destruct_eqX (ln u1+j) (ln u1);[integers|].
             destruct_eqX (ln u1+i) (ln u1).
             ++ replace i with 0 in * by lia.
                intros [V|V];[integers|].
                simpl;simpl_nat.
                apply PeanoNat.Nat.ltb_ge in L as ->.
                apply IHu2;auto.
                ** simpl in *;lia.
                ** apply path_par_right in V;simpl_nat;tauto||lia.
             ++ intro V;simpl;simpl_nat.
                apply PeanoNat.Nat.ltb_ge in L as ->.
                apply IHu2;auto.
                ** simpl in *;lia.
                ** destruct V as [V|V];apply path_par_right in V;
                     simpl_nat;tauto||lia.
        * intros i0 j0 Ie.
          simpl in Ie;
            rewrite edges_graph_union,in_app_iff,edges_graph_map,
            edges_graph_map,graph.in_map,graph.in_map in Ie.
          destruct Ie as [Ie|Ie]; destruct Ie as (i&j&Ie&<-&<-).
          -- intro V;simpl.
             pose proof L as Lk;apply PeanoNat.Nat.ltb_lt in L as ->.
             revert V;unfold replace.
             destruct_eqX i (ln u1);[integers|].
             destruct_eqX i (ln u1 + ln u2);[integers|].
             destruct_eqX j (ln u1);
               [|destruct_eqX j (ln u1 + ln u2);[integers|]].
             ++ intros [V|V];[|integers].
                apply IHu1;lia||auto.
                apply path_par_left in V;tauto||integers.
             ++ intro V;apply IHu1;lia||auto.
                destruct V as [V|V];apply path_par_left in V;
                  tauto||integers.
          -- intro V;simpl.
             pose proof L as Lk;apply PeanoNat.Nat.ltb_lt in L as ->.
             revert V;unfold replace,Basics.compose,incr in *.
             destruct_eqX (ln u1+j) (ln u1);[integers|].
             destruct_eqX (ln u1+i) (ln u1);
               [destruct_eqX 0 (ln u1 + ln u2);[integers|]
               |destruct_eqX (ln u1 + i) (ln u1 + ln u2);
                [integers|]];
               destruct_eqX (ln u1 + j) (ln u1 + ln u2). 
             ++ replace i with 0 in * by lia.
                intros [V|V];integers.
             ++ intros [V|V];integers.
             ++ intros [V|V];[|integers].
                apply path_par_cross in V;[|lia].
                simpl in *;integers.
             ++ intros [V|V];[|integers].
                apply path_par_cross in V;[|lia].
                integers.
    - apply μ_internal;auto.
  Qed.

  (** We are now equipped to prove the lemma necessary for the
      induction step corresponding to the sequential product: assuming
      there is a morphism from [v1⨾v2] to [u], then one of three
      things happen: 
      - every variable in [v1] is a test, and there is a morphism from [v2] to [u];
      - every variable in [v2] is a test, and there is a morphism from [v1] to [u];
      - there is an internal vertex of [u], say [k], such that the graph of [pref u k] (resp. [suf u k]) is smaller that that of [v1] (resp. [v2]). 
   *)
  Lemma morphism_seq_step (u v1 v2 : 𝐒𝐏 X) A :
    A ⊨ 𝕲 u ⊲ 𝕲 (v1 ⨾ v2) ->
    (𝒱 v1 ⊆ A /\ A ⊨ 𝕲 u ⊲ 𝕲 v2)
    \/ (𝒱 v2 ⊆ A /\ A ⊨ 𝕲 u ⊲ 𝕲 v1)
    \/ (exists k, 0 < k < ln u /\ k ∈ (vertices u)
            /\ A ⊨ 𝕲 (pref u k) ⊲ 𝕲 v1
            /\ A ⊨ 𝕲 (suf u k) ⊲ 𝕲 v2).
  Proof.
    intros (φ&M).
    destruct_eqb (φ (ln v1)) 0;
      [|destruct_eqb (φ (ln v1)) (ln u)].
    - destruct M as (M1&M2&M3&M4);left;split.
      + cut (forall i, i ∈ ⌊𝕲 v1⌋ -> φ i = 0).
        * intro h;apply supid_hom_order;exists (fun _ => 0);repeat split.
          -- intros i a j Ie.
             cut ((i,a,j)∈edges(𝕲 (v1 ⨾ v2))).
             ++ intros Ie';apply M3 in Ie' as [Ie'|F];[tauto|].
                rewrite (h i),(h j) in F.
                ** integers.
                ** is_vertex.
                ** is_vertex.
             ++ simpl;rewrite edges_graph_union,in_app_iff;tauto.
          -- intros i I;now left.
        * intros i I.
          repeat rewrite graph_input in *;
            repeat rewrite graph_output in *.
          assert (P: i -[𝕲 (v1⨾v2)]→ ln v1).
          -- apply 𝕲_connected in I as (L1&L2).
             repeat rewrite graph_input in *;
               repeat rewrite graph_output in *.
             simpl;apply path_graph_union;tauto.
          -- eapply morphism_is_order_preserving in P;eauto.
             rewrite E in P.
             integers.
      + exists(φ ∘ (id ∘ ⨥ (ln v1))) ;repeat split.
        * unfold Basics.compose,id,incr.
          repeat rewrite graph_input;simpl_nat;auto.
        * unfold Basics.compose,id;simpl;rewrite <- M2.
          repeat rewrite graph_output;simpl.
          f_equal;lia.
        * rewrite <- (app_nil_l A) at 1.
          eapply is_premorphism_compose;[|apply M3].
          eapply is_premorphism_compose_weak;
            [|simpl;apply seq_graph_union_decomp;auto].
          apply graph_map_morphism.
        * intros i I.
          apply vertices_graph_vertices;
            unfold Basics.compose,id;simpl.
          apply vertices_graph_vertices,M4.
          apply vertices_graph_vertices;simpl.
          apply in_app_iff;right;apply in_map_iff;exists i;split;auto.
          apply vertices_graph_vertices,I.
    - destruct M as (M1&M2&M3&M4);right;left;split.
      + cut (forall i, i ∈ ⌊𝕲 v2⌋ -> φ (ln v1 + i) = ln u).
        * intro h;apply supid_hom_order;exists (fun _ => 0);repeat split.
          -- intros i a j Ie.
             cut ((ln v1+i,a,ln v1+j)∈edges(𝕲 (v1 ⨾ v2))).
             ++ intros Ie';apply M3 in Ie' as [Ie'|F];[tauto|].
                rewrite (h i),(h j) in F.
                ** integers.
                ** is_vertex.
                ** is_vertex.
             ++ simpl;rewrite edges_graph_union,in_app_iff,
                      edges_graph_map,graph.in_map.
                right;exists i,j;split;auto.
          -- intros i I;now left.
        * intros i I.
          cut (ln u <= φ (ln v1+i)).
          -- intro L;cut (ln v1+i ∈ ⌊𝕲 (v1 ⨾ v2)⌋).
             ++ intros V;apply M4 in V;integers.
             ++ rewrite <- vertices_graph_vertices in *.
                simpl;apply in_app_iff;right;
                  apply in_map_iff;exists i;split;auto.
          -- rewrite <- E.
             eapply path_𝕲;
               apply (morphism_is_order_preserving M3).
             replace (ln v1) with (⨥ (ln v1) 0) at 1 by integers.
             replace (ln v1 + i) with (⨥ (ln v1) i) at 1 by integers.
             simpl;apply path_graph_union;right.
             apply graph_map_path;rewrite <- (graph_input v2).
             apply 𝕲_connected,I. 
      + exists (φ ∘ id) ;repeat split.
        * rewrite <- M1;unfold Basics.compose,id.
          f_equal;repeat rewrite graph_input;simpl;auto.
        * unfold Basics.compose,id.
          repeat rewrite graph_output;simpl;auto.
        * rewrite <- (app_nil_l A) at 1.
          eapply is_premorphism_compose;[|apply M3].
          simpl;apply seq_graph_union_decomp;auto.
        * intros i I;unfold Basics.compose,id.
          apply M4.
          apply vertices_graph_vertices;simpl.
          apply in_app_iff;left.
          apply vertices_graph_vertices,I.
    - right;right; set (k:= φ (ln v1)); exists k.
      assert (0 < k < ln u /\ k ∈ vertices u)
        as (Rk & Vk).
      + cut (φ (ln v1) ∈ vertices u).
        * intro V;split;auto.
          unfold k;integers.
        * apply vertices_graph_vertices,M.
          apply vertices_graph_vertices;simpl;
            apply in_app_iff;left;apply vertex_ln.
      + split;[|split];auto.
        cut ((A⊨ 𝕲(v1⨾v2) -{μ u k ∘ φ}→ 𝕲(moustache u k))
             /\ (μ u k ∘ φ) (ln v1) = split_point u k).
        * rewrite @split_moustache;auto.
          intros (M'&E);eapply morphism_seq_split;eauto.
          rewrite @ln_pref_split_point,E;auto.
        * split.
          -- rewrite <- (app_nil_r A).
             eapply is_morphism_compose;[|apply μ_morph;auto].
             cut (forall i, i ∈ vertices (v1 ⨾ v2)
                       -> (i -[𝕲 (v1 ⨾ v2)]→ ln v1)
                         \/ ln v1 -[𝕲 (v1 ⨾ v2)]→ i);
               [intro hyp;repeat split|].
             ++ rewrite input_filter_graph;apply M. 
             ++ rewrite output_filter_graph;apply M.
             ++ intros i a j Ie;rewrite edges_filter_graph,filter_In.
                pose proof (In_vertices Ie) as (Vi&Vj).
                pose proof Ie as I;apply M in Ie as [Ie|Ie];
                  [tauto|right;split;[tauto|]].
                apply orb_true_iff;repeat rewrite exists_path_spec.
                destruct (hyp _ Vi) as [Pi|Pi].
                ** destruct (hyp _ Vj) as [Pj|Pj].
                   --- right;eapply morphism_is_order_preserving;eauto.
                       apply M.
                   --- destruct_eqX i (ln v1);[left;reflexivity|].
                       destruct_eqX j (ln v1);[right;reflexivity|].
                       exfalso;apply path_𝕲 in Pi;
                         apply path_𝕲 in Pj.
                       simpl in I;rewrite edges_graph_union in I.
                       rewrite in_app_iff,edges_graph_map,
                       graph.in_map in I.
                       destruct I as [I|(x&y&I&<-&<-)];integers.
                **  left;eapply morphism_is_order_preserving;eauto.
                    apply M.
             ++ intros i I;rewrite filter_graph_visible;
                  [ | |apply vertices_graph_vertices ];auto.
                unfold visible;apply filter_In;split;
                  [apply M,I|].
                rewrite orb_true_iff;repeat rewrite exists_path_spec.
                apply vertices_graph_vertices,hyp in I.
                destruct M as (_&_&M&_);destruct I as [P|P];
                  apply (morphism_is_order_preserving M) in P;auto.
             ++ intros i I;simpl in I.
                rewrite in_app_iff,in_map_iff in I.
                destruct I as [I|(j&<-&I)].
                ** left;simpl;apply path_graph_union;auto.
                   left;rewrite <- graph_output.
                   apply 𝕲_connected;is_vertex.
                ** right.
                   replace (ln v1) with (⨥ (ln v1) 0) at 1
                     by integers.
                   replace (ln v1 + j) with (⨥ (ln v1) j) at 1
                     by integers.
                   simpl;apply path_graph_union;right.
                   apply graph_map_path.
                   rewrite <- (graph_input v2);apply 𝕲_connected.
                   is_vertex.
          -- unfold Basics.compose;unfold k.
             apply @μ_k;auto.
  Qed.
  
  (** ** Completeness lemma *)
  Lemma 𝐒𝐏_inf_is_morph A u v :
    A ⊨ 𝕲 u ⊲ 𝕲 v -> A ⊨ u << v.
  Proof.
    set (hidden_graph :=fun u => (input (𝕲 u),edges (𝕲 u),output (𝕲 u))).
    assert (hidden_graph_𝕲: forall u, hidden_graph u = 𝕲 u)
      by (intro u';unfold hidden_graph;destruct (𝕲 u') as ((i,e),o);
          reflexivity).
    revert u ;induction v;
      repeat rewrite <- hidden_graph_𝕲;simpl;
        repeat rewrite hidden_graph_𝕲;
        clear hidden_graph hidden_graph_𝕲;intros u M.
    - apply (@incl_𝐒𝐏_inf _ []);[intro;simpl;tauto|].
      destruct M as (φ&M);apply (@morph_var A _ _ φ),M. 
    - apply morphism_seq_step in M as [(E&M)|[(E&M)|(k&Rk&Vk&M1&M2)]].
      + transitivity v2;[apply IHv2|];auto.
      + transitivity v1;[apply IHv1|];auto.
      + etransitivity;[eapply @incl_𝐒𝐏_inf,@𝐒𝐏_inf_pref_suf;
                       eauto using incl_nil|].
        apply 𝐒𝐏_inf_seq;auto.
    - simpl in M;destruct M as (φ&M).
      eapply par_morphism_graph_union_left in M as (M1&M2);auto.
      transitivity (u∥u);auto;apply 𝐒𝐏_inf_par;[apply IHv1|apply IHv2].
      -- etransitivity;[eexists;apply M1|].
         eapply hom_order_incl;[apply incl_nil|].
         eexists; eapply graph_map_morphism.
      -- etransitivity;[eexists;apply M2|].
         eapply hom_order_incl;[apply incl_nil|].
         eexists; eapply graph_map_morphism.
  Qed.

  (** * Main result *)
  Theorem 𝐒𝐏_inf_is_morph_iff A u v :
    A ⊨ 𝕲 u ⊲ 𝕲 v <-> A ⊨ u << v.
  Proof. split;[apply 𝐒𝐏_inf_is_morph|apply is_morph_𝐒𝐏_inf]. Qed.

  (** * Reduction *)
  Proposition reduce_term_spec A u v :
    A ⊨ u << v -> exists w, w ∈ reduce_term A v /\ [] ⊨ u << w.
  Proof.
    revert u;induction v;intros u M.
    - apply 𝐒𝐏_inf_is_morph_iff in M as (ϕ&M).
      apply morph_var in M.
      exists (𝐒𝐏_var x);simpl;auto.
    - apply 𝐒𝐏_inf_is_morph_iff,morphism_seq_step in M as [(I&M)|[(I&M)|(k&Lk&Ik&M1&M2)]].
      + apply 𝐒𝐏_inf_is_morph_iff,IHv2 in M as (w&Iw&M).
        exists w;split;auto.
        apply inclb_correct in I.
        simpl;rewrite I.
        destruct (inclb (𝒱 v2) A);mega_simpl;tauto.
      + apply 𝐒𝐏_inf_is_morph_iff,IHv1 in M as (w&Iw&M).
        exists w;split;auto.
        apply inclb_correct in I.
        simpl;rewrite I.
        destruct (inclb (𝒱 v1) A);mega_simpl;tauto.
      + apply 𝐒𝐏_inf_is_morph_iff,IHv1 in M1 as (w1&Iw1&M1).
        apply 𝐒𝐏_inf_is_morph_iff,IHv2 in M2 as (w2&Iw2&M2).
        exists (w1⨾w2);split.
        * simpl;destruct (inclb (𝒱 v1) A),(inclb (𝒱 v2) A);mega_simpl;repeat right;auto;
            apply in_bimap;exists w1,w2;auto.
        * rewrite <- M1,<-M2.
          etransitivity;[|apply (@split_moustache_inf _ _ Lk Ik)].
          apply (@𝐒𝐏_inf_moustache _ _ Ik).
    - destruct (IHv1 u) as (w1&I1&M1);[|destruct (IHv2 u) as (w2&I2&M2)].
      + etransitivity;[apply M|];auto.
      + etransitivity;[apply M|];auto.
        transitivity (v2∥v1);auto.
      + exists (w1∥w2);split.
        * simpl;apply in_bimap;exists w1,w2;split;auto.
        * rewrite <- M1,<-M2;auto.
  Qed.
      
  
End s.
(* begin hide *)
Arguments 𝕲 {X} u.
Ltac ln_pos :=
  repeat (match goal with
          | [ u : 𝐒𝐏 _ |- _ ] =>
            match goal with
            | [ _ : 0 < ln u |- _ ] => fail 1
            | _ => let L := fresh "L" in
                  pose proof (ln_non_zero u) as L
            end
          end).
Ltac show_integers :=
  (match goal with
   | [ h : _ -[ _ ]→ _ |- _ ] =>
     apply path_𝕲 in h
   | [ h : In _ (edges (𝕲 _ )) |- _ ] =>
     apply 𝕲_bounded in h
   | [ h : In _ ⌊ _ ⌋ |- _ ] =>
     apply vertices_bounded in h
   | [ h : In _ (vertices _ ) |- _ ] =>
     apply vertices_graph_vertices,vertices_bounded in h
   end).
Ltac integers :=
  ln_pos;repeat (show_integers;auto);unfold incr,decr in *;
  simpl in *;lia.  
Ltac is_vertex :=
  repeat (match goal with
          | [h: In (_,_,_) (edges (𝕲 _)) |- _ ] =>
            apply In_vertices in h;auto;destruct h
          | [ |- _ ∈ (graph_vertices _) ] =>
            apply vertices_graph_vertices
          end);
  tauto.


Ltac destruct_bool b :=
  match b with
  | Nat.eqb ?x ?y =>
    let X :=fresh "X" in
    (assert (X:x=y) by assumption;
     apply PeanoNat.Nat.eqb_eq in X;
     try rewrite X;clear X)
    || (assert (X:x<>y) by assumption;
        apply PeanoNat.Nat.eqb_neq in X;
        try rewrite X;clear X)
    || destruct_eqb x y
  | eqX ?x ?y => 
    let X :=fresh "X" in
    (assert (X:x=y) by assumption;
     apply PeanoNat.Nat.eqb_eq in X;
     try rewrite X;clear X)
    || (assert (X:x<>y) by assumption;
        apply PeanoNat.Nat.eqb_neq in X;
        try rewrite X;clear X)
    || destruct_eqX_D NatNum x y
  | Nat.leb ?x ?y =>
    let X :=fresh "X" in
    (assert (X:x<=y) by (auto with arith);
     apply PeanoNat.Nat.leb_le in X;
     try rewrite X;clear X)
    || (assert (X:y<x) by (auto with arith);
        apply PeanoNat.Nat.leb_gt in X;
        try rewrite X;clear X)
    || destruct_leb x y
    
  | Nat.ltb ?x ?y =>
    let X :=fresh "X" in
    (assert (X:x<y) by (auto with arith);
     apply PeanoNat.Nat.ltb_lt in X;
     try rewrite X;clear X)
    || (assert (X:y<=x) by (auto with arith);
        apply PeanoNat.Nat.ltb_ge in X;
        try rewrite X;clear X)
    || destruct_ltb x y
  end ;
  (simpl_nat;simpl in * ; integers || auto).

Ltac destr_bool :=
  let rec study_bool b :=
      match b with
      | _ ( if ?b then ?n else ?m ) ?c =>
        study_bool b
      | _ _ ( if ?b then ?n else ?m ) =>
        study_bool b
      | _ => destruct_bool b
      end
  in
  let rec study_term n :=
      match n with
      | if ?b then ?n else ?m => study_bool b
      | ?n ∥ ?m => first [study_term n|study_term m]
      | ?n ⨾ ?m => first [study_term n|study_term m]
      end
  in
  let rec study_nat n :=
      match n with
      | if ?b then ?n else ?m => study_bool b
      | ?n - ?m => first [study_nat n|study_nat m]
      | ?n + ?m => first [study_nat n|study_nat m]
      | ln ?u => study_term u
      | _ ?n => study_nat n
      end
  in
  let rec aux p :=
      match p with
      | ?p /\ ?q => first [aux q|aux p]
      | ?p \/ ?q => first [aux q|aux p]
      | ?p -> ?q => first [aux q|aux p]
      | (?n1,_,?n2) = (?m1,_,?m2)  =>
        first[study_nat n1|study_nat n2|
              study_nat m1|study_nat m2]
      | ?n = ?m  => first[study_nat n|study_nat m]
      | In (?n,_,?m) _ => first[study_nat n|study_nat m]
      | In ?n _ => study_nat n
      | ~ ?p => aux p
      | _ ⊨ ?u << ?v => first[study_term u|study_term v]
      end in
  match goal with
  | |- ?p => (aux p)
  end.
(* end hide *)



