(** This module introduces weak terms, and establishes some of their
general properties. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export tools sp_terms.

(** * Plain weak terms *)
(** ** Definitions *)
Section s0.
  (** Fix a set of variables [X]. *)
  Variable X : Set.

  (** A weak term consists of a pair whose first projection is either
  a series-parallel term or the constant [None] (which stands for [1]
  in this context), and whose second projection is a list of
  variables, called the set of tests of the term. *)
  Definition 𝐖 : Set := (option (𝐒𝐏 X)) * list X.

  (** The weak term [𝟭] is the pair [(None,[])].*)
  Definition 𝐖_one : 𝐖 := (None,[]).
  
End s0.
(* begin hide *)
Arguments 𝐖_one {X}.
(* end hide *)
Notation " 𝟭 " := 𝐖_one.

Section s.
  Context {X : Set}.
  (** We define the sequential product of weak terms as follows. *)
  Definition 𝐖_seq (u v : 𝐖 X) : 𝐖 X :=
    match (u,v) with
    | ((None,A),(s,B)) => (s,A++B)
    | ((s,A),(None,B)) => (s,A++B)
    | ((Some s,A),(Some t,B)) => (Some (s⨾t),A++B)
    end.
  Infix " ⊗ " := 𝐖_seq (at level 40).
  
  (** We define the parallel product of weak terms as follows. *)
  Definition 𝐖_par (u v : 𝐖 X) : 𝐖 X :=
    match (u,v) with
    | ((None,A),(None,B)) => (None,A++B)
    | ((None,A),(Some s,B)) => (None,A++B++𝒱 s)
    | ((Some s,A),(None,B)) => (None,A++B++𝒱 s)
    | ((Some s,A),(Some t,B)) => (Some (s∥t),A++B)
    end.
  Infix " ⊕ " := 𝐖_par (at level 50).

  (** We use the parametric ordering on series-parallel terms to
  define the ordering of weak_terms. *)
  Global Instance 𝐖_inf : Smaller (𝐖 X) :=
    fun e f =>
      match (e,f) with
      | ((None,A),(None,B)) => B ⊆ A
      | ((None,A),(Some u,B)) => B ⊆ A /\ 𝒱 u ⊆ A
      | ((Some u,A),(None,B)) => False
      | ((Some u,A),(Some v,B)) => B ⊆ A /\ A ⊨ u << v
      end.


  (* begin hide *)
  Lemma 𝐖_seq_None_s A s B : (None,A)⊗(s,B) = (s,A++B).
  Proof. reflexivity. Qed.
  Lemma 𝐖_seq_s_None s A B : (s,A)⊗(None,B) = (s,A++B).
  Proof. destruct s;reflexivity. Qed.
  Lemma 𝐖_seq_Some_Some u A v B :
    (Some u,A)⊗(Some v,B) = (Some (u ⨾ v),A++B).
  Proof. reflexivity. Qed.
  Lemma 𝐖_par_None_None A B : (None,A)⊕(None,B) = (None,A++B).
  Proof. reflexivity. Qed.
  Lemma 𝐖_par_None_Some A s B : (None,A)⊕(Some s,B) = (None,A++B++𝒱 s).
  Proof. reflexivity. Qed.
  Lemma 𝐖_par_Some_None s A B : (Some s,A)⊕(None,B) = (None,A++B++𝒱 s).
  Proof. destruct s;reflexivity. Qed.
  Lemma 𝐖_par_Some_Some u A v B :
    (Some u,A)⊕(Some v,B) = (Some (u ∥ v),A++B).
  Proof. reflexivity. Qed.
  Lemma 𝐖_inf_None_None A B :
   ((None,A)≤(None,B)) = (B ⊆ A).
  Proof. reflexivity. Qed.
  Lemma 𝐖_inf_Some_None u A B :
   ((Some u,A)≤(None,B)) = False.
  Proof. reflexivity. Qed.
  Lemma 𝐖_inf_None_Some u A B :
   ((None,A)≤(Some u,B)) = (B ⊆ A /\ 𝒱 u ⊆ A).
  Proof. reflexivity. Qed.
  Lemma 𝐖_inf_Some_Some u A v B :
   ((Some u,A)≤(Some v,B)) = (B ⊆ A /\ A ⊨ u << v).
  Proof. reflexivity. Qed.
  Hint Rewrite 𝐖_inf_None_None 𝐖_inf_None_Some
       𝐖_inf_Some_None 𝐖_inf_Some_Some
       𝐖_seq_None_s 𝐖_seq_Some_Some 𝐖_seq_s_None
       𝐖_par_None_None 𝐖_par_None_Some 𝐖_par_Some_None
       𝐖_par_Some_Some
    : simpl_typeclasses.
  (* end hide *)

  (** As usual, this relation is indeed a preorder. *)
  Global Instance 𝐖_inf_PreOrder : PreOrder smaller.
  Proof.
    split.
    - intros ([u|],A);rsimpl;firstorder.
    - intros ([u|],A) ([v|],B) ([w|],C);
        rsimpl;try firstorder.
      -- now rewrite H2;apply (incl_𝐒𝐏_inf H).
      -- intros (I&S) (J&E);split.
         --- etransitivity;eauto.
         --- rewrite (𝐒𝐏_inf_variables E).
             intro x;rewrite in_app_iff;firstorder.
  Qed.

  (** By taking its symmetric closure, we get an equivalence relation.*)
  Global Instance 𝐖_equiv : Equiv (𝐖 X) := fun e f => e ≤ f /\ f ≤ e.
  (* begin hide *)
  Lemma 𝐖_equiv_inf u v :
    (u ≡ v) = (u ≤ v /\ v ≤ u).
  Proof. reflexivity. Qed.
  Hint Rewrite 𝐖_equiv_inf : simpl_typeclasses.
  (* end hide *)
  
  Global Instance 𝐖_equiv_Equivalence :
    Equivalence equiv.
  Proof.
    split;split;auto.
    - reflexivity.
    - reflexivity.
    - destruct H;auto.
    - destruct H;auto.
    - destruct H;destruct H0;etransitivity;eauto.
    - destruct H;destruct H0;etransitivity;eauto.
  Qed.

  Global Instance 𝐖_inf_PartialOrder: PartialOrder equiv smaller.
  Proof. intros e f;rsimpl;firstorder. Qed.

  (** We now define the size and the set of variables of a weak term. *)
  Global Instance 𝐖_size : Size (𝐖 X) :=
    fun u =>
      match u with
      | (None,A) => # A
      | (Some s,A) => |s| + # A
      end.

  Global Instance 𝐖_variables : Alphabet 𝐖 X :=
    fun u =>
      match u with
      | (None,A) => A
      | (Some u,A) => 𝒱 u ++ A
      end.
  (* begin hide *)
  Lemma 𝐖_size_None A : |(None,A)| = # A.
  Proof. reflexivity. Qed.
  Lemma 𝐖_size_Some u A : |(Some u,A)| = |u| + # A.
  Proof. reflexivity. Qed.
  Lemma 𝐖_variables_None A : 𝒱 (None,A) = A.
  Proof. reflexivity. Qed.
  Lemma 𝐖_variables_Some u A : 𝒱 (Some u,A) = 𝒱 u ++ A.
  Proof. reflexivity. Qed.
  Hint Rewrite 𝐖_size_None 𝐖_size_Some 𝐖_variables_None
       𝐖_variables_Some
    : simpl_typeclasses.
  (* end hide *)

  (** ** Properties of weak terms. *)
  (** Both product act as a union with respect to sets of variables. *)
  Lemma variables_𝐖_par (s t : 𝐖 X) :
    𝒱 (s ⊕ t) ≈ 𝒱 s ++ 𝒱 t.
  Proof.
    destruct s as ([s|],A);destruct t as ([t|],B);simpl;
    unfold 𝒱;simpl;unfold 𝒱;simpl;intro;
    repeat rewrite in_app_iff;tauto.
  Qed.
  
  Lemma variables_𝐖_seq (s t : 𝐖 X) :
    𝒱 (s ⊗ t) ≈ 𝒱 s ++ 𝒱 t.
  Proof.
    destruct s as ([s|],A);destruct t as ([t|],B);simpl;
    unfold 𝒱;simpl;unfold 𝒱;simpl;intro;
    repeat rewrite in_app_iff;tauto.
  Qed.

  (** Both products are monotone operations. *)
  Global Instance 𝐖_seq_𝐖_inf :
    Proper (smaller ==> smaller ==> smaller) 𝐖_seq.
  Proof.
    intros ([u1|],A1) ([v1|],B1) h1 ([u2|],A2) ([v2|],B2) h2;
      rsimpl in *;
    try destruct h1 as (I1&h1);try destruct h2 as (I2&h2);try tauto;
    try (split;[now (rewrite I1 || rewrite h1);
                 (rewrite I2 || rewrite h2)|]);auto.
    - apply 𝐒𝐏_inf_seq;eapply incl_𝐒𝐏_inf;eauto.
      -- now apply incl_appl.
      -- now apply incl_appr.
    - etransitivity;[|apply 𝐒𝐏_inf_axinf,𝐒𝐏_axinf_1_seqr].
      -- eapply incl_𝐒𝐏_inf;eauto.
         now apply incl_appl.
      -- apply incl_appr;auto.
    - eapply incl_𝐒𝐏_inf;eauto.
      now apply incl_appl.
    - etransitivity;[|apply 𝐒𝐏_inf_axinf,𝐒𝐏_axinf_1_seql].
      -- eapply incl_𝐒𝐏_inf;eauto.
         now apply incl_appr.
      -- apply incl_appl;auto.
    - rsimpl in *;rewrite h1,h2;reflexivity.
    - rsimpl;rewrite h1;apply incl_appl;reflexivity.
    - eapply incl_𝐒𝐏_inf;eauto.
      now apply incl_appr.
    - rsimpl;rewrite h2;apply incl_appr;reflexivity.
    - rsimpl;now rewrite h1,h2.
  Qed.
  
  Global Instance 𝐖_seq_𝐖_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐖_seq.
  Proof.
    intros ? ? (h1&h2) ? ? (h3&h4);split.
    - now rewrite h1,h3.
    - now rewrite h2,h4.
  Qed.
  Global Instance 𝐖_par_𝐖_inf :
    Proper (smaller ==> smaller ==> smaller) 𝐖_par.
  Proof.
    intros ([u1|],A1) ([v1|],B1) h1 ([u2|],A2) ([v2|],B2) h2;
      rsimpl in *;try destruct h1 as (I1&h1);try destruct h2 as (I2&h2);
        try tauto;try split.
    - now rewrite I1,I2.
    - apply 𝐒𝐏_inf_par;eapply incl_𝐒𝐏_inf;eauto.
      -- now apply incl_appl.
      -- now apply incl_appr.
    - now rewrite I1,I2,<-app_ass;apply incl_appl.
    - apply 𝐒𝐏_inf_variables in h1;simpl;rewrite h2,h1.
      intro ;repeat rewrite in_app_iff; tauto.
    - apply 𝐒𝐏_inf_variables in h1.
      rewrite h2,h1,I1;intro ;repeat rewrite in_app_iff; tauto.
    - rewrite I2,I1.
      intro ;repeat rewrite in_app_iff; tauto.
    - apply 𝐒𝐏_inf_variables in h2;rewrite h2,h1.
      intro ;repeat rewrite in_app_iff; tauto.
    - now rewrite I1,I2.
    - simpl;rewrite h2,h1;reflexivity.
    - rewrite I1,h2,h1;intro ;repeat rewrite in_app_iff; tauto.
    - apply 𝐒𝐏_inf_variables in h2.
      rewrite h2,h1,I2;intro ;repeat rewrite in_app_iff; tauto.
    - rewrite h1,I2,h2;intro ;repeat rewrite in_app_iff; tauto.
    - now rewrite h1,h2.
  Qed.
  
  Global Instance 𝐖_par_𝐖_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐖_par.
  Proof.
    intros ? ? (h1&h2) ? ? (h3&h4);split.
    - now rewrite h1,h3.
    - now rewrite h2,h4.
  Qed.

  (** We now list some (in)equalities between weak terms. *)
  Lemma 𝐖_par_comm e f : e⊕f ≡ f⊕e.
  Proof.
    destruct e as([u|],A);destruct f as ([v|],B);split;rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
    
  Lemma 𝐖_par_idem e : e ⊕ e ≡ e.
  Proof.
    destruct e as([u|],A);split;rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.

  Lemma 𝐖_seq_assoc e f g : e⊗(f ⊗ g) ≡ (e⊗f)⊗g.
  Proof.
    destruct e as ([u|],A);destruct f as ([v|],B);
    destruct g as ([w|],C);
    split;rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
    
  Lemma 𝐖_par_assoc e f g : e⊕(f ⊕ g) ≡ (e⊕f)⊕g.
  Proof.
    destruct e as ([u|],A);destruct f as ([v|],B);
    destruct g as ([w|],C);split;
    rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖_seq_one_l e : 𝟭 ⊗ e ≡ e.
  Proof.
    destruct e as([u|],A);split;unfold 𝐖_one;rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖_seq_one_r e : e ⊗ 𝟭 ≡ e.
  Proof.
    destruct e as([u|],A);split;
    unfold 𝐖_one;rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff; simpl;tauto);auto.
  Qed.

  Lemma 𝐖_par_inf e f : (e ⊕ f) ≤ e.
  Proof.
    destruct e as([u|],A);destruct f as ([v|],B);
      rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖_par_one_l e : (𝟭 ⊕ e) ≤ ((𝟭 ⊕ e)⊗e).
  Proof.
    destruct e as([u|],A);unfold 𝐖_one;rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff;simpl; tauto);auto.
  Qed.
  Lemma 𝐖_par_one_r e : (𝟭 ⊕ e) ≤ (e⊗(𝟭 ⊕ e)).
  Proof.
    destruct e as([u|],A);unfold 𝐖_one;rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff;simpl; tauto);auto.
  Qed.

  Lemma 𝐖_par_one_seq_par e f : (𝟭 ⊕ (e⊗f)) ≡ (𝟭 ⊕ (e ⊕ f)).
  Proof.
    destruct e as([u|],A);destruct f as ([v|],B);
    split;unfold 𝐖_one;rsimpl;
    try split;try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖_par_one_seq_par_one e f : ((𝟭 ⊕ e)⊗(𝟭⊕f)) ≡ (𝟭⊕(e⊕f)).
  Proof.
    destruct e as([u|],A);destruct f as ([v|],B);split;
    unfold 𝐖_one;rsimpl;
    try split;try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖_par_one_seq e f : ((𝟭 ⊕ e)⊗f) ≡ (f⊗(𝟭⊕e)).
  Proof.
    destruct e as([u|],A);destruct f as ([v|],B);split;
    unfold 𝐖_one;rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖_par_one_par e f g : (((𝟭 ⊕ e)⊗f)⊕g) ≡ ((𝟭⊕e)⊗(f⊕g)).
  Proof.
    destruct e as([u|],A);destruct f as ([v|],B);
    destruct g as ([w|],C);split;
    unfold 𝐖_one;rsimpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.

  (** The size of [e ⊕ f] is smaller than the sum of the sizes of [e]
  and [f], plus one. *)
  Lemma 𝐖_size_𝐖_par e f : |e ⊕ f| <= S (|e| + |f|).
  Proof.
    destruct e as ([u|],A);destruct f as ([v|],B);
      rsimpl;repeat rewrite app_length;try lia.
    - pose proof (𝐒𝐏_size_𝐒𝐏_variables u);lia. 
    - pose proof (𝐒𝐏_size_𝐒𝐏_variables v);lia. 
  Qed. 

  (** The size of [e ⊗ f] is smaller than the sum of the sizes of [e]
  and [f], plus one. *)
 Lemma 𝐖_size_𝐖_seq e f : |e ⊗ f| <= S (|e| + |f|).
  Proof.
    destruct e as ([u|],A);destruct f as ([v|],B);rsimpl;auto;
    repeat rewrite app_length;try lia.
  Qed.
                             
End s.
(* begin hide *)
Infix " ⊗ " := 𝐖_seq (at level 40).
Infix " ⊕ " := 𝐖_par (at level 50).

Hint Rewrite @𝐖_inf_None_None @𝐖_inf_None_Some
       @𝐖_inf_Some_None @𝐖_inf_Some_Some
       @𝐖_seq_None_s @𝐖_seq_Some_Some @𝐖_seq_s_None
       @𝐖_par_None_None @𝐖_par_None_Some @𝐖_par_Some_None
       @𝐖_par_Some_Some @𝐖_equiv_inf
       @𝐖_size_None @𝐖_size_Some @𝐖_variables_None @𝐖_variables_Some
  : simpl_typeclasses.

(* end hide *)

(** * Primed weak terms. *)
Section primed.
  Context { X : Set } {decX : decidable_set X}.

  (** ** Definitions *)
  (** Primed weak terms are weak terms over a duplicated alphabet,
  such that their set of tests are balanced (that is each variable
  appears positively if and only if it appears negatively). *)
  Definition 𝐖' := { w : 𝐖 (@X' X) | balanced (snd w) = true }.

  Lemma 𝐖'_dec (w1 w2 : 𝐖') : w1 = w2 <-> π{w1} = π{w2}.
  Proof.
    split;[intros ->;reflexivity|].
    apply eq_sig_hprop.
    intro x;destruct (balanced (snd x)).
    + intros p q;apply UIP.
    + intro p;discriminate.
  Qed.

  (** We define the primed weak term [𝟭']. *)
  Definition 𝐖'_one  : 𝐖' := exist _ (None,[]) (eq_refl _).
  Notation " 𝟭' " := 𝐖'_one.

  (** We define their set of variables using the set of primed
  variables of the underlining series-parallel term over the
  duplicated alphabet. *)
  Definition 𝐖'_variables (u : 𝐖') :=
    match π{u} with
    | (None,A) => A
    | (Some u,A) => 𝐒𝐏'_variables u ++ A
    end.

  (** The size of a primed weak term is simply its size as a weak
  term. *)
  Global Instance 𝐖'_size : Size 𝐖' := fun u => |π{u}|.
  (* begin hide *)
  Lemma 𝐖'_size_simpl x p : |exist _ x p|=|x|.
  Proof. reflexivity. Qed.
  Hint Rewrite 𝐖'_size_simpl : simpl_typeclasses.
  (* end hide *)

  (** The set of variables of a primed weak term is always balanced. *)
  Lemma is_balanced_𝐖'_variables w :
    is_balanced (𝐖'_variables w).
  Proof.
    destruct w as (([u|],A),P);pose proof P as h;apply balanced_spec in h;
      unfold is_balanced in *;simpl in *;
        [|tauto]; unfold 𝐖'_variables;simpl;intros a;
          rewrite in_app_iff,in_app_iff,(h _),(is_balanced_𝐒𝐏'_variables _ _);
          tauto.
  Qed.

  (** Sequential product of primed weak terms. *)
  Definition 𝐖'_seq (u v : 𝐖') : 𝐖' :=
    match (u,v) with
    | ((exist _ (None,A) p1),(exist _ (None,B) p2)) =>
      exist _ (None,A++B)
            (balanced_app p1 p2)
    | ((exist _ (None,A) p1),(exist _ (Some s,B) p2)) =>
      exist _ (Some s,A++B) (balanced_app p1 p2)
    | ((exist _ (Some s,A) p1),(exist _ (None,B) p2)) =>
      exist _ (Some s,A++B) (balanced_app p1 p2)
    | ((exist _ (Some s,A) p1),(exist _ (Some t,B) p2)) =>
      exist _ (Some (s⨾t),A++B) (balanced_app p1 p2)
    end.
  Infix " ⊗' " := 𝐖'_seq (at level 40).

  (** Parallel product of primed weak terms. *)
  Definition 𝐖'_par (u v : 𝐖') : 𝐖':=
    match (u,v) with
    | ((exist _ (None,A) p1),(exist _ (None,B) p2)) =>
      exist _ (None,A++B)
            (balanced_app p1 p2)
    | ((exist _ (None,A) p1),(exist _ (Some s,B) p2)) =>
      exist _ (None,A++B++𝐒𝐏'_variables s)
            (balanced_app p1 (balanced_app
                                p2 (balanced_𝐒𝐏'_variables s)))
    | ((exist _ (Some s,A) p1),(exist _ (None,B) p2)) =>
      exist _ (None,A++B++𝐒𝐏'_variables s)
            (balanced_app p1 (balanced_app
                                p2(balanced_𝐒𝐏'_variables s)))
    | ((exist _ (Some s,A) p1),(exist _ (Some t,B) p2)) =>
      exist _ (Some (s∥t),A++B) (balanced_app p1 p2)
    end.
  Infix " ⊕' " := 𝐖'_par (at level 50).

  (** The order relations on primed weak terms are simply imported
  from those of weak terms. *)
  Global Instance 𝐖'_inf : Smaller 𝐖' := fun u v => π{u} ≤ π{v}.
  Global Instance 𝐖'_equiv : Equiv 𝐖' := fun u v => π{u} ≡ π{v}.
  
  Global Instance 𝐖'_inf_PreOrder : PreOrder smaller.
  Proof.
    unfold smaller,𝐖'_inf.
    split.
    - intros (e&h);reflexivity.
    - intros (e&h1) (f&h2) (g&h3);etransitivity;eauto.
  Qed.
  
  Global Instance 𝐖'_equiv_Equivalence:  Equivalence equiv.
  Proof.
    unfold equiv,𝐖'_equiv.
    split.
    - intros (e&P);reflexivity.
    - intros (e&P) (f&Q);intro h;symmetry;tauto.
    - intros (e&P) (f&Q) (g&R);intros h1 h2;etransitivity;eauto.
  Qed.
  
  Global Instance 𝐖'_inf_PartialOrder : PartialOrder equiv smaller.
  Proof.
    unfold equiv,smaller,𝐖'_inf, 𝐖'_equiv;
    intros (?&?) (?&?).
    pose proof (𝐖_inf_PartialOrder x x0) as h;simpl in *.
    rewrite h;tauto.
  Qed.

  (** ** Properties of primed weak terms. *)
  Lemma variables_𝐖'_par (s t : 𝐖') :
    𝐖'_variables (s ⊕' t) ≈ 𝐖'_variables s ++ 𝐖'_variables t.
  Proof.
    destruct s as (([s|],A),p1);destruct t as (([t|],B),p2);simpl;intro;
    unfold 𝐖'_par, 𝐖'_variables;simpl;
    repeat rewrite in_app_iff;tauto.
  Qed.
  
  Lemma variables_𝐖'_seq (s t : 𝐖') :
    𝐖'_variables (s ⊗' t) ≈ 𝐖'_variables s ++ 𝐖'_variables t.
  Proof.
    destruct s as (([s|],A),p1);destruct t as (([t|],B),p2);simpl;intro;
    unfold 𝐖'_seq, 𝐖'_variables;simpl;
    repeat rewrite in_app_iff;tauto.
  Qed.

  Global Instance 𝐖'_par_𝐖'_inf :
    Proper (smaller ==> smaller ==> smaller) 𝐖'_par.
  Proof.
    intros (([u1|],A1),P1) (([v1|],B1),Q1) h1 (([u2|],A2),P2)
           (([v2|],B2),Q2) h2;
    unfold smaller,𝐖'_inf,smaller,𝐖_inf,
    is_balanced in *;simpl in *;try destruct h1 as (I1&h1);
    try destruct h2 as (I2&h2);try tauto.
    - split.
      -- now rewrite I1,I2.
      -- apply 𝐒𝐏_inf_par;eapply incl_𝐒𝐏_inf;eauto.
         --- now apply incl_appl.
         --- now apply incl_appr.
    - split.
      -- now rewrite I1,I2,<-app_ass;apply incl_appl.
      -- apply 𝐒𝐏_inf_variables in h1.
         unfold 𝒱;simpl;
           rewrite h2,h1,𝐒𝐏_variables_𝐒𝐏'_variables.
         intro ;repeat rewrite in_app_iff; tauto.
    - apply 𝐒𝐏_inf_variables in h1.
      rewrite h2,I1;intros (a&b);repeat rewrite in_app_iff;
      repeat rewrite (𝐒𝐏_variables_𝐒𝐏'_variables_iff _ a b).
      intros [I|[I|[I|I]]];try apply h1,in_app_iff in I as [I|I];
        try tauto;destruct b;auto;apply balanced_spec in P1;apply P1 in I;auto.
    - split.
      -- rewrite I2,I1.
         intro ;repeat rewrite in_app_iff; tauto.
      -- apply 𝐒𝐏_inf_variables in h2.
         unfold 𝒱 in *;simpl;
           rewrite h2,h1,<-𝐒𝐏_variables_𝐒𝐏'_variables.
         intro ;repeat rewrite in_app_iff; tauto.
    - split.
      -- now rewrite I1,I2.
      -- unfold 𝒱 in *;simpl;rewrite h2,h1;reflexivity.
    - rewrite h2,I1;intros (a&b);repeat rewrite in_app_iff;
      repeat rewrite (𝐒𝐏_variables_𝐒𝐏'_variables_iff _ a b).
      intros [I|[I|[I|I]]];try apply h1 in I;
      try tauto;destruct b;auto;apply balanced_spec in P1;apply P1 in I;auto.
    - apply 𝐒𝐏_inf_variables in h2.
      rewrite h1,I2;intros (a&b);repeat rewrite in_app_iff;
      repeat rewrite (𝐒𝐏_variables_𝐒𝐏'_variables_iff _ a b).
      intros [I|[I|[I|I]]];try apply h2,in_app_iff in I as [I|I];
      try tauto;destruct b;auto;apply balanced_spec in P2;apply P2 in I;auto.
    - rewrite h1,I2;intros (a&b);repeat rewrite in_app_iff;
      repeat rewrite (𝐒𝐏_variables_𝐒𝐏'_variables_iff _ a b).
      intros [I|[I|[I|I]]];try apply h2 in I;
      try tauto;destruct b;auto;apply balanced_spec in P2;apply P2 in I;auto.
    - now rewrite h1,h2.
  Qed.
  Global Instance 𝐖'_seq_𝐖'_inf :
    Proper (smaller ==> smaller ==> smaller) 𝐖'_seq.
  Proof.
     intros (([u1|],A1),P1) (([v1|],B1),Q1) h1 (([u2|],A2),P2)
           (([v2|],B2),Q2) h2;
    unfold smaller,𝐖'_inf,smaller,𝐖_inf in *;
    simpl in *;try destruct h1 as (I1&h1);try destruct h2 as (I2&h2);
    try tauto;try (split;[now (rewrite I1 || rewrite h1);
                           (rewrite I2 || rewrite h2)|]);auto.
    - apply 𝐒𝐏_inf_seq;eapply incl_𝐒𝐏_inf;eauto.
      -- now apply incl_appl.
      -- now apply incl_appr.
    - etransitivity;[|apply 𝐒𝐏_inf_axinf,𝐒𝐏_axinf_1_seqr].
      -- eapply incl_𝐒𝐏_inf;eauto.
         now apply incl_appl.
      -- apply incl_appr;auto.
    - eapply incl_𝐒𝐏_inf;eauto.
      now apply incl_appl.
    - etransitivity;[|apply 𝐒𝐏_inf_axinf,𝐒𝐏_axinf_1_seql].
      -- eapply incl_𝐒𝐏_inf;eauto.
         now apply incl_appr.
      -- apply incl_appl;auto.
    - unfold 𝒱;simpl;rewrite h1,h2;reflexivity.
    - apply incl_appl;auto.
    - eapply incl_𝐒𝐏_inf;eauto.
      now apply incl_appr.
    - apply incl_appr;auto.
    - now rewrite h1,h2.
  Qed.
  
  Global Instance 𝐖'_par_𝐖'_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐖'_par.
  Proof.
    intros u1 u2 h1 v1 v2 h2.
    apply 𝐖'_inf_PartialOrder in h1 as (h11&h12).
    apply 𝐖'_inf_PartialOrder in h2 as (h21&h22).
    apply 𝐖'_inf_PartialOrder;split;unfold Basics.flip in *;
    apply 𝐖'_par_𝐖'_inf;auto.
  Qed.
  Global Instance 𝐖'_seq_𝐖'_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐖'_seq.
  Proof.
    intros u1 u2 h1 v1 v2 h2.
    apply 𝐖'_inf_PartialOrder in h1 as (h11&h12).
    apply 𝐖'_inf_PartialOrder in h2 as (h21&h22).
    apply 𝐖'_inf_PartialOrder;split;unfold Basics.flip in *;
    apply 𝐖'_seq_𝐖'_inf;auto.
  Qed.

  Lemma 𝐖'_par_comm e f : e ⊕' f ≡ f ⊕' e.
  Proof.
    destruct e as(([u|],A),P);destruct f as (([v|],B),Q);
    split;unfold 𝐖_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
    
  Lemma 𝐖'_par_idem e : e ⊕' e ≡ e.
  Proof.
    destruct e as(([u|],A),P);split;
    unfold 𝐖'_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.

  Lemma 𝐖'_seq_assoc e f g : e⊗'(f ⊗' g) ≡ (e⊗'f)⊗'g.
  Proof.
    destruct e as (([u|],A),?);destruct f as (([v|],B),?);
    destruct g as (([w|],C),?);
    split;unfold 𝐖_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
    
  Lemma 𝐖'_par_assoc e f g : e⊕'(f ⊕' g) ≡ (e⊕'f)⊕'g.
  Proof.
    destruct e as (([u|],A),?);destruct f as (([v|],B),?);
    destruct g as (([w|],C),?);split;
    unfold 𝐖_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖'_seq_one_l e : 𝟭' ⊗' e ≡ e.
  Proof.
    destruct e as(([u|],A),?);split;
    unfold 𝐖_one,𝐖_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖'_seq_one_r e : e ⊗' 𝟭' ≡ e.
  Proof.
    destruct e as(([u|],A),?);split;
    unfold 𝐖_one,𝐖_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; simpl;tauto);auto.
  Qed.

  Lemma 𝐖'_par_inf e f : e ⊕' f ≤ e.
  Proof.
    destruct e as(([u|],A),?);destruct f as (([v|],B),?);
    unfold 𝐖'_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
    rewrite 𝐒𝐏_variables_𝐒𝐏'_variables.
    intro ;repeat rewrite in_app_iff; tauto.
  Qed.
  Lemma 𝐖'_par_one_l e : 𝟭' ⊕' e ≤ (𝟭' ⊕' e)⊗'e.
  Proof.
    destruct e as(([u|],A),?);unfold 𝐖'_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff;simpl; tauto);auto.
    rewrite 𝐒𝐏_variables_𝐒𝐏'_variables.
    intro ;repeat rewrite in_app_iff; tauto.
  Qed.
  Lemma 𝐖'_par_one_r e : 𝟭' ⊕' e ≤ e⊗'(𝟭' ⊕' e).
  Proof.
    destruct e as(([u|],A),?);unfold 𝐖'_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff;simpl; tauto);auto.
    rewrite 𝐒𝐏_variables_𝐒𝐏'_variables.
    intro ;repeat rewrite in_app_iff; tauto.
  Qed.

  Lemma 𝐖'_par_one_seq_par e f : 𝟭' ⊕' (e⊗'f) ≡ 𝟭' ⊕' (e ⊕' f).
  Proof.
    destruct e as(([u|],A),?);destruct f as (([v|],B),?);
    split;unfold 𝐖_one,𝐖_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖'_par_one_seq_par_one e f: (𝟭' ⊕' e)⊗'(𝟭'⊕'f) ≡ 𝟭'⊕'(e⊕'f).
  Proof.
    destruct e as(([u|],A),?);destruct f as (([v|],B),?);split;
    unfold 𝐖_one,𝐖_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖'_par_one_seq e f : (𝟭' ⊕' e)⊗'f ≡ f⊗'(𝟭'⊕'e).
  Proof.
    destruct e as(([u|],A),?);destruct f as (([v|],B),?);split;
    unfold 𝐖_one,𝐖_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.
  Lemma 𝐖'_par_one_par e f g: ((𝟭' ⊕' e)⊗'f)⊕'g ≡ (𝟭'⊕'e)⊗'(f⊕'g).
  Proof.
    destruct e as(([u|],A),?);destruct f as (([v|],B),?);
    destruct g as (([w|],C),?);split;
    unfold 𝐖_one,𝐖_inf;simpl;try split;
    try (intro ;repeat rewrite in_app_iff; tauto);auto.
  Qed.

  Lemma 𝐖'_size_𝐖'_par e f : |e ⊕' f| <= S (|e| + |f|).
  Proof.
    destruct e as (([u|],A),?);destruct f as (([v|],B),?);
    unfold 𝐖'_par;rsimpl.
    - setoid_rewrite 𝐖_size_Some;rewrite 𝐒𝐏_size_par,app_length.
      lia.
    - setoid_rewrite 𝐖_size_Some;setoid_rewrite 𝐖_size_None.
      repeat rewrite app_length;
        pose proof (𝐒𝐏_size_𝐒𝐏'_variables u);lia. 
    - setoid_rewrite 𝐖_size_Some;setoid_rewrite 𝐖_size_None.
      repeat rewrite app_length;
        pose proof (𝐒𝐏_size_𝐒𝐏'_variables v);lia.
    - setoid_rewrite 𝐖_size_None;rewrite app_length; lia.
  Qed.
  Lemma 𝐖'_size_𝐖'_seq e f : |e ⊗' f| <= S (|e| + |f|).
  Proof.
    destruct e as (([u|],A),?);destruct f as (([v|],B),?);
    unfold 𝐖'_seq,size,𝐖'_size,size,𝐖_size,size ;simpl;auto;
    repeat rewrite app_length;try lia.
  Qed.

  Lemma sub_identity_par_one (A : list (@X' X)) (p : balanced A = true) :
    (exist _ (None,A) p) ≡ 𝟭' ⊕' (exist _ (None,A) p).
  Proof.
    unfold 𝐖'_par, 𝐖'_one,equiv,𝐖'_equiv,proj1_sig;reflexivity.
  Qed.

End primed.

Arguments 𝐖'_one { X decX }.
Infix " ⊕' " := 𝐖'_par (at level 50).
Infix " ⊗' " := 𝐖'_seq (at level 40).
Notation "𝟭'" := 𝐖'_one.

Section language.
  (** * Language interpretation *)
  Open Scope lang.
  Context { X : Set }.

  (** The function [T σ t] is the full language if the assignment [σ]
  is test-compatible with the set of tests of the weak term [t], or
  the empty language otherwise.*)
  Definition T {Σ} (σ : 𝕬[X→Σ]) (t : 𝐖 X) : language Σ := fun _ => snd t ⊢ σ.

  (** The interpretation of the weak term [(s,A)] with respect to the
  assignment [σ] is either the [σ]-interpretation of [s] (as a series
  parallel term) if [σ] is test-compatible with [A], or [0]
  otherwise. *)
  Global Instance to_lang_𝐖 {Σ} : semantics 𝐖 language X Σ :=
    fun σ t => T σ t ∩ (match fst t with
                     | None => 1
                     | Some u => ⟦u⟧σ
                     end).

  (* begin hide *)
  Global Instance semSmaller_𝐖 : SemSmaller (𝐖 X) :=
    (@semantic_containment _ _ _ _ _).
  Global Instance semEquiv_𝐖 : SemEquiv (𝐖 X) :=
    (@semantic_equality _ _ _ _ _).
  Hint Unfold semSmaller_𝐖 semEquiv_𝐖 : semantics. 
  Global Instance 𝐖_sem_equiv :
    Equivalence (fun e f : 𝐖 X => e ≃ f).
  Proof. once (typeclasses eauto). Qed.
  Global Instance 𝐖_sem_PreOrder :
    PreOrder (fun e f : 𝐖 X => e ≲ f).
  Proof. once (typeclasses eauto). Qed.
  Global Instance 𝐖_sem_PartialOrder :
    PartialOrder (fun e f : 𝐖 X => e ≃ f)
                 (fun e f : 𝐖 X => e ≲ f).
  Proof.
    eapply semantic_containment_PartialOrder;once (typeclasses eauto).
  Qed.
  (* end hide *)

  (** The interpretation of a sequential product is the concatenation
  of the interpretations. *)
  Lemma 𝐖_prod_semantics Σ u v (σ : 𝕬[X→Σ]) : (⟦u⊗v⟧σ) ≃ (⟦u⟧σ) ⋅ (⟦v⟧σ).
  Proof.
    destruct u as (u,A);destruct v as (v,B).
    assert (E: T σ ((u, A) ⊗ (v, B)) ≃ T σ (u,A) ∩ T σ (v,B))
      by (unfold 𝐖_seq,interprete,to_lang_𝐖,T;simpl;
          destruct u;destruct v;simpl;unfold intersection;intro;
          apply test_compatible_app).
    unfold 𝐖_seq in *;unfold interprete,to_lang_𝐖.
    rewrite E;destruct u;destruct v;simpl.
    - rewrite 𝐒𝐏_prod;intro w;unfold prod,intersection;firstorder.
    - intro w;unfold prod,intersection,unit,T;split.
      + intros ((h1&h2)&h3);exists w;exists [];rewrite app_nil_r;tauto.
      + intros (u&v&(h1&h2)&(h3&->)&h4).
        rewrite h4,app_nil_r in *;tauto.
    - intro w;unfold prod,intersection,unit,T;split.
      + intros ((h1&h2)&h3);exists [];exists w;simpl;tauto.
      + intros (u&v&(h1&->)&(h2&h3)&h4).
        rewrite h4 in *;tauto.
    - intro w;unfold prod,intersection,unit,T;split.
      + intros ((h1&h2)&->);exists [];exists [];tauto.
      + intros (u&v&(h1&->)&(h2&->)&->);tauto.
  Qed.
  
  (** The interpretation of a parallel product is the intersection
  of the interpretations. *)
  Lemma 𝐖_intersection_semantics Σ u v (σ : 𝕬[X→Σ]) : ⟦u⊕v⟧σ ≃ (⟦u⟧σ) ∩ (⟦v⟧σ).
  Proof.
    destruct u as (u,A);destruct v as (v,B).
    unfold 𝐖_par in *;unfold interprete,to_lang_𝐖.
    destruct u;destruct v;simpl;rsimpl;
      unfold intersection,unit,T;simpl;intro w;split;
        repeat rewrite test_compatible_app;
        try rewrite test_compatible_nil;try tauto.
    - intros (?&->);tauto.
    - intros (?&?&->);tauto.
    - intros (?&->);tauto.
    - intros ((?&->)&?);tauto.
  Qed.

  (** Both products are monotone. *)
  Global Instance sem_incl_𝐖_seq :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐖_seq X).
  Proof.
    intros e f E g h F Σ σ w;simpl.
    intro;apply 𝐖_prod_semantics.
    apply 𝐖_prod_semantics in H.
    destruct H as (u&v&hu&hv&->);exists u;exists v;split;[|split;auto].
    - now apply E.
    - now apply F.
  Qed.
  
  Global Instance sem_incl_𝐖_inter :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐖_par X).
  Proof.
    intros e f E g h F Σ σ w;simpl.
    intro;apply 𝐖_intersection_semantics.
    apply 𝐖_intersection_semantics in H.
    destruct H as (he&hf);split.
    - now apply E.
    - now apply F.
  Qed.

  Global Instance sem_eq_𝐖_seq :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐖_seq X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.

  Global Instance sem_eq_𝐖_inter :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐖_par X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.

  Section Ξ.
    Variable Σ : Set.

    (** [Ξ] transforms an assignment from [X] to [Σ] into an
    assignment from the duplicated alphabet to the same [Σ]. *)
    Definition Ξ (σ : 𝕬[X→Σ]) : (𝕬[X'→Σ]) :=
      fun x =>
        match x with
        | (a,true) => σ a
        | (a,false) => (σ a)¯
        end.

    (** [Ξ] preserves test-compatibility. *)
    Lemma test_compatible_Ξ (σ : 𝕬[X→Σ]) A :
      map fst A ⊢ σ <-> A ⊢ Ξ σ.
    Proof.
      unfold test_compatible;setoid_rewrite in_map_iff.
      split;intro hyp;[intros (a,[|]) I|intros x ((a,[|])&<-&I)];simpl in *;
        unfold mirror in *;simpl in *;try apply hyp || apply hyp in I;
          try (eexists;split;eauto);auto.
    Qed.

  End Ξ.

  Close Scope lang.
End language.

(* begin hide *)
Hint Unfold semSmaller_𝐖 semEquiv_𝐖 : semantics. 
(* end hide *)