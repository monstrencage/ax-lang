(** Using the translation of terms into primed weak terms we can
reduce the axiomatic containment of [𝐓]-terms to the ordering of
graphs. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Import terms_w_terms w_terms_graphs.

Definition 𝕲t {X : Set} `{decidable_set X} (u : 𝐓 X) := 𝕲w π{𝐓_to_𝐖' u}.

Theorem 𝐓_inf_is_weak_graph_inf {X : Set} `{decidable_set X}:
  forall u v : 𝐓 X, u ≤ v <-> 𝕲t u ≲ 𝕲t v.
Proof.
  intros u v;rewrite 𝐓_to_𝐖'_inf_iff;auto.
  unfold 𝐖'_inf,𝕲t;simpl.
  case_eq (𝐓_to_𝐖' u);intros x tx Ex;simpl.
  case_eq (𝐓_to_𝐖' v);intros y ty Ey;simpl.
  rewrite Ex,Ey;simpl.
  apply 𝐖_inf_is_weak_graph_inf;auto using dec_X'.
Qed.

