(** In this module we define and study an algebra of terms with
sequential composition and its unit, intersection, and a mirror
operation restricted to variables. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export tools language.
Delimit Scope term_scope with term.

Open Scope term.

Section def.
  (** * Definitions *)
  
  Variable X : Set.
  Variable Λ : decidable_set X.

  (** In this module, terms are built out of five constructs: the unit
  [1]; variables and mirrored variables; and two binary products, the
  sequential product [⋅] and the meet [∩]. *)
  Inductive 𝐓 : Set :=
  | 𝐓_one : 𝐓
  | 𝐓_var : X -> 𝐓
  | 𝐓_cvar : X -> 𝐓
  | 𝐓_seq : 𝐓 -> 𝐓 -> 𝐓
  | 𝐓_inter : 𝐓 -> 𝐓 -> 𝐓.

  Notation " 1 " := 𝐓_one : term_scope.
  Infix " ⋅ " := 𝐓_seq (at level 40) : term_scope.
  Infix " ∩ " := 𝐓_inter (at level 45) : term_scope.

  (** The axioms are organised as follows: most axioms are identity
  axioms, in the group [𝐓_ax]. These will be used to define both the
  equivalence and order relations. Then there is the axiom in
  [𝐓_axinf], only used for the order relation, and the axiom in
  [𝐓_axeq], to define the equivalence relation. *)
  Inductive 𝐓_ax : 𝐓 -> 𝐓 -> Prop :=
  (** [⟨𝐓,⋅,1⟩] is a monoid. *)
  | 𝐓_ax_seq_assoc e f g : 𝐓_ax (e⋅(f ⋅ g)) ((e⋅f)⋅g)
  | 𝐓_ax_seq_one_left e : 𝐓_ax (1⋅e) e
  | 𝐓_ax_seq_one_right e : 𝐓_ax (e⋅1) e
  (** [∩] is associative, commutative and idempotent. *)
  | 𝐓_ax_inter_assoc e f g : 𝐓_ax (e∩(f ∩ g)) ((e∩f)∩g)
  | 𝐓_ax_inter_comm e f : 𝐓_ax (e∩f) (f∩e)
  | 𝐓_ax_inter_idem e : 𝐓_ax (e ∩ e) e
  (** [1∩ _ ] does not distinguish the two products. *)
  | 𝐓_ax_inter_one_seq e f : 𝐓_ax (1 ∩ (e⋅f)) (1 ∩ (e ∩ f))
  (** [1∩ _ ] does not distinguish the two types of variables. *)
  | 𝐓_ax_inter_one_conv a : 𝐓_ax (1 ∩ 𝐓_cvar a) (1 ∩ 𝐓_var a)
  (** Sub-units commute with other terms. *)
  | 𝐓_ax_inter_one_comm_seq e f : 𝐓_ax ((1 ∩ e)⋅f) (f⋅(1∩e))
  (** Together with the previous axiom, this allows one to group all
  the sub-units together in a term.*)
  | 𝐓_ax_inter_one_inter e f g : 𝐓_ax (((1 ∩ e)⋅f)∩g) ((1∩e)⋅(f∩g)).

  (** The intersection is smaller than its arguments. *)
  Inductive 𝐓_axinf : 𝐓 -> 𝐓 -> Prop :=
  | 𝐓_inter_inf e f : 𝐓_axinf (e ∩ f) e.

  (** This equality law is equivalent to saying that the sequential
  product is monotone with respect to the order relation (rule
  [𝐓_inf_seq] below). *)
  Inductive 𝐓_axeq : 𝐓 -> 𝐓 -> Prop :=
  | 𝐓_ax_exchange a b c d : 𝐓_axeq (((a∩b)⋅(c∩d))∩(a⋅c)) ((a∩b)⋅(c∩d)).

  (** Order relation on terms. *)
  Inductive 𝐓_inf : Smaller 𝐓 :=
  | 𝐓_inf_refl e : e ≤ e
  | 𝐓_inf_trans e f g : e ≤ f -> f ≤ g -> e ≤ g
  | 𝐓_inf_axinf e f : 𝐓_axinf e f -> e ≤ f
  | 𝐓_inf_ax e f : 𝐓_ax e f \/ 𝐓_ax f e -> e ≤ f
  | 𝐓_inf_seq e f g h : e ≤ g -> f ≤ h -> (e ⋅ f) ≤ (g ⋅ h)
  | 𝐓_inf_inter e f g h : e ≤ g -> f ≤ h -> (e ∩ f) ≤ (g ∩ h).
  Global Instance 𝐓_Smaller : Smaller 𝐓 := 𝐓_inf.

  (** Equivalence relation on terms. *)
  Global Inductive 𝐓_eq : Equiv 𝐓 :=
  | 𝐓_eq_refl e : e ≡ e
  | 𝐓_eq_trans f e g : e ≡ f -> f ≡ g -> e ≡ g
  | 𝐓_eq_sym e f : e ≡ f -> f ≡ e
  | 𝐓_eq_seq e f g h : e ≡ g -> f ≡ h -> e ⋅ f ≡ g ⋅ h
  | 𝐓_eq_inter e f g h : e ≡ g -> f ≡ h -> e ∩ f ≡ g ∩ h
  | 𝐓_eq_ax e f : 𝐓_ax e f \/ 𝐓_axeq e f -> e ≡ f.
  
  Global Instance 𝐓_Equiv : Equiv 𝐓 := 𝐓_eq.
  (*begin hide*)
  Hint Constructors 𝐓_eq 𝐓_ax 𝐓_inf 𝐓_axinf 𝐓_axeq.

  Lemma 𝐓_seq_assoc e f g : e⋅(f ⋅ g) ≡ (e⋅f)⋅g.
  Proof. auto. Qed.
  Lemma 𝐓_seq_one_left e : 1⋅e ≡ e.
  Proof. auto. Qed.
  Lemma 𝐓_seq_one_right e : e⋅1 ≡ e.
  Proof. auto. Qed.
  Lemma 𝐓_inter_assoc e f g : e ∩ (f ∩ g) ≡  (e ∩ f) ∩ g.
  Proof. auto. Qed.
  Lemma 𝐓_inter_comm e f : e ∩ f ≡ f∩e.
  Proof. auto. Qed.
  Lemma 𝐓_inter_idem e : e ∩ e ≡ e.
  Proof. auto. Qed.
  Lemma 𝐓_inter_one_seq e f : 1 ∩ (e⋅f) ≡ 1 ∩ (e ∩ f).
  Proof. auto. Qed.
  Lemma 𝐓_inter_one_conv a : 1 ∩ 𝐓_cvar a ≡ 1 ∩ 𝐓_var a.
  Proof. auto. Qed.
  Lemma 𝐓_inter_one_comm_seq e f : (1 ∩ e)⋅f ≡ f⋅(1∩e).
  Proof. auto. Qed.
  Lemma 𝐓_inter_one_inter e f g : ((1 ∩ e)⋅f)∩g ≡ (1∩e)⋅(f∩g).
  Proof. auto. Qed.
  Lemma 𝐓_exchange a b c d : ((a∩b)⋅(c∩d))∩(a⋅c) ≡ (a∩b)⋅(c∩d).
  Proof. auto. Qed.
  (* end hide *)

  (** * General properties *)
  
  (** The axiomatic equivalence of terms is an equivalence relation. *)
  Global Instance 𝐓_eq_Equivalence : Equivalence equiv.
  Proof.
    split.
    - intro e;apply 𝐓_eq_refl.
    - now intros e f h;apply 𝐓_eq_sym.
    - now intros e f g h h';apply (𝐓_eq_trans h h').
  Qed.
  
  (** The axiomatic containment of terms is a preorder relation. *)
  Global Instance 𝐓_inf_PreOrder : PreOrder smaller.
  Proof.
    split.
    - now intro e;eauto.
    - now intros e f g;eauto.
  Qed.

  (** Both products are monotone with respect to both relations. *)
  Global Instance 𝐓_inter_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐓_inter.
  Proof. now intros e f hef g h hgh;apply 𝐓_eq_inter. Qed.

  Global Instance 𝐓_seq_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐓_seq.
  Proof. now intros e f hef g h hgh;apply 𝐓_eq_seq. Qed.

  Global Instance 𝐓_inter_smaller :
    Proper (smaller ==> smaller ==> smaller) 𝐓_inter.
  Proof. now intros e f hef g h hgh;apply 𝐓_inf_inter. Qed.

  Global Instance 𝐓_seq_smaller :
    Proper (smaller ==> smaller ==> smaller) 𝐓_seq.
  Proof. now intros e f hef g h hgh;apply 𝐓_inf_seq. Qed.

  (** To be larger than an intersection, it suffices to be larger than
  its arguments. *)
  Lemma 𝐓_inter_elim e f g : e ≤ g -> f≤ g -> e ∩ f ≤ g.
  Proof. intros h1 h2;transitivity (g∩g);auto. Qed.

  (** The intersection is a least upper bound. *)
  Lemma 𝐓_inter_intro e f g : e ≤ f -> e ≤ g -> e ≤ f ∩ g.
  Proof. intros;transitivity (e∩e);auto. Qed.

  (** The equivalence relation is contained in the order relation.*)
  Lemma 𝐓_eq_inf u v : u ≡ v -> u ≤ v.
  Proof.
    intro L;cut (u ≤ v /\ v ≤ u);[tauto|];induction L;intuition;eauto.
    - destruct H0;auto.
    - destruct H0; apply 𝐓_inter_intro;auto.
  Qed.

  (** The order relation can be encoded inside the equivalence relation. *)
  Lemma 𝐓_inf_eq_inter u v : u ≤ v <-> u ≡ v ∩ u.
  Proof.
    split.
    - intro L;induction L;auto.
      + rewrite IHL; rewrite IHL0 at 1;auto.
      + destruct H;transitivity ((e∩e)∩f);auto.
      + destruct H.
        * transitivity (e∩e);auto.
        * transitivity (e∩e);auto.
      + rewrite IHL at 1;rewrite IHL0 at 1.
        rewrite <- 𝐓_exchange,<-IHL,<-IHL0;auto.
      + rewrite IHL at 1;rewrite IHL0 at 1.
        transitivity (g∩(e∩(h∩f)));auto.
        transitivity (g∩((e∩h)∩f));auto.
        transitivity (g∩((h∩e)∩f));auto.
        transitivity (g∩(h∩(e∩f)));auto.
    - intro h;apply 𝐓_eq_inf in h as ->;auto.
  Qed.

  (** With the last two lemma, it is very simple to check that
  axiomatic containment is a partial order with respect to axiomatic
  equivalence. *)
  Global Instance 𝐓_inf_PartialOrder : PartialOrder equiv smaller.
  Proof.
    intros e f;split.
    - intros h;split;unfold Basics.flip;apply 𝐓_eq_inf;eauto.
    - intros (E1&E2);unfold Basics.flip in E2.
      apply 𝐓_inf_eq_inter in E1;apply 𝐓_inf_eq_inter in E2.
      transitivity (e∩f);eauto.
  Qed.

  (** The following few laws are simple consequences of our axioms. *)  
  Lemma 𝐓_inter_one_abs e f : (1 ∩ e)⋅(1 ∩ f) ≡ 1 ∩ (e ∩ f).
  Proof.
    transitivity ((1∩e)∩f);auto.
    transitivity (((1 ∩ e)⋅1)∩f);auto.
  Qed.

  Lemma 𝐓_inter_onel e : 1 ∩ e ≤ (1 ∩ e)⋅e.
  Proof.
    transitivity (1∩(e∩e));auto.
    rewrite <- 𝐓_inter_one_abs.
    apply 𝐓_inf_seq;auto.
    transitivity (e∩1);auto.
  Qed.
    
  Lemma 𝐓_inter_oner e : 1 ∩ e ≤ e ⋅ (1 ∩ e).
  Proof.
    transitivity (1∩(e∩e));auto.
    rewrite <- 𝐓_inter_one_abs.
    apply 𝐓_inf_seq;auto.
    transitivity (e∩1);auto.
  Qed.    

  Lemma 𝐓_inf_one_prod_is_inter u v : u ≤ 1 -> v ≤ 1 -> u ⋅ v ≡ u ∩ v.
  Proof.
    intros.
    apply 𝐓_inf_eq_inter in H as ->.
    apply 𝐓_inf_eq_inter in H0 as ->.
    rewrite 𝐓_inter_one_abs.
    transitivity (1 ∩ 1 ∩ (u∩ v));auto.
    transitivity (1 ∩ (1 ∩ (u∩ v)));auto.
    transitivity (1 ∩ ((1 ∩ u)∩ v));auto.
    transitivity (1 ∩ ((u∩1)∩ v));auto.
    transitivity (1 ∩ (u∩(1∩ v)));auto.
  Qed.

End def.
(* begin hide *)
Arguments 𝐓_one {X}.
Notation " 1 " := 𝐓_one : term_scope.
Infix " ⋅ " := 𝐓_seq (at level 40) : term_scope.
Infix " ∩ " := 𝐓_inter (at level 45) : term_scope.
Hint Constructors 𝐓_eq 𝐓_ax 𝐓_inf 𝐓_axinf.
(* end hide *)
Section s.
  (** * Variables and size *)
  Context{ X : Set }.
  Context{ Λ : decidable_set X }.
  
  (** As usual, we define the size of a term as the number of nodes in
  its syntax tree. *)
  Global Instance 𝐓_size : Size (𝐓 X) :=
    fix 𝐓_size t :=
      match t with
      | 1 | 𝐓_cvar _ | 𝐓_var _ => S 0
      | s ⋅ t | s ∩ t => S (𝐓_size s + 𝐓_size t)
      end.
  (* begin hide *)
  Lemma 𝐓_size_1 : |1| = S 0. trivial. Qed.
  Lemma 𝐓_size_cvar a : |𝐓_cvar a| = S 0. trivial. Qed.
  Lemma 𝐓_size_var a : |𝐓_var a| = S 0. trivial. Qed.
  Lemma 𝐓_size_seq e f : |e⋅f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐓_size_inter e f : |e∩f| = S(|e|+|f|). trivial. Qed.
  Hint Rewrite 𝐓_size_1 𝐓_size_cvar 𝐓_size_var 𝐓_size_seq 𝐓_size_inter
       : simpl_typeclasses.
  (* end hide *)

  (** [ ⟨ [a_1 ;...; a_n] ⟩ ] is the term [(a_1 ∩ a_1') ∩ ( ... ∩ ((a_n ∩ a_n') ∩ 1))],
      where [a_i'] represents the mirrored version of a variable. *)
  Definition to_test :=
    fold_right (fun a (t : 𝐓 X) => 𝐓_var a ∩ 𝐓_cvar a ∩ t) 1.
  Notation " ⟨ l ⟩ " := (to_test l) : term_scope.

  (** The variables of a term are the variables appearing in it either
  plainly or in mirrored version. *)
  Global Instance suppT : Alphabet 𝐓 X :=
    fix 𝐓_variables (s : 𝐓 X) :=
      match s with
      | 1 => []
      | 𝐓_var a => [a]
      | 𝐓_cvar a => [a]
      | s ⋅ t | s ∩ t => 𝐓_variables s ++ 𝐓_variables t
      end.
  
  (* begin hide *)
  Lemma 𝐓_var_1 : 𝒱 1 = []. trivial. Qed. 
  Lemma 𝐓_var_var a : 𝒱 (𝐓_var a) = [a]. trivial. Qed.
  Lemma 𝐓_var_cvar a : 𝒱 (𝐓_cvar a) = [a]. trivial. Qed.
  Lemma 𝐓_var_seq u v : 𝒱 (u⋅v) = 𝒱 u ++𝒱 v . trivial. Qed.
  Lemma 𝐓_var_inter u v : 𝒱 (u∩v) = 𝒱 u ++𝒱 v . trivial. Qed.
  Hint Rewrite 𝐓_var_1 𝐓_var_var 𝐓_var_cvar 𝐓_var_seq 𝐓_var_inter
    : simpl_typeclasses.
  (* end hide *)

  (** If [e] is smaller than [f], then every variable of [f] appears in [e]. *)
  Lemma 𝐓_inf_variables_incl (e f : 𝐓 X): e ≤ f -> 𝒱 f ⊆ 𝒱 e.
  Proof.
    intros E;induction E;rsimpl;auto.
    - reflexivity.
    - etransitivity;eauto.
    - destruct H;rsimpl;intro;repeat rewrite in_app_iff; tauto.
    - destruct H as [[]|[]];rsimpl;intro;repeat rewrite in_app_iff;
        simpl; tauto.
    - now rewrite IHE,IHE0.
    - now rewrite IHE,IHE0.
  Qed.

  (** * Properties of [⟨_⟩] *)
  
  (** The set of variables of the term [⟨l⟩] is equivalent to [l]
  itself. *)
  Lemma 𝐓_variables_to_test (l : list X) : 𝒱 ⟨l⟩ ≈ l.
  Proof. induction l;firstorder congruence. Qed.

  (** The size of the term [⟨l⟩] is related to the length of [l] as follows. *)
  Lemma 𝐓_size_to_test (l : list X) : |⟨l⟩| = 4 * (# l) + 1.
  Proof. induction l;rsimpl;lia. Qed.

  (** The function [⟨_⟩] only produces sub-units. *)
  Lemma 𝐓_to_test_one (l : list X) : ⟨l⟩ ≤ 1.
  Proof.
    induction l;simpl.
    - reflexivity.
    - rewrite IHl;eauto.
  Qed.

  (** The function [⟨_⟩] transforms unions into intersections. *)
  Lemma 𝐓_to_test_app (l m : list X) :
    ⟨l++m⟩ ≡ ⟨l⟩ ∩ ⟨m⟩.
  Proof.
    induction l;simpl;auto.
    - rewrite <- 𝐓_inf_one_prod_is_inter;auto.
      apply 𝐓_to_test_one.
    - rewrite IHl;auto.
  Qed.

  (** Adding to a list an element that is already there does not
  change the image by [⟨_⟩].  *)  
  Lemma 𝐓_to_test_in (a : X) l : a ∈ l -> ⟨l⟩ ≡ ⟨a::l⟩.
  Proof.
    induction l as [|b l];simpl;[tauto|].
    intros [->|I];auto.
    - transitivity
        (((𝐓_var a ∩ 𝐓_cvar a)∩(𝐓_var a ∩ 𝐓_cvar a)) ∩ ⟨l⟩);auto.
    - rewrite (IHl I) at 1;simpl;auto.
      set (x:=𝐓_var a); set (x':=𝐓_cvar a);
      set (y:=𝐓_var b); set (y':=𝐓_cvar b);set (z:= ⟨l⟩).
      transitivity ((y ∩ y') ∩ (x ∩ x') ∩ z);auto.
      transitivity ((x ∩ x') ∩ (y ∩ y') ∩ z);auto.
  Qed.

  (** [⟨_⟩] is contravariant with respect to the order. *)
  Lemma 𝐓_to_test_incl_inf (l m : list X) :
    l ⊆ m -> ⟨m⟩ ≤ ⟨l⟩.
  Proof.
    revert m;induction l;intros m I;simpl;auto.
    - apply 𝐓_to_test_one.
    - rewrite (@𝐓_to_test_in a m);[|now apply I;left].
      simpl;apply 𝐓_inf_inter;auto.
      apply IHl;intros ? ?;apply I;now right.
  Qed.

  Global Instance to_test_inf :
    Proper (Basics.flip (@incl X) ==> smaller) to_test.
  Proof. intros l m; apply 𝐓_to_test_incl_inf. Qed.

  Global Instance to_test_inf_flip :
    Proper (@incl X ==> Basics.flip smaller) to_test.
  Proof. intros l m; apply 𝐓_to_test_incl_inf. Qed.

  (** [⟨_⟩] is monotone with respect to equivalence. *)
  Global Instance to_test_equivalent :
    Proper (@equivalent X ==> equiv) to_test.
  Proof.
    intros l m E; apply 𝐓_inf_PartialOrder;
      unfold Basics.flip;split;apply 𝐓_to_test_incl_inf;auto;
    rewrite E;reflexivity.
  Qed.

  (** Applying [⟨_⟩] to the set of variables of a term is equivalent
  to intersecting it with [1]. *) 
  Lemma 𝐓_to_test_is_par_one (u : 𝐓 X) : ⟨𝒱 u⟩ ≡ 1 ∩ u.
  Proof.
    induction u;rsimpl;auto.
    - transitivity (1 ∩ (𝐓_var x ∩ 𝐓_var x));auto.
      transitivity (1 ∩ (𝐓_var x ∩ 𝐓_cvar x));auto.
      transitivity (1 ∩ (𝐓_cvar x ∩ 𝐓_var x));auto.
      transitivity (1 ∩ 𝐓_cvar x ∩ 𝐓_var x);auto.
      transitivity (1 ∩ 𝐓_var x ∩ 𝐓_var x);auto.
    - transitivity (1 ∩ (𝐓_cvar x ∩ 𝐓_cvar x));auto.
      transitivity (1 ∩ (𝐓_var x ∩ 𝐓_cvar x));auto.
      transitivity (1 ∩ 𝐓_var x ∩ 𝐓_cvar x);auto.
      transitivity (1 ∩ 𝐓_cvar x ∩ 𝐓_cvar x);auto.
    - rewrite 𝐓_to_test_app;auto.
      rewrite IHu1,IHu2.
      transitivity (1 ∩ u1 ∩ 1 ∩ u2);auto.
      transitivity (1 ∩ (1 ∩ u1) ∩ u2);auto.
      transitivity (1 ∩ 1 ∩ u1 ∩ u2);auto.
      transitivity (1 ∩ 1 ∩ (u1 ∩ u2));auto.
      transitivity (1 ∩ (u1 ∩ u2));auto.
    - rewrite 𝐓_to_test_app;auto.
      rewrite IHu1,IHu2.
      transitivity (1 ∩ u1 ∩ 1 ∩ u2);auto.
      transitivity (1 ∩ (1 ∩ u1) ∩ u2);auto.
      transitivity (1 ∩ 1 ∩ u1 ∩ u2);auto.
      transitivity (1 ∩ 1 ∩ (u1 ∩ u2));auto.
  Qed.
  
  (** We now prove some equational laws of [⟨_⟩]. *)
  Lemma 𝐓_to_test_comm A (e : 𝐓 X) : ⟨A⟩ ⋅ e ≡ e ⋅ ⟨A⟩.
  Proof.
    pose proof (𝐓_to_test_one A)as l;
      apply 𝐓_inf_eq_inter in l as->;auto.
  Qed.
  
  Lemma 𝐓_to_test_dupl (A : list X) : ⟨A⟩ ≡ ⟨A⟩ ⋅ ⟨A⟩.
  Proof.
    pose proof (𝐓_to_test_one A)as l;
      apply 𝐓_inf_eq_inter in l as->;auto.
    transitivity ((1 ∩ ⟨A⟩) ∩ (1 ∩ ⟨A⟩));auto.
    symmetry;apply 𝐓_inf_one_prod_is_inter;auto.
  Qed.
  
  Lemma 𝐓_to_test_distr1 A (e f : 𝐓 X) : ⟨A⟩ ⋅ (e∩f) ≡ (⟨A⟩ ⋅ e)∩ f.
  Proof.
    pose proof (𝐓_to_test_one A)as l;
      apply 𝐓_inf_eq_inter in l as->;auto.
  Qed.
  
  Lemma 𝐓_to_test_distr2 A (e f : 𝐓 X) : ⟨A⟩ ⋅ (e∩f) ≡ e∩(⟨A⟩ ⋅ f).
  Proof.
    transitivity (⟨ A ⟩ ⋅ (f ∩ e));auto.
    rewrite 𝐓_to_test_distr1;auto.
  Qed.

  Lemma 𝐓_to_test_distr A (e f : 𝐓 X) : ⟨A⟩ ⋅ (e∩f) ≡ (⟨A⟩ ⋅ e)∩(⟨A⟩ ⋅ f).
  Proof.
    rewrite 𝐓_to_test_dupl at 1.
    transitivity (⟨ A ⟩ ⋅ (⟨ A ⟩ ⋅ (e ∩ f)));auto.
    rewrite 𝐓_to_test_distr1,𝐓_to_test_distr2;auto.
  Qed.

  Lemma 𝐓_weak_seq A B (u : 𝐓 X) : (⟨ A ⟩ ⋅ u) ⋅ ⟨ B ⟩ ≡ (⟨ A ⟩ ∩ ⟨ B ⟩) ⋅ u.
  Proof.
    rewrite <- 𝐓_inf_one_prod_is_inter;auto using 𝐓_to_test_one.
    transitivity ((⟨ A ⟩ ⋅ u) ⋅ ⟨ B ⟩);auto.
    transitivity (⟨ A ⟩ ⋅ (u ⋅ ⟨ B ⟩));auto.
    rewrite <- (𝐓_to_test_comm _ u);auto.
  Qed.
  
  Lemma 𝐓_weak_par A B (u v : 𝐓 X) : (⟨ A ⟩ ⋅ u) ∩ (⟨ B ⟩ ⋅ v) ≡ (⟨ A ⟩ ∩ ⟨ B ⟩) ⋅ (u ∩ v).
  Proof.
    pose proof (𝐓_to_test_one A) as h.
    apply 𝐓_inf_eq_inter in h as ->.
    transitivity ((1 ∩ ⟨ A ⟩) ⋅ (u ∩ ⟨ B ⟩ ⋅ v));auto.
    transitivity ((1 ∩ ⟨ A ⟩) ⋅ (⟨ B ⟩ ⋅ v∩u));auto.
    pose proof (𝐓_to_test_one B) as h.
    apply 𝐓_inf_eq_inter in h as ->.
    transitivity ((1 ∩ ⟨ A ⟩) ⋅ ((1∩ ⟨ B ⟩) ⋅ (v∩u)));auto.
    transitivity ((1 ∩ ⟨ A ⟩) ⋅ (1∩ ⟨ B ⟩) ⋅ (v∩u));auto.
    apply 𝐓_eq_seq;auto.
    rewrite 𝐓_inter_one_abs.
    transitivity (1 ∩ ⟨ A ⟩ ∩ 1 ∩ ⟨ B ⟩);auto.
    transitivity (1 ∩ ⟨ A ⟩ ∩ ⟨ B ⟩);auto.
    apply 𝐓_eq_inter;auto.
    transitivity (1∩(1 ∩ ⟨ A ⟩));auto.
    transitivity (1∩1 ∩ ⟨ A ⟩);auto.
  Qed.

  (** * Mirror of a term *)

  (** We define the following mirror function on terms. *)
  Fixpoint 𝐓_conv (t : 𝐓 X) :=
    match t with
    | 1 => 1
    | 𝐓_var a => 𝐓_cvar a
    | 𝐓_cvar a => 𝐓_var a
    | u ∩ v => 𝐓_conv u ∩ 𝐓_conv v
    | u ⋅ v => 𝐓_conv v ⋅ 𝐓_conv u
    end.

  Notation " t ° " := (𝐓_conv t) (at level 25) : term_scope.

  (** It is idempotent. *)
  Lemma 𝐓_conv_idem t : t°° ≡ t.
  Proof. induction t;simpl;auto. Qed.

  (** It disappears under [1∩_]. *)
  Lemma 𝐓_conv_one t : 1 ∩ (t°) ≡ 1 ∩ t.
  Proof.
    induction t;simpl;auto.
    - transitivity (1∩(t1∩t2));auto.
      transitivity (1∩(t2∩t1));auto.
      rewrite <- 𝐓_inter_one_abs,<-IHt1,<-IHt2,𝐓_inter_one_abs;auto.
    - rewrite <- 𝐓_inter_one_abs,IHt1,IHt2,𝐓_inter_one_abs;auto.
  Qed.

  (** This operation is also monotone. *)
  Global Instance 𝐓_conv_equiv : Proper (equiv ==> equiv) 𝐓_conv.
  Proof.
    intros e f E;induction E;simpl;eauto.
    destruct H as [[]|[]];simpl;eauto.
    + transitivity  (1 ∩ (f0° ∩ e0°));auto.
    + transitivity ((1 ∩ e0°)⋅f0°∩ g°);auto.
      transitivity ((1 ∩ e0°)⋅(f0°∩ g°));auto.
    + apply 𝐓_eq_ax;right;apply 𝐓_ax_exchange.
  Qed.
  Global Instance 𝐓_conv_inf : Proper (smaller ==> smaller) 𝐓_conv.
  Proof.
    intros e f E.
    rewrite 𝐓_inf_eq_inter in *.
    rewrite E at 1;simpl;auto.
  Qed.

End s.
(* begin hide *)
Notation " t ° " := (𝐓_conv t) (at level 25) : term_scope.
Notation " ⟨ l ⟩ " := (to_test l) : term_scope.
Hint Rewrite @𝐓_size_1 @𝐓_size_cvar @𝐓_size_var @𝐓_size_seq
     @𝐓_size_inter : simpl_typeclasses.
Hint Rewrite @𝐓_var_1 @𝐓_var_var @𝐓_var_cvar @𝐓_var_seq @𝐓_var_inter
  : simpl_typeclasses.
(* end hide *)

Section language.
  (** * Language interpretation *)
  Context { X : Set }.
  Open Scope lang.

  (** We interpret expressions as languages in the obvious way: *)
  Global Instance to_lang_𝐓 {Σ} : semantics 𝐓 language X Σ :=
    fix to_lang_𝐓 σ e:=
      match e with
      | 1%term => 1
      | 𝐓_var a => (σ a)
      | 𝐓_cvar a => (σ a)¯
      | (e ⋅ f)%term => (to_lang_𝐓 σ e) ⋅ (to_lang_𝐓 σ f)
      | (e ∩ f)%term => (to_lang_𝐓 σ e) ∩ (to_lang_𝐓 σ f)
      end.

  (* begin hide *)
  Global Instance semSmaller_𝐓 : SemSmaller (𝐓 X) :=
    (@semantic_containment _ _ _ _ _).
  Global Instance semEquiv_𝐓 : SemEquiv (𝐓 X) :=
    (@semantic_equality _ _ _ _ _).
  Hint Unfold semSmaller_𝐓 semEquiv_𝐓 : semantics. 
  Global Instance 𝐓_sem_equiv :
    Equivalence (fun e f : 𝐓 X => e ≃ f).
  Proof. once (typeclasses eauto). Qed.
  Global Instance 𝐓_sem_PreOrder :
    PreOrder (fun e f : 𝐓 X => e ≲ f).
  Proof. once (typeclasses eauto). Qed.
  Global Instance 𝐓_sem_PartialOrder :
    PartialOrder (fun e f : 𝐓 X => e ≃ f)
                 (fun e f : 𝐓 X => e ≲ f).
  Proof.
    eapply semantic_containment_PartialOrder;once (typeclasses eauto).
  Qed.

  Section invert.
    Context { Σ : Set } { σ : 𝕬[X→Σ] }.
    Lemma 𝐓_prod e f : (⟦(e⋅f)%term⟧σ) = (⟦e⟧σ) ⋅ (⟦f⟧σ).
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐓_intersection e f : (⟦(e∩f)%term⟧σ) = (⟦e⟧σ) ∩ (⟦f⟧σ).
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐓_mirror a : (⟦𝐓_cvar a ⟧σ) = (σ a)¯.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐓_variable a : (⟦𝐓_var a⟧ σ) = σ a. 
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐓_unit : (⟦1%term⟧σ) = 1.
    Proof. unfold interprete;simpl;auto. Qed.
  End invert.
  Hint Rewrite @𝐓_unit @𝐓_variable @𝐓_intersection
       @𝐓_prod @𝐓_mirror: simpl_typeclasses.
  (* end hide *)

  (** Both products are monotone. *)
  Global Instance sem_incl_𝐓_seq :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐓_seq X).
  Proof.
    intros e f E g h F Σ σ;simpl;rsimpl;revert E F;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_incl_𝐓_inter :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐓_inter X).
  Proof.
    intros e f E g h F Σ σ;simpl;rsimpl;revert E F;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_eq_𝐓_seq :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐓_seq X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.
  Global Instance sem_eq_𝐓_inter :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐓_inter X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.
  
  Context { Σ : Set } { σ : 𝕬[X→Σ] } {A : list X}.

  (** A word [w] is contained in the [σ]-interpretation of the term
  [⟨A⟩] if and only if [w] is empty and [σ] is test-compatible with
  [A]. *)
  Lemma test_compatible_𝐓_to_test w : w ∊ (⟦⟨A⟩⟧ σ) <-> A ⊢ σ /\ w = [].
  Proof.
    unfold test_compatible;revert w;induction A;intro;simpl;rsimpl.
    -- unfold unit;intros;firstorder.
    -- repeat autounfold with semantics;intros;rewrite IHl;auto;
         split;rsimpl. 
       --- intros (h&(f&->));firstorder.
           revert H H0;rsimpl;rewrite H1 in *;auto.
       --- intros (h&->);repeat split;auto.
  Qed.

  (** If [σ] is test-compatible with [A] then the [σ]-interpretation
  of the term [⟨A⟩] is the unit language. *)
  Lemma test_compatible_𝐓_to_test_true : A ⊢ σ -> ⟦⟨A⟩⟧σ ≃ 1.
  Proof.
    intros T w;rsimpl;rewrite test_compatible_𝐓_to_test;tauto.
  Qed.

  (** Otherwise the [σ]-interpretation of the term [⟨A⟩] is the empty
  language.  *)
  Lemma test_compatible_𝐓_to_test_false : ~ A ⊢ σ -> ⟦⟨A⟩⟧σ ≃ 0.
  Proof.
    intros??;rsimpl;rewrite test_compatible_𝐓_to_test;
      autounfold with *;tauto.
  Qed.
  
  (** This is just the contrapositive of the previous statement. *)
  Lemma test_compatible_𝐓_to_test_not_empty w :
    w∊(⟦⟨A⟩⟧σ) ->  A ⊢ σ.
  Proof. intro h; rewrite test_compatible_𝐓_to_test in h;tauto. Qed.

Close Scope lang.
  
End language.

Close Scope term.

(* begin hide *)

Hint Rewrite @𝐓_unit @𝐓_variable @𝐓_intersection
     @𝐓_prod @𝐓_mirror: simpl_typeclasses.
Hint Unfold semSmaller_𝐓 semEquiv_𝐓 : semantics. 
(* end hide *)
