(** This module defines the full signature of language algebra we
consider here, and its finite complete axiomatization. We also define
here some normalisation functions, and list some of their properties. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export tools language.

Delimit Scope expr_scope with expr.
Open Scope expr_scope.

Section s.
  (** * Main definitions *)
  Variable X : Set.
  Variable dec_X : decidable_set X.

  (** [𝐄 X] is the type of expressions with variables ranging over the
  type [X]. They are built out of the constants [0] and [1], the
  concatenation (also called sequential product) [⋅], the intersection
  [∩], the union [+] and the mirror image, denoted by the postfix
  operator [°]. *)
  Inductive 𝐄 : Set :=
  | 𝐄_one : 𝐄
  | 𝐄_zero : 𝐄
  | 𝐄_var : X -> 𝐄
  | 𝐄_seq : 𝐄 -> 𝐄 -> 𝐄
  | 𝐄_inter : 𝐄 -> 𝐄 -> 𝐄
  | 𝐄_plus : 𝐄 -> 𝐄 -> 𝐄
  | 𝐄_conv : 𝐄 -> 𝐄.

  Notation "x ⋅ y" := (𝐄_seq x y) (at level 40) : expr_scope.
  Notation "x + y" := (𝐄_plus x y) (left associativity, at level 50) : expr_scope.
  Notation "x ∩ y" := (𝐄_inter x y) (at level 45) : expr_scope.
  Notation "x ¯" := (𝐄_conv x) (at level 25) : expr_scope.
  Notation " 1 " := 𝐄_one : expr_scope.
  Notation " 0 " := 𝐄_zero : expr_scope.

  (** The size of an expression is the number of nodes in its syntax
  tree. *)
  Global Instance size_𝐄 : Size 𝐄 :=
    fix 𝐄_size (e: 𝐄) : nat :=
      match e with
      | 0 | 1 | 𝐄_var _ => 1%nat
      | e + f | e ∩ f | e ⋅ f => S (𝐄_size e + 𝐄_size f)
      | e ¯ => S (𝐄_size e)
      end.
  (* begin hide *)
  Lemma 𝐄_size_one : |1| = 1%nat. trivial. Qed.
  Lemma 𝐄_size_zero : |0| = 1%nat. trivial. Qed.
  Lemma 𝐄_size_var a : |𝐄_var a| = 1%nat. trivial. Qed.
  Lemma 𝐄_size_seq e f : |e⋅f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐄_size_inter e f : |e∩f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐄_size_plus e f : |e+f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐄_size_conv e : |e ¯| = S(|e|). trivial. Qed.
  Hint Rewrite 𝐄_size_one 𝐄_size_zero 𝐄_size_var 𝐄_size_seq
       𝐄_size_inter 𝐄_size_plus 𝐄_size_conv
    : simpl_typeclasses.
  Fixpoint eqb e f :=
    match (e,f) with
    | (1,1) | (0,0) => true
    | (𝐄_var a,𝐄_var b) => eqX a b
    | (e ¯,f ¯) => eqb e f
    | (e1 + e2,f1 + f2)
    | (e1 ⋅ e2,f1 ⋅ f2)
    | (e1 ∩ e2,f1 ∩ f2) => eqb e1 f1 && eqb e2 f2
    | _ => false
    end.
  Lemma eqb_reflect e f : reflect (e = f) (eqb e f).
  Proof.
    apply iff_reflect;symmetry;split;
      [intro h;apply Is_true_eq_left in h;revert f h
      |intros <-;apply Is_true_eq_true];induction e;
        try destruct f;simpl;autorewrite with quotebool;firstorder.
    - apply Is_true_eq_true,eqX_correct in h as ->;auto.
    - erewrite IHe1;[|eauto]; erewrite IHe2;[|eauto];auto.
    - erewrite IHe1;[|eauto]; erewrite IHe2;[|eauto];auto.
    - erewrite IHe1;[|eauto]; erewrite IHe2;[|eauto];auto.
    - erewrite IHe;[|eauto];auto.
    - apply Is_true_eq_left,eqX_correct;auto.
  Qed.
  (* end hide *)

  (** If the set of variables [X] is decidable, then so is the set of
  expressions. _Note that we are here considering syntactic equality,
  as no semantic or axiomatic equivalence relation has been defined
  for expressions_. *)
  Global Instance 𝐄_decidable_set : decidable_set 𝐄.
  Proof. exact (Build_decidable_set eqb_reflect). Qed.

  (** The following are the axioms of the algebra of languages over
  this signature.*)
  Inductive ax : 𝐄 -> 𝐄 -> Prop :=
  (** [⟨𝐄,⋅,1⟩] is a monoid. *)
  | ax_seq_assoc e f g : ax (e⋅(f ⋅ g)) ((e⋅f)⋅g)
  | ax_seq_1 e : ax (1⋅e) e
  | ax_1_seq e : ax (e⋅1) e
  (** [⟨𝐄,+,0⟩] is a commutative idempotent monoid. *)
  | ax_plus_com e f : ax (e+f) (f+e)
  | ax_plus_idem e : ax (e+e) e
  | ax_plus_ass e f g : ax (e+(f+g)) ((e+f)+g)
  | ax_plus_0 e : ax (e+0) e
  (** [⟨𝐄,⋅,+,1,0⟩] is an idempotent semiring. *)
  | ax_seq_0 e : ax (e⋅0) 0
  | ax_0_seq e : ax (0⋅e) 0
  | ax_plus_seq e f g: ax ((e + f)⋅g) (e⋅g + f⋅g)
  | ax_seq_plus e f g: ax (e⋅(f + g)) (e⋅f + e⋅g)
  (** [⟨𝐄,∩⟩] is a commutative and idempotent semigroup. *)
  | ax_inter_assoc e f g : ax (e∩(f ∩ g)) ((e∩f)∩g)
  | ax_inter_comm e f : ax (e∩f) (f∩e)
  | ax_inter_idem e : ax (e ∩ e) e
  (** [⟨𝐄,+,∩⟩] forms a distributive lattice, and [0] is absorbing for
  [∩]. *)
  | ax_plus_inter e f g: ax ((e + f)∩g) (e∩g + f∩g)
  | ax_inter_plus e f : ax ((e∩f)+e) e
  | ax_inter_0 e : ax (e∩0) 0
  (** [ ¯] is an involution that flips concatenations and commutes with
  every other operation. *)
  | ax_conv_conv e : ax (e ¯¯) e
  | ax_conv_1 : ax (1 ¯) 1
  | ax_conv_0 : ax (0 ¯) 0
  | ax_conv_plus e f: ax ((e + f) ¯) (e ¯ + f ¯)
  | ax_conv_seq e f: ax ((e ⋅ f) ¯) (f ¯ ⋅ e ¯)
  | ax_conv_inter e f: ax ((e∩f) ¯) (e ¯ ∩ f ¯)
  (** The four laws that follow are the most interesting ones. They
  concern _subunits_, terms that are smaller than [1]. With our
  signature, any term can be projected to a subunit using the
  operation [1 ∩ _ ]. *)
  (** - For subunits, intersection and concatenation coincide. *)
  | ax_inter_1_seq e f : ax (1 ∩ (e⋅f)) (1 ∩ (e ∩ f))
  (** - Mirror image is the identity on subunits. *)
  | ax_inter_1_conv e : ax (1 ∩ (e ¯)) (1 ∩ e)
  (** - Subunits commute with every term. *)
  | ax_inter_1_comm_seq e f : ax ((1 ∩ e)⋅f) (f⋅(1 ∩ e))
  (** - This last law is less intuitive, but with the previous ones,
  it allows one to extract from any union-free expressions a single
  global subunit.*)
  | ax_inter_1_inter e f g : ax (((1 ∩ e)⋅f) ∩ g) ((1 ∩ e)⋅(f ∩ g)).

  
  (** We use these axioms to generate an axiomatic equivalence
  relation and an axiomatic order relations. *)
  Inductive 𝐄_eq : Equiv 𝐄 :=
  | eq_refl e : e ≡ e
  | eq_trans f e g : e ≡ f -> f ≡ g -> e ≡ g
  | eq_sym e f : e ≡ f -> f ≡ e
  | eq_plus e f g h : e ≡ g -> f ≡ h -> (e + f) ≡ (g + h)
  | eq_seq e f g h : e ≡ g -> f ≡ h -> (e ⋅ f) ≡ (g ⋅ h)
  | eq_inter e f g h : e ≡ g -> f ≡ h -> (e ∩ f) ≡ (g ∩ h)
  | eq_conv e f : e ≡ f -> (e ¯) ≡ (f ¯)
  | eq_ax e f : ax e f -> e ≡ f.
  Global Instance 𝐄_Equiv : Equiv 𝐄 := 𝐄_eq.

  (** For the order to match the equivalence relation, we need to add
  this axiom, stating that [0] is a minimal element. *)
  Inductive 𝐄_inf : Smaller 𝐄 :=
  | inf_refl e : e ≤ e
  | inf_trans f e g : e ≤ f -> f ≤ g -> e ≤ g
  | inf_plus e f g h : e ≤ g -> f ≤ h -> (e + f) ≤ (g + h)
  | inf_seq e f g h : e ≤ g -> f ≤ h -> (e ⋅ f) ≤ (g ⋅ h)
  | inf_inter e f g h : e ≤ g -> f ≤ h -> (e ∩ f) ≤ (g ∩ h)
  | inf_conv e f : e ≤ f -> (e ¯) ≤ (f ¯)
  | inf_ax e f : ax e f \/ ax f e -> e ≤ f
  | inf_zero e : 0 ≤ e.
  Global Instance 𝐄_Smaller : Smaller 𝐄 := 𝐄_inf.

  Hint Constructors 𝐄_eq ax 𝐄_inf.

  (** * Some elementary properties of this algebra *)

  (** It is immediate to check that the equivalence we defined is
  indeed an equivalence relation, that the order relation is a
  preorder, and that every operator is monotone for both relations. *)
  Global Instance equiv_Equivalence : Equivalence equiv.
  Proof. split;intro;eauto. Qed.

  Global Instance smaller_PreOrder : PreOrder smaller.
  Proof. split;intro;eauto. Qed.

  Global Instance inter_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐄_inter.
  Proof. now intros e f hef g h hgh;apply eq_inter. Qed.

  Global Instance plus_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐄_plus.
  Proof. now intros e f hef g h hgh;apply eq_plus. Qed.

  Global Instance seq_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐄_seq.
  Proof. now intros e f hef g h hgh;apply eq_seq. Qed.
  
  Global Instance conv_equiv :
    Proper (equiv ==> equiv) 𝐄_conv.
  Proof. now intros e f hef;apply eq_conv. Qed.
  
  Global Instance inter_smaller :
    Proper (smaller ==> smaller ==> smaller) 𝐄_inter.
  Proof. now intros e f hef g h hgh;apply inf_inter. Qed.

  Global Instance plus_smaller :
    Proper (smaller ==> smaller ==> smaller) 𝐄_plus.
  Proof. now intros e f hef g h hgh;apply inf_plus. Qed.

  Global Instance seq_smaller :
    Proper (smaller ==> smaller ==> smaller) 𝐄_seq.
  Proof. now intros e f hef g h hgh;apply inf_seq. Qed.

  Global Instance conv_smaller :
    Proper (smaller ==> smaller) 𝐄_conv.
  Proof. now intros e f hef;apply inf_conv. Qed.
  
  (** From the axioms, we can infer the following simple laws. *)
  Lemma equiv_0_inter e : (0∩e) ≡ 0.
  Proof. rewrite <- (eq_ax (ax_inter_0 e)) at 2;auto. Qed.

  Lemma inter_plus e f g : (e∩(f + g)) ≡ (e∩f + e∩g).
  Proof.
    rewrite (eq_ax (ax_inter_comm _ _)).
    rewrite (eq_ax (ax_plus_inter _ _ _));auto.
  Qed.

  Lemma inf_ax_inter_l e f : e ∩ f ≤ e.
  Proof.
    transitivity (e ∩ f + 0);auto.
    transitivity (e ∩ f + e);auto.
  Qed.

  Lemma inf_ax_inter_r e f : e ∩ f ≤ f.
  Proof.
    transitivity (f∩e);auto.
    apply inf_ax_inter_l.
  Qed.

  (** We can now prove that if [e] is smaller than [f], then their
  union must be equal to [f]. *)
  Lemma smaller_equiv e f : e ≤ f -> e + f ≡ f.
  Proof.
    intro E;induction E;auto.
    - transitivity (f+g);auto.
      transitivity ((e+f)+g);auto.
      transitivity (e+(f+g));auto.
    - transitivity (e + f + (h + g));auto.
      transitivity ((e + f + h) + g);auto.
      transitivity (e + (f + h) + g);auto.
      transitivity (e + h + g);auto.
      transitivity (e + (h + g));auto.
      transitivity (e + (g + h));auto.
      transitivity ((e + g) + h);auto.
    - rewrite <- IHE, <- IHE0.
      repeat (rewrite (eq_ax (ax_plus_seq _ _ _))
              || rewrite (eq_ax (ax_seq_plus _ _ _))).
      transitivity (e ⋅ f + (e ⋅ h + (g ⋅ f + g ⋅ h)));auto.
      transitivity (e ⋅ f + (e ⋅ f + (e ⋅ h + (g ⋅ f + g ⋅ h))));auto.
      transitivity ((e ⋅ f + e ⋅ f) + (e ⋅ h + (g ⋅ f + g ⋅ h)));auto.
    - rewrite <- IHE, <- IHE0.
      repeat (rewrite (eq_ax (ax_plus_inter _ _ _))
              || rewrite inter_plus).
      transitivity (e ∩ f + (e ∩ h + (g ∩ f + g ∩ h)));auto.
      transitivity (e ∩ f + (e ∩ f + (e ∩ h + (g ∩ f + g ∩ h))));auto.
      transitivity ((e ∩ f + e ∩ f) + (e ∩ h + (g ∩ f + g ∩ h)));auto.
    - rewrite <- IHE;repeat rewrite (eq_ax (ax_conv_plus _ _));auto.
      transitivity (e ¯ + e ¯ + f ¯);auto.
    - destruct H as [H|H];rewrite (eq_ax H);auto.
    - transitivity (e+0);auto.
  Qed.

  (** This allows us to prove that the order we defined is indeed
  antisymmetric with respect to [≡]. *)
  Global Instance smaller_PartialOrder : PartialOrder equiv smaller.
  Proof.
    intros e f;split.
    - intro h;unfold relation_conjunction,predicate_intersection,
              Basics.flip;induction h;simpl in *;firstorder eauto.
    - intros (h1&h2);unfold Basics.flip in h2.
      transitivity (e+f);[transitivity (f+e)|];auto.
      -- symmetry;apply (smaller_equiv h2).
      -- apply (smaller_equiv h1).
  Qed.

  (** We now strengthen our earlier lemma, by proving the following
  reduction from ordering to equivalence. *)
  Lemma smaller_equiv_iff_plus e f : e ≤ f <-> e + f ≡ f.
  Proof.
    split;[apply smaller_equiv|].
    intros <-;transitivity (e+0);auto.
  Qed.

  (** Mirror is actually more than monotone, it is bijective. *)
  Lemma smaller_conv_iff e f :
    e ≤ f <-> e ¯ ≤ f ¯.
  Proof.
    split;auto.
    rewrite <- (eq_ax (ax_conv_conv e)) at 2.
    rewrite <- (eq_ax (ax_conv_conv f)) at 2.
    auto.
  Qed.

  (** We establish few properties of subunits. *)
  Lemma inter_1_abs e f : ((1 ∩ e)⋅(1 ∩ f)) ≡ (1 ∩ (e ∩ f)).
  Proof.
    transitivity ((1∩1)∩(e∩f));auto.
    transitivity ((1∩1∩1)∩(e∩f));auto.
    transitivity (((1∩1)∩(1∩(e∩f))));auto.
    transitivity (((1∩1)∩((1∩e)∩f)));auto.
    transitivity (((1∩1)∩(1∩e)∩f));auto.
    transitivity ((1∩(1∩(1∩e))∩f));auto.
    transitivity ((1∩((1∩e)∩1)∩f));auto.
    transitivity (1∩((1∩e)∩1∩f));auto.
    transitivity (1∩((1∩e)∩(1∩f)));auto.
    transitivity (1∩((1∩e)⋅(1∩f)));auto.
    apply smaller_PartialOrder;unfold Basics.flip;split;auto.
    - transitivity (((1 ∩ e) ⋅ (1 ∩ f))∩((1 ∩ e) ⋅ (1 ∩ f)));
        auto;apply inf_inter;auto.
      transitivity (1⋅1);auto.
      apply inf_seq;apply inf_ax_inter_l.
    - apply inf_ax_inter_r.
  Qed.
    
  Lemma inter_onel e : (1 ∩ e + (1 ∩ e)⋅e) ≡ ((1 ∩ e)⋅e).
  Proof.
    assert (E1:(1 ∩ e) + e ≡ e) by eauto.
    assert (E2:(1 ∩ e) ≡ (1 ∩ e)⋅(1 ∩ e))
      by (transitivity (1 ∩ (e ∩ e));auto using inter_1_abs).
    rewrite E2 at 1.
    rewrite <- (eq_ax (ax_seq_plus _ _ _ )).
    rewrite E1;auto.
  Qed.

  Lemma inter_oner e : (1 ∩ e + e⋅(1 ∩ e)) ≡ (e⋅(1 ∩ e)).
  Proof.
    rewrite <- (eq_ax (ax_inter_1_comm_seq _ _)).
    apply inter_onel.
  Qed.                      

  (** Using these, we get a second reduction from ordering to
  equivalence, relying on intersection rather than union. *)
  Lemma smaller_equiv_iff_inter e f : e ≤ f <-> e ∩ f ≡ e.
  Proof.
    split;intro E.
    - apply smaller_PartialOrder;unfold relation_conjunction,
                                 predicate_intersection,
                                 Basics.flip;split.
      -- apply inf_ax_inter_l.
      -- transitivity (e∩e);auto.
    - rewrite<- E; apply inf_ax_inter_r.
  Qed.

  (** A product is larger than [1] if and only if both its arguments are.*)
  Lemma split_one e f : 1 ≤ e⋅f <-> 1 ≤ e /\ 1 ≤ f.
  Proof.
    split;[|intros (<-&<-);auto].
    intro E.
    apply smaller_equiv_iff_inter in E.
    rewrite (eq_ax (ax_inter_1_seq _ _)) in E.
    rewrite <- E;split;auto.
    - rewrite <-(inf_ax_inter_l e f) at 2;auto.
      apply inf_ax_inter_r.
    - rewrite <-(inf_ax_inter_r e f) at 2;auto.
      apply inf_ax_inter_r.
  Qed.

  (** * Sum of a list of elements *)

  (** The expression [⋃ [e1;e2;...;en]] is [e1+(e2+...(en+0)...)]*)
  Definition sum := fold_right 𝐄_plus 0.
  Notation " ⋃ l " := (sum l) (at level 30).
  
  (** This operator satisfies some simple distributivity properties. *)
  Lemma sum_app l m : ⋃ (l++m) ≡ ⋃ l + ⋃ m.
  Proof.
    unfold sum;rewrite fold_right_app.
    generalize dependent (fold_right 𝐄_plus 0 m);clear m.
    induction l;simpl;intro e;auto.
    - transitivity (e+0);auto.
    - rewrite IHl;auto.
  Qed.
  
  Lemma seq_distr l m : ⋃ l ⋅ ⋃ m ≡ ⋃ (bimap (fun e f : 𝐄 => e ⋅ f) l m).
  Proof.
    revert m;induction l;simpl;auto.
    intro m;rewrite sum_app,<-IHl.
    transitivity (a⋅⋃m+ ⋃l⋅⋃m);auto.
    apply eq_plus;auto.
    clear IHl;induction m;simpl;auto.
    rewrite <- IHm;auto.
  Qed.

  Lemma inter_distr l m : ⋃ l ∩ ⋃ m ≡ ⋃  bimap (fun e f : 𝐄 => e ∩ f) l m.
  Proof.
    revert m;induction l;simpl;auto.
    + intros;transitivity 0;eauto.
    + intro m;rewrite sum_app,<-IHl.
      transitivity (a ∩ ⋃ m + ⋃ l ∩ ⋃ m);auto.
      apply eq_plus;auto.
      clear IHl;induction m as [|b m];simpl;auto.
      rewrite <- IHm.
      transitivity ((b + ⋃ m) ∩ a);auto.
      transitivity (b ∩ a + ⋃ m ∩ a);auto.
  Qed.

  (** If [l⊆m], then [⋃ l ≤ ⋃ m]*)
  Lemma fold_right_incl_smaller :
    Proper ((@incl 𝐄) ==> smaller) (fold_right 𝐄_plus 0).
  Proof.
    intro l;induction l;simpl;intros m L;auto.
    rewrite (IHl m).
    - assert (I:a ∈ m) by (now apply L;left).
      clear l L IHl.
      induction m;simpl in *;try tauto.
      destruct I as [->|I];eauto.
      -- transitivity ((a + a) + fold_right 𝐄_plus 0 m);eauto.
      -- rewrite <- (IHm I) at 2.
         transitivity (a + a0 + fold_right 𝐄_plus 0 m);eauto.
         transitivity (a0 + a + fold_right 𝐄_plus 0 m);eauto.
    - now intros x I;apply L;right.
  Qed.

  (** If [a] appears in [m], then the following identity holds: *)
  Lemma split_list a m :
    a ∈ m ->⋃ m ≡ a +⋃ rm a m.
  Proof.
    intro h;transitivity (⋃ (a::rm a m));auto;
      apply smaller_PartialOrder;split.
    - apply fold_right_incl_smaller.
      intros x I;destruct_eqX x a.
      -- left;auto.
      -- right; apply in_rm;tauto.
    - apply fold_right_incl_smaller;intros x [->|I];auto.
      apply in_rm in I as (_&I);auto.
  Qed.
  
  (** * Normalisation of expressions *)

  (** An expression is called a term if it doesn't contain unions or
  [0], and if [ ¯] only appears on variables. *)
  Fixpoint is_term (e : 𝐄) : bool :=
    match e with
    | 1 | 𝐄_var _ | (𝐄_var _) ¯ => true
    | e ⋅ f | e ∩ f => is_term e && is_term f
    | _ ¯ | _ + _ | 0 => false
    end.

  (** [comb e true] produces a list of terms whose sum is equal to [e]. *)
  (** Here is the definition  of [comb]: *)
  Fixpoint comb e (dir : bool) :=
    match e with
    | 0 => []
    | 1 => [1]
    | e + f => comb e dir ++ comb f dir
    | e ∩ f => bimap (fun e f => e ∩ f) (comb e dir) (comb f dir)
    | e ¯ => comb e (negb dir)
    | 𝐄_var a => if dir then [𝐄_var a] else [𝐄_var a ¯]
    | e ⋅ f =>
      if dir
      then bimap (fun e f => e ⋅ f) (comb e dir) (comb f dir)
      else bimap (fun e f => e ⋅ f) (comb f dir) (comb e dir)
    end.

  (** We prove the claim that indeed every expression in [comb e d] is a term. *)
  Lemma is_term_comb e d u : u ∈ (comb e d) -> is_term u = true.
  Proof.
    revert d u;induction e;intros d u;simpl.
    - intros [<-|F];tauto.
    - tauto.
    - destruct d;intros [<-|F];simpl in *;tauto.
    - destruct d;rewrite in_bimap;intros (a&b&<-&I1&I2);simpl.
      + apply IHe1 in I1 as ->;apply IHe2 in I2 as ->;reflexivity.
      + apply IHe1 in I2 as ->;apply IHe2 in I1 as ->;reflexivity.
    - rewrite in_bimap;intros (a&b&<-&I1&I2);simpl;
        apply IHe1 in I1 as ->;apply IHe2 in I2 as ->;reflexivity.
    - rewrite in_app_iff;firstorder.
    - apply IHe.
  Qed.

  (** We can now verify the second claim, that [e] is equal to the sum
  of [comb e true]. *)
  Lemma comb_equiv e : e ≡ ⋃ comb e true.
  Proof.
    cut (e ≡⋃ comb e true /\ e ¯ ≡⋃ comb e false);[tauto|].
    induction e;simpl;auto.
    - split;eauto.
    - destruct IHe1 as (E1&E1');destruct IHe2 as (E2&E2');split.
      + rewrite <- seq_distr;auto.
      + rewrite <- seq_distr;eauto.
    - destruct IHe1;destruct IHe2;split;rewrite <- inter_distr;eauto.
    - destruct IHe1;destruct IHe2;split;rewrite sum_app;eauto.
    - destruct IHe;split;eauto.
  Qed.

  Close Scope expr_scope.
End s.
(* begin hide *)
Arguments 𝐄_one {X}.
Arguments 𝐄_zero {X}.
Arguments eqb {X} {dec_X} e%expr f%expr.
Hint Constructors 𝐄_eq ax 𝐄_inf.
Hint Rewrite @𝐄_size_one @𝐄_size_zero @𝐄_size_var @𝐄_size_seq
     @𝐄_size_inter @𝐄_size_plus @𝐄_size_conv
  : simpl_typeclasses.
(* end hide *)

Infix " ⋅ " := 𝐄_seq (at level 40) : expr_scope.
Infix " + " := 𝐄_plus (left associativity, at level 50) : expr_scope.
Infix " ∩ " := 𝐄_inter (at level 45) : expr_scope.
Notation "x ¯" := (𝐄_conv x) (at level 25) : expr_scope.
Notation " 1 " := 𝐄_one : expr_scope.
Notation " 0 " := 𝐄_zero : expr_scope.
Notation "⋃  l  " := (sum l) (at level 30).

Section language.
  (** * Language interpretation *)
  Context { X : Set }.

  (** We interpret expressions as languages in the obvious way: *)
  Global Instance to_lang_𝐄 {Σ}: semantics 𝐄 language X Σ :=
    fix to_lang_𝐄 σ e:=
      match e with
      | 1 => 1%lang
      | 0 => 0%lang
      | 𝐄_var a => (σ a)
      | e + f => ((to_lang_𝐄 σ e) + (to_lang_𝐄 σ f))%lang
      | e ⋅ f => ((to_lang_𝐄 σ e) ⋅ (to_lang_𝐄 σ f))%lang
      | e ∩ f => ((to_lang_𝐄 σ e) ∩ (to_lang_𝐄 σ f))%lang
      | e ¯ => (to_lang_𝐄 σ e) ¯%lang
      end.

  (* begin hide *)
  Global Instance semSmaller_𝐄 : SemSmaller (𝐄 X) :=
    (@semantic_containment _ _ _ _ _).
  Global Instance semEquiv_𝐄 : SemEquiv (𝐄 X) :=
    (@semantic_equality _ _ _ _ _).
  Hint Unfold semSmaller_𝐄 semEquiv_𝐄 : semantics. 

  Section rsimpl.
    Context { Σ : Set }{σ : 𝕬[X→Σ] }.
    Lemma 𝐄_union e f : (⟦ e+f ⟧σ) = ((⟦e⟧σ) + ⟦f⟧σ)%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_prod e f :  (⟦ e⋅f ⟧σ) = ((⟦e⟧σ) ⋅ ⟦f⟧σ)%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_intersection e f : (⟦ e∩f ⟧σ) = ((⟦e⟧σ) ∩ ⟦f⟧σ)%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_mirror e :  (⟦ e ¯⟧σ) = (⟦e⟧σ) ¯%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_variable a : (⟦𝐄_var a⟧ σ) = σ a.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_unit : (⟦1⟧σ) = 1%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_empty : (⟦0⟧σ) = 0%lang.
    Proof. unfold interprete;simpl;auto. Qed.
  End rsimpl.
  Hint Rewrite @𝐄_unit @𝐄_empty @𝐄_variable @𝐄_intersection
       @𝐄_prod @𝐄_union @𝐄_mirror
    : simpl_typeclasses.

  Global Instance sem_incl_𝐄_plus :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐄_plus X).
  Proof.
    intros e f E g h F ? ?;simpl;rsimpl;revert E F;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_incl_𝐄_fois :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐄_seq X).
  Proof.
    intros e f E g h F Σ σ;simpl;revert E F;rsimpl;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_incl_𝐄_inter :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐄_inter X).
  Proof.
    intros e f E g h F Σ σ;simpl;revert E F;rsimpl;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_incl_𝐄_conv :
    Proper (ssmaller ==> ssmaller) (@𝐄_conv X).
  Proof.
    intros e f E Σ σ;simpl;revert E;rsimpl;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_eq_𝐄_plus :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐄_plus X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.
  Global Instance sem_eq_𝐄_seq :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐄_seq X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.
  Global Instance sem_eq_𝐄_inter :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐄_inter X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.
  Global Instance sem_eq_𝐄_conv :
    Proper (sequiv ==> sequiv) (@𝐄_conv X).
  Proof.
    intros e f E Σ σ;simpl;autounfold;rsimpl.
    intro w;rewrite (E Σ σ (rev w));tauto.
  Qed.
  Global Instance 𝐄_sem_equiv :
    Equivalence (fun e f : 𝐄 X => e ≃ f).
  Proof. once (typeclasses eauto). Qed.
  Global Instance 𝐄_sem_PreOrder :
    PreOrder (fun e f : 𝐄 X => e ≲ f).
  Proof. once (typeclasses eauto). Qed.
  Global Instance 𝐄_sem_PartialOrder :
    PartialOrder (fun e f : 𝐄 X => e ≃ f)
                 (fun e f : 𝐄 X => e ≲ f).
  Proof.
    eapply semantic_containment_PartialOrder;once (typeclasses eauto).
  Qed.

  (* end hide *)

  (** This interpretation is sound in the sense that axiomatically
  equivalent expressions are semantically equivalent. Differently put,
  the axioms we imposed hold in every algebra of languages. *)
  Theorem 𝐄_eq_equiv_lang : forall e f : 𝐄 X, e ≡ f ->  e ≃ f.
  Proof.
    assert (dumb : forall A (w:list A), rev w = nil <-> w = nil)
      by (intros A w;split;[intro h;rewrite <-(rev_involutive w),h
                           |intros ->];reflexivity).
    intros e f E Σ σ;induction E;rsimpl;try firstorder.
    destruct H;rsimpl;repeat autounfold with semantics;try (now firstorder). 
    - intro w;firstorder.
      + rewrite H1,H3;clear x0 w H1 H3;rewrite<- app_ass.
        eexists;eexists;split;[eexists;eexists;split;[|split]
                              |split];eauto.
      + rewrite H1,H3;clear x w H1 H3;rewrite app_ass.
        eexists;eexists;split;
          [|split;[eexists;eexists;split;[|split]|]];eauto.
    - intro w;firstorder subst.
      + simpl in *;auto.
      + exists nil;exists w;simpl;auto.
    - intro w;firstorder subst.
      + rewrite app_nil_r;auto.
      + exists w;exists nil;rewrite app_nil_r;auto.
    - intro w;rewrite rev_involutive;tauto.
    - intro w;firstorder.
      + rewrite <- (rev_involutive w),H1,rev_app_distr;eauto.
        exists (rev x0);exists (rev x).
        setoid_rewrite rev_involutive;auto.
      + rewrite H1,rev_app_distr;eauto.
    - intros w;firstorder subst.
      + destruct x;[|destruct x0];auto; discriminate. 
      + destruct x;destruct x0;try discriminate;auto.
      + exists [];exists [];subst;auto.
    - intro w;firstorder subst;simpl in *;auto.
    - intro w;firstorder subst;simpl in *. 
      + exists x0;exists [];rewrite app_nil_r;auto.
      + exists [];exists x;rewrite app_nil_r;auto. 
    - intro w;firstorder subst;simpl in *;auto. 
  Qed.

  (** This extends to ordering as well. *)
  Lemma 𝐄_inf_incl_lang (e f : 𝐄 X) : e ≤ f -> e ≲ f.
  Proof.
    intro E;apply smaller_equiv,𝐄_eq_equiv_lang in E.
    intros Σ σ w I;apply E;rsimpl;left;auto.
  Qed.

  (** A word is in the language of a sum if and only if it is in the
  language of one of its components. *)
  Lemma sum_lang l Σ (σ:𝕬[X→Σ]) w :
    (w ∊ ⟦⋃ l⟧ σ) <-> (exists t : 𝐄 X, t ∈ l /\ (⟦ t ⟧ σ) w).
  Proof.
    induction l;simpl;rsimpl;autounfold with semantics.
    - firstorder.
    - split;[intros [h|IH];[|apply IHl in IH as (t&I&IH)]
            |intros (t&[<-|I]&IH)];auto.
      -- exists a;split;auto.
      -- exists t;split;auto.
      -- right;apply IHl;eauto.
  Qed.

  (** Thus we may find for every word in the language of [e] a term in
  [comb e true] containing that word. *)
  Lemma comb_lang e Σ (σ:𝕬[X→Σ]) w :
    w ∊ (⟦e⟧σ) <-> exists t, t ∈ (comb e true) /\ w ∊ (⟦t⟧σ).
  Proof.
    pose proof (comb_equiv e) as P.
    apply 𝐄_eq_equiv_lang in P.
    rewrite (P Σ σ w),sum_lang;tauto.
  Qed.

End language.
(* begin hide *)
Hint Rewrite @𝐄_unit @𝐄_empty @𝐄_variable @𝐄_intersection
     @𝐄_prod @𝐄_union @𝐄_mirror
  : simpl_typeclasses.
(* end hide *)
(*  LocalWords:  subunits
 *)
