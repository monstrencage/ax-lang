(** In this module we describe the reductions between terms (type
[𝐓]), primed weak terms ([𝐖']) and series-parallel terms ([𝐒𝐏]). *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export terms w_terms.
Open Scope term_scope.
Section s.
  Variable X : Set.
  Variable dec_X : decidable_set X.

  (** We start by defining a conversion function from series parallel
  terms over a duplicated alphabet into terms over the original
  alphabet. *)
  Fixpoint 𝐒𝐏_to_𝐓 (e : 𝐒𝐏 (@X' X)) : 𝐓 X :=
    match e with
    | 𝐒𝐏_var (a,true) => 𝐓_var a
    | 𝐒𝐏_var (a,false) => 𝐓_cvar a
    | e ⨾ f => 𝐒𝐏_to_𝐓 e ⋅ 𝐒𝐏_to_𝐓 f
    | e ∥ f => 𝐒𝐏_to_𝐓 e ∩ 𝐒𝐏_to_𝐓 f
    end.

  (** Using this function, we build a translation from primed weak
  terms into terms. *)
  Definition 𝐖'_to_𝐓 (e : 𝐖') : 𝐓 X:=
    match e with
    | (exist _ (None,A) _) => ⟨map fst A⟩
    | (exist _ (Some u,A) _ ) =>
      ⟨map fst A⟩ ⋅ 𝐒𝐏_to_𝐓 u
    end.

  (** For the converse direction, we directly translate terms into
  primed weak terms. *)
  Fixpoint 𝐓_to_𝐖' (e : 𝐓 X) : 𝐖' :=
    match e with
    | 1 => 𝟭'
    | 𝐓_var a => (exist _ (Some (𝐒𝐏_var (a,true)),[]) (eq_refl _))
    | 𝐓_cvar a =>
      (exist _ (Some (𝐒𝐏_var (a,false)),[]) (eq_refl _))
    | e ⋅ f => (𝐓_to_𝐖' e) ⊗' (𝐓_to_𝐖' f)
    | e ∩ f => (𝐓_to_𝐖' e) ⊕' (𝐓_to_𝐖' f)
    end.

  (** The translation from terms to primed weak terms is monotone with
  respect to the ordering of [𝐖']. *)
  Lemma 𝐓_to_𝐖'_inf e f : e ≤ f -> 𝐓_to_𝐖' e ≤ 𝐓_to_𝐖' f.
  Proof.
    intro E;induction E;auto.
    - reflexivity.
    - etransitivity;eauto.
    - destruct H;simpl;apply 𝐖'_par_inf.
    - destruct H as [[]|[]];simpl.
      -- apply 𝐖'_seq_assoc.
      -- apply 𝐖'_seq_one_l.
      -- apply 𝐖'_seq_one_r.
      -- apply 𝐖'_par_assoc.
      -- apply 𝐖'_par_comm.
      -- apply 𝐖'_par_idem.
      -- apply 𝐖'_par_one_seq_par.
      -- unfold 𝐖'_par,smaller,𝐖'_inf;simpl;reflexivity.
      -- apply 𝐖'_par_one_seq.
      -- apply 𝐖'_par_one_par.
      -- apply 𝐖'_seq_assoc.
      -- apply 𝐖'_seq_one_l.
      -- apply 𝐖'_seq_one_r.
      -- apply 𝐖'_par_assoc.
      -- apply 𝐖'_par_comm.
      -- apply 𝐖'_par_idem.
      -- apply 𝐖'_par_one_seq_par.
      -- unfold 𝐖'_par,smaller,𝐖'_inf;simpl;reflexivity.
      -- apply 𝐖'_par_one_seq.
      -- apply 𝐖'_par_one_par.
    - now simpl;rewrite IHE,IHE0.
    - now simpl;rewrite IHE,IHE0.
  Qed.
         
  (** The translation from series-parallel terms into terms is size
  preserving. *)
  Lemma size_𝐒𝐏_to_𝐓 s : |𝐒𝐏_to_𝐓 s| = |s|.
  Proof. induction s as [(a,[|]) |s1 s2|s1 s2 ];rsimpl;auto. Qed.

  (** The translation from terms to primed weak terms reduces the
  size. *)
  Lemma size_term_to_𝐖' s : |𝐓_to_𝐖' s| <= |s|.
  Proof.
    induction s;rsimpl;auto.
    - rewrite 𝐖'_size_𝐖'_seq;lia.
    - rewrite 𝐖'_size_𝐖'_par;lia.
  Qed.

  (** From weak terms to terms the size may increase, but is bounded as follows: *)
  Lemma size_𝐖'_to_𝐓 s : |s| <= |𝐖'_to_𝐓 s| <= 4 * |s| + 1.
  Proof.
    unfold size at 1 4;unfold 𝐖'_size;unfold size at 1 4;unfold 𝐖_size.
    destruct s as (([s|],A),?);rsimpl;rewrite 𝐓_size_to_test;try lia.
    - set (n:= # A).
      replace (# (map fst A)) with n
      by (unfold n;symmetry;apply map_length).
      rewrite size_𝐒𝐏_to_𝐓.
      assert (|s| > 0) by (induction s;rsimpl;lia);lia.
    - set (n:= # A).
      replace (# (map fst A)) with n
      by (unfold n;symmetry;apply map_length).
      lia.
  Qed.

  (** We may recover the variables of a term by map the first
  projection to the set of variables of its translation in [𝐖']. *)
  Lemma variables_𝐓_to_𝐖' (t : 𝐓 X) : 𝒱 t  ≈ map fst (𝐖'_variables (𝐓_to_𝐖' t)).
  Proof.
    induction t;simpl;try (intro;simpl;tauto).
    - rewrite variables_𝐖'_seq,map_app,<-IHt1,<-IHt2;reflexivity.
    - rewrite variables_𝐖'_par,map_app,<-IHt1,<-IHt2;reflexivity.
  Qed.

  (* begin hide *)
  Lemma 𝐓_to_𝐖'_to_test l (p : balanced l = true) :
    𝐓_to_𝐖' (⟨map fst l⟩) ≡ (exist _ (None,l) p).
  Proof.
    assert (exists m, m ≈ map fst l /\ NoDup m) as (m&E&N).
    - clear p;induction l.
      -- exists [];split;[intro;simpl;tauto|apply NoDup_nil].
      -- destruct a as (a&b);simpl;destruct IHl as (m&e&N);
         setoid_rewrite <- e.
         --- destruct (inb_dec a m) as [(_&I)|(_&I)].
             ---- exists m;split;auto.
                  intro x;split;simpl;auto;intros[<-|h];tauto.
             ---- exists (a::m);split;[|apply NoDup_cons;auto].
                  intro x;split;intros [<- | J];simpl;tauto.
    - transitivity (𝐓_to_𝐖' (⟨m⟩)).
      -- apply 𝐖'_inf_PartialOrder;unfold Basics.flip;split;
         apply 𝐓_to_𝐖'_inf,𝐓_to_test_incl_inf;auto;
         rewrite E;reflexivity.
      -- revert l p E;induction m;simpl;auto;intros l p E;
         unfold 𝐖'_equiv,𝐖_equiv,𝐖_inf,
         proj1_sig;simpl.
         --- split;destruct l as [|(x1,x2) l]; reflexivity || exfalso;
             assert (F:x1 ∈ []) by (rewrite (E x1);now left);
             simpl in F;tauto.
         --- apply NoDup_cons_iff in N as (nI&N).
             assert (m ≈ map fst (filter (fun y =>negb(eqX(fst y)a))l)
                     /\ is_balanced(filter(fun y=>negb(eqX(fst y)a))l))
             as (E'&p').
             ---- split.
                  ----- intro x;rewrite in_map_iff;
                  setoid_rewrite filter_In;
                  setoid_rewrite negb_true_iff;setoid_rewrite eqX_false.
                  pose proof (E x) as ha;rewrite in_map_iff in ha.
                  split;[intro I|intros ((y&b)&<-&I&nE)];simpl.
                  ------ assert (x ∈ (a::m)) as I' by (now right).
                  apply ha in I' as ((y1&y2)&<-&I');simpl in *.
                  exists (y1,y2);split;auto;split;auto;simpl.
                  intros ->;tauto.
                  ------ cut (y ∈ (a::m));
                    [intros [<-|?];simpl in *;tauto|].
                  apply ha;eexists;split;eauto.
                  ----- intro x;rewrite filter_In,filter_In.
                  apply balanced_spec in p;rewrite (p _);tauto.
             ---- unfold equiv,smaller;simpl.
                  apply balanced_spec in p'.
                  eapply (IHm N _ p') in E';
                    unfold equiv,𝐖'_equiv,equiv,𝐖_equiv,
                    smaller,𝐖_inf,proj1_sig in E'.
                  destruct (𝐓_to_𝐖' (⟨m⟩)) as (([w|],A),pm);
                    try tauto;simpl;split;destruct E' as (E1&E2).
                  ----- rewrite <- E1;intros (x1,x2) I;
                    rewrite in_app_iff, filter_In,negb_true_iff,eqX_false;
                    simpl;destruct_eqX x1 a;try tauto.
                  right;destruct x2;tauto.
                  ----- rewrite E2;intros (x1,x2) ;
                    rewrite in_app_iff, filter_In,negb_true_iff,eqX_false.
                  simpl;intros [(I&_)|[Eq|[Eq|[Eq|[Eq|Eq]]]]];auto;
                    try symmetry in Eq;inversion Eq;
                      assert (J:a ∈ (a::m)) by (now left);
                      apply E,in_map_iff in J as ((y1&y2)&Eq'&J);
                      simpl in Eq';rewrite Eq' in *;destruct y2;auto;
                        apply balanced_spec in p; apply p;auto.
  Qed.
  (* end hide *)
  (** Going from primed weak terms to terms and back yields an
  equivalent [𝐖']-term. *)
  Lemma 𝐖'_to_𝐓_to_𝐖' t : t ≡ 𝐓_to_𝐖' (𝐖'_to_𝐓 t).
  Proof.
    destruct t as (([t|],A),p).
    - induction t ;simpl in *;auto.
      -- rewrite 𝐓_to_𝐖'_to_test;
        unfold 𝐖'_seq,equiv,𝐖'_equiv,proj1_sig;simpl;
         destruct x as (x&[|]);simpl; now rewrite app_nil_r.
      -- pose proof (IHt1 p) as h1;pose proof (IHt2 p) as h2;
         revert h1 h2;clear IHt1 IHt2.
         repeat rewrite (@𝐓_to_𝐖'_to_test _ p).
         set (a:=𝐓_to_𝐖' (𝐒𝐏_to_𝐓 t1)).
         set (b:=𝐓_to_𝐖' (𝐒𝐏_to_𝐓 t2)).
         intros h1 h2.
         transitivity ((exist _ (Some t1,A) p)
                         ⊗'(exist _ (Some t2,A) p));auto.
         --- split;unfold 𝐖_seq,𝐖_inf;split;auto;
             intro;rewrite in_app_iff;tauto.
         --- rewrite h1,h2;clear h1 h2.
             etransitivity;[apply 𝐖'_seq_assoc|].
             etransitivity.
             ---- apply 𝐖'_seq_𝐖'_equiv;[|reflexivity].
                  rewrite sub_identity_par_one.
                  symmetry;apply 𝐖'_par_one_seq.
             ---- repeat rewrite 𝐖'_seq_assoc.
                  rewrite sub_identity_par_one,𝐖'_par_one_seq_par_one.
                  rewrite 𝐖'_par_idem, 𝐖'_par_assoc,𝐖'_par_idem.
                  apply 𝐖'_seq_𝐖'_equiv;[|reflexivity].
                  apply 𝐖'_seq_𝐖'_equiv;[|reflexivity].
                  unfold equiv,𝐖'_equiv;reflexivity.
      --  pose proof (IHt1 p) as h1;pose proof (IHt2 p) as h2;
         revert h1 h2;clear IHt1 IHt2.
         repeat rewrite (@𝐓_to_𝐖'_to_test _ p).
         set (a:=𝐓_to_𝐖' (𝐒𝐏_to_𝐓 t1)).
         set (b:=𝐓_to_𝐖' (𝐒𝐏_to_𝐓 t2)).
         intros h1 h2;transitivity
                        (exist _ (Some t1,A) p⊕'exist _ (Some t2,A) p);
         auto.
         --- split;unfold 𝐖_par,𝐖_inf;split;auto;
             intro;rewrite in_app_iff;tauto.
         --- rewrite h1,h2;clear h1 h2.
             rewrite sub_identity_par_one.
             rewrite 𝐖'_par_one_par,(𝐖'_par_comm a _).
             rewrite 𝐖'_par_one_par,(𝐖'_par_comm a _).
             rewrite 𝐖'_seq_assoc,𝐖'_par_one_seq_par_one,𝐖'_par_idem.
             reflexivity.
    - simpl;rewrite (@𝐓_to_𝐖'_to_test _ p);eauto;reflexivity.
    Grab Existential Variables. auto.
  Qed.

  (** We can relate the variables of a series parallel term (over the
  duplicated alphabet) with the variables of its corresponding
  term. *)
  Lemma variables_𝐒𝐏_to_𝐓 (u : 𝐒𝐏 (@X' X)) : 𝒱 (𝐒𝐏_to_𝐓 u) ≈ map fst (𝒱 u).
  Proof.
    induction u;rsimpl.
    - destruct x as (x&[|]);reflexivity.
    - rewrite IHu1,IHu2,map_app; reflexivity.
    - rewrite IHu1,IHu2,map_app;reflexivity.
  Qed.

  (** The translation from primed weak terms to terms is monotone. *)
  Lemma 𝐖'_to_𝐓_inf e f : e ≤ f -> 𝐖'_to_𝐓 e ≤ 𝐖'_to_𝐓 f.
  Proof.
    destruct e as (([e|],A),pA);destruct f as (([f|],B),pB);
      unfold 𝐖_inf;simpl;try tauto.
    - intros (I&E);
      etransitivity;[|apply 𝐓_inf_seq;
                       [apply 𝐓_to_test_incl_inf;eauto;
                        apply map_incl_Proper,I|reflexivity]].
      clear pB pA B I;induction E;simpl;auto.
      -- eauto.
      -- destruct H as [H|H];destruct H;simpl;auto.
      -- destruct H;simpl;auto.
         ++ rewrite 𝐓_seq_assoc.
            apply 𝐓_inf_seq;auto.
            rewrite 𝐓_to_test_dupl at 1;apply 𝐓_inf_seq;auto.
            apply (@map_incl_Proper _ _ fst) in H.
            rewrite <-variables_𝐒𝐏_to_𝐓 in H.
            transitivity (𝐒𝐏_to_𝐓 e∩1);auto.
            transitivity (1∩𝐒𝐏_to_𝐓 e);auto.
            rewrite<-𝐓_to_test_is_par_one; apply 𝐓_to_test_incl_inf;
              auto.
         ++ rewrite 𝐓_to_test_comm,𝐓_to_test_comm.
            rewrite <- 𝐓_seq_assoc.
            apply 𝐓_inf_seq; auto.
            rewrite 𝐓_to_test_dupl at 1;apply 𝐓_inf_seq;auto.
            apply (@map_incl_Proper _ _ fst) in H.
            rewrite <-variables_𝐒𝐏_to_𝐓 in H.
            transitivity (𝐒𝐏_to_𝐓 e∩1);auto.
            transitivity (1∩𝐒𝐏_to_𝐓 e);auto.
            rewrite<-𝐓_to_test_is_par_one; apply 𝐓_to_test_incl_inf;
              auto.
      -- rewrite 𝐓_to_test_dupl.
         rewrite (𝐓_inf_one_prod_is_inter (𝐓_to_test_one _)
                                          (𝐓_to_test_one _)).
         rewrite 𝐓_seq_assoc,<- 𝐓_weak_seq,IHE1.
         rewrite <- 𝐓_seq_assoc,IHE2.
         etransitivity;[|rewrite 𝐓_seq_assoc,<- 𝐓_weak_seq];
           auto.
      -- clear E1 E2. 
         rewrite 𝐓_to_test_distr,𝐓_to_test_distr;auto.
    - now unfold smaller,𝐖'_inf,smaller,𝐖_inf.
    - intros (I&S);rewrite 𝐓_to_test_dupl.
      apply 𝐓_inf_seq;[apply 𝐓_to_test_incl_inf;
                      try apply map_incl_Proper;auto|].
      apply (@map_incl_Proper _ _ fst) in S.
      rewrite <-variables_𝐒𝐏_to_𝐓 in S.
      etransitivity;[apply 𝐓_to_test_incl_inf,S;auto|].
      rewrite 𝐓_to_test_is_par_one;eauto.
    - unfold 𝐖'_inf,𝐖_inf;simpl.
      intro;apply 𝐓_to_test_incl_inf,map_incl_Proper;auto.
  Qed.
      
  (** Going from terms to primed weak terms and back yields an
  equivalent term. *)
    Lemma 𝐓_to_𝐖'_to_𝐓 t :
    t ≡ 𝐖'_to_𝐓 (𝐓_to_𝐖' t).
  Proof.
    induction t;simpl;auto.
    - rewrite IHt1 at 1;rewrite IHt2 at 1;simpl.
      destruct (𝐓_to_𝐖' t1) as (([u|],A),p1);
        destruct (𝐓_to_𝐖' t2) as (([v|],B),p2);
        clear IHt1 IHt2 t1 t2;simpl.
      -- rewrite map_app,𝐓_to_test_app;auto.
         rewrite 𝐓_seq_assoc,𝐓_weak_seq;auto.
      -- rewrite map_app,𝐓_to_test_app;auto.
         rewrite 𝐓_weak_seq;auto.
      -- rewrite map_app,𝐓_to_test_app;auto.
         rewrite 𝐓_seq_assoc.
         rewrite <- 𝐓_inf_one_prod_is_inter;auto using 𝐓_to_test_one.
      -- rewrite map_app,𝐓_to_test_app;auto.
         rewrite <- 𝐓_inf_one_prod_is_inter;auto using 𝐓_to_test_one.
    - rewrite IHt1 at 1;rewrite IHt2 at 1;simpl.
      destruct (𝐓_to_𝐖' t1) as (([u|],A),p1);
        destruct (𝐓_to_𝐖' t2) as (([v|],B),p2);
        clear IHt1 IHt2 t1 t2;simpl.
      -- rewrite map_app,𝐓_to_test_app.
         rewrite 𝐓_weak_par;auto.
      -- repeat rewrite map_app,𝐓_to_test_app.
         setoid_rewrite <- fst_𝐒𝐏_variables.
         rewrite <-variables_𝐒𝐏_to_𝐓.
         rewrite 𝐓_to_test_is_par_one,𝐓_inter_assoc.
         transitivity ((⟨ map fst A ⟩ ∩ 𝐒𝐏_to_𝐓 u) ∩ ⟨ map fst B ⟩).
         ++ pose proof (𝐓_to_test_one (map fst B)) as L.
            apply 𝐓_inf_eq_inter in L as ->.
            repeat rewrite 𝐓_inter_assoc;apply 𝐓_eq_inter;auto.
            repeat rewrite (𝐓_inter_comm _ 1);auto.
         ++ repeat rewrite<- 𝐓_inter_assoc.
            apply 𝐓_eq_inter;auto.
            rewrite 𝐓_inter_comm,𝐓_inter_assoc.
            apply 𝐓_eq_inter;auto.
            rewrite 𝐓_inter_comm;apply 𝐓_inf_eq_inter.
            apply 𝐓_to_test_one.
      -- repeat rewrite map_app,𝐓_to_test_app.
         setoid_rewrite <- fst_𝐒𝐏_variables;
           rewrite <-variables_𝐒𝐏_to_𝐓.
         rewrite 𝐓_to_test_is_par_one,𝐓_inter_assoc.
         transitivity (⟨ map fst A ⟩ ∩ (⟨ map fst B ⟩ ∩ 𝐒𝐏_to_𝐓 v)).
         ++ pose proof (𝐓_to_test_one (map fst A)) as L.
            apply 𝐓_inf_eq_inter in L as ->.
            repeat rewrite (𝐓_inter_comm 1 _);auto.
            repeat rewrite <- 𝐓_inter_assoc;apply 𝐓_eq_inter;auto.
         ++ repeat rewrite<- 𝐓_inter_assoc.
            apply 𝐓_eq_inter;auto.
            rewrite 𝐓_inter_assoc.
            apply 𝐓_eq_inter;auto.
            rewrite 𝐓_inter_comm;apply 𝐓_inf_eq_inter.
            apply 𝐓_to_test_one.
      -- rewrite map_app,𝐓_to_test_app;auto.
  Qed.

  (** The translation from primed weak terms into terms is a
  reduction, in the sense that two [𝐖'] terms are ordered if and only
  if their images are ordered. *)
  Theorem 𝐖'_to_𝐓_inf_iff e f : e ≤ f <-> 𝐖'_to_𝐓 e ≤ 𝐖'_to_𝐓 f.
  Proof.
    split;[apply 𝐖'_to_𝐓_inf|].
    intro h;apply 𝐓_to_𝐖'_inf in h.
    rewrite <- 𝐖'_to_𝐓_to_𝐖',<- 𝐖'_to_𝐓_to_𝐖' in h;auto.
  Qed.

  (** Similarly, the translation from terms to primed weak terms is a
  reduction. *)
  Theorem 𝐓_to_𝐖'_inf_iff e f : e ≤ f <-> 𝐓_to_𝐖' e ≤ 𝐓_to_𝐖' f.
  Proof.
    split;[apply 𝐓_to_𝐖'_inf|].
    intro h;apply 𝐖'_to_𝐓_inf in h.
    rewrite <- 𝐓_to_𝐖'_to_𝐓,<- 𝐓_to_𝐖'_to_𝐓 in h;auto.
  Qed.
End s.
Close Scope term_scope.

Arguments 𝐓_to_𝐖' {X dec_X}.

(** * Languages *)
Section languages.
  Context {X : Set} {decX : decidable_set X}.

  (** [𝐒𝐏_to_𝐓] is language preserving, for interpretations of
assignments of the shape [Ξ σ]. *)
  Lemma 𝐒𝐏_to_𝐓_lang t Σ (σ : 𝕬[X→Σ]) : ⟦t⟧(Ξ σ) ≃ ⟦𝐒𝐏_to_𝐓 t⟧σ.
  Proof.
    induction t;intro w;simpl;autounfold;rsimpl;try tauto.
    - destruct x as (x,[|]);simpl;rsimpl;tauto.
    - autounfold with *;setoid_rewrite (IHt1 _);setoid_rewrite (IHt2 _);tauto.
    - autounfold with *;rewrite (IHt1 _),(IHt2 _);tauto.
  Qed.

  (** [𝐖'_to_𝐓] is language preserving, for interpretations of
assignments of the shape [Ξ σ]. *)
  Lemma 𝐖'_to_𝐓_lang (t : 𝐖') Σ (σ : 𝕬[X→Σ]) :
    ⟦π{t}⟧(Ξ σ) ≃ ⟦𝐖'_to_𝐓 t⟧σ.
  Proof.
    destruct t as (([t|],A),p);simpl in p;
      unfold to_lang_𝐖,𝐖'_to_𝐓,test_compatible.
    - simpl;rsimpl;rewrite <- 𝐒𝐏_to_𝐓_lang;
        intro;unfold prod;setoid_rewrite test_compatible_𝐓_to_test.
      unfold interprete,intersection,T at 1;simpl.
      setoid_rewrite test_compatible_Ξ;split.
      + intros ? ;exists [];exists w;simpl;tauto.
      + intros (?&?&(?&->)&?&->);simpl;tauto.
    - simpl;intro w.
      unfold interprete at 1;unfold intersection,T;simpl.
      rewrite test_compatible_𝐓_to_test,test_compatible_Ξ;tauto.
  Qed.

  (** So is [𝐓_to_𝐖'], under similar hypotheses. *)
  Lemma 𝐓_to_𝐖'_lang s Σ (σ : 𝕬[X→Σ]) :
    ⟦s⟧σ ≃ ⟦π{𝐓_to_𝐖' s}⟧(Ξ σ).
  Proof.
    autounfold with semantics.
    induction s;intros;simpl;repeat autounfold with semantics;unfold test_compatible;
      rsimpl;simpl.
    - unfold interprete,to_lang_𝐖,T,test_compatible,intersection;firstorder.
    - unfold interprete,to_lang_𝐖,T,test_compatible,intersection;firstorder.
    - unfold interprete,to_lang_𝐖,T,test_compatible,intersection;firstorder.
    - unfold prod.
      setoid_rewrite (IHs1 _);setoid_rewrite (IHs2 _);
        clear IHs1 IHs2.
      destruct (𝐓_to_𝐖' s1) as ((t1&A1),p1);
        destruct (𝐓_to_𝐖' s2) as ((t2&A2),p2);
        simpl in *.
      destruct t1 as [t1|];destruct t2 as [t2|];simpl.
      + unfold interprete,to_lang_𝐖,T,intersection;simpl.
        setoid_rewrite test_compatible_app.
        rsimpl;unfold prod;firstorder.
      + unfold interprete,to_lang_𝐖,T,intersection;simpl.
        setoid_rewrite test_compatible_app.
        rsimpl;unfold prod,unit;split.
        * intros (?&?&?&(?&->)&->);rewrite app_nil_r;tauto.
        * intro;exists w;exists nil;rewrite app_nil_r;tauto.
      + unfold interprete,to_lang_𝐖,T,intersection;simpl.
        setoid_rewrite test_compatible_app.
        rsimpl;unfold prod,unit;split.
        * intros (?&?&(?&->)&?&->);rewrite app_nil_l;tauto.
        * intro;exists nil;exists w;rewrite app_nil_l;tauto.
      + unfold interprete,to_lang_𝐖,T,intersection;simpl.
        setoid_rewrite test_compatible_app.
        rsimpl;unfold prod,unit;split.
        * intros (?&?&(?&->)&(?&->)&->);rewrite app_nil_r;tauto.
        * intro;exists nil;exists w;rewrite app_nil_l;tauto.
    - unfold intersection.
      setoid_rewrite (IHs1 _);setoid_rewrite (IHs2 _);
        clear IHs1 IHs2.
      destruct (𝐓_to_𝐖' s1) as ((t1&A1),p1);
        destruct (𝐓_to_𝐖' s2) as ((t2&A2),p2);
        simpl in *.
      destruct t1 as [t1|];destruct t2 as [t2|];simpl.
      + unfold interprete,to_lang_𝐖,T,intersection;simpl.
        setoid_rewrite test_compatible_app.
        rsimpl;unfold prod;firstorder.
      + unfold interprete,to_lang_𝐖,T,intersection;simpl.
        repeat setoid_rewrite test_compatible_app.
        rsimpl;unfold unit.
        rewrite <- (test_compatible_Ξ _ (𝐒𝐏'_variables t1)).
        unfold test_compatible.
        setoid_rewrite <- fst_𝐒𝐏_variables;split.
        * intros ((h1&h2)&h3&->).
          rewrite <- test_compatible_nil in h2.
          repeat split;auto.
          intros x I;apply in_map_iff in I as ((a&b)&<-&I).
          apply h2 in I;destruct b;simpl in *;auto.
        * intros ((h1&h2&h3)&->);repeat split;auto.
          apply test_compatible_nil.
          intros (a,[|]) I;simpl;unfold mirror;simpl.
          -- apply h3,in_map_iff;eexists;split;eauto;reflexivity.
          -- apply h3,in_map_iff;eexists;split;eauto;reflexivity.
      + unfold interprete,to_lang_𝐖,T,intersection;simpl.
        repeat setoid_rewrite test_compatible_app.
        rsimpl;unfold unit.
        rewrite <- (test_compatible_Ξ _ (𝐒𝐏'_variables t2)).
        unfold test_compatible.
        setoid_rewrite <- fst_𝐒𝐏_variables;split.
        * intros ((h1&->)&h2&h3);
            rewrite <- test_compatible_nil in h3.
          repeat split;auto.
          intros x I;apply in_map_iff in I as ((a&b)&<-&I).
          apply h3 in I;destruct b;simpl in *;auto.
        * intros ((h1&h2&h3)&->);repeat split;auto.
          apply test_compatible_nil.
          intros (a,[|]) I;simpl;unfold mirror;simpl.
          -- apply h3,in_map_iff;eexists;split;eauto;reflexivity.
          -- apply h3,in_map_iff;eexists;split;eauto;reflexivity.
      + unfold interprete,to_lang_𝐖,T,unit,intersection;simpl.
        repeat setoid_rewrite test_compatible_app;tauto.
  Qed.

End languages.