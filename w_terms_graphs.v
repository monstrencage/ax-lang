(** This module is devoted to the translation of weak terms into graphs. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export sp_terms_graphs w_terms.

Section s.
  Variable X : Set.
  Variable Λ : decidable_set X.

  (** A weak graph is a pair of a graph and a set of tests. *)
  Definition weak_graph : Set := (graph nat X) * list X.

  (** We define the graph of a weak term using the graph of its
  series-parallel term, or the point graph [(0,[],0)] in the case of
  the constant [None]. *)
  Definition 𝕲w (x : 𝐖 X) : weak_graph :=
    match x with
    | (Some u,A) => (𝕲 u,A)
    | (None,A) => ((0,[],0),A)
    end.

  (** We extend the ordering of graphs to an ordering of weak graphs. *)
  Global Instance weak_graph_inf : SemSmaller weak_graph :=
    fun g h => snd h ⊆ snd g /\ snd g ⊨ fst g ⊲ fst h.

  (** This allows us to obtain the following theorem, equating the
  axiomatic containment of weak terms to the ordering of weak
  graphs. *)
  Theorem 𝐖_inf_is_weak_graph_inf : forall x y, x ≤ y <-> 𝕲w x ≲ 𝕲w y.
  Proof.
    intros ([u|],A) ([v|],B);
      unfold ssmaller,𝐖_inf,
      𝕲w,smaller,weak_graph_inf;simpl.
    - split;intros (I&h);split;auto.
      -- now apply is_morph_𝐒𝐏_inf.
      -- now apply 𝐒𝐏_inf_is_morph.
    - split;[intros []|].
      intros (_&(φ&φ0&φo&φe)).
      rewrite graph_input in φ0;rewrite graph_output in φo.
      unfold input,output in φ0,φo;simpl in *.
      pose proof (ln_non_zero u) as F;lia.
    - rewrite supid_hom_order;reflexivity.
    - split;[|tauto].
      intro I;split;auto;clear I.
      reflexivity.
  Qed.

  (** We can test the ordering of weak graph using a boolean
  function. *)
  Definition weak_graph_infb (g h : weak_graph) :=
    forallb (fun n => existsb (eqX n) (snd g)) (snd h)
            && ex_is_morphismb NatNum NatNum _ (snd g) (fst h) (fst g).
  
  Lemma weak_graph_infb_correct g h :
    weak_graph_infb g h = true <-> g ≲ h.
  Proof.
    unfold ssmaller,weak_graph_inf,weak_graph_infb,incl.
    rewrite andb_true_iff,ex_is_morphismb_correct,forallb_forall.
    assert (in_ex : forall T l (b : T),
               b ∈ l <-> exists x, x ∈ l /\ b = x)
      by (now intros T l b;split;
          [intro I;exists b;split|intros (x&I&->)]).
    setoid_rewrite existsb_exists.
    setoid_rewrite (@eqX_correct X Λ).
    setoid_rewrite <- in_ex.
    unfold hom_order;tauto.
  Qed.
End s.

Arguments 𝕲w {X} x.