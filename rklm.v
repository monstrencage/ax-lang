(** This module defines the full signature of language algebra we
consider here, and its finite complete axiomatization. We also define
here some normalisation functions, and list some of their properties. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Import tools lklc language.

Delimit Scope lat_scope with lat.
Open Scope lat_scope.

(** Strong induction on the length of a list. *)
Lemma len_induction {A} (P : list A -> Prop) :
  P [] -> (forall a l, (forall m, length m <= length l -> P m) -> P (a::l)) -> forall l, P l.
Proof.
  intros h0 hn l;remember (length l) as N.
  assert (len:length l <= N) by lia.
  clear HeqN;revert l len;induction N.
  - intros [|a l];simpl;try lia;auto.
  - intros [|a l];simpl;auto.
    intro len;apply hn;intros m;simpl;intros len'.
    apply IHN;lia.
Qed.

(** Induction principle for lists in the reverse order. *)
Lemma rev_induction {A} (P : list A -> Prop) :
  P [] -> (forall a l, P l -> P (l++[a])) -> forall l, P l.
Proof.
  intros;rewrite <- rev_involutive;induction (rev l);simpl;auto.
Qed.


Lemma Levi {A:Set} (u v u' v' : list A) :
  u++v = u'++v' -> exists w, (u = u'++w /\ v' = w++v) \/ (u'=u++w /\ v = w++v').
Proof.
  revert v u' v';induction u as [|a u];intros v u' v' E.
  - exists u';right;simpl in *;tauto.
  - destruct u' as [|b u'].
    + exists (a::u);left;simpl in *;subst;tauto.
    + simpl in E;inversion E as [[e ih]];clear E;subst.
      apply IHu in ih as (w&[h|h]);destruct h as (->&->);exists w;simpl;tauto.
Qed.


Section s.
  (** * Main definitions *)
  Variable X : Set.
  Variable dec_X : decidable_set X.

  (** [𝐄' X] is the type of expressions with variables ranging over the
  type [X]. They are built out of the constant [0], the
  concatenation (also called sequential product) [⋅], the intersection
  [∩], the union [+], and the non-zero iteration, denoted by [⁺]. *)
  Inductive 𝐄' : Set :=
  | 𝐄'_zero : 𝐄'
  | 𝐄'_var : X -> 𝐄'
  | 𝐄'_seq : 𝐄' -> 𝐄' -> 𝐄'
  | 𝐄'_inter : 𝐄' -> 𝐄' -> 𝐄'
  | 𝐄'_plus : 𝐄' -> 𝐄' -> 𝐄'
  | 𝐄'_iter : 𝐄' -> 𝐄'.

  Notation "x ⋅ y" := (𝐄'_seq x y) (at level 40) : lat_scope.
  Notation "x + y" := (𝐄'_plus x y) (left associativity, at level 50) : lat_scope.
  Notation "x ∩ y" := (𝐄'_inter x y) (at level 45) : lat_scope.
  Notation "x ⁺" := (𝐄'_iter x) (at level 25) : lat_scope.
  Notation " 0 " := 𝐄'_zero : lat_scope.

  (** The size of an expression is the number of nodes in its syntax
  tree. *)
  Global Instance size_𝐄' : Size 𝐄' :=
    fix 𝐄'_size (e: 𝐄') : nat :=
      match e with
      | 0 | 𝐄'_var _ => 1%nat
      | e + f | e ∩ f | e ⋅ f => S (𝐄'_size e + 𝐄'_size f)
      | e ⁺ => S (𝐄'_size e)
      end.
  (* begin hide *)
  Lemma 𝐄'_size_zero : |0| = 1%nat. trivial. Qed.
  Lemma 𝐄'_size_var a : |𝐄'_var a| = 1%nat. trivial. Qed.
  Lemma 𝐄'_size_seq e f : |e⋅f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐄'_size_inter e f : |e∩f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐄'_size_plus e f : |e+f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐄'_size_iter e : |e⁺| = S(|e|). trivial. Qed.
  Hint Rewrite 𝐄'_size_zero 𝐄'_size_var 𝐄'_size_seq
       𝐄'_size_inter 𝐄'_size_plus 𝐄'_size_iter
    : simpl_typeclasses.
  Fixpoint eqb e f :=
    match (e,f) with
    | (0,0) => true
    | (𝐄'_var a,𝐄'_var b) => eqX a b
    | (e⁺,f⁺) => eqb e f
    | (e1 + e2,f1 + f2)
    | (e1 ⋅ e2,f1 ⋅ f2)
    | (e1 ∩ e2,f1 ∩ f2) => eqb e1 f1 && eqb e2 f2
    | _ => false
    end.
  Lemma eqb_reflect e f : reflect (e = f) (eqb e f).
  Proof.
    apply iff_reflect;symmetry;split;
      [intro h;apply Is_true_eq_left in h;revert f h
      |intros <-;apply Is_true_eq_true];induction e;
        try destruct f;simpl;autorewrite with quotebool;firstorder.
    - apply Is_true_eq_true,eqX_correct in h as ->;auto.
    - erewrite IHe1;[|eauto]; erewrite IHe2;[|eauto];auto.
    - erewrite IHe1;[|eauto]; erewrite IHe2;[|eauto];auto.
    - erewrite IHe1;[|eauto]; erewrite IHe2;[|eauto];auto.
    - erewrite IHe;[|eauto];auto.
    - apply Is_true_eq_left,eqX_correct;auto.
  Qed.
  (* end hide *)

  (** If the set of variables [X] is decidable, then so is the set of
  expressions. _Note that we are here considering syntactic equality,
  as no semantic or axiomatic equivalence relation has been defined
  for expressions_. *)
  Global Instance 𝐄'_decidable_set : decidable_set 𝐄'.
  Proof. exact (Build_decidable_set eqb_reflect). Qed.

  (** The following are the axioms of the algebra of languages over
  this signature.*)
  Inductive ax : 𝐄' -> 𝐄' -> Prop :=
  (** [⟨𝐄',⋅,1⟩] is a monoid. *)
  | ax_seq_assoc e f g : ax (e⋅(f ⋅ g)) ((e⋅f)⋅g)
  (** [⟨𝐄',+,0⟩] is a commutative idempotent monoid. *)
  | ax_plus_com e f : ax (e+f) (f+e)
  | ax_plus_idem e : ax (e+e) e
  | ax_plus_ass e f g : ax (e+(f+g)) ((e+f)+g)
  | ax_plus_0 e : ax (e+0) e
  (** [⟨𝐄',⋅,+,1,0⟩] is an idempotent semiring. *)
  | ax_seq_0 e : ax (e⋅0) 0
  | ax_0_seq e : ax (0⋅e) 0
  | ax_plus_seq e f g: ax ((e + f)⋅g) (e⋅g + f⋅g)
  | ax_seq_plus e f g: ax (e⋅(f + g)) (e⋅f + e⋅g)
  (** [⟨𝐄',∩⟩] is a commutative and idempotent semigroup. *)
  | ax_inter_assoc e f g : ax (e∩(f ∩ g)) ((e∩f)∩g)
  | ax_inter_comm e f : ax (e∩f) (f∩e)
  | ax_inter_idem e : ax (e ∩ e) e
  (** [⟨𝐄',+,∩⟩] forms a distributive lattice, and [0] is absorbing for
  [∩]. *)
  | ax_plus_inter e f g: ax ((e + f)∩g) (e∩g + f∩g)
  | ax_inter_plus e f : ax ((e∩f)+e) e
  | ax_inter_0 e : ax (e∩0) 0
  (** The axioms for [⁺] are as follow: *)
  | ax_iter_left e : ax (e⁺) (e + e⋅e⁺)
  | ax_iter_right e : ax (e⁺) (e + e⁺ ⋅e).

  (** Additionally, we need these two implications: *)
  Inductive ax_impl : 𝐄' -> 𝐄' -> 𝐄' -> 𝐄' -> Prop:=
  | ax_right_ind e f : ax_impl (e⋅f + f) f (e⁺⋅f + f) f
  | ax_left_ind e f : ax_impl (f ⋅ e + f) f (f ⋅e⁺ + f) f.

  (** We use these axioms to generate an axiomatic equivalence
  relation and an axiomatic order relations. *)
  Inductive 𝐄'_eq : Equiv 𝐄' :=
  | eq_refl e : e ≡ e
  | eq_trans f e g : e ≡ f -> f ≡ g -> e ≡ g
  | eq_sym e f : e ≡ f -> f ≡ e
  | eq_plus e f g h : e ≡ g -> f ≡ h -> (e + f) ≡ (g + h)
  | eq_seq e f g h : e ≡ g -> f ≡ h -> (e ⋅ f) ≡ (g ⋅ h)
  | eq_inter e f g h : e ≡ g -> f ≡ h -> (e ∩ f) ≡ (g ∩ h)
  | eq_iter e f : e ≡ f -> (e⁺) ≡ (f⁺)
  | eq_ax e f : ax e f -> e ≡ f
  | eq_ax_impl e f g h : ax_impl e f g h -> e ≡ f -> g ≡ h.
  Global Instance 𝐄'_Equiv : Equiv 𝐄' := 𝐄'_eq.

  Global Instance 𝐄'_Smaller : Smaller 𝐄' := (fun e f => e + f ≡ f).

  Hint Constructors 𝐄'_eq ax ax_impl.

  Global Instance ax_equiv : subrelation ax equiv. 
  Proof. intros e f E;apply eq_ax,E. Qed.
  
  Close Scope lat_scope.
End s.
(* begin hide *)
Arguments 𝐄'_zero {X}.
Arguments eqb {X} {dec_X} e%lat f%lat.
Hint Constructors 𝐄'_eq ax ax_impl.
Hint Rewrite @𝐄'_size_zero @𝐄'_size_var @𝐄'_size_seq
     @𝐄'_size_inter @𝐄'_size_plus @𝐄'_size_iter
  : simpl_typeclasses.
(* end hide *)

Infix " ⋅ " := 𝐄'_seq (at level 40) : lat_scope.
Infix " + " := 𝐄'_plus (left associativity, at level 50) : lat_scope.
Infix " ∩ " := 𝐄'_inter (at level 45) : lat_scope.
Notation "x ⁺" := (𝐄'_iter x) (at level 25) : lat_scope.
Notation " 0 " := 𝐄'_zero : lat_scope.

Section language.
  (** * Language interpretation *)
  Context { X : Set }.

  (** We interpret expressions as languages in the obvious way: *)
  Global Instance to_lang_𝐄' {Σ}: semantics 𝐄' language X Σ :=
    fix to_lang_𝐄' σ e:=
      match e with
      | 0 => 0%lang
      | 𝐄'_var a => (σ a)
      | e + f => ((to_lang_𝐄' σ e) + (to_lang_𝐄' σ f))%lang
      | e ⋅ f => ((to_lang_𝐄' σ e) ⋅ (to_lang_𝐄' σ f))%lang
      | e ∩ f => ((to_lang_𝐄' σ e) ∩ (to_lang_𝐄' σ f))%lang
      | e⁺ => (to_lang_𝐄' σ e)⁺%lang
      end.

  (* begin hide *)
  Global Instance semSmaller_𝐄' : SemSmaller (𝐄' X) :=
    (@semantic_containment _ _ _ _ _).
  Global Instance semEquiv_𝐄' : SemEquiv (𝐄' X) :=
    (@semantic_equality _ _ _ _ _).
  Hint Unfold semSmaller_𝐄' semEquiv_𝐄' : semantics.
  
  Section rsimpl.
    Context { Σ : Set }{σ : 𝕬[X→Σ] }.
    Lemma 𝐄'_union e f : (⟦ e+f ⟧σ) = ((⟦e⟧σ) + ⟦f⟧σ)%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄'_prod e f :  (⟦ e⋅f ⟧σ) = ((⟦e⟧σ) ⋅ ⟦f⟧σ)%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄'_intersection e f : (⟦ e∩f ⟧σ) = ((⟦e⟧σ) ∩ ⟦f⟧σ)%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄'_iter_l e :  (⟦ e⁺⟧σ) = (⟦e⟧σ)⁺%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄'_variable a : (⟦𝐄'_var a⟧ σ) = σ a.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄'_empty : (⟦0⟧σ) = 0%lang.
    Proof. unfold interprete;simpl;auto. Qed.
  End rsimpl.
  Hint Rewrite @𝐄'_empty @𝐄'_variable @𝐄'_intersection
       @𝐄'_prod @𝐄'_union @𝐄'_iter_l
    : simpl_typeclasses.
  (* end hide *)
  Theorem klm_completeness : forall e f, e ≡ f <-> e ≃ f.
  Admitted.

End language.
Hint Rewrite @𝐄'_empty @𝐄'_variable @𝐄'_intersection
     @𝐄'_prod @𝐄'_union @𝐄'_iter_l
  : simpl_typeclasses.

Section translation.

  Context { X : Set }.

  Fixpoint to_𝐄 (e : 𝐄' (X*bool)) :=
    match e with
    | 0 => 0%expr
    | 𝐄'_var (a,true) => 𝐄_var a
    | 𝐄'_var (a,false) => 𝐄_var a ¯
    | e + f => (to_𝐄 e + to_𝐄 f)%expr
    | e ⋅ f => (to_𝐄 e ⋅ to_𝐄 f)%expr
    | e ∩ f => (to_𝐄 e ∩ to_𝐄 f)%expr
    | e ⁺ => (to_𝐄 e ⁺)%expr
    end.

  Fixpoint from_𝐄 (e : 𝐄 X) :=
    match e with
    | 0%expr => 0
    | 𝐄_var a => 𝐄'_var (a,true)
    | 𝐄_var a ¯ => 𝐄'_var (a,false)
    | (e + f)%expr => from_𝐄 e + from_𝐄 f
    | (e ⋅ f)%expr => from_𝐄 e ⋅ from_𝐄 f
    | (e ∩ f)%expr => from_𝐄 e ∩ from_𝐄 f
    | (e ⁺)%expr => from_𝐄 e ⁺
    | _ => 0
    end.

  Lemma to_from_id e : from_𝐄 (to_𝐄 e) = e.
  Proof. induction e as [|(a&[])| | | |];simpl;congruence. Qed.

  Lemma from_to_id e : is_clean e = true -> one_free e = true -> to_𝐄 (from_𝐄 e) = e.
  Proof.
    induction e as [ | | | | | |[]|];simpl;try discriminate||reflexivity;
      repeat rewrite andb_true_iff;firstorder congruence.
  Qed.

  Lemma to_𝐄_clean e : is_clean (to_𝐄 e) = true.
  Proof. induction e as [|(a&[])| | | |];simpl;try rewrite IHe1,IHe2;reflexivity||congruence. Qed.

  Lemma to_𝐄_one_free e : one_free (to_𝐄 e) = true.
  Proof. induction e as [|(a&[])| | | |];simpl;try rewrite IHe1,IHe2;reflexivity||congruence. Qed.

  Lemma to_𝐄_morph e f : e ≡ f -> to_𝐄 e ≡ to_𝐄 f.
  Proof.
    intros E;induction E;simpl;auto.
    - eauto.
    - destruct H;simpl;auto.
    - destruct H;simpl;eauto.
  Qed.

  (* Lemma from_𝐄_lang_eq e : forall σ, ⟦e⟧σ ≃ ⟦from_𝐄 e⟧σ. *)

  Section s.
    Context {Σ : Set}.
    Notation " ∙ " := (inr true).
    Notation " ∗ " := (inr false).
    Notation Σ' := (Σ + bool)%type.
    
    Definition pad' (u : list Σ) : list Σ' :=
      flat_map (fun a => [∙;∗;inl a]) u.
    
    Definition pad (u : list Σ) : list Σ' := [∙;∗;∗]++pad' u++[∙;∗;∗].
    
    Notation " u ↑ " := (pad u) (at level 20).
    
    Inductive valid : language Σ' :=
    | valid_pad u : valid (u↑)
    | valid_pad' u : valid (rev (u↑))
    | valid_pad_app u v : valid v -> valid (u↑++v)
    | valid_pad'_app u v : valid v -> valid (rev (u↑)++v).

    Hint Constructors valid.
    
    Lemma valid_app u v : valid u -> valid v -> valid (u++v).
    Proof.
      intro V;revert v;induction V as [u|u|u1 u2|u1 u2];intro v;repeat rewrite app_ass;auto.
    Qed.
    Hint Resolve valid_app.
    
    (* Fixpoint good (u : list Σ') := *)
    (*   match u with *)
    (*   | ∙::∗::∗::[] => true *)
    (*   | ∙::∗::inl _::u *)
    (*   | ∙::∗::∗::u => good u *)
    (*   | _ => false *)
    (*   end. *)

    Inductive Good : language Σ' :=
    | Good_pad u : Good (u↑)
    | Good_pad_app u v : Good v -> Good (u↑++v).
    Hint Constructors Good.
    
    Lemma Good_app u v : Good u -> Good v -> Good (u++v).
    Proof.
      intro G;revert v;induction G as [u|u1 u2];intro v;repeat rewrite app_ass;auto.
    Qed.
    Hint Resolve Good_app.
    
    (* Lemma Good_good u : good u = true <-> Good u. *)
    (* Proof. *)
    (*   split. *)
    (*   - induction u as [|s u ih] using len_induction;[discriminate|]. *)
    (*     destruct s as [|[]];try discriminate. *)
    (*     destruct u as [|[|[]] [|[|[]]u]]; try discriminate. *)
    (*     + intro G;simpl in G;apply ih in G;[|simpl;auto]. *)
    (*       inversion G as [v|v w G'];subst. *)
    (*       * apply (Good_pad (σ::v)). *)
    (*       * apply (Good_pad_app (σ::v) G').      *)
    (*     + intro G;destruct u. *)
    (*       * apply (Good_pad []). *)
    (*       * apply (Good_pad_app []),ih,G. *)
    (*         simpl;lia. *)
    (*   - intro G;induction G. *)
    (*     + induction u;simpl;auto. *)
    (*     + clear G. *)
    (*       revert v IHG;induction u;intro v;simpl. *)
    (*       * destruct v;auto. *)
    (*       * apply IHu. *)
    (* Qed. *)

    Lemma Good_valid : Good ≲ valid.
    Proof. intros u G;induction G;auto. Qed.
    
    Fixpoint decrypt (u : list Σ') :=
      match u with
      | ∙::∗::inl a::u =>a::decrypt u
      | ∙::∗::∗::u =>decrypt u
      | _ => []
      end.

    Definition map_lang {A B : Set} (f : list A -> list B) L : language B :=
      fun u => exists v, v ∊ L /\ u = f v.
    
    Notation " u ↓ " := (decrypt u) (at level 30).

    Notation " L ⇊ " := (map_lang decrypt L) (at level 30).

    
    Definition pad_map (σ : 𝕬[(X*bool) → Σ] ) : 𝕬[X → Σ'] :=
      fun x => (fun u => ((exists v, v ∊ σ (x,true) /\ Good u /\ u ↓ = v)
                    \/ (exists v, v ∊ σ (x,false) /\ Good (rev u) /\ (rev u) ↓ = v))).

    Notation " σ ⇈ " := (pad_map σ) (at level 20).

    Lemma pad_decrypt u : u ↑↓ = u.
    Proof. unfold pad;simpl;induction u;simpl;congruence. Qed.

    Inductive decode : list Σ' -> list (list Σ * bool) -> Prop :=
    | decode_pad u : decode (u↑) [(u,true)]
    | decode_pad' u : decode (rev (u↑)) [(u,false)]
    | decode_pad_app u v w : decode v w -> decode (u↑++v) ((u,true)::w)
    | decode_pad'_app u v w : decode v w -> decode (rev (u↑)++v) ((u,false)::w).

    Hint Constructors decode.
    
    Fixpoint encode c :=
      match c with
      | [] => []
      | (u,true)::c => u↑++encode c
      | (u,false)::c => rev(u↑)++encode c
      end.

    Lemma decode_valid u : valid u <-> exists v, decode u v.
    Proof.
      split.
      - intro V;induction V as [u|u|u1 u2|u1 u2].
        + exists [(u,true)];auto.
        + exists [(u,false)];auto.
        + destruct IHV as (v&D).
          exists ((u1,true)::v);auto.
        + destruct IHV as (v&D).
          exists ((u1,false)::v);auto.
      - intros (v&D);induction D;auto.
    Qed.

    Lemma decode_encode u v : decode u v <-> v <> [] /\ u = encode v.
    Proof.
      split.
      - intro D;induction D;simpl;auto.
        + rewrite app_nil_r;split;[discriminate|reflexivity].
        + rewrite app_nil_r;split;[discriminate|reflexivity].
        + destruct IHD as (_&->);split;[discriminate|reflexivity].
        + destruct IHD as (_&->);split;[discriminate|reflexivity].
      - intros (N&->).
        induction v as [|(u&[]) [|s v]].
        + simpl;tauto.
        + simpl;rewrite app_nil_r;auto.
          apply (decode_pad u).
        + assert (N': s::v <> []) by discriminate.
          generalize dependent (s::v);clear s v.
          intros;simpl.
          apply IHv in N';auto.
          apply (decode_pad_app u N').
        + simpl;rewrite app_nil_r;auto.
          repeat rewrite app_ass;simpl.
          replace [∗;∗;∙] with (rev [∙;∗;∗ : Σ']) by reflexivity.
          rewrite <- rev_app_distr.
          apply (decode_pad' u).
        + assert (N': s::v <> []) by discriminate.
          generalize dependent (s::v);clear s v.
          intros;simpl.
          rewrite (app_ass _ _ [∙]);simpl;rewrite (app_ass _ _ [∗;∙]);simpl.
          replace [∗;∗;∙] with (rev [∙;∗;∗ : Σ']) by reflexivity.
          rewrite <- rev_app_distr.
          apply IHv in N'.
          apply (decode_pad'_app u),N'.
    Qed.

    Lemma pad_inj u v : u ↑ = v ↑ -> u = v.
    Proof. intro E;rewrite <- pad_decrypt,<- E,pad_decrypt;reflexivity. Qed.    

    Lemma pad_app_inj u u' v v' : u↑++u' = v ↑++v' -> u = v /\ u' = v'.
    Proof.
      unfold pad;intro E;inversion E as [[E']];clear E.
      revert v E';induction u;intros w.
      - destruct w as [|? []];simpl;intro E;inversion E;tauto.
      - destruct w as [|];simpl;intro E;inversion E;subst.
        apply IHu in H1 as (?&?);subst;tauto.
    Qed.

    Lemma pad_app_inj' u u' v v' : u'++u↑ = v'++v ↑ -> u' = v' /\ u = v.
    Proof.
      revert v';induction u';intros w.
      - destruct w as [|? []];simpl;intro E.
        + apply pad_inj in E;subst;auto.
        + destruct u,v;inversion E.
        + destruct u,l;inversion E;subst.
          * assert (L : length (l++v↑) = 3) by (rewrite <- H3;reflexivity).
            rewrite app_length in L;destruct v;simpl in *;lia.
          * destruct l as [|? [|? []]];inversion H3;subst.
            exfalso;revert H4;clear.
            revert u v;induction l as [|? [|? []] ih] using len_induction;intros [] v;try discriminate.
            -- destruct v;discriminate.
            -- simpl;intro E;inversion E;subst.
               symmetry in H3;apply app_eq_nil in H3 as (_&F);discriminate.
            -- simpl;intro E;inversion E;subst.
               apply ih in H3;simpl;auto.
      - destruct w as [|];simpl;intro E;inversion E;subst.
        destruct u' as [|? []];inversion H1;subst.
        + exfalso;revert H3;clear.
          revert u v;induction l as [|? [|? []] ih] using len_induction;intros u [];try discriminate.
          -- destruct u;discriminate.
          -- simpl;intro E;inversion E;subst.
             apply app_eq_nil in H3 as (_&F);discriminate.
          -- simpl;intro E;inversion E;subst.
             apply ih in H3;simpl;auto.
        + apply IHu' in H1 as (->&->);split;reflexivity.
    Qed.
    
    Lemma pad_no_app u v w : u↑++v = w ↑ -> v = [].
    Proof. rewrite <- (app_nil_r (w↑));intro E;apply pad_app_inj in E as (?&?);subst;auto. Qed.

    Lemma valid_rev u : valid u -> valid (rev u).
    Proof.
      intro V;induction V;auto.
      - rewrite rev_involutive;auto.
      - rewrite rev_app_distr;auto.
      - rewrite rev_app_distr,rev_involutive;auto.
    Qed.

    Opaque pad.
    Lemma open_pad u : u ↑ = [∙; ∗; ∗] ++ pad' u ++ [∙; ∗; ∗].
    Proof. reflexivity. Qed.
    
    Lemma encode_inj u v : encode u = encode v -> u = v.
    Proof.
      revert v;induction u as [|(u&[]) l];intros [|(v&[]) m];simpl;auto;try discriminate.
      - rewrite open_pad,rev_app_distr,rev_app_distr;discriminate.
      - intros E;apply pad_app_inj in E as (->&E).
        f_equal;apply IHl,E.
      - repeat rewrite (open_pad v),rev_app_distr,rev_app_distr;discriminate. 
      - rewrite open_pad,rev_app_distr,rev_app_distr;discriminate.
      - repeat rewrite (open_pad u),rev_app_distr,rev_app_distr;discriminate. 
      - intro E.
        assert (E' : rev (rev (u ↑) ++ encode l) = rev (rev (v ↑) ++ encode m))
          by (rewrite E;reflexivity).
        repeat rewrite rev_app_distr,rev_involutive in E'.
        apply pad_app_inj' in E' as (E1&->).
        f_equal;apply IHl.
        rewrite <- rev_involutive,<-E1,rev_involutive;reflexivity.
    Qed.
    
    Lemma decode_inj u v1 v2 : decode u v1 -> decode u v2 -> v1 = v2.
    Proof.
      intros D1 D2;apply decode_encode in D1 as (_&->).
      apply decode_encode in D2 as (_&E);apply encode_inj,E.
    Qed.

    Lemma valid_expr e σ : 
      one_free e = true -> is_clean e = true -> (⟦e⟧ σ ⇈) ≲ valid.
    Proof.
      induction e;simpl;repeat rewrite andb_true_iff;rsimpl.
      - discriminate.
      - intros _ _ u F;exfalso;apply F.
      - intros _ _ u [(_&_&h&_)|(_&_&h&_)].
        + apply Good_valid,h.
        + apply Good_valid,valid_rev in h.
          rewrite rev_involutive in h;assumption.
      - intros (O1&O2) (C1&C2) u (u1&u2&I1&I2&->).
        apply IHe1 in I1;apply IHe2 in I2;try assumption;eauto.
      - intros (O1&O2) (C1&C2) u (I1&I2).
        apply IHe1;auto.
      - intros (O1&O2) (C1&C2) u [I|I];[apply IHe1|apply IHe2];auto.
      - destruct e;try discriminate;simpl;rsimpl.
        intros _ _ u [(_&_&h&_)|(_&_&h&_)].
        + apply Good_valid,valid_rev in h.
          rewrite rev_involutive in h;assumption.
        + rewrite rev_involutive in h;apply Good_valid,h.
      - intros O C;pose proof (IHe O C) as IHe1;clear O C IHe.
        rewrite 𝐄_iter_l;intros u (n&In);revert u In.
        induction n as [|n IHe2].
        + intros u (u1&u2&Iu&->&->).
          rewrite app_nil_r;apply IHe1,Iu.
        + intros u (u1&u2&I1&I2&->).
          apply IHe1 in I1;apply IHe2 in I2;try assumption;eauto.
    Qed.

    Lemma decode_app u v u' v' : decode u v -> decode u' v' -> decode (u++u') (v++v').
    Proof.
      intro D;revert u' v';induction D;intros u' v' D';simpl;auto.
      - rewrite app_ass; apply IHD in D';auto.
      - rewrite app_ass; apply IHD in D';auto.
    Qed.

    Lemma decode_rev u v :
      decode u v -> decode (rev u) (rev (map (fun p => ((fst p),negb (snd p))) v)).
    Proof.
      intro D;induction D;simpl;auto.
      - rewrite rev_involutive;auto.
      - repeat rewrite rev_app_distr.
        apply decode_app;auto.
      - repeat rewrite rev_app_distr.
        apply decode_app;auto.
        rewrite rev_involutive;auto.
    Qed. 

    Lemma Good_decode u : Good u <-> exists v, decode u v /\ forallb snd v = true.
    Proof.
      split.
      - intro G;induction G.
        + exists [(u,true)];simpl;auto.
        + destruct IHG as (w&D&E).
          exists ((u,true)::w);simpl;auto.
      - intros (v&D&E).
        apply decode_encode in D as (N&->).
        induction v as [|(?&[]) v];[tauto| |discriminate].
        simpl in *;auto.
        destruct v.
        + simpl;rewrite app_nil_r;auto.
        + apply Good_pad_app,IHv;discriminate||auto.
    Qed.

    Lemma Good_rev u : Good u -> ~ Good (rev u).
    Proof.
      intros G G'.
      apply Good_decode in G as (w&D&F).
      apply Good_decode in G' as (w'&D'&F').
      apply decode_rev in D.
      apply (decode_inj D') in D;subst.
      apply decode_encode in D' as (N&_).
      revert N F F';clear.
      induction w as [|(?&[])w];simpl;[tauto| |discriminate].
      rewrite forallb_app;simpl;rewrite andb_false_r;discriminate.
    Qed.
    
    Lemma pad_unpad_true (σ : 𝕬[(X*bool) → Σ]) x :
      σ (x,true) ≃ (σ⇈ x ∩ Good)%lang ⇊.
    Proof.
      intro u;split.
      - intros I.
        exists (u↑);split;[|rewrite pad_decrypt;reflexivity].
        split;auto.
        left;exists u;rewrite pad_decrypt;auto using Good_pad.
      - intros (w&([(v&I&G'&<-)|(v&I&G'&E)]&G)&->).
        + assumption.
        + exfalso;subst.
          apply Good_rev in G;tauto.
    Qed.
    
    Lemma pad_unpad_false (σ : 𝕬[(X*bool) → Σ]) x :
      σ (x,false) ≃ ((σ⇈ x ¯ ∩ Good)%lang)⇊.
    Proof.
      intro u;split.
      - intros I;exists (u↑);split;[|rewrite pad_decrypt;reflexivity].
        split;auto.
        right;exists u;rewrite rev_involutive.
        rewrite pad_decrypt;auto.
      - intros (w&([(v&I&G'&E)|(v&I&G'&E)]&G)&->).
        + apply Good_rev in G;tauto.
        + subst;rewrite rev_involutive in I;assumption.
    Qed.

    Lemma pad_decrypt_app u v : (u↑++v) ↓ = u++v↓.
    Proof.
      rewrite open_pad;simpl.
      revert v;induction u;intro v;simpl;auto.
      rewrite IHu;reflexivity.
    Qed.
    
    Lemma decrypt_app_Good u v : Good u -> (u++v) ↓ = u↓++v↓.
    Proof.
      intro G;revert v;induction G as [u|u w];intro v.
      - rewrite pad_decrypt,pad_decrypt_app;reflexivity.
      - rewrite app_ass,pad_decrypt_app,pad_decrypt_app,IHG.
        rewrite app_ass;reflexivity.
    Qed.
    
    Lemma decrypt_Good u v : Good u -> decode u v -> u ↓ = concat (map fst v).
    Proof.
      intros G D;apply Good_decode in G as (w&D'&T).
      apply (decode_inj D) in D';subst.
      revert T;induction D;try discriminate.
      - simpl;rewrite app_nil_r,pad_decrypt;reflexivity.
      - simpl;intro h;rewrite <- IHD by assumption.
        apply pad_decrypt_app.
    Qed.

    Inductive bigger : relation (list Σ') :=
    | big_refl u : Good u -> bigger u u
    | big_trans u v w : bigger u v -> bigger v w -> bigger u w
    | big_app u1 u2 v1 v2: bigger u1 u2 -> bigger v1 v2 -> bigger (u1++v1) (u2++v2)
    | big_nil u v : bigger ((u++v)↑) (u↑++v↑).

    Hint Constructors bigger.

    Lemma bigger_good u v : bigger u v -> Good u /\ Good v.
    Proof.
      intros B;induction B;try auto||tauto.
      split;apply Good_app;tauto.
    Qed.

    Lemma bigger_decrypt u v : bigger u v ->  u ↓ = v ↓.
    Proof.
      intros B;induction B;auto.
      - etransitivity;eassumption.
      - apply bigger_good in B1 as (G1&G2).
        apply bigger_good in B2 as (G1'&G2').
        repeat rewrite decrypt_app_Good by auto.
        congruence.
      - repeat rewrite decrypt_app_Good by (apply Good_pad).
        repeat rewrite pad_decrypt;reflexivity.
    Qed.
    
    Lemma valid_nil : ~ valid [].
    Proof.
      intro V;inversion V.
      - rewrite open_pad in H0;simpl in H0;rewrite rev_app_distr in H0;discriminate.
      - rewrite open_pad in H;simpl in H;rewrite rev_app_distr in H;discriminate.
    Qed.
    
    
    Lemma Good_app_valid u v : valid u -> valid v -> Good (u++v) -> Good u /\ Good v.
    Proof.
      remember (u++v) as w;intros V1 V2 G;revert u v Heqw V1 V2;induction G as [u|u v];
        intros u1 u2 E V1 V2.
      - exfalso.
        inversion V1;subst.
        + symmetry in E;apply pad_no_app in E;subst;apply valid_nil;auto.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
        + rewrite app_ass in E.
          symmetry in E;apply pad_no_app in E;subst;apply valid_nil;auto.
          apply app_eq_nil in E as (->&->);tauto.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
      - apply Levi in E as (w&[(E&->)|(->&->)]).
        + cut (u1 = u↑ /\ w = []);[intros (->&->);simpl in *;auto|].
          revert E V1;clear;intros E V.
          inversion V;subst.
          * symmetry in E;pose proof (pad_no_app E);subst.
            rewrite app_nil_r in E;apply pad_inj in E;subst;auto.
          * repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
          * rewrite app_ass in E.
            symmetry in E;apply pad_no_app in E;subst;exfalso;apply valid_nil;auto.
            apply app_eq_nil in E as (->&->);tauto.
          * repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
        + destruct w as [|x w'];[rewrite app_nil_r in *;simpl in *;auto|].
          remember (x::w') as w;assert (N: w <> []) by (subst;discriminate);clear x w' Heqw.
          destruct (IHG w u2) as (G1&G2);auto.
          apply decode_valid in V1 as (v&V1).
          apply decode_encode in V1 as (N'&V1).
          destruct v as [|(u'&[])];try tauto.
          * simpl in *.
            apply pad_app_inj in V1 as (<-&->).
            apply decode_valid;exists v.
            apply decode_encode;split;[|auto].
            destruct v;[tauto|discriminate].
          * simpl in V1.
            repeat rewrite open_pad in V1;simpl in V1;rewrite rev_app_distr in V1;discriminate.
    Qed.

    Lemma rev_inv {A:Set} (u v: list A): u = v -> rev u = rev v.
    Proof. intro;subst;reflexivity. Qed.

    Lemma Levi_valid u1 u2 v1 v2 :
      u1++u2 = v1++v2 -> valid u1 -> valid u2 -> valid v1 -> valid v2 ->
      (u1=v1/\u2=v2) \/ exists w, valid w /\ ((u1 = v1++w /\ v2 = w++u2) \/ (v1 = u1++w /\ u2 = w++v2)).
    Proof.
      intros E V;revert u2 v1 v2 E;induction V as [u1|u1|u1 w1|u1 w1];intros u2 v1 v2 E Vu2 Vv1 Vv2.
      - inversion Vv1;subst.
        + apply pad_app_inj in E as (->&->).
          tauto.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
        + rewrite app_ass in E.
          apply pad_app_inj in E as (->&->).
          right;exists v;split;[auto|right;split;auto].
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
      - inversion Vv1;subst.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
        + apply rev_inv in E;repeat rewrite rev_app_distr,rev_involutive in E.
          apply pad_app_inj' in E as (E&->).
          apply rev_inv in E;repeat rewrite rev_involutive in E;subst;auto.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
        + rewrite app_ass in E.
          apply rev_inv in E;repeat rewrite rev_app_distr,rev_involutive in E.
          apply pad_app_inj' in E as (E&->).
          apply rev_inv in E;repeat rewrite rev_involutive in E;subst;auto.
          right;exists v;split;[auto|right;split;auto].
      - inversion Vv1;subst;rewrite app_ass in *.
        + apply pad_app_inj in E as (->&<-).
          right;exists w1;auto.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
        + rewrite app_ass in E.
          apply pad_app_inj in E as (->&E).
          destruct (IHV u2 v v2) as [(->&->)|(w&Vw&[(->&->)|(->&->)])];auto.
          * right;exists w;repeat rewrite app_ass;auto.
          * right;exists w;repeat rewrite app_ass;auto.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
      - inversion Vv1;subst.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
        + rewrite app_ass in E;apply rev_inv in E;repeat rewrite rev_app_distr in E;
            repeat rewrite rev_involutive in E.
          apply pad_app_inj' in E as (E&<-).
          apply rev_inv in E;repeat rewrite rev_involutive in E;subst;auto.
          repeat rewrite <- rev_app_distr in *;repeat rewrite rev_involutive in *.
          right;exists w1;auto.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
        + rewrite app_ass in E;apply rev_inv in E;repeat rewrite rev_app_distr in E;
            repeat rewrite rev_involutive in E.
          rewrite <- app_ass in E.
          apply pad_app_inj' in E as (E&<-).
          apply rev_inv in E;repeat rewrite rev_app_distr in E;
            repeat rewrite rev_involutive in E;subst;auto.
          destruct (IHV u2 v v2) as [(->&->)|(w&Vw&[(->&->)|(->&->)])];auto.
          * right;exists w;repeat rewrite app_ass;auto.
          * right;exists w;repeat rewrite app_ass;auto.
    Qed.

    Lemma bigger_app_inv u1 u2 v :
      bigger (u1++u2) v -> valid u1 -> valid u2 ->
      exists v1 v2, v = v1 ++ v2 /\ bigger u1 v1 /\ bigger u2 v2.
    Proof.
      cut (forall u, bigger u v -> forall u1 u2, u = u1++u2 -> valid u1 -> valid u2 ->
                                      exists v1 v2, v = v1 ++ v2 /\ bigger u1 v1 /\ bigger u2 v2);
        [intros h B V1 V2;apply (h (u1++u2));auto|clear u1 u2].
      intros u B;induction B;intros w1 w2 E V1 V2.
      - subst;eapply Good_app_valid in H as (G1&G2);auto.
        exists w1,w2;repeat split;apply big_refl;auto.
      - subst;destruct (IHB1 w1 w2) as (v1&v2&->&Bv1&Bv2);auto.
        pose proof (bigger_good Bv1) as (_&G1).
        pose proof (bigger_good Bv2) as (_&G2).
        apply Good_valid in G1;apply Good_valid in G2.
        destruct (IHB2 v1 v2) as (v1'&v2'&->&Bv1'&Bv2');auto.
        exists v1',v2';repeat split.
        + eapply big_trans;eauto.
        + eapply big_trans;eauto.
      - pose proof (bigger_good B1) as (Gu1&Gu2).
        pose proof (bigger_good B2) as (Gv1&Gv2).
        apply Good_valid in Gu1;apply Good_valid in Gu2.
        apply Good_valid in Gv1;apply Good_valid in Gv2.
        apply Levi_valid in E as [(->&->)|(w&Vw&[(->&->)|(->&->)])];auto.
        + exists u2,v2;auto.
        + destruct (IHB1 w1 w) as (m1&m2&->&B3&B4);auto.
          exists m1,(m2++v2);repeat split;auto.
          apply app_ass.
        + destruct (IHB2 w w2) as (m1&m2&->&B3&B4);auto.
          exists (u2++m1),m2;repeat split;auto.
          symmetry;apply app_ass.
      - exfalso;inversion V1;subst.
        + rewrite <- (app_nil_r ((u++v)↑)) in E.
          apply pad_app_inj in E as (_&<-).
          apply valid_nil in V2;tauto.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
        + rewrite <- (app_nil_r ((u++v)↑)) in E;rewrite app_ass in E.
          apply pad_app_inj in E as (_&E).
          symmetry in E;apply app_eq_nil in E as (->&->).
          apply valid_nil in V2;tauto.
        + repeat rewrite open_pad in E;simpl in E;rewrite rev_app_distr in E;discriminate.
    Qed.
    
    Lemma pad_expr_switch σ e u v :
      one_free e = true -> is_clean e = true ->
      bigger u v -> (⟦e⟧ σ⇈) u -> (⟦e⟧ σ⇈) v.
    Proof.
      intros O C B I;revert O C u I v B;induction e;simpl;rsimpl;
        repeat rewrite andb_true_iff;try discriminate||intros (O1&O2)(C1&C2) u I v B
                                     ||intros O C u I v B.
      - apply I.
      - destruct I as [(w&I&G&<-)|(w&I&G&<-)].
        + left;exists (u↓);repeat split;auto.
          * eapply bigger_good;eassumption.
          * symmetry;apply bigger_decrypt;auto.
        + exfalso.
          apply bigger_good in B as (G'&_).
          apply Good_rev in G';tauto.
      - destruct I as (u1&u2&I1&I2&->).
        apply bigger_app_inv with (u1:= u1) (u2:= u2) in B.
        + destruct B as (v1&v2&->&B1&B2).
          exists v1,v2;repeat split;auto.
          * eapply IHe1,B1;auto.
          * eapply IHe2,B2;auto.
        + eapply valid_expr,I1;auto.
        + eapply valid_expr,I2;auto.
      - destruct I as (I1&I2).
        split;[eapply IHe1|eapply IHe2];eauto.
      - destruct I as [I|I];[left;eapply IHe1|right;eapply IHe2];eauto.
      - destruct e;try discriminate.
        destruct I as [(w&I&G&<-)|(w&I&G&<-)].
        + exfalso.
          apply bigger_good in B as (G'&_).
          apply Good_rev in G';tauto.
        + rewrite rev_involutive in *.
          right;exists (u↓);repeat split;auto.
          * rewrite rev_involutive;eapply bigger_good;eassumption.
          * symmetry;apply bigger_decrypt;auto.
            rewrite rev_involutive;eauto.
      - rewrite 𝐄_iter_l in *.
        destruct I as (n&I);exists n.
        revert u v B I;induction n.
        + intros u v B (u1&u2&I&->&->);exists v,nil;rewrite app_nil_r in *;repeat split.
          eapply IHe;eauto.
        + intros u v B (u1&u2&I1&I2&->).
          apply bigger_app_inv with (u1:= u1) (u2:= u2) in B.
          * destruct B as (v1&v2&->&B1&B2).
            exists v1,v2;repeat split;auto.
            -- eapply IHe,B1;auto.
            -- eapply IHn;eauto.
          * eapply valid_expr,I1;auto.
          * cut ((⟦ e ⁺%expr⟧ σ ⇈) u2);[apply valid_expr;auto|exists n;assumption].
    Qed.

    Lemma Good_pad_app_iff u v :  Good (u↑++v) <-> v = [] \/ Good v.
    Proof.
      split;[|intros [->|I];try rewrite app_nil_r;auto].
      repeat rewrite Good_decode.
      intros (w&E&F).
      remember (u↑++v) as u'.
      inversion E;subst.
      - symmetry in H;pose proof (pad_no_app H);subst;auto.
      - exfalso.
        rewrite open_pad in H;simpl in H;rewrite rev_app_distr in H;discriminate.
      - apply pad_app_inj in H0 as (->&->).
        right;exists w0;tauto.
      - exfalso.
        rewrite open_pad in H0;simpl in H0;rewrite rev_app_distr in H0;discriminate.
    Qed.

    Lemma bigger_min v : Good v -> bigger ((v ↓) ↑) v.
    Proof.
      intros G;induction G.
      - rewrite pad_decrypt;apply big_refl;auto.
      - rewrite decrypt_app_Good by auto.
        cut (bigger (u ↑ ++ v ↓↑) (u ↑ ++ v));auto.
        intro B;eapply big_trans,B.
        rewrite pad_decrypt.
        apply big_nil.
    Qed. 
    
    Lemma bigger_lub u v : Good u -> Good v -> u↓=v↓ -> exists w, bigger u w /\ bigger v w.
    Proof.
      revert u v;intros u1 u2 G1 G2 E;pose proof G1 as D1;pose proof G2 as D2.
      apply Good_decode in D1 as (v1&D1&F1).
      apply Good_decode in D2 as (v2&D2&F2).
      rewrite (decrypt_Good G1 D1) in E.
      rewrite (decrypt_Good G2 D2) in E.
      apply decode_encode in D1 as (N1&->).
      apply decode_encode in D2 as (N2&->).
      clear G1 G2.
      remember (length (v1++v2)) as N.
      assert (hn: length (v1++v2) <= N) by lia.
      clear HeqN.
      revert v1 v2 hn E N1 N2 F1 F2;induction N;intros [|(x&[]) v1]  [|(y&[]) v2];intros;
        try discriminate||tauto||(simpl in *;lia).
      clear N1 N2;simpl in *.
      case_eq v2;[intros -> |intros xx yy EE;assert (N2 : xx::yy <> []) by discriminate;
                            rewrite <- EE in *;clear xx yy EE];
      (case_eq v1;[intros -> |intros xx yy EE;assert (N1 : xx::yy <> []) by discriminate;
                             rewrite <- EE in *;clear xx yy EE]);simpl in *;repeat rewrite app_nil_r in *.
      - subst;exists (y↑);split;apply big_refl;auto.
      - subst.
        exists (x ↑ ++ encode v1);split.
        + apply big_refl.
          apply Good_pad_app.
          apply Good_decode;exists v1;split;auto.
          apply decode_encode;split;auto.
        + replace (concat (map fst v1)) with (encode v1↓).
          * eapply big_trans.
            -- apply big_nil.
            -- apply big_app.
               ++ apply big_refl;auto.
               ++ apply bigger_min.
                  apply Good_decode;exists v1;split;auto.
                  apply decode_encode;split;auto.
          * revert F1;clear;induction v1 as [|(?&[])];simpl;auto;try discriminate.
            intro;rewrite decrypt_app_Good;auto.
            rewrite pad_decrypt,IHv1;auto.
      - subst.
        exists (y ↑ ++ encode v2);split.
        + replace (concat (map fst v2)) with (encode v2↓).
          * eapply big_trans.
            -- apply big_nil.
            -- apply big_app.
               ++ apply big_refl;auto.
               ++ apply bigger_min.
                  apply Good_decode;exists v2;split;auto.
                  apply decode_encode;split;auto.
          * revert F2;clear;induction v2 as [|(?&[])];simpl;auto;try discriminate.
            intro;rewrite decrypt_app_Good;auto.
            rewrite pad_decrypt,IHv2;auto.
        + apply big_refl.
          apply Good_pad_app.
          apply Good_decode;exists v2;split;auto.
          apply decode_encode;split;auto.
      - apply Levi in E as (w&[(->&E)|(->&E)]).
        + destruct (IHN ((w,true)::v1) v2) as (M&B1&B2);simpl;auto.
          * rewrite app_length in *;simpl in *;lia.
          * discriminate.
          * simpl in *.
            apply bigger_app_inv in B1 as (M1&M2&->&B11&B12);
              [|apply bigger_good in B1 as (h&_);apply Good_valid in h;auto
               |apply decode_valid;exists v1;apply decode_encode;split;auto].
            exists (y↑++M1++M2);split.
            -- eapply big_trans.
               ++ apply big_app;[apply big_nil|].
                  apply big_refl.
                  apply Good_decode;exists v1;split;auto.
                  apply decode_encode;split;auto.
               ++ rewrite app_ass.
                  apply big_app;[apply big_refl;auto|].
                  apply big_app;auto.
            -- apply big_app;auto.
        + destruct (IHN v1 ((w,true)::v2)) as (M&B1&B2);simpl;auto.
          * rewrite app_length in *;simpl in *;lia.
          * discriminate.
          * simpl in *.
            apply bigger_app_inv in B2 as (M1&M2&->&B11&B12);
              [|apply bigger_good in B2 as (h&_);apply Good_valid in h;auto
               |apply decode_valid;exists v2;apply decode_encode;split;auto].
            exists (x↑++M1++M2);split.
            -- apply big_app;auto.
            -- eapply big_trans.
               ++ apply big_app;[apply big_nil|].
                  apply big_refl.
                  apply Good_decode;exists v2;split;auto.
                  apply decode_encode;split;auto.
               ++ rewrite app_ass.
                  apply big_app;[apply big_refl;auto|].
                  apply big_app;auto.
    Qed.
    
    Lemma pad_unpad_expr σ e :
      one_free e = true -> is_clean e = true ->
      ⟦from_𝐄 e⟧σ ≃ (((⟦e⟧ σ⇈) ∩ Good)%lang)⇊.
    Proof.
      induction e;simpl;repeat rewrite andb_true_iff.
      - discriminate.
      - intros _ _ u;rsimpl;split.
        + intro F;exfalso;apply F.
        + intros (?&(F&_)&_);apply F.
      - intros _ _;rsimpl.
        apply pad_unpad_true.
      - intros (O1&O2) (C1&C2);rsimpl.
        rewrite IHe1,IHe2 by assumption.
        intros u;split.
        + intros (u1&u2&I1&I2&->).
          destruct I1 as (v1&(I1&G1)&->).
          destruct I2 as (v2&(I2&G2)&->).
          exists (v1++v2);split.
          * split.
            -- exists v1,v2;tauto.
            -- apply Good_app;auto.
          * symmetry;apply decrypt_app_Good;assumption.
        + intros (w&((u1&u2&I1&I2&->)&G)&->).
          apply Good_app_valid in G as (G1&G2).
          * rewrite decrypt_app_Good;auto.
            exists (u1↓),(u2↓);repeat split;auto.
            -- exists u1;repeat split;assumption.
            -- exists u2;repeat split;assumption.
          * eapply valid_expr,I1;assumption.
          * eapply valid_expr,I2;assumption.
      - intros (O1&O2)(C1&C2);rsimpl;rewrite IHe1,IHe2;auto.
        intro u;split.
        + intros ((v&(I1&G1)&->)&(v'&(I2&G2)&E)).
          destruct (bigger_lub G1 G2 E) as (w&B1&B2).
          exists w;repeat split.
          * eapply pad_expr_switch,I1;auto.
          * eapply pad_expr_switch,I2;auto.
          * apply bigger_good in B1;tauto.
          * apply bigger_decrypt,B1.
        + intros (v&((I1&I2)&G)&->).
          split;exists v;repeat split;auto.
      - intros (O1&O2)(C1&C2);rsimpl;rewrite IHe1,IHe2;auto.
        intros u;split.
        + intros [I|I];destruct I as (v&(I&G)&->);exists v;repeat split;auto.
          * left;assumption.
          * right;assumption.
        + intros (v&([I|I]&G)&->);[left|right];exists v;repeat split;auto.
      - destruct e;try discriminate.
        intros _ _;rsimpl.
        apply pad_unpad_false.
      - rsimpl;intros O C;pose proof (IHe O C) as ->.
        rewrite 𝐄_iter_l.
        intro u;unfold iter.
        cut (forall n, (((((⟦ e ⟧ σ ⇈) ∩ Good) ⇊) ^{ S n})%lang u) <->
                  (((fun u0 : list Σ' => ((⟦ e ⟧ σ ⇈) ^{ S n}) u0) ∩ Good)%lang ⇊) u);
          [intros h;setoid_rewrite h;clear;
           split;[intros (n&v&(I&G)&->);exists v;repeat split;auto;exists n;auto
                 |intros (v&((n&I)&G)&->);exists n,v;repeat split;auto]|].
        pose proof (IHe O C) as IHe1;clear IHe.
        intro n;revert u;induction n as [|n IHe2];intros u;split.
        + intros (u1&u2&(v&(I&G)&->)&->&->).
          exists v;repeat split;auto using app_nil_r.
          exists v,[];repeat split;auto using app_nil_r.
        + intros (v&((u1&u2&I&->&->)&G)&->).
          rewrite app_nil_r in *.
          exists (u1↓),[];repeat split;auto using app_nil_r.
          exists u1;repeat split;auto.
        + intros (u1&u2&I1&I2&->).
          destruct I1 as (v1&(I1&G1)&->).
          apply IHe2 in I2.
          destruct I2 as (v2&(I2&G2)&->).
          exists (v1++v2);split.
          * split.
            -- exists v1,v2;tauto.
            -- apply Good_app;auto.
          * symmetry;apply decrypt_app_Good;assumption.
        + intros (w&((u1&u2&I1&I2&->)&G)&->).
          apply Good_app_valid in G as (G1&G2).
          * rewrite decrypt_app_Good;auto.
            exists (u1↓),(u2↓);repeat split;auto.
            -- exists u1;repeat split;assumption.
            -- apply IHe2;exists u2;repeat split;assumption.
          * eapply valid_expr,I1;assumption.
          * cut ((⟦e ⁺%expr⟧ σ⇈) u2);[|exists n;apply I2].
            intro I;eapply valid_expr,I;assumption.
    Qed.
  End s.
  
  Proposition Completeness_one_free :
    forall e f : 𝐄 X , one_free e = true -> one_free f = true -> e ≡ f <-> e ≃ f.
  Proof.
    intros e f he hf.
    split.
    - apply 𝐄_eq_equiv_lang.
    - intros E.
      rewrite (comb_equiv e),(comb_equiv f).
      rewrite (𝐄_eq_equiv_lang (comb_equiv e)),(𝐄_eq_equiv_lang (comb_equiv f)) in E.
      rewrite <- from_to_id, <- (@from_to_id (comb e true)).
      + apply to_𝐄_morph.
        apply klm_completeness.
        intros Σ σ.
        repeat rewrite pad_unpad_expr.
        * intro;unfold map_lang,intersection.
          setoid_rewrite (E _ (pad_map σ) _);reflexivity.
        * apply one_free_comb,hf.
        * apply is_clean_comb.
        * apply one_free_comb,he.
        * apply is_clean_comb.
      + apply is_clean_comb.
      + apply one_free_comb,he.
      + apply is_clean_comb.
      + apply one_free_comb,hf.
  Qed.

  Corollary Completeness_inf_one_free :
    forall e f : 𝐄 X , one_free e = true -> one_free f = true -> e ≤ f <-> e ≲ f.
  Proof.
    intros e f he hf;unfold smaller,𝐄_Smaller.
    rewrite Completeness_one_free by (simpl;try rewrite he;rewrite hf;reflexivity).
    split.
    - intros <-;intros ? ? ?;rsimpl;unfold union;tauto.
    - intros h;split;rsimpl.
      + intros [?|?];[apply h|];tauto.
      + unfold union;tauto.
  Qed.
  
End translation.


(*  LocalWords:  subunits
 *)






