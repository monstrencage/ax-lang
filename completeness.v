(** We finally arrive to the main result of this development: the
proof that our axiomatization is complete for the equational theory of
languages. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Import tools rklm lklc language w_terms.

Section t.
  Variable X : Set.
  Variable dec_X : decidable_set X.

  Proposition completeness_inf :
    forall e f : 𝐄 X, e ≤ f <-> e ≲ f.
  Proof.
    intros e f;split;[apply 𝐄_inf_incl_lang|].
    intros E.
    rewrite (expand_eq dec_X e).
    rewrite (𝐄_eq_equiv_lang (expand_eq dec_X e)) in E.
    apply Σ_small;intros g Ig.
    apply in_map_iff in Ig as ((A&[e'|])&<-&Ig).
    - simpl;destruct (@reduction_one_free _ dec_X A e' f) as (f'&Of'&E__f&E__e).
      + eapply one_free_expand;eauto.
      + etransitivity;[|apply E].
        apply 𝐄_inf_incl_lang.
        eapply Σ_big;reflexivity|| auto.
        apply in_map_iff;exists (A,Some e');auto.
      + apply Completeness_inf_one_free in E__e;[|eapply one_free_expand;eauto|auto].
        rewrite E__e,E__f;reflexivity.
    - simpl;rewrite <- (@testA_spec _ dec_X).
      cut (test A ≲ f);[|rewrite <- E;
                         apply 𝐄_inf_incl_lang;
                         eapply Σ_big;reflexivity|| auto;
                         apply in_map_iff;exists (A,None);auto].
      clear E Ig e.
      intro E;apply ssmaller_sub_terms_inf_iff in E.
      destruct (sub_terms_test_member A) as (w&Iw&E1&Ew).
      apply E in Iw as (w'&Iw'&Ew').
      destruct w as ((?&B)&pp);simpl in *;subst.
      unfold smaller,𝐖'_inf in Ew';simpl in Ew'.
      clear pp.
      cut (𝐖'_variables w'⊆ balance A);
        [|destruct w' as (([w'|]&C)&?);unfold smaller,𝐖_inf in Ew';unfold 𝐖'_variables;simpl in *;
          rewrite <- Ew;[|apply Ew'];
          destruct Ew' as (->&I);apply incl_app;[|reflexivity];
          rewrite is_balanced_balance by apply is_balanced_𝐒𝐏'_variables;
          rewrite Ew,<- balance_incl_iff,𝐒𝐏'_variables_fst,I,Ew;
          apply balance_incl_iff;rewrite <- is_balanced_balance by apply balance_balanced;reflexivity].
      clear B Ew' Ew E;revert w' Iw';induction f;simpl.
      + reflexivity.
      + tauto.
      + intros ? -> ;unfold 𝐖'_variables;simpl.
        intros h;apply inb_spec.
        cut ((x,true)∈balance A);[|apply h;now left].
        unfold balance;clear;intros I;apply in_flat_map in I as (y&Iy&Ix).
        repeat destruct Ix as [Ix|Ix];inversion Ix;subst;auto.
      + intros ? (u1&u2&->&I1&I2) h.
        rewrite variables_𝐖'_seq in h.
        rewrite (IHf1 u1),(IHf2 u2);auto;rewrite <- h;
          [apply incl_appr;reflexivity|apply incl_appl;reflexivity].
      + intros ? (u1&u2&->&I1&I2) h.
        rewrite variables_𝐖'_par in h.
        rewrite (IHf1 u1),(IHf2 u2);auto;rewrite <- h;
          [apply incl_appr;reflexivity|apply incl_appl;reflexivity].
      + intros w [Iw|Iw] h;[rewrite (IHf1 w)|rewrite (IHf2 w),orb_true_r];auto.
      + intros w Iw h;apply (IHf (w`));auto.
        rewrite <- h;clear.
        unfold 𝐖'_variables;destruct w as (([]&B)&?);simpl;[rewrite prime_var|];reflexivity.
      + intros w ([|n]&In) h.
        * apply (IHf w In h).
        * destruct In as (w1&?&->&Iw1&_).
          apply (IHf w1 Iw1).
          rewrite <- h,variables_𝐖'_seq.
          apply incl_appl;reflexivity.
  Qed.
  
  Theorem Completeness_of_Reversible_Kleene_Lattices :
    forall e f : 𝐄 X, e ≡ f <-> e ≃ f. 
  Proof.
    intros e f.
    rewrite (smaller_PartialOrder e f).
    unfold relation_conjunction,predicate_intersection,Basics.flip;simpl.
    repeat rewrite completeness_inf;symmetry.
    apply 𝐄_sem_PartialOrder.
  Qed.
    
End t.

