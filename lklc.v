(** This module defines the full signature of language algebra we
consider here, and its finite complete axiomatization. We also define
here some normalisation functions, and list some of their properties. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Import tools language.

Delimit Scope expr_scope with expr.
Open Scope expr_scope.

Section s.
  (** * Main definitions *)
  Variable X : Set.
  Variable dec_X : decidable_set X.

  (** [𝐄 X] is the type of expressions with variables ranging over the
  type [X]. They are built out of the constants [0] and [1], the
  concatenation (also called sequential product) [⋅], the intersection
  [∩], the union [+], the mirror image, denoted by the postfix
  operator [̅], and the non-zero iteration, denoted by [⁺]. *)
  Inductive 𝐄 : Set :=
  | 𝐄_one : 𝐄
  | 𝐄_zero : 𝐄
  | 𝐄_var : X -> 𝐄
  | 𝐄_seq : 𝐄 -> 𝐄 -> 𝐄
  | 𝐄_inter : 𝐄 -> 𝐄 -> 𝐄
  | 𝐄_plus : 𝐄 -> 𝐄 -> 𝐄
  | 𝐄_conv : 𝐄 -> 𝐄
  | 𝐄_iter : 𝐄 -> 𝐄.

  Notation "x ⋅ y" := (𝐄_seq x y) (at level 40) : expr_scope.
  Notation "x + y" := (𝐄_plus x y) (left associativity, at level 50) : expr_scope.
  Notation "x ∩ y" := (𝐄_inter x y) (at level 45) : expr_scope.
  Notation "x ¯" := (𝐄_conv x) (at level 25) : expr_scope.
  Notation "x ⁺" := (𝐄_iter x) (at level 25) : expr_scope.
  Notation " 1 " := 𝐄_one : expr_scope.
  Notation " 0 " := 𝐄_zero : expr_scope.

  (** The size of an expression is the number of nodes in its syntax
  tree. *)
  Global Instance size_𝐄 : Size 𝐄 :=
    fix 𝐄_size (e: 𝐄) : nat :=
      match e with
      | 0 | 1 | 𝐄_var _ => 1%nat
      | e + f | e ∩ f | e ⋅ f => S (𝐄_size e + 𝐄_size f)
      | e ¯ | e ⁺ => S (𝐄_size e)
      end.
  (* begin hide *)
  Lemma 𝐄_size_one : |1| = 1%nat. trivial. Qed.
  Lemma 𝐄_size_zero : |0| = 1%nat. trivial. Qed.
  Lemma 𝐄_size_var a : |𝐄_var a| = 1%nat. trivial. Qed.
  Lemma 𝐄_size_seq e f : |e⋅f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐄_size_inter e f : |e∩f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐄_size_plus e f : |e+f| = S(|e|+|f|). trivial. Qed.
  Lemma 𝐄_size_conv e : |e ¯| = S(|e|). trivial. Qed.
  Lemma 𝐄_size_iter e : |e⁺| = S(|e|). trivial. Qed.
  Hint Rewrite 𝐄_size_one 𝐄_size_zero 𝐄_size_var 𝐄_size_seq
       𝐄_size_inter 𝐄_size_plus 𝐄_size_conv 𝐄_size_iter
    : simpl_typeclasses.
  Fixpoint eqb e f :=
    match (e,f) with
    | (1,1) | (0,0) => true
    | (𝐄_var a,𝐄_var b) => eqX a b
    | (e ¯,f ¯) | (e⁺,f⁺) => eqb e f
    | (e1 + e2,f1 + f2)
    | (e1 ⋅ e2,f1 ⋅ f2)
    | (e1 ∩ e2,f1 ∩ f2) => eqb e1 f1 && eqb e2 f2
    | _ => false
    end.
  Lemma eqb_reflect e f : reflect (e = f) (eqb e f).
  Proof.
    apply iff_reflect;symmetry;split;
      [intro h;apply Is_true_eq_left in h;revert f h
      |intros <-;apply Is_true_eq_true];induction e;
        try destruct f;simpl;autorewrite with quotebool;firstorder.
    - apply Is_true_eq_true,eqX_correct in h as ->;auto.
    - erewrite IHe1;[|eauto]; erewrite IHe2;[|eauto];auto.
    - erewrite IHe1;[|eauto]; erewrite IHe2;[|eauto];auto.
    - erewrite IHe1;[|eauto]; erewrite IHe2;[|eauto];auto.
    - erewrite IHe;[|eauto];auto.
    - erewrite IHe;[|eauto];auto.
    - apply Is_true_eq_left,eqX_correct;auto.
  Qed.
  (* end hide *)

  (** If the set of variables [X] is decidable, then so is the set of
  expressions. _Note that we are here considering syntactic equality,
  as no semantic or axiomatic equivalence relation has been defined
  for expressions_. *)
  Global Instance 𝐄_decidable_set : decidable_set 𝐄.
  Proof. exact (Build_decidable_set eqb_reflect). Qed.

  (** The following are the axioms of the algebra of languages over
  this signature.*)
  Inductive ax : 𝐄 -> 𝐄 -> Prop :=
  (** [⟨𝐄,⋅,1⟩] is a monoid. *)
  | ax_seq_assoc e f g : ax (e⋅(f ⋅ g)) ((e⋅f)⋅g)
  | ax_seq_1 e : ax (1⋅e) e
  | ax_1_seq e : ax (e⋅1) e
  (** [⟨𝐄,+,0⟩] is a commutative idempotent monoid. *)
  | ax_plus_com e f : ax (e+f) (f+e)
  | ax_plus_idem e : ax (e+e) e
  | ax_plus_ass e f g : ax (e+(f+g)) ((e+f)+g)
  | ax_plus_0 e : ax (e+0) e
  (** [⟨𝐄,⋅,+,1,0⟩] is an idempotent semiring. *)
  | ax_seq_0 e : ax (e⋅0) 0
  | ax_0_seq e : ax (0⋅e) 0
  | ax_plus_seq e f g: ax ((e + f)⋅g) (e⋅g + f⋅g)
  | ax_seq_plus e f g: ax (e⋅(f + g)) (e⋅f + e⋅g)
  (** [⟨𝐄,∩⟩] is a commutative and idempotent semigroup. *)
  | ax_inter_assoc e f g : ax (e∩(f ∩ g)) ((e∩f)∩g)
  | ax_inter_comm e f : ax (e∩f) (f∩e)
  | ax_inter_idem e : ax (e ∩ e) e
  (** [⟨𝐄,+,∩⟩] forms a distributive lattice, and [0] is absorbing for
  [∩]. *)
  | ax_plus_inter e f g: ax ((e + f)∩g) (e∩g + f∩g)
  | ax_inter_plus e f : ax ((e∩f)+e) e
  | ax_inter_0 e : ax (e∩0) 0
  (** [¯] is an involution that flips concatenations and commutes with
  every other operation. *)
  | ax_conv_conv e : ax (e ¯¯) e
  | ax_conv_1 : ax (1¯) 1
  | ax_conv_0 : ax (0¯) 0
  | ax_conv_plus e f: ax ((e + f)¯) (e ¯ + f ¯)
  | ax_conv_seq e f: ax ((e ⋅ f)¯) (f ¯ ⋅ e ¯)
  | ax_conv_inter e f: ax ((e∩f)¯) (e ¯ ∩ f ¯)
  | ax_conv_iter e : ax (e⁺¯) (e ¯⁺)
  (** The axioms for [⁺] are as follow: *)
  | ax_iter_left e : ax (e⁺) (e + e⋅e⁺)
  | ax_iter_right e : ax (e⁺) (e + e⁺ ⋅e)
  (** The five laws that follow are the most interesting ones. They
  concern _subunits_, terms that are smaller than [1]. With our
  signature, any term can be projected to a subunit using the
  operation [1 ∩ _ ]. *)
  (** - For subunits, intersection and concatenation coincide. *)
  | ax_inter_1_seq e f : ax (1 ∩ (e⋅f)) (1 ∩ (e ∩ f))
  (** - Mirror image is the identity on subunits. *)
  | ax_inter_1_conv e : ax (1 ∩ (e ¯)) (1 ∩ e)
  (** - Subunits commute with every term. *)
  | ax_inter_1_comm_seq e f : ax ((1 ∩ e)⋅f) (f⋅(1 ∩ e))
  (** - This law is less intuitive, but with the previous ones,
  it allows one to extract from any union-free expressions a single
  global subunit.*)
  | ax_inter_1_inter e f g : ax (((1 ∩ e)⋅f) ∩ g) ((1 ∩ e)⋅(f ∩ g))
  | ax_inter_1_iter e f g : ax ((g + (1 ∩ e) ⋅ f)⁺) (g⁺ + (1 ∩ e) ⋅ (g + f)⁺).

  (** Additionally, we need these two implications: *)
  Inductive ax_impl : 𝐄 -> 𝐄 -> 𝐄 -> 𝐄 -> Prop:=
  | ax_right_ind e f : ax_impl (e⋅f + f) f (e⁺⋅f + f) f
  | ax_left_ind e f : ax_impl (f ⋅ e + f) f (f ⋅e⁺ + f) f.

  (** We use these axioms to generate an axiomatic equivalence
  relation and an axiomatic order relations. *)
  Inductive 𝐄_eq : Equiv 𝐄 :=
  | eq_refl e : e ≡ e
  | eq_trans f e g : e ≡ f -> f ≡ g -> e ≡ g
  | eq_sym e f : e ≡ f -> f ≡ e
  | eq_plus e f g h : e ≡ g -> f ≡ h -> (e + f) ≡ (g + h)
  | eq_seq e f g h : e ≡ g -> f ≡ h -> (e ⋅ f) ≡ (g ⋅ h)
  | eq_inter e f g h : e ≡ g -> f ≡ h -> (e ∩ f) ≡ (g ∩ h)
  | eq_conv e f : e ≡ f -> (e ¯) ≡ (f ¯)
  | eq_iter e f : e ≡ f -> (e⁺) ≡ (f⁺)
  | eq_ax e f : ax e f -> e ≡ f
  | eq_ax_impl e f g h : ax_impl e f g h -> e ≡ f -> g ≡ h.
  Global Instance 𝐄_Equiv : Equiv 𝐄 := 𝐄_eq.

  Global Instance 𝐄_Smaller : Smaller 𝐄 := (fun e f => e + f ≡ f).

  Hint Constructors 𝐄_eq ax ax_impl.

  Global Instance ax_equiv : subrelation ax equiv. 
  Proof. intros e f E;apply eq_ax,E. Qed.

  (** * Some elementary properties of this algebra *)

  (** It is immediate to check that the equivalence we defined is
  indeed an equivalence relation, that the order relation is a
  preorder, and that every operator is monotone for both relations. *)
  Global Instance equiv_Equivalence : Equivalence equiv.
  Proof. split;intro;eauto. Qed.

  Global Instance inter_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐄_inter.
  Proof. now intros e f hef g h hgh;apply eq_inter. Qed.

  Global Instance plus_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐄_plus.
  Proof. now intros e f hef g h hgh;apply eq_plus. Qed.

  Global Instance seq_equiv :
    Proper (equiv ==> equiv ==> equiv) 𝐄_seq.
  Proof. now intros e f hef g h hgh;apply eq_seq. Qed.
  
  Global Instance conv_equiv :
    Proper (equiv ==> equiv) 𝐄_conv.
  Proof. now intros e f hef;apply eq_conv. Qed.
  
  Global Instance iter_equiv :
    Proper (equiv ==> equiv) 𝐄_iter.
  Proof. now intros e f hef;apply eq_iter. Qed.

  Global Instance smaller_PreOrder : PreOrder smaller.
  Proof.
    split;intro;unfold smaller,𝐄_Smaller;intros.
    - auto.
    - transitivity (y + z);[|auto].
      transitivity (x + y + z);[|auto].
      transitivity (x + (y + z));[|auto].
      auto.
  Qed.

  Global Instance smaller_PartialOrder : PartialOrder equiv smaller.
  Proof.
    intros e f;split;unfold smaller,𝐄_Smaller;unfold Basics.flip.
    - intros E;split.
      + rewrite E;auto.
      + rewrite E;auto.
    - intros (E1&E2).
      rewrite <- E1.
      rewrite <- E2 at 1;auto.
  Qed.

  Global Instance smaller_equiv : subrelation equiv smaller.
  Proof. intros e f E;apply smaller_PartialOrder in E as (E&_);apply E. Qed.

  (** From the axioms, we can infer the following simple laws. *)
  Lemma equiv_0_inter e : (0∩e) ≡ 0.
  Proof. rewrite <- (ax_inter_0 e) at 2;auto. Qed.

  Lemma inter_plus e f g : (e∩(f + g)) ≡ (e∩f + e∩g).
  Proof.
    rewrite (eq_ax (ax_inter_comm _ _)).
    rewrite (eq_ax (ax_plus_inter _ _ _));auto.
  Qed.

  Global Instance inter_smaller :
    Proper (smaller ==> smaller ==> smaller) 𝐄_inter.
  Proof.
    intros e f hef g h hgh;unfold smaller,𝐄_Smaller in *.
    rewrite <- hef,<-hgh.
    repeat rewrite inter_plus.
    repeat rewrite (ax_inter_comm (e+f) g).
    repeat rewrite (ax_inter_comm (e+f) h).
    repeat rewrite inter_plus.
    rewrite (ax_inter_comm e g).
    repeat rewrite (ax_plus_ass _ _ _).
    auto.
  Qed.
  
  Global Instance plus_smaller :
    Proper (smaller ==> smaller ==> smaller) 𝐄_plus.
  Proof.
    intros e f hef g h hgh;unfold smaller,𝐄_Smaller in *.
    transitivity (e + g + (h + f));[auto|].
    transitivity (e + (g + (h + f)));[auto|].
    transitivity (e + (g + h + f));[auto|].
    transitivity (e + (h + f));[auto|].
    transitivity (e + (f + h));[auto|].
    transitivity (e + f + h);auto.
  Qed.

  Global Instance seq_smaller :
    Proper (smaller ==> smaller ==> smaller) 𝐄_seq.
  Proof.
    intros e f hef g h hgh;unfold smaller,𝐄_Smaller in *.
    rewrite <- hef,<-hgh.
    rewrite (eq_ax (ax_seq_plus _ _ _)).
    repeat rewrite (eq_ax (ax_plus_seq _ _ _)).
    repeat rewrite (eq_ax (ax_plus_ass _ _ _)).
    auto.
  Qed.

  Global Instance conv_smaller :
    Proper (smaller ==> smaller) 𝐄_conv.
  Proof.
    intros e f hef;unfold smaller,𝐄_Smaller in *.
    rewrite <- (eq_ax (ax_conv_plus _ _)),hef;reflexivity.
  Qed.

  Lemma iter_left e : e ⋅ e⁺ ≤ e ⁺.
  Proof.
    unfold smaller,𝐄_Smaller.
    rewrite (eq_ax(ax_iter_left _)) at 2 3.
    transitivity (e ⋅ e ⁺ + e + e ⋅ e ⁺);[auto|].
    transitivity (e + e ⋅ e ⁺ + e ⋅ e ⁺);[auto|].
    transitivity (e + (e ⋅ e ⁺ + e ⋅ e ⁺));auto.
  Qed.
  Lemma iter_incr e : e ≤ e ⁺.
  Proof.
    unfold smaller,𝐄_Smaller.
    rewrite (eq_ax(ax_iter_left _)).
    transitivity (e + e + e ⋅ e ⁺);auto.
  Qed.

  Lemma iter_seq e : e ⁺⋅e ⁺ ≤ e⁺.
  Proof. apply (eq_ax_impl (ax_right_ind _ _)),iter_left. Qed.

  Lemma iter_idempotent e : e ⁺ ⁺ ≡ e ⁺.
  Proof.
    rewrite (eq_ax (ax_iter_right _)).
    rewrite (eq_ax (ax_plus_com _ _)).
    apply (eq_ax_impl (ax_right_ind _ _)).
    apply iter_seq.
  Qed.

  Lemma right_ind e f : e⋅f ≤ f -> e⁺⋅f≤f.
  Proof. intro h;eapply eq_ax_impl,h;auto. Qed.
  Lemma left_ind e f : e⋅f ≤ e -> e⋅f⁺≤e.
  Proof. intro h;eapply eq_ax_impl,h;auto. Qed.

  Lemma plus_inf e f g : e ≤ g -> f ≤ g -> e + f ≤ g.
  Proof.
    intros h1 h2;unfold smaller,𝐄_Smaller in *. 
    rewrite <- h1,<-h2 at 2;auto.
  Qed.
  Lemma plus_left e f g : e ≤ f -> e ≤ f + g.
  Proof.
    intros h1;unfold smaller,𝐄_Smaller in *.
    transitivity (e+f+g);auto.
  Qed.
  Lemma plus_right e f g : e ≤ g -> e ≤ f + g.
  Proof.
    intros h1;unfold smaller,𝐄_Smaller in *.
    transitivity (f+g+e);auto.
    transitivity (f+(g+e));auto.
    transitivity (f+(e+g));auto.
  Qed.
  
  Global Instance iter_smaller :
    Proper (smaller ==> smaller) 𝐄_iter.
  Proof.
    intros e f hef.
    rewrite (eq_ax(ax_iter_right e)) at 1.
    apply plus_inf.
    - rewrite hef.
      rewrite (eq_ax(ax_iter_right f)).
      apply plus_left;reflexivity.
    - rewrite hef at 2.
      rewrite (iter_incr f) at 1.
      apply right_ind.
      rewrite hef.
      apply iter_left.
  Qed.

  Lemma zero_minimal e : 0 ≤ e.
  Proof.
    unfold smaller,𝐄_Smaller.
    transitivity (e + 0);auto.
  Qed.
  
  Lemma inf_ax_inter_l e f : e ∩ f ≤ e.
  Proof. apply (eq_ax (ax_inter_plus _ _)). Qed.

  Lemma inf_ax_inter_r e f : e ∩ f ≤ f.
  Proof. rewrite ax_inter_comm,inf_ax_inter_l;reflexivity. Qed.

  Lemma inter_glb e f g : g ≤ e -> g ≤ f -> g ≤ e ∩ f.
  Proof. intros <- <- ;apply smaller_equiv;auto. Qed.

  Proposition smaller_inter e f : e ≤ f <-> e∩f ≡ e.
  Proof.
    split;[|intros <-;apply inf_ax_inter_r].
    intros E.
    apply smaller_PartialOrder;unfold Basics.flip;split;[apply inf_ax_inter_l|].
    apply inter_glb;[reflexivity|assumption].
  Qed.

  (** Mirror is actually more than monotone, it is bijective. *)
  Lemma smaller_conv_iff e f :
    e ≤ f <-> e ¯ ≤ f ¯.
  Proof.
    split;[apply conv_smaller|].
    rewrite <- (eq_ax (ax_conv_conv e)) at 2.
    rewrite <- (eq_ax (ax_conv_conv f)) at 2.
    apply conv_smaller.
  Qed.

  (** We establish few properties of subunits. *)
  Lemma inter_1_abs e f : ((1 ∩ e)⋅(1 ∩ f)) ≡ (1 ∩ (e ∩ f)).
  Proof.
    transitivity ((1∩1)∩(e∩f));auto.
    transitivity ((1∩1∩1)∩(e∩f));auto.
    transitivity (((1∩1)∩(1∩(e∩f))));auto.
    transitivity (((1∩1)∩((1∩e)∩f)));auto.
    transitivity (((1∩1)∩(1∩e)∩f));auto.
    transitivity ((1∩(1∩(1∩e))∩f));auto.
    transitivity ((1∩((1∩e)∩1)∩f));auto.
    transitivity (1∩((1∩e)∩1∩f));auto.
    transitivity (1∩((1∩e)∩(1∩f)));auto.
    transitivity (1∩((1∩e)⋅(1∩f)));auto.
    apply smaller_PartialOrder;unfold Basics.flip;split;auto.
    - rewrite <- ax_inter_idem at 1.
      apply inter_smaller;[|reflexivity].
      transitivity (1⋅1);[|apply smaller_equiv;auto].
      apply seq_smaller;apply inf_ax_inter_l.
    - apply inf_ax_inter_r.
  Qed.
    
  Lemma inter_onel e : (1 ∩ e + (1 ∩ e)⋅e) ≡ ((1 ∩ e)⋅e).
  Proof.
    assert (E1:(1 ∩ e) + e ≡ e) by eauto.
    assert (E2:(1 ∩ e) ≡ (1 ∩ e)⋅(1 ∩ e))
      by (transitivity (1 ∩ (e ∩ e));auto using inter_1_abs).
    rewrite E2 at 1.
    rewrite <- (ax_seq_plus _ _ _ ).
    rewrite E1;auto.
  Qed.

  Lemma inter_oner e : (1 ∩ e + e⋅(1 ∩ e)) ≡ (e⋅(1 ∩ e)).
  Proof.
    rewrite <- (eq_ax (ax_inter_1_comm_seq _ _)).
    apply inter_onel.
  Qed.                      

  (** A product is larger than [1] if and only if both its arguments are.*)
  Lemma split_one e f : 1 ≤ e⋅f <-> 1 ≤ e /\ 1 ≤ f.
  Proof.
    split;[|intros (<-&<-);apply smaller_equiv;auto].
    intro E.
    apply smaller_inter in E.
    rewrite (eq_ax (ax_inter_1_seq _ _)) in E.
    rewrite <- E;split;auto.
    - rewrite <-(inf_ax_inter_l e f) at 2;auto.
      apply inf_ax_inter_r.
    - rewrite <-(inf_ax_inter_r e f) at 2;auto.
      apply inf_ax_inter_r.
  Qed.

  Lemma iter_0 : 0 ⁺ ≡ 0.
  Proof. rewrite ax_iter_left;transitivity (0+0);auto. Qed.
  Lemma iter_1 : 1 ⁺ ≡ 1.
  Proof.
    rewrite ax_iter_left.
    rewrite ax_plus_com.
    apply (eq_ax_impl (ax_left_ind _ _)).
    transitivity (1+1);auto.
  Qed.
    
  (** * Sum of a list of elements *)

  (** The expression [Σ [e1;e2;...;en]] is [e1+(e2+...(en)...)]*)
  Fixpoint Σ L :=
    match L with
    | [] => 0
    | [a] => a
    | a::L => a + Σ L
    end.

  Lemma sum_fold L : Σ L ≡ fold_right 𝐄_plus 0 L.
  Proof.
    induction L as [|e L];[reflexivity|].
    simpl;rewrite <- IHL;clear IHL;destruct L;simpl;auto.
  Qed.
  
  (** This operator satisfies some simple distributivity properties. *)
  Lemma sum_app l m : Σ (l++m) ≡ Σ l + Σ m.
  Proof.
    repeat rewrite sum_fold;rewrite fold_right_app.
    generalize dependent (fold_right 𝐄_plus 0 m);clear m.
    induction l;simpl;intro e;auto.
    - transitivity (e+0);auto.
    - rewrite IHl;auto.
  Qed.
  
  Lemma seq_distr l m : Σ l ⋅ Σ m ≡ Σ (bimap (fun e f : 𝐄 => e ⋅ f) l m).
  Proof.
    repeat rewrite sum_fold;revert m;induction l;simpl;auto.
    intro m.
    repeat rewrite <-sum_fold;rewrite sum_app.
    repeat rewrite sum_fold;rewrite <-IHl.
    transitivity (a⋅Σ m+ Σ l⋅Σ m);repeat rewrite sum_fold;auto.
    apply eq_plus;auto.
    clear IHl;induction m;simpl;auto.
    rewrite <- IHm;auto.
  Qed.

  Lemma inter_distr l m : Σ l ∩ Σ m ≡ Σ  (bimap (fun e f : 𝐄 => e ∩ f) l m).
  Proof.
    repeat rewrite sum_fold;revert m;induction l;simpl;auto.
    + intros;transitivity 0;eauto.
    + intro m;repeat rewrite <-sum_fold;rewrite sum_app;
        repeat rewrite sum_fold; rewrite <-IHl.
      transitivity (a ∩ Σ m + Σ l ∩ Σ m);repeat rewrite sum_fold;auto.
      apply eq_plus;auto.
      clear IHl;induction m as [|b m];simpl;auto.
      rewrite <- IHm.
      transitivity ((b + Σ m) ∩ a);repeat rewrite sum_fold;auto.
      transitivity (b ∩ a + Σ m ∩ a);repeat rewrite sum_fold;auto.
  Qed.

  (** If [l⊆m], then [Σ l ≤ Σ m]*)
  Lemma fold_right_incl_smaller :
    Proper ((@incl 𝐄) ==> smaller) Σ.
  Proof.
    intros l m I;repeat rewrite sum_fold;revert m I;induction l;simpl;intros m L;[apply zero_minimal|].
    rewrite (IHl m).
    - assert (I:a ∈ m) by (now apply L;left).
      clear l L IHl.
      induction m as [|b m];simpl in *;try tauto.
      destruct I as [->|I];eauto.
      -- apply smaller_equiv.
         transitivity (a + a + Σ m);repeat rewrite sum_fold;auto.
      -- rewrite <- (IHm I) at 2.
         apply smaller_equiv.
         transitivity (a + b + Σ m);repeat rewrite sum_fold;auto.
         transitivity (b + a + Σ m);repeat rewrite sum_fold;auto.
    - now intros x I;apply L;right.
  Qed.

  (** If [a] appears in [m], then the following identity holds: *)
  Lemma split_list a m :
    a ∈ m ->Σ m ≡ a +Σ (rm a m).
  Proof.
    intro h;transitivity (Σ (a::rm a m));repeat rewrite sum_fold;auto;
      apply smaller_PartialOrder;split;repeat rewrite <-sum_fold.
    - apply fold_right_incl_smaller.
      intros x I;destruct_eqX x a.
      -- left;auto.
      -- right; apply in_rm;tauto.
    - apply fold_right_incl_smaller;intros x [->|I];auto.
      apply in_rm in I as (_&I);auto.
  Qed.

  Lemma Σ_small e L : (forall f, f ∈ L -> f ≤ e) -> Σ L ≤ e.
  Proof.
    repeat rewrite sum_fold;induction L;simpl;intros hyp.
    - apply zero_minimal.
    - apply plus_inf.
      + apply hyp;auto.
      + apply IHL.
        intros;apply hyp;auto.
  Qed.

  Lemma Σ_big e L f : f ∈ L -> e ≤ f -> e ≤ Σ L.
  Proof.
    intros I E.
    rewrite (split_list I).
    rewrite E;apply plus_left;reflexivity.
  Qed.                                  

  (** * Zero-test *)

  Fixpoint test0 e :=
    match e with
    | 0 => true
    | 1 | 𝐄_var _ => false
    | e ¯ | e ⁺ => test0 e
    | e + f => test0 e && test0 f
    | e ∩ f | e ⋅ f => test0 e || test0 f
    end.

  Lemma test0_equiv : Proper (equiv ==> eq) test0.
  Proof.
    intros e f E;induction E;simpl;try congruence.
    - apply eq_true_iff_eq.
      destruct H;simpl;repeat rewrite orb_true_iff||rewrite andb_true_iff;simpl;try tauto.
      split;[intros [?|?]|];tauto||discriminate.
    - destruct H;simpl in *;auto.
  Qed.
    
  Lemma test0_spec e : test0 e = true <-> e ≡ 0.
  Proof.
    split.
    - induction e;simpl;repeat rewrite orb_true_iff||rewrite andb_true_iff;simpl.
      + discriminate.
      + reflexivity.
      + discriminate.
      + intros [h|h];[rewrite IHe1|rewrite IHe2];auto.
      + intros [h|h];[rewrite IHe1|rewrite IHe2];auto.
        transitivity (e2∩0);auto.
      + intros (h1&h2);rewrite IHe1,IHe2;auto.
      + intros h;rewrite IHe;auto.
      + intros h;rewrite IHe;auto.
        rewrite ax_iter_left.
        transitivity (0+0);auto.
    - intros E;apply test0_equiv in E as ->;reflexivity.
  Qed.
  (** * Unit-test *)

  Fixpoint test_sub e :=
    match e with
    | 0 | 1 => true
    | 𝐄_var _ => false
    | e ¯ | e ⁺ => test_sub e
    | e ∩ f => test_sub e || test_sub f
    | e + f => test_sub e && test_sub f
    | e ⋅ f => test0 e || test0 f || test_sub e && test_sub f
    end.

  Lemma test0_test_sub e : test0 e = true -> test_sub e = true.
  Proof.
    induction e;try (now simpl; reflexivity||auto with bool).
    - simpl;destruct (test0 e1),(test0 e2),(test_sub e1),(test_sub e2);
        try reflexivity||firstorder discriminate.
    - simpl;destruct (test0 e1),(test0 e2),(test_sub e1),(test_sub e2);
        try reflexivity||firstorder discriminate.
  Qed.

  Lemma test_sub_equiv : Proper (equiv ==> eq) test_sub.
  Proof.
    intros e f E;induction E;simpl;try congruence.
    - apply test0_equiv in H as ->;apply test0_equiv in H0 as ->;congruence.
    - destruct H;try (apply eq_true_iff_eq;simpl;
                      repeat rewrite orb_true_iff||rewrite andb_true_iff;simpl;tauto).
      + simpl; pose proof (@test0_test_sub e) as h.
        destruct (test0 e);simpl;auto.
        rewrite <- h;auto.
      + simpl; pose proof (@test0_test_sub e) as h.
        destruct (test0 e);simpl;auto.
        * rewrite <- h;auto.
        * rewrite andb_true_r;reflexivity.
      + simpl.
        pose proof (@test0_test_sub e) as h1.
        pose proof (@test0_test_sub f) as h2.
        pose proof (@test0_test_sub g) as h3.
        destruct (test0 e),(test0 f),(test0 g);
          try rewrite h1 by reflexivity;try rewrite h2 by reflexivity;
            try rewrite h3 by reflexivity;repeat (simpl;try rewrite andb_true_r);auto.
        destruct (test_sub g);simpl;repeat rewrite andb_true_r||rewrite andb_false_r;reflexivity.
      + simpl.
        pose proof (@test0_test_sub e) as h1.
        pose proof (@test0_test_sub f) as h2.
        pose proof (@test0_test_sub g) as h3.
        destruct (test0 e),(test0 f),(test0 g);
          try rewrite h1 by reflexivity;try rewrite h2 by reflexivity;
            try rewrite h3 by reflexivity;repeat (simpl;try rewrite andb_true_r);auto.
        destruct (test_sub e);simpl;reflexivity.
      + simpl.
        pose proof (@test0_test_sub e) as h1.
        pose proof (@test0_test_sub f) as h2.
        pose proof (@test0_test_sub g) as h3.
        destruct (test0 e),(test0 f),(test0 g);
          try rewrite h1 by reflexivity;try rewrite h2 by reflexivity;
            try rewrite h3 by reflexivity;repeat (simpl;try rewrite andb_true_r);auto.
        apply orb_true_r.
      + simpl.
        pose proof (@test0_test_sub e) as h1.
        pose proof (@test0_test_sub f) as h2.
        pose proof (@test0_test_sub g) as h3.
        destruct (test0 e),(test0 f),(test0 g);
          try rewrite h1 by reflexivity;try rewrite h2 by reflexivity;
            try rewrite h3 by reflexivity;repeat (simpl;try rewrite andb_true_r);auto.
        * rewrite andb_diag;reflexivity.
        * destruct (test_sub g);reflexivity.
    - destruct H;simpl in *;auto.
  Qed.

  Lemma test_sub_spec e : test_sub e = true <-> e ≤ 1.
  Proof.
    split.
    - induction e;simpl;repeat rewrite orb_true_iff||rewrite andb_true_iff;simpl.
      + reflexivity.
      + intros _;apply zero_minimal.
      + discriminate.
      + repeat rewrite test0_spec.
        intros [[-> | ->]|(h1&h2)].
        * etransitivity;[|apply zero_minimal].
          apply smaller_equiv;auto.
        * etransitivity;[|apply zero_minimal].
          apply smaller_equiv;auto.
        * rewrite IHe1,IHe2;auto.
          apply smaller_equiv;auto.
      + intros [h|h];[rewrite IHe1|rewrite IHe2];auto.
        * apply inf_ax_inter_l.
        * apply inf_ax_inter_r.
      + intros (h1&h2);rewrite IHe1,IHe2;auto.
        apply smaller_equiv;auto.
      + intros h;rewrite IHe;auto.
        apply smaller_equiv;auto.
      + intros h;rewrite IHe;auto.
        rewrite iter_1;reflexivity.
    - intros E;apply test_sub_equiv in E.
      simpl in E;rewrite andb_true_r in E;auto.
  Qed.

  Fixpoint test_sup e :=
    match e with
    | 0 | 𝐄_var _ => false
    | 1 => true
    | e + f => test_sup e || test_sup f
    | e ⋅ f | e ∩ f => test_sup e && test_sup f
    | e ¯ | e ⁺ => test_sup e
    end.

  Lemma test0_test_sup e : test0 e && test_sup e = false.
  Proof.
    induction e;simpl;try reflexivity;auto with bool;
      destruct (test0 e1),(test0 e2),(test_sup e1),(test_sup e2);discriminate||reflexivity.
  Qed.
    
  Lemma test_sup_equiv : Proper (equiv ==> eq) test_sup.
  Proof.
    intros e f E;induction E;simpl;try congruence.
    - destruct H; (apply eq_true_iff_eq;simpl;
                   repeat rewrite orb_true_iff||rewrite andb_true_iff;simpl;try tauto).
      + firstorder discriminate.
      + firstorder discriminate.
      + firstorder discriminate.
    - destruct H;simpl in *;auto.
  Qed.

  Lemma test_sup_spec e : test_sup e = true <-> 1 ≤ e.
  Proof.
    split.
    - induction e;simpl;repeat rewrite orb_true_iff||rewrite andb_true_iff;simpl.
      + reflexivity.
      + discriminate.
      + discriminate.
      + intros (h1&h2);rewrite <-IHe1,<-IHe2;auto.
        apply smaller_equiv;auto.
      + intros (h1&h2);rewrite <-IHe1,<-IHe2;auto.
        apply smaller_equiv;auto.
      + intros [h|h];[rewrite IHe1|rewrite IHe2];auto.
        * apply plus_left;reflexivity.
        * apply plus_right;reflexivity.
      + intros h;rewrite <- IHe;auto.
        rewrite ax_conv_1;reflexivity.
      + intros h;rewrite <- IHe;auto.
        rewrite iter_1;reflexivity.
    - intros E;apply test_sup_equiv in E;simpl in *;firstorder.
  Qed.

  Definition test1 e := test_sub e && test_sup e.
  
  Lemma test1_spec e : test1 e = true <-> e ≡ 1.
  Proof.
    unfold test1;rewrite andb_true_iff,test_sub_spec,test_sup_spec.
    pose proof (smaller_PartialOrder e 1) as h;simpl in h;rewrite h.
    unfold Basics.flip;split;[intros;split|intros (?&?);split];tauto.
  Qed.

  Definition ϵ e := if test_sup e then 1 else 0.
  
  (** * Normalisation of expressions *)

  (** An expression is called clean if [¯] only appears on variables. *)
  Fixpoint is_clean (e : 𝐄) : bool :=
    match e with
    | 0 | 1 | 𝐄_var _ | (𝐄_var _)¯ => true
    | e + f | e ⋅ f | e ∩ f => is_clean e && is_clean f
    | e ⁺ => is_clean e
    | _ ¯ => false
    end.

  Fixpoint zero_free e :=
    match e with
    | 0 => false
    | e + f | e ∩ f | e ⋅ f => zero_free e && zero_free f
    | e ⁺ | e ¯ => zero_free e
    | _ => true
    end.

  (** If [e] is not empty, [comb e true] produces a clean and zero-free expression equal to [e]. *)
  (** Here is the definition  of [comb]: *)
  Fixpoint comb e (dir : bool) :=
    match e with
    | 0 => 0
    | 1 => 1
    | e + f =>
      match test0 e,test0 f with
      | true,true => 0
      | true,false => comb f dir
      | false,true => comb e dir
      | false,false => comb e dir + comb f dir
      end
    | e ∩ f => (comb e dir) ∩ (comb f dir)
    | e ¯ => comb e (negb dir)
    | e ⁺ => (comb e dir)⁺
    | 𝐄_var a => if dir then 𝐄_var a else 𝐄_var a ¯
    | e ⋅ f =>
      if dir
      then (comb e dir)⋅(comb f dir)
      else (comb f dir)⋅(comb e dir)
    end.

  (** We prove the claim that indeed [comb e d] is clean. *)
  Lemma is_clean_comb e d : is_clean (comb e d) = true.
  Proof.
    revert d;induction e;intros d;simpl.
    - reflexivity.
    - reflexivity.
    - destruct d;simpl;reflexivity.
    - destruct d;simpl;rewrite IHe1,IHe2;reflexivity.
    - rewrite IHe1,IHe2;reflexivity.
    - destruct (test0 e2),(test0 e1);simpl;try rewrite IHe1;try rewrite IHe2;reflexivity.
    - apply IHe.
    - apply IHe.
  Qed.

  (** If [e] is not equivalent to zero, then [comb e d] is zero-free. *)
  Lemma zero_free_comb e d : test0 e = false -> zero_free (comb e d) = true.
  Proof.
    revert d;induction e;intros d;simpl.
    - reflexivity.
    - discriminate.
    - destruct d;simpl;reflexivity.
    - rewrite orb_false_iff;intros (I1&I2).
      destruct d;simpl.
      + eapply IHe1 in I1 as ->;eapply IHe2 in I2 as ->;reflexivity.
      + eapply IHe1 in I1 as ->;eapply IHe2 in I2 as ->;reflexivity.
    - rewrite orb_false_iff;intros (I1&I2).
      eapply IHe1 in I1 as ->;eapply IHe2 in I2 as ->;reflexivity.
    - rewrite andb_false_iff;intros [I|I];rewrite I.
      + case_eq (test0 e2);intro E.
        * apply IHe1,I.
        * simpl;rewrite IHe1,IHe2;auto.
      +  case_eq (test0 e1);intro E.
        * apply IHe2,I.
        * simpl;rewrite IHe1,IHe2;auto.
    - apply IHe.
    - apply IHe.
  Qed.

  (** We can now verify the second claim, that [e] is equal to [comb e true]. *)
  Lemma comb_equiv e : e ≡ comb e true.
  Proof.
    cut (e ≡ comb e true /\ e ¯ ≡ comb e false);[tauto|].
    induction e;simpl in *;auto.
    - destruct IHe1 as (<-&<-).
      destruct IHe2 as (<-&<-).
      split;auto.
    - destruct IHe1 as (<-&<-).
      destruct IHe2 as (<-&<-).
      split;auto.
    - case_eq (test0 e1);case_eq (test0 e2);simpl;(repeat rewrite test0_spec).
      + intros -> ->;split;eauto.
      + intros _ ->.
        destruct IHe2 as (<-&<-);split;eauto.
      + intros -> _ .
        destruct IHe1 as (<-&<-);split;auto.
      + intros _ _ .
        destruct IHe1 as (<-&<-);auto.
        destruct IHe2 as (<-&<-);auto.
    - destruct IHe as (<-&<-);auto.
    - destruct IHe as (<-&<-);auto.
  Qed.

  Definition test := fold_right (fun a => 𝐄_inter (𝐄_var a ∩ 𝐄_var a ¯)) 1.

  Lemma test_sub_id A : test A ≤ 1.
  Proof.
    induction A as [|a A];simpl;[reflexivity|].
    rewrite IHA,inf_ax_inter_r;reflexivity.
  Qed.

  Lemma test_var a A : a ∈ A -> test A ≤ 𝐄_var a ∩ 𝐄_var a ¯.
  Proof.
    intro Ia;induction A as [|b m];simpl.
    - simpl in Ia;tauto.
    - destruct Ia as [->|Ia].
      + apply inf_ax_inter_l.
      + apply IHm in Ia as ->.
        apply inf_ax_inter_r.
  Qed.
    
  Global Instance test_incl : Proper ((@incl X) ==> Basics.flip smaller) test.
  Proof.
    unfold Basics.flip;intros l m I;induction l as [|a l];simpl.
    - apply test_sub_id.
    - assert (ih : l ⊆ m) by (intros x Ix;apply I;now right).
      apply IHl in ih as <-.
      apply inter_glb;[|reflexivity].
      apply test_var, I;now left.
  Qed.

  Global Instance test_equivalent : Proper (@equivalent X ==> equiv) test.
  Proof.
    intros l m E;apply incl_PartialOrder in E as (I1&I2);apply smaller_PartialOrder;
      apply test_incl in I1;apply test_incl in I2;unfold Basics.flip in *.
    split;tauto.
  Qed.
  
  Fixpoint testA A e :=
    match e with
    | 0 => false
    | 1 => true
    | 𝐄_var a => inb a A
    | e + f => (testA A e) ||  (testA A f)
    | e ⋅ f | e ∩ f => (testA A e)&&(testA A f)
    | e⁺ | e ¯ => testA A e
    end.


  Lemma testA_equiv A e f : e ≡ f -> testA A e = testA A f.
  Proof.
    intros E;induction E;simpl;try congruence.
    - destruct H;simpl;try destruct (testA A e);try destruct (testA A f);try destruct (testA A g);
        reflexivity.
    - destruct H;simpl;destruct (testA A e),(testA A f); reflexivity.
  Qed.

  Lemma testA_test_A A : testA A (test A) = true.
  Proof.
    cut (forall l, l ⊆ A -> testA A (test l) = true);[intros hyp;apply hyp;reflexivity|].
    intro l;induction l as [|a l];simpl;auto.
    intros I.
    rewrite IHl by (intros x Ix;apply I;now right).
    rewrite andb_true_r,andb_diag,inb_spec.
    apply I;now left.
  Qed.
    

  Lemma testA_spec A e : testA A e = true <-> test A ≤ e.
  Proof.
    split.
    - induction e;simpl.
      + intros _;apply test_sub_id.
      + discriminate.
      + rewrite inb_spec.
        intros Ix;rewrite (test_var Ix),inf_ax_inter_l;reflexivity.
      + rewrite andb_true_iff;intros (h1&h2);rewrite <-IHe1,<-IHe2;auto.
        pose proof (test_sub_id A) as E;apply smaller_inter in E as <-.
        repeat rewrite (ax_inter_comm _ 1).
        rewrite inter_1_abs,ax_inter_idem;reflexivity.
      + rewrite andb_true_iff;intros (h1&h2);rewrite <-IHe1,<-IHe2;auto.
        rewrite ax_inter_idem;reflexivity.
      + rewrite orb_true_iff.
        intros [h|h].
        * rewrite IHe1;auto.
          apply plus_left;reflexivity.
        * rewrite IHe2;auto.
          apply plus_right;reflexivity.
      + intro h;rewrite <-IHe;auto.
        pose proof (test_sub_id A) as E;apply smaller_inter in E as <-.
        repeat rewrite (ax_inter_comm _ 1).
        rewrite ax_conv_inter,ax_conv_1, ax_inter_1_conv.
        reflexivity.
      + intro h;rewrite <-IHe;auto.
        pose proof (test_sub_id A) as E;apply smaller_inter in E as <-.
        repeat rewrite (ax_inter_comm _ 1).
        apply smaller_equiv;clear.
        transitivity ((0 + (1 ∩ test A)⋅1)⁺);[|rewrite ax_plus_com,ax_plus_0;auto].
        rewrite ax_inter_1_iter.
        rewrite (ax_plus_com 0),ax_plus_0.
        rewrite iter_0,iter_1.
        transitivity ((1∩test A)⋅1);auto.
        transitivity ((1∩test A)⋅1 + 0);auto.
    - intro E;apply (testA_equiv A) in E.
      simpl in E.
      rewrite testA_test_A in E;rewrite <- E;reflexivity.  
  Qed.

  Lemma test_dup A : test A ≡ test A ⋅ test A.
  Proof.
    pose proof (test_sub_id A) as E;apply smaller_inter in E as <-.
    repeat rewrite (ax_inter_comm _ 1).
    rewrite inter_1_abs,ax_inter_idem;reflexivity.
  Qed.

  Lemma test_seq A e : test A ⋅ e ≡ e ⋅ test A.
  Proof.
    pose proof (test_sub_id A) as E;apply smaller_inter in E as <-.
    repeat rewrite (ax_inter_comm _ 1).
    rewrite ax_inter_1_comm_seq;reflexivity.
  Qed.

  Lemma test_inter_left A e f : test A ⋅ (e ∩ f) ≡ (test A ⋅ e)∩ f.
  Proof.
    pose proof (test_sub_id A) as E;apply smaller_inter in E.
    rewrite (ax_inter_comm (test A) 1) in E;rewrite <- E;clear E.
    rewrite <- ax_inter_1_inter;reflexivity.
  Qed.
  
  Lemma test_inter A e f : test A ⋅ (e ∩ f) ≡ (test A ⋅ e)∩(test A ⋅ f).
  Proof.
    rewrite test_dup at 1;rewrite <-ax_seq_assoc.
    rewrite test_inter_left,(ax_inter_comm _ f),test_inter_left;auto.
  Qed.

  Lemma test_conv A : test A ¯ ≡ test A.
  Proof.
    pose proof (test_sub_id A) as E;apply smaller_inter in E.
    rewrite (ax_inter_comm (test A) 1) in E;rewrite <- E;clear E.
    rewrite ax_conv_inter,ax_conv_1.
    rewrite ax_inter_1_conv;reflexivity.
  Qed.

  Lemma test_iter A : test A ⁺ ≡ test A.
  Proof.
    rewrite ax_iter_left,ax_plus_com.
    apply (eq_ax_impl (ax_left_ind _ _)).
    rewrite <- test_dup;auto.
  Qed.

  Lemma test_prod_cap A B : test A ∩ test B ≡ test A ⋅ test B.
  Proof.
    pose proof (test_sub_id A) as e1.
    pose proof (test_sub_id B) as e2.
    apply smaller_inter in e1 as <-.
    apply smaller_inter in e2 as <-.
    repeat rewrite (ax_inter_comm _ 1).
    rewrite inter_1_abs.
    transitivity (1 ∩ 1 ∩ (test A∩test B));auto.
    transitivity (1 ∩ (1 ∩ (test A∩test B)));auto.
    transitivity (1 ∩ ((1 ∩ test A)∩test B));auto.
    transitivity (((1 ∩ test A)∩test B) ∩ 1);auto.
    transitivity ((1 ∩ test A)∩(test B ∩ 1));auto.
  Qed.

  Lemma test_app A B : test (A++B) ≡ test A ∩ test B.
  Proof.
    induction A;simpl.
    - rewrite ax_inter_comm.
      symmetry;apply smaller_inter,test_sub_id.
    - rewrite IHA;auto.
  Qed.
                            
  Fixpoint project A e :=
    match e with
    | 0 => 0
    | 1 => 0
    | 𝐄_var a => 𝐄_var a
    | e + f => (project A e) + (project A f)
    | e ⋅ f =>
      match (testA A e),(testA A f) with
      | true,true => (project A e)+(project A f)+(project A e)⋅(project A f)
      | true,false => (project A f)+(project A e)⋅(project A f)
      | false,true => (project A e)+(project A e)⋅(project A f)
      | false,false => (project A e)⋅(project A f)
      end
    | e ∩ f => (project A e) ∩ (project A f)
    | e⁺ => (project A e)⁺
    | e ¯ => (project A e)¯
    end.

  Lemma project_spec A e : test A ⋅ project A e ≤ e.
  Proof.
    induction e;simpl.
    - transitivity 0;[|apply zero_minimal].
      apply smaller_equiv;auto.
    - transitivity 0;[|apply zero_minimal].
      apply smaller_equiv;auto.
    - rewrite test_sub_id;apply smaller_equiv;auto.
    - case_eq (testA A e1);case_eq (testA A e2);
        intros h2 h1;repeat rewrite testA_spec in * || rewrite ax_seq_plus|| apply plus_inf.
      + rewrite <- h2,<-test_seq.
        rewrite <-IHe1 at 2.
        rewrite ax_seq_assoc,<-test_dup;reflexivity.
      + rewrite <- h1.
        rewrite <-IHe2 at 2.
        rewrite ax_seq_assoc,<-test_dup;reflexivity.
      + rewrite <-IHe1,<-IHe2 at 2.
        repeat rewrite ax_seq_assoc.
        rewrite <-test_seq. 
        repeat rewrite ax_seq_assoc.
        rewrite <-test_dup.
        reflexivity.
      + rewrite <- h1.
        rewrite <-IHe2 at 2.
        rewrite ax_seq_assoc,<-test_dup;reflexivity.
      + rewrite <-IHe1,<-IHe2 at 2.
        repeat rewrite ax_seq_assoc.
        rewrite <-test_seq. 
        repeat rewrite ax_seq_assoc.
        rewrite <-test_dup.
        reflexivity.
      + rewrite <- h2,<-test_seq.
        rewrite <-IHe1 at 2.
        rewrite ax_seq_assoc,<-test_dup;reflexivity.
      + rewrite <-IHe1,<-IHe2 at 2.
        repeat rewrite ax_seq_assoc.
        rewrite <-test_seq. 
        repeat rewrite ax_seq_assoc.
        rewrite <-test_dup.
        reflexivity.
      + rewrite <-IHe1,<-IHe2 at 2.
        repeat rewrite ax_seq_assoc.
        rewrite <-test_seq. 
        repeat rewrite ax_seq_assoc.
        rewrite <-test_dup.
        reflexivity.
    - rewrite test_inter,IHe1,IHe2;reflexivity.
    - rewrite ax_seq_plus,IHe1,IHe2;reflexivity.
    - rewrite <- IHe at 2.
      rewrite ax_conv_seq,test_conv,<-test_seq;reflexivity.
    - rewrite <- IHe at 2.
      rewrite <- (ax_plus_0 (test A ⋅ project A e)),(ax_plus_com _ 0).
      pose proof (test_sub_id A) as E;apply smaller_inter in E.
      rewrite (ax_inter_comm (test A) 1) in E;rewrite <- E at 2.
      rewrite ax_inter_1_iter.
      rewrite iter_0,E.
      apply smaller_equiv.
      transitivity (test A ⋅ (0+project A e)⁺);[|eauto].
      apply eq_seq,eq_iter;eauto.
  Qed.


  Definition proj A e :=
    if testA A e then test A + test A ⋅ project A e else test A ⋅ project A e.

  Lemma proj_inf A e : proj A e ≤ e.
  Proof.
    unfold proj.
    case_eq (testA A e).
    - rewrite testA_spec;intro h.
      rewrite project_spec,h;apply plus_inf;reflexivity.
    - intros _;apply project_spec.
  Qed.

  Fixpoint Vars e :=
    match e with
    | 0 | 1 => []
    | 𝐄_var a => [a]
    | e + f | e ∩ f | e ⋅ f => Vars e ++ Vars f
    | e ⁺ | e ¯ => Vars e
    end.

  Lemma test_Vars e A : Vars e ⊆ A -> test0 e = false -> test A ≤ 1 ∩ e.
  Proof.
    intros E T;apply testA_spec;simpl.
    induction e;simpl;auto.
    - apply inb_spec,E;now left.
    - simpl in T;apply orb_false_iff in T as (T1&T2).
      apply andb_true_iff;split;[apply IHe1|apply IHe2];auto;rewrite <- E;simpl.
      + apply incl_appl;reflexivity.
      + apply incl_appr;reflexivity.
    - simpl in T;apply orb_false_iff in T as (T1&T2).
      apply andb_true_iff;split;[apply IHe1|apply IHe2];auto;rewrite <- E;simpl.
      + apply incl_appl;reflexivity.
      + apply incl_appr;reflexivity.
    - simpl in T;apply andb_false_iff in T as [T|T].
      + rewrite IHe1;auto.
        rewrite <- E;simpl;apply incl_appl;reflexivity.
      + rewrite IHe2,orb_true_r;auto.
        rewrite <- E;simpl;apply incl_appr;reflexivity.
  Qed.
  
  (* Lemma test_Vars_eq e : test0 e = false -> test (Vars e) ≡ 1 ∩ e. *)
  (* Proof. *)
  (*   intros T;apply smaller_PartialOrder;unfold Basics.flip;split;[apply test_Vars,T;reflexivity|]. *)
  (*   clear T;induction e;simpl. *)
  (*   - apply smaller_equiv;auto. *)
  (*   - etransitivity;[|apply zero_minimal]. *)
  (*     apply smaller_equiv;auto. *)
  (*   - transitivity ((1 ∩ 𝐄_var x)∩(1 ∩ 𝐄_var x ¯)). *)
  (*     + rewrite ax_inter_1_conv. *)
  (*       apply smaller_equiv;auto. *)
  (*     + apply inter_glb. *)
  (*       * apply inter_glb. *)
  (*         -- rewrite inf_ax_inter_l,inf_ax_inter_r;reflexivity. *)
  (*         -- rewrite inf_ax_inter_r,inf_ax_inter_r;reflexivity. *)
  (*       * rewrite inf_ax_inter_l,inf_ax_inter_l;reflexivity. *)
  (*   - rewrite ax_inter_1_seq. *)
  (*     rewrite test_app;apply inter_glb. *)
  (*     + rewrite <- IHe1;apply inter_glb. *)
  (*       * apply inf_ax_inter_l. *)
  (*       * rewrite inf_ax_inter_r,inf_ax_inter_l;reflexivity. *)
  (*     + rewrite <- IHe2;apply inter_glb. *)
  (*       * apply inf_ax_inter_l. *)
  (*       * rewrite inf_ax_inter_r,inf_ax_inter_r;reflexivity. *)
  (*   - rewrite test_app;apply inter_glb. *)
  (*     + rewrite <- IHe1;apply inter_glb. *)
  (*       * apply inf_ax_inter_l. *)
  (*       * rewrite inf_ax_inter_r,inf_ax_inter_l;reflexivity. *)
  (*     + rewrite <- IHe2;apply inter_glb. *)
  (*       * apply inf_ax_inter_l. *)
  (*       * rewrite inf_ax_inter_r,inf_ax_inter_r;reflexivity. *)
  (*   - rewrite inter_plus,test_app;apply inter_glb. *)
  (*     + rewrite IHe1;apply plus_inf. *)

  Fixpoint inter_1 e :=
    match e with
    | 0 => []
    | 1 => [[]]
    | 𝐄_var a => [[a]]
    | e+f => inter_1 e ++ inter_1 f
    | e⋅f | e∩f => bimap (@app X) (inter_1 e) (inter_1 f)
    | e⁺ => map (@concat X) (rm [] (subsets (inter_1 e)))
    | e ¯ => inter_1 e
    end.
  
  Lemma inter_1_spec e : 1 ∩ e ≡ Σ (map test (inter_1 e)).
  Proof.
    induction e.
    - simpl;transitivity 1;auto.
    - simpl;auto.
    - simpl. 
      transitivity (1 ∩ (𝐄_var x ∩ 𝐄_var x ¯));auto.
      transitivity (1 ∩ 1 ∩ (𝐄_var x ∩ 𝐄_var x ¯));auto.
      transitivity (1 ∩ (1 ∩ (𝐄_var x ∩ 𝐄_var x ¯)));auto.
      transitivity (1 ∩ ((1 ∩ 𝐄_var x) ∩ 𝐄_var x ¯));auto.
      transitivity (1 ∩ (1 ∩ 𝐄_var x) ∩ 𝐄_var x ¯);auto.
      transitivity ((1 ∩ 𝐄_var x) ∩ 1 ∩ 𝐄_var x ¯);auto.
      transitivity ((1 ∩ 𝐄_var x) ∩ (1 ∩ 𝐄_var x ¯));auto.
      transitivity ((1 ∩ 𝐄_var x) ∩ (1 ∩ 𝐄_var x));auto.
    - transitivity (1∩(e1∩e2));auto.
      transitivity (1∩1∩(e1∩e2));auto.
      transitivity (1∩(1∩(e1∩e2)));auto.
      transitivity (1∩((1∩e1)∩e2));auto.
      transitivity (1∩(1∩e1)∩e2);auto.
      transitivity ((1∩e1)∩1∩e2);auto.
      transitivity ((1∩e1)∩(1∩e2));auto.
      rewrite IHe1,IHe2.
      rewrite inter_distr;simpl.
      rewrite map_bimap.
      generalize (inter_1 e2);generalize (inter_1 e1); clear IHe1 IHe2 e1 e2.
      induction l;intro m;simpl;auto.
      repeat rewrite sum_app.
      apply eq_plus;[|apply IHl].
      rewrite map_map.
      apply smaller_PartialOrder;unfold Basics.flip;split;
        apply Σ_small;intros f If;apply in_map_iff in If as (b&<-&Ib);
          (eapply Σ_big;[apply in_map_iff;exists b;eauto|]);rewrite test_app;reflexivity.
    - transitivity (1∩1∩(e1∩e2));auto.
      transitivity (1∩(1∩(e1∩e2)));auto.
      transitivity (1∩((1∩e1)∩e2));auto.
      transitivity (1∩(1∩e1)∩e2);auto.
      transitivity ((1∩e1)∩1∩e2);auto.
      transitivity ((1∩e1)∩(1∩e2));auto.
      rewrite IHe1,IHe2.
      rewrite inter_distr;simpl.
      rewrite map_bimap.
      generalize (inter_1 e2);generalize (inter_1 e1); clear IHe1 IHe2 e1 e2.
      induction l;intro m;simpl;auto.
      repeat rewrite sum_app.
      apply eq_plus;[|apply IHl].
      rewrite map_map.
      apply smaller_PartialOrder;unfold Basics.flip;split;
        apply Σ_small;intros f If;apply in_map_iff in If as (b&<-&Ib);
          (eapply Σ_big;[apply in_map_iff;exists b;eauto|]);rewrite test_app;reflexivity.
    - rewrite ax_inter_comm,ax_plus_inter;repeat rewrite (ax_inter_comm _ 1).
      rewrite IHe1,IHe2;simpl;rewrite map_app,sum_app;reflexivity.
    - rewrite ax_inter_1_conv,IHe;reflexivity.
    - transitivity (Σ (map test (inter_1 e))).
      + simpl;rewrite <- IHe;auto;clear IHe.
        rewrite ax_iter_left.
        rewrite ax_inter_comm,ax_plus_inter.
        repeat rewrite (ax_inter_comm _ 1).
        rewrite ax_inter_1_seq.
        pose proof (iter_incr e) as E.
        apply smaller_inter in E as ->;auto.
      + simpl;clear IHe;generalize (inter_1 e);intro L;clear e.
        repeat rewrite sum_fold.
        induction L;simpl.
        * rewrite eqX_refl;simpl;reflexivity.
        * rewrite rm_app,map_app,map_app.
          repeat rewrite <-sum_fold;rewrite sum_app;repeat rewrite sum_fold.
          rewrite <- IHL.
          apply smaller_PartialOrder;unfold Basics.flip;split.
          -- apply plus_inf;[|apply plus_right;reflexivity].
             apply plus_left;clear IHL.
             rewrite <- sum_fold.
             eapply Σ_big;[|reflexivity].
             apply in_map_iff;exists a;split;[reflexivity|].
             apply in_map_iff;exists [a];simpl;split;[rewrite app_nil_r;reflexivity|].
             apply in_rm;split;[discriminate|].
             apply in_map_iff;exists [];split;[reflexivity|].
             clear a;induction L;simpl;auto.
             mega_simpl.
          -- apply plus_inf;[|apply plus_right;reflexivity].
             repeat rewrite <- sum_fold in *.
             apply plus_left,Σ_small.
             intros f If.
             apply in_map_iff in If as (M&<-&I).
             apply test_incl.
             apply in_map_iff in I as (N&<-&IN).
             apply in_rm in IN as (_&IN).
             apply in_map_iff in IN as (K&<-&_);simpl.
             apply incl_appl;reflexivity.
  Qed.
  
  Definition NF := list (list X * option 𝐄).

  Definition seq_nf l1 l2 : NF :=
    bimap (fun p1 p2 =>
             match p1,p2 with
             | (A,Some e),(B,Some f) => (A++B,Some (e⋅f))
             | (A,Some e),(B,None)
             | (A,None),(B,Some e) => (A++B,Some e)
             | (A,None),(B,None) => (A++B,None)
             end)
          l1 l2.

  
  Definition inter_nf l1 l2 : NF :=
    concat (bimap (fun p1 p2 =>
                     match p1,p2 with
                     | (A,Some e),(B,Some f) => [(A++B,Some (e ∩ f))]
                     | (A,Some e),(B,None)
                     | (A,None),(B,Some e) => map (fun C => (A++B++C,None)) (inter_1 e)
                     | (A,None),(B,None) => [(A++B,None)]
                     end)
                  l1 l2).
  
  Fixpoint iter_nf acc l : NF :=
    match l with
    | [] => match acc with [] => [] | L => [([],Some(Σ L ⁺))] end
    | (A,None)::l => (A,None)::iter_nf acc l ++ map (fun p => (A++fst p,snd p)) (iter_nf acc l)
    | (A,Some e)::l => iter_nf acc l ++ map (fun p => (A++fst p,snd p)) (iter_nf (e::acc) l)
    end.
  
  Fixpoint expand e : NF :=
    match e with
    | 0 => []
    | 1 => [([],None)]
    | 𝐄_var a => [([],Some (𝐄_var a))]
    | e + f => (expand e) ++ (expand f)
    | e ⋅ f => seq_nf (expand e) (expand f)
    | e ∩ f => inter_nf (expand e) (expand f)
    | e⁺ => iter_nf [] (expand e)
    | e ¯ => map (fun p => match snd p with None =>(fst p,None)
                                  | Some e' => (fst p,Some (e' ¯)) end)
               (expand e)
    end.

  Definition nf_to_term p :=
    match p with
    | (A,None) => test A
    | (A,Some e') => test A ⋅ e'
    end.

  Lemma iter_nf_aux B L: 
    Σ (map nf_to_term (map (fun p => (B ++ fst p, snd p)) L))
      ≡ test B ⋅ Σ (map nf_to_term L).
  Proof.
    repeat rewrite sum_fold;induction L as [|(A,[f|]) m];simpl;auto.
    -- rewrite test_app,test_prod_cap,ax_seq_plus.
       repeat rewrite ax_seq_assoc.
       rewrite IHm;reflexivity.
    -- rewrite test_app,test_prod_cap,ax_seq_plus.
       repeat rewrite ax_seq_assoc.
       rewrite IHm;reflexivity.
  Qed.
      
  Lemma expand_eq e : e ≡ Σ (map nf_to_term (expand e)).
  Proof.
    induction e;simpl;auto.
    - rewrite IHe1,IHe2,seq_distr at 1;clear IHe1 IHe2.
      generalize (expand e1);generalize (expand e2);intros m l.
      apply smaller_PartialOrder;unfold Basics.flip;simpl;split;apply Σ_small;intros e Ie.
      + apply in_bimap in Ie as (f&g&<-&If&Ig).
        apply in_map_iff in If as ((A&F)&<-&Il).
        apply in_map_iff in Ig as ((B&G)&<-&Im).
        destruct F as [f|],G as [g|].
        * apply Σ_big with (f := nf_to_term (A++B,Some (f⋅g))).
          -- apply in_map_iff;exists (A++B,Some (f⋅g));split;[reflexivity|].
             apply in_bimap;exists (A,Some f),(B,Some g);split;[reflexivity|tauto].
          -- simpl;rewrite test_app,test_prod_cap.
             apply smaller_equiv.
             transitivity (test A ⋅ (test B ⋅ (f ⋅ g)));auto.
             transitivity (test A ⋅ (test B ⋅ f ⋅ g));auto.
             rewrite (test_seq B f).
             transitivity (test A ⋅ (f ⋅ (test B ⋅ g)));auto.
        * apply Σ_big with (f := nf_to_term (A++B,Some f)).
          -- apply in_map_iff;exists (A++B,Some f);split;[reflexivity|].
             apply in_bimap;exists (A,Some f),(B,None);split;[reflexivity|tauto].
          -- simpl;rewrite test_app,test_prod_cap.
             apply smaller_equiv.
             transitivity (test A ⋅ (test B ⋅ f));auto.
             rewrite (test_seq B f);auto.
        * apply Σ_big with (f := nf_to_term (A++B,Some g)).
          -- apply in_map_iff;exists (A++B,Some g);split;[reflexivity|].
             apply in_bimap;exists (A,None),(B,Some g);split;[reflexivity|tauto].
          -- simpl;rewrite test_app,test_prod_cap.
             apply smaller_equiv;auto.
        * apply Σ_big with (f := nf_to_term (A++B,None)).
          -- apply in_map_iff;exists (A++B,None);split;[reflexivity|].
             apply in_bimap;exists (A,None),(B,None);split;[reflexivity|tauto].
          -- simpl;rewrite test_app,test_prod_cap;reflexivity.
      + apply in_map_iff in Ie as ((?,?)&<-&I);unfold seq_nf in I.
        apply in_bimap in I as ((A,F)&(B,G)&<-&Il&Im).
        apply Σ_big with (f := nf_to_term (A,F) ⋅ nf_to_term (B,G)).
        * apply in_bimap;exists (nf_to_term (A,F)),(nf_to_term (B,G));split;[reflexivity|].
          split;apply in_map_iff;eexists;eauto.
        * destruct F as [f|],G as [g|];simpl.
          -- simpl;rewrite test_app,test_prod_cap.
             apply smaller_equiv.
             transitivity (test A ⋅ (test B ⋅ (f ⋅ g)));auto.
             transitivity (test A ⋅ (test B ⋅ f ⋅ g));auto.
             rewrite (test_seq B f).
             transitivity (test A ⋅ (f ⋅ (test B ⋅ g)));auto.
          -- simpl;rewrite test_app,test_prod_cap.
             apply smaller_equiv.
             transitivity (test A ⋅ (test B ⋅ f));auto.
             rewrite (test_seq B f);auto.
          -- simpl;rewrite test_app,test_prod_cap.
             apply smaller_equiv;auto.
          -- simpl;rewrite test_app,test_prod_cap;reflexivity.
    - rewrite IHe1,IHe2,inter_distr at 1;clear IHe1 IHe2.
      generalize (expand e1);generalize (expand e2);intros m l.
      apply smaller_PartialOrder;unfold Basics.flip;simpl;split;apply Σ_small;intros e Ie.
      + apply in_bimap in Ie as (f&g&<-&If&Ig).
        apply in_map_iff in If as ((A&F)&<-&Il).
        apply in_map_iff in Ig as ((B&G)&<-&Im).
        destruct F as [f|],G as [g|].
        * apply Σ_big with (f := nf_to_term (A++B,Some (f∩g))).
          -- apply in_map_iff;exists (A++B,Some (f∩g));split;[reflexivity|].
             apply in_concat;exists [(A++B,Some (f∩g))];split;[simpl;tauto|].
             apply in_bimap;exists (A,Some f),(B,Some g);split;[reflexivity|tauto].
          -- simpl;rewrite test_app, test_prod_cap.
             rewrite (ax_inter_comm f g),<-ax_seq_assoc.
             rewrite test_inter_left,(ax_inter_comm _ f),test_inter_left;reflexivity.
        * simpl;rewrite <- test_inter_left.
          pose proof (test_sub_id B) as E;apply smaller_inter in E;rewrite ax_inter_comm in E.
          rewrite <- E,ax_inter_assoc,(ax_inter_comm f 1).
          rewrite inter_1_spec.
          transitivity (Σ (map (fun C => test (A ++ B ++ C)) (inter_1 f))).
          -- repeat rewrite sum_fold;clear;induction (inter_1 f);clear f;simpl.
             ++ rewrite ax_inter_comm,ax_inter_0,ax_seq_0;reflexivity.
             ++ rewrite <- IHl.
                rewrite ax_plus_inter,ax_seq_plus.
                apply plus_smaller;[|reflexivity].
                rewrite test_app,(test_prod_cap _ (B++a)).
                apply seq_smaller;[reflexivity|].
                rewrite test_app;apply smaller_equiv;auto.
          -- clear E;simpl;unfold inter_nf.
             apply Σ_small;intros g Ig;apply in_map_iff in Ig as (C&<-&IC).
             apply Σ_big with (f:= nf_to_term (A++B++C,None)).
             ++ apply in_map_iff;exists (A++B++C,None);split;[reflexivity|].
                apply in_concat;exists (map (fun C0 : list X => (A ++ B ++ C0, None)) (inter_1 f)).
                split;[apply in_map_iff;exists C;tauto|].
                apply in_bimap;exists (A,Some f),(B,None);tauto.
             ++ reflexivity.
        * simpl.
          rewrite ax_inter_comm,<- test_inter_left,ax_inter_comm.
          pose proof (test_sub_id A) as E;apply smaller_inter in E.
          rewrite <- E,<-ax_inter_assoc;clear E.
          rewrite inter_1_spec.
          transitivity (Σ (map (fun C => test (A ++ B ++ C)) (inter_1 g))).
          -- repeat rewrite sum_fold;clear;induction (inter_1 g);clear g;simpl.
             ++ rewrite ax_inter_0,ax_seq_0;reflexivity.
             ++ rewrite <- IHl.
                rewrite ax_inter_comm,ax_plus_inter,ax_seq_plus,(ax_inter_comm (fold_right _ _ _)).
                apply plus_smaller;[|reflexivity].
                rewrite ax_inter_comm at 1.
                rewrite <- test_app,<-test_prod_cap.
                rewrite ax_inter_comm,<-test_app,app_ass.
                apply smaller_equiv,test_equivalent;intro;repeat rewrite in_app_iff;tauto.
          -- simpl;unfold inter_nf.
             apply Σ_small;intros f If;apply in_map_iff in If as (C&<-&IC).
             apply Σ_big with (f:= nf_to_term (A++B++C,None)).
             ++ apply in_map_iff;exists (A++B++C,None);split;[reflexivity|].
                apply in_concat;exists (map (fun C0 : list X => (A ++ B ++ C0, None)) (inter_1 g)).
                split;[apply in_map_iff;exists C;tauto|].
                apply in_bimap;exists (A,None),(B,Some g);tauto.
             ++ reflexivity.
        * apply Σ_big with (f := nf_to_term (A++B,None)).
          -- apply in_map_iff;exists (A++B,None);split;[reflexivity|].
             apply in_concat;exists [(A++B,None)];split;[simpl;tauto|].
             apply in_bimap;exists (A,None),(B,None);split;[reflexivity|tauto].
          -- simpl;rewrite test_app,test_prod_cap;reflexivity.
      + apply in_map_iff in Ie as ((?,?)&<-&I);unfold seq_nf in I.
        unfold inter_nf in I.
        apply in_concat in I as (L&IL&I).
        apply in_bimap in I as ((A,F)&(B,G)&<-&Il&Im).
        apply Σ_big with (f := nf_to_term (A,F) ∩ nf_to_term (B,G)).
        * apply in_bimap;exists (nf_to_term (A,F)),(nf_to_term (B,G));split;[reflexivity|].
          split;apply in_map_iff;eexists;eauto.
        * destruct F as [f|],G as [g|];simpl.
          -- destruct IL as [E|E];inversion E;subst;clear E.
             rewrite <- test_inter_left,(ax_inter_comm f (_⋅_)),<-test_inter_left.
             rewrite ax_inter_comm,ax_seq_assoc.
             rewrite test_app,test_prod_cap;reflexivity.
          -- apply in_map_iff in IL as (C&E&IC);inversion E;subst;clear E.
             rewrite test_app,<-test_inter_left,test_prod_cap.
             apply seq_smaller;[reflexivity|].
             pose proof (test_sub_id B) as E;apply smaller_inter in E;rewrite ax_inter_comm in E.
             rewrite <- E,ax_inter_assoc,(ax_inter_comm f 1).
             rewrite test_app,ax_inter_comm;apply inter_smaller;[|reflexivity].
             rewrite inter_1_spec;apply Σ_big with (f:=test C);[|reflexivity].
             apply in_map_iff;exists C;tauto.
          -- apply in_map_iff in IL as (C&E&IC);inversion E;subst;clear E.
             pose proof (test_sub_id A) as E;apply smaller_inter in E;rewrite ax_inter_comm in E.
             rewrite ax_inter_comm,<-test_inter_left.
             rewrite <- E,ax_inter_assoc,(ax_inter_comm g 1).
             repeat rewrite test_app.
             rewrite ax_inter_assoc,(ax_inter_comm (test A)),<-ax_inter_assoc.
             rewrite <- test_app,test_prod_cap,test_app,ax_inter_comm.
             apply seq_smaller;[|apply inter_smaller];try reflexivity.
             rewrite inter_1_spec;apply Σ_big with (f:=test C);[|reflexivity].
             apply in_map_iff;exists C;tauto.
          -- destruct IL as [E|E];inversion E;subst;clear E.
             simpl;rewrite test_app,test_prod_cap;reflexivity.
    - rewrite IHe1,IHe2 at 1.
      rewrite map_app,sum_app;reflexivity.
    - rewrite IHe at 1;simpl;clear.
      repeat rewrite sum_fold.
      induction (expand e) as [|(A&[e'|]) l];simpl;auto.
      + rewrite ax_conv_plus,ax_conv_seq,test_conv,test_seq,IHl;reflexivity.
      + rewrite ax_conv_plus,test_conv,IHl;reflexivity.
    - cut (forall L l, Σ (map nf_to_term (iter_nf l L))
                    ≡ ((Σ l) + Σ (map nf_to_term L))⁺).
      + intro hyp;rewrite IHe at 1;rewrite hyp;simpl.
        rewrite ax_plus_com,ax_plus_0;reflexivity.
      + clear e IHe.
        intros L l;repeat rewrite sum_fold;revert l.
        induction L as [|(B,[e|]) L];intros l.
        * destruct l as [|e l];simpl;repeat rewrite ax_plus_0||rewrite iter_0.
          -- reflexivity.
          -- rewrite ax_seq_1,<-sum_fold;destruct l;[auto|reflexivity].
        * simpl;repeat rewrite <- sum_fold.
          rewrite map_app,sum_app.
          transitivity ((Σ l + (Σ (map nf_to_term L)+test B ⋅ e)) ⁺);auto.
          transitivity ((Σ l + Σ (map nf_to_term L)+test B ⋅ e) ⁺);auto.
          pose proof (test_sub_id B) as E;apply smaller_inter in E;rewrite ax_inter_comm in E;
            rewrite <- E,ax_inter_1_iter,E;clear E.
          rewrite <- (ax_plus_ass _ _ e).
          rewrite <- (ax_plus_com e _).
          rewrite (ax_plus_ass _ e).
          rewrite <- (ax_plus_com e _).
          setoid_replace (e + Σ l) with (Σ (e::l)) by (repeat rewrite sum_fold;reflexivity).
          setoid_rewrite <- sum_fold in IHL.
          rewrite <- IHL,<-IHL.
          apply eq_plus;[reflexivity|].
          rewrite iter_nf_aux;reflexivity.
        * simpl;repeat rewrite <- sum_fold.
          rewrite map_app,sum_app.
          transitivity ((Σ l + (Σ (map nf_to_term L)+test B)) ⁺);auto.
          transitivity ((Σ l + (Σ (map nf_to_term L)+test B⋅1)) ⁺);auto.
          transitivity ((Σ l + Σ (map nf_to_term L)+test B ⋅ 1) ⁺);auto.
          pose proof (test_sub_id B) as E;apply smaller_inter in E;rewrite ax_inter_comm in E;
            rewrite <- E,ax_inter_1_iter,E;clear E.
          rewrite <- (ax_plus_com 1 _).
          assert (forall e, (1+e)⁺ ≡ 1+e⁺) as -> .
          -- clear;intro e;apply smaller_PartialOrder;unfold Basics.flip;split.
             ++ transitivity ((1+e⁺)⋅(1+e)⁺).
                ** rewrite ax_plus_seq,ax_seq_1.
                   apply plus_left;reflexivity.
                ** apply left_ind.
                   repeat rewrite ax_seq_1||rewrite ax_1_seq||rewrite ax_plus_seq||rewrite ax_seq_plus.
                   repeat apply plus_inf.
                   --- apply plus_left;reflexivity.
                   --- apply plus_right,iter_incr.
                   --- apply plus_right;reflexivity.
                   --- apply plus_right;rewrite ax_iter_right at 2.
                       apply plus_right;reflexivity.
             ++ rewrite <- iter_1 at 1;apply plus_inf;apply iter_smaller;
                  [apply plus_left|apply plus_right];reflexivity.
          -- rewrite ax_seq_plus,ax_1_seq.
             setoid_rewrite <- sum_fold in IHL.
             rewrite <- IHL.
             repeat rewrite ax_seq_plus.
             rewrite iter_nf_aux.
             transitivity (test B + Σ (map nf_to_term (iter_nf l L)) + test B ⋅ Σ (map nf_to_term (iter_nf l L)));auto.
             transitivity (Σ (map nf_to_term (iter_nf l L))+ test B + test B ⋅ Σ (map nf_to_term (iter_nf l L)));auto.
  Qed.

  Fixpoint one_free e :=
    match e with
    | 1 => false
    | e + f | e ∩ f | e ⋅ f => one_free e && one_free f
    | e ⁺ | e ¯ => one_free e
    | _ => true
    end.


  Lemma one_free_sum L : (forall f, f ∈ L -> one_free f = true) -> one_free (Σ L) = true.
  Proof.
    induction L;simpl;auto.
    intros h.
    destruct L.
    - apply h;auto.
    - simpl in *;rewrite IHL,h;auto.
  Qed.

  Lemma one_free_expand e : forall A f, (A,Some f) ∈ expand e -> one_free f = true.
  Proof.
    induction e;simpl.
    - intros ? ? [E|E];inversion E.
    - tauto.
    - intros ? ? [E|E];inversion E;subst;clear E;reflexivity.
    - unfold seq_nf;intros x y I.
      apply in_bimap in I as ((A&[f|])&(B&[g|])&E&I1&I2);symmetry in E;inversion E;subst;clear E.
      + simpl;apply andb_true_iff;split;[eapply IHe1|eapply IHe2];eassumption.
      + eapply IHe1;eassumption.
      + eapply IHe2;eassumption.
    - unfold inter_nf;intros x y I.
      apply in_concat in I as (L&IL&I).
      apply in_bimap in I as ((A&[f|])&(B&[g|])&<-&I1&I2).
      + destruct IL as [E|E];inversion E.
        simpl;apply andb_true_iff;split;[eapply IHe1|eapply IHe2];eassumption.
      + apply in_map_iff in IL as (C&E&IC);inversion E.
      + apply in_map_iff in IL as (C&E&IC);inversion E.
      + destruct IL as [E|E];inversion E.
    - intros x y I.
      apply in_app_iff in I as [I|I].
      + eapply IHe1;eassumption.
      + eapply IHe2;eassumption.
    - intros A f I.
      apply in_map_iff in I as ((B&[g|])&E&I);inversion E;subst;clear E.
      simpl;eapply IHe;eassumption.
    - intros A f If.
      cut (forall m l A f, (forall f, f ∈ l -> one_free f = true)
                  -> (forall A f, (A,Some f) ∈ m -> one_free f = true)
                  -> ((A,Some f) ∈ iter_nf l m -> one_free f = true));
        [intro h;eapply h;simpl;try eassumption;simpl;tauto|]. 
      clear e IHe If A f.
      induction m as [|(A,[e|]) m];intros l B f hl hm;simpl.
      + destruct l as [|g l];[simpl;tauto|].
        generalize dependent (g :: l);clear g l;intros l hl.
        intros [E|E];inversion E;subst.
        apply one_free_sum;auto.
      + rewrite in_app_iff;intros [I|I].
        * eapply IHm;try eassumption.
          intros;eapply hm;right;eassumption.
        * apply in_map_iff in I as ((C&g)&E&IC);simpl in *;inversion E;subst.
          apply (IHm (e::l) C);[| |assumption].
          -- intros g [<-|I].
             ++ apply (hm A e);tauto.
             ++ apply hl,I.
          -- intros x y I;apply (hm x y);tauto.
      + intros [E|I];[inversion E|].
        apply in_app_iff in I as [I|I].
        * apply (IHm l B);try assumption.
          intros x y Ix;apply (hm x y);simpl;tauto.
        * apply in_map_iff in I as ((C&g)&E&I).
          inversion E;subst;clear E.
          apply (IHm l C);try assumption.
          intros x y Ix;apply (hm x y);simpl;tauto.
  Qed.

  Lemma one_free_comb e b : one_free e = true -> one_free (comb e b) = true.
  Proof.
    revert b;induction e;intro b;simpl.
    - discriminate.
    - reflexivity.
    - destruct b;simpl;reflexivity.
    - rewrite andb_true_iff;intros (h1,h2);destruct b;simpl;rewrite IHe1,IHe2;auto.
    - rewrite andb_true_iff;intros (h1,h2);destruct b;simpl;rewrite IHe1,IHe2;auto.
    - rewrite andb_true_iff;intros (h1,h2).
      destruct (test0 e1),(test0 e2);simpl;try rewrite IHe1;try rewrite IHe2;auto.
    - apply IHe.
    - apply IHe.
  Qed.

  Lemma one_free_project_inf A e : one_free e = true -> e ≤ project A e.
  Proof.
    induction e;simpl;try discriminate||(rewrite andb_true_iff;intros (T1&T2))||intro T;
      try reflexivity.
    - rewrite IHe1,IHe2 at 1 by auto.
      destruct (testA A e1),(testA A e2);repeat apply plus_right;reflexivity.
    - rewrite IHe1,IHe2 at 1 by auto;reflexivity.
    - rewrite IHe1,IHe2 at 1 by auto;reflexivity.
    - rewrite IHe at 1 by auto;reflexivity.
    - rewrite IHe at 1 by auto;reflexivity.
  Qed.

  Corollary one_free_project A e : one_free e = true -> test A ⋅ e ≡ test A ⋅ project A e.
  Proof.
    intro O; apply smaller_PartialOrder;unfold Basics.flip;split.
    - rewrite <- one_free_project_inf by apply O;reflexivity.
    - rewrite test_dup at 1.
      rewrite <-ax_seq_assoc,project_spec;reflexivity.
  Qed.

  Close Scope expr_scope.
End s.
(* begin hide *)
Arguments 𝐄_one {X}.
Arguments 𝐄_zero {X}.
Arguments inter_1 {X dec_X}.
Arguments expand {X dec_X}.
Arguments eqb {X} {dec_X} e%expr f%expr.
Hint Constructors 𝐄_eq ax ax_impl.
Hint Rewrite @𝐄_size_one @𝐄_size_zero @𝐄_size_var @𝐄_size_seq
     @𝐄_size_inter @𝐄_size_plus @𝐄_size_conv @𝐄_size_iter
  : simpl_typeclasses.
Arguments project {X dec_X} A e.
Arguments testA {X dec_X} A e.

(* end hide *)

Infix " ⋅ " := 𝐄_seq (at level 40) : expr_scope.
Infix " + " := 𝐄_plus (left associativity, at level 50) : expr_scope.
Infix " ∩ " := 𝐄_inter (at level 45) : expr_scope.
Notation "x ¯" := (𝐄_conv x) (at level 25) : expr_scope.
Notation "x ⁺" := (𝐄_iter x) (at level 25) : expr_scope.
Notation " 1 " := 𝐄_one : expr_scope.
Notation " 0 " := 𝐄_zero : expr_scope.

Require Import sp_terms.

Section terms.
  Context { X : Set } {decX : decidable_set X}.

  Fixpoint iter_prod (P : 𝐒𝐏 (@X' X) -> Prop) n :=
    match n with
    | O => P
    | S n => fun w => exists w1 w2, w = w1 ⨾ w2 /\ P w1 /\ iter_prod P n w2
    end.

  Fixpoint prime (e : 𝐒𝐏 (@X' X)) : 𝐒𝐏 (@X' X) :=
    match e with
    | 𝐒𝐏_var (x,true) => 𝐒𝐏_var (x,false) 
    | 𝐒𝐏_var (x,false) => 𝐒𝐏_var (x,true)
    | e ⨾ f => prime f ⨾ prime e
    | e ∥ f => prime e ∥ prime f
    end.

  Notation " e * " := (prime e) (at level 30).

  Lemma prime_idem w : w * * = w.
  Proof.
    induction w as [(?&[|])| |];simpl;reflexivity||congruence.
  Qed.

  Lemma prime_var w : 𝐒𝐏'_variables (w *) ≈ 𝐒𝐏'_variables w.
  Proof. induction w as [(?&[])| |];simpl;intro;mega_simpl;simpl; firstorder. Qed.
    
  Global Instance prime_monotone A : is_balanced A -> Proper(𝐒𝐏_inf A ==> 𝐒𝐏_inf A) prime.
  Proof.
    intros B u v E;induction E;eauto.
    - destruct H as [[]|[]];simpl;auto.
    - destruct H;simpl;auto.
      + apply 𝐒𝐏_inf_axinf,𝐒𝐏_axinf_1_seqr.
        cut (𝐒𝐏'_variables e ⊆ A).
        * intro h;rewrite<- prime_var in h.
          intros (a,b) Ia;apply h,𝐒𝐏_variables_𝐒𝐏'_variables_iff;destruct b;tauto.
        * intros (a,b) Ia;apply 𝐒𝐏_variables_𝐒𝐏'_variables_iff in Ia as [Ia|Ia];
            destruct b;apply H,Ia||apply B,H,Ia.
      + apply 𝐒𝐏_inf_axinf,𝐒𝐏_axinf_1_seql.
        cut (𝐒𝐏'_variables e ⊆ A).
        * intro h;rewrite<- prime_var in h.
          intros (a,b) Ia;apply h,𝐒𝐏_variables_𝐒𝐏'_variables_iff;destruct b;tauto.
        * intros (a,b) Ia;apply 𝐒𝐏_variables_𝐒𝐏'_variables_iff in Ia as [Ia|Ia];
            destruct b;apply H,Ia||apply B,H,Ia.
    - simpl;rewrite IHE1,IHE2;reflexivity.
    - simpl;rewrite IHE1,IHE2;reflexivity.
  Qed.

  Global Instance terms_inf : Smaller (𝐒𝐏 (@X' X)) := (𝐒𝐏_inf []).
  Global Instance terms_eq : Equiv (𝐒𝐏 (@X' X)) := (fun u v => u ≤ v /\ v ≤ u).
  Global Instance terms_eq_equiv : Equivalence equiv.
  Proof.
    split.
    - intros e;split;reflexivity.
    - intros e f (E1&E2);split;assumption.
    - intros e f g (E11&E12) (E21&E22);split;etransitivity;eassumption.
  Qed.
  Global Instance terms_inf_order : PartialOrder equiv smaller.
  Proof. split;unfold equiv,terms_eq;auto. Qed.
  
  Global Instance prime_nil_monotone : Proper(smaller ==> smaller) prime.
  Proof. apply prime_monotone,balanced_nil. Qed.
  
  Global Instance prime_nil_morph : Proper(equiv ==> equiv) prime.
  Proof. intros u v (E1&E2);split;[rewrite E1|rewrite E2];reflexivity. Qed.

  Global Instance ax_smaller : subrelation 𝐒𝐏_ax smaller.
  Proof. intros e f A;apply 𝐒𝐏_inf_ax;auto. Qed.
  Global Instance ax_smaller_bis : subrelation (Basics.flip 𝐒𝐏_ax) smaller.
  Proof. intros e f A;apply 𝐒𝐏_inf_ax;auto. Qed.
  Global Instance ax_eq : subrelation 𝐒𝐏_ax equiv.
  Proof. intros e f A;split;rewrite A;reflexivity. Qed.
  Global Instance ax_eq_bis : subrelation (Basics.flip 𝐒𝐏_ax) equiv.
  Proof. intros e f A;split;rewrite A;reflexivity. Qed.

  Lemma iter_prod_last L w n : (exists w', w ≡ w' /\ iter_prod L (S n) w')
                               <-> exists w1 w2, w ≡ w1 ⨾ w2 /\ iter_prod L n w1 /\ L w2.
  Proof.
    revert w;induction n;simpl;intro w;split.
    - intros (w'&E&(w1&w2&->&I1&I2)).
      exists w1,w2;split;[auto|tauto].
    - intros (w1&w2&E&I1&I2).
      exists (w1⨾ w2);split;[auto|].
      exists w1,w2;auto.
    - intros (w'&E1&w1&w2&->&I1&I2).
      assert (I2': exists w', (w2 ≡ w') /\ iter_prod L (S n) w') by (exists w2;split;[reflexivity|apply I2]).
      apply IHn in I2' as (w2'&w3&E&I2'&I3).
      exists (w1⨾ w2'),w3;split;[rewrite E1,E,𝐒𝐏_seq_assoc;reflexivity|].
      split;[exists w1,w2'|];auto.
    - intros (w0&w3&E&(w1&w2&->&I1&I2)&I3).
      assert (I2': exists w', (w2⨾w3 ≡ w') /\ iter_prod L (S n) w')
        by (apply IHn;exists w2,w3;split;[reflexivity|auto]).
      destruct I2' as (w'&E'&I2').
      exists (w1⨾w');split;[|exists w1,w';auto].
      rewrite E,<-E',𝐒𝐏_seq_assoc;reflexivity.
  Qed.      
    
  Fixpoint sp_terms (e : 𝐄 X) := 
    match e with
    | 1 => fun w => False
    | 0 => fun _ => False
    | 𝐄_var x => fun w => w = 𝐒𝐏_var (x,true)
    | e ¯ => fun w => sp_terms e (w*)
    | e1 ⋅ e2 => fun w => exists w1 w2, w = w1 ⨾ w2 /\ sp_terms e1 w1 /\ sp_terms e2 w2
    | e1 ∩ e2 => fun w => exists w1 w2, w = w1 ∥ w2 /\ sp_terms e1 w1 /\ sp_terms e2 w2
    | e1 + e2 => fun w => sp_terms e1 w \/ sp_terms e2 w
    | e ⁺ => fun w => exists n, iter_prod (sp_terms e) n w
    end.

  Definition sp_terms_inf e f := (forall w, sp_terms e w -> exists w', sp_terms f w' /\ w ≤ w').
  Definition sp_terms_eq e f := sp_terms_inf e f /\ sp_terms_inf f e. 

  Infix " ≦ " := sp_terms_inf (at level 80).
  Infix " ⩵ " := sp_terms_eq (at level 80).

  Global Instance sp_terms_inf_PreOrder : PreOrder sp_terms_inf.
  Proof.
    split.
    - intros e w Iw;exists w;split;[assumption|reflexivity].
    - intros e f g E1 E2 w1 Iw1;apply E1 in Iw1 as (w2&Iw2&Ew2);apply E2 in Iw2 as (w3&Iw3&Ew3).
      exists w3;split;[assumption|rewrite Ew2,Ew3;reflexivity].
  Qed.

  Global Instance sp_terms_eq_Equivalence : Equivalence sp_terms_eq.
  Proof.
    split.
    - intro e;split;reflexivity.
    - intros e f (E1&E2);split;assumption.
    - intros e f g (E1&E2) (E3&E4);split;etransitivity;eassumption.
  Qed.

  Global Instance sp_terms_inf_PartialOrder : PartialOrder sp_terms_eq sp_terms_inf.
  Proof. unfold sp_terms_eq,sp_terms_inf;split;auto. Qed.

  Global Instance seq_sp_terms_inf :
    Proper (sp_terms_inf ==> sp_terms_inf ==> sp_terms_inf) (@𝐄_seq X).
  Proof.
    intros e1 e2 ih1 f1 f2 ih2 w (w1&w2&->&Iw1&Iw2).
    apply ih1 in Iw1 as (w1'&Iw1&Ew1).
    apply ih2 in Iw2 as (w2'&Iw2&Ew2).
    exists (w1'⨾w2');split.
    - exists w1',w2';tauto.
    - rewrite Ew1,Ew2;reflexivity.
  Qed.
  
  Global Instance plus_sp_terms_inf :
    Proper (sp_terms_inf ==> sp_terms_inf ==> sp_terms_inf) (@𝐄_plus X).
  Proof.
    intros e1 e2 ih1 f1 f2 ih2 w [Iw1|Iw1].
    - apply ih1 in Iw1 as (w2&Iw2&Ew2).
      exists w2;split;[left|];assumption.
    - apply ih2 in Iw1 as (w2&Iw2&Ew2).
      exists w2;split;[right|];assumption.
  Qed.

  Global Instance inter_sp_terms_inf :
    Proper (sp_terms_inf ==> sp_terms_inf ==> sp_terms_inf) (@𝐄_inter X).
  Proof.
    intros e1 e2 ih1 f1 f2 ih2 w (w1&w2&->&Iw1&Iw2).
    apply ih1 in Iw1 as (w1'&Iw1&Ew1).
    apply ih2 in Iw2 as (w2'&Iw2&Ew2).
    exists (w1'∥w2');split.
    - exists w1',w2';tauto.
    - rewrite Ew1,Ew2;reflexivity.
  Qed.

  Global Instance iter_sp_terms_inf :
    Proper (sp_terms_inf ==> sp_terms_inf) (@𝐄_iter X).
  Proof.
    intros e1 e2 ih1 w (n&Iw);simpl;revert w Iw;induction n;simpl;
      intros w (w1&w2&->&Iw1&Iw2)||intros w1 Iw1;
      apply ih1 in Iw1 as (w1'&Iw1&Ew1);
      try (apply IHn in Iw2 as (w2'&(n'&Iw2)&Ew2)).
    - exists w1';split;[exists O|];auto.
    - exists (w1'⨾w2');split;[exists (S n'),w1',w2'|];auto.
      rewrite Ew1,Ew2;reflexivity.
  Qed.

   Global Instance conv_sp_terms_inf :
    Proper (sp_terms_inf ==> sp_terms_inf) (@𝐄_conv X).
  Proof.
    intros e1 e2 ih1 w Iw.
    apply ih1 in Iw as (w'&Iw&Ew).
    exists (w'*);split.
    - simpl;rewrite prime_idem;assumption.
    - rewrite <- (prime_idem w),Ew;reflexivity.
  Qed.


  Global Instance seq_sp_terms_eq :
    Proper (sp_terms_eq ==> sp_terms_eq ==> sp_terms_eq) (@𝐄_seq X).
  Proof.
    intros e1 e2 (Ee1&Ee2) f1 f2 (Ef1&Ef2);split;[rewrite Ee1,Ef1|rewrite Ee2,Ef2];reflexivity.
  Qed.
  
  Global Instance plus_sp_terms_eq :
    Proper (sp_terms_eq ==> sp_terms_eq ==> sp_terms_eq) (@𝐄_plus X).
  Proof.
    intros e1 e2 (Ee1&Ee2) f1 f2 (Ef1&Ef2);split;[rewrite Ee1,Ef1|rewrite Ee2,Ef2];reflexivity.
  Qed.

  Global Instance inter_sp_terms_eq :
    Proper (sp_terms_eq ==> sp_terms_eq ==> sp_terms_eq) (@𝐄_inter X).
  Proof.
    intros e1 e2 (Ee1&Ee2) f1 f2 (Ef1&Ef2);split;[rewrite Ee1,Ef1|rewrite Ee2,Ef2];reflexivity.
  Qed.

  Global Instance iter_sp_terms_eq :
    Proper (sp_terms_eq ==> sp_terms_eq) (@𝐄_iter X).
  Proof.
    intros e1 e2 (Ee1&Ee2);split;[rewrite Ee1|rewrite Ee2];reflexivity.
  Qed.

  Global Instance conv_sp_terms_eq :
    Proper (sp_terms_eq ==> sp_terms_eq) (@𝐄_conv X).
  Proof.
    intros e1 e2 (Ee1&Ee2);split;[rewrite Ee1|rewrite Ee2];reflexivity.
  Qed.

  Definition balance (A : list X) : list (@X' X) :=
    flat_map (fun a => [(a,true);(a,false)]) A.
  Lemma balance_balanced A : is_balanced (balance A).
  Proof.
    intro a;unfold balance;repeat rewrite in_flat_map;simpl.
    split;intros (x&I&E);repeat destruct E as [E|E];inversion E;subst;exists a;auto.
  Qed.

  Lemma balance_vars t : 𝒱 t ++ 𝒱 (t*) ≈ 𝐒𝐏'_variables t.
  Proof.
    induction t as [(x,[])| |];rsimpl.
    - intro;simpl;tauto.
    - intro;simpl;tauto.
    - rewrite <- IHt1,<-IHt2;clear;intro;mega_simpl;tauto.
    - rewrite <- IHt1,<-IHt2;clear;intro;mega_simpl;tauto.
  Qed. 
    
  Definition red_prod {X : Set}`{decidable_set X} (A : list X) e f :=
    match testA A e,testA A f with
    | true,true => [e ;f; e ⋅ f]
    | true,false => [f; e ⋅ f]
    | false,true => [e ; e⋅f]
    | false,false => [e ⋅ f]
    end.
      
  
  Fixpoint reduce_expr A e :=
    match e with
    | 0 => []
    | 1 => []
    | 𝐄_var x => [𝐄_var x]
    | e + f => reduce_expr A e ++ reduce_expr A f
    | e ⋅ f => concat (bimap (red_prod A) (reduce_expr A e) (reduce_expr A f))
    | e ∩ f => bimap (@𝐄_inter X) (reduce_expr A e) (reduce_expr A f)
    | e ¯ => map (@𝐄_conv X) (reduce_expr A e)
    | e ⁺ => map (fun l => (Σ l)⁺) (rm [] (subsets (reduce_expr A e)))
    end.

  Lemma sum_sp_terms l s : sp_terms (Σ l) s <-> exists e, e ∈ l /\ sp_terms e s.
  Proof.
    revert s;induction l as [|e l];simpl.
    - intros s;split;[|intros (?&F&_)];tauto.
    - intros s;destruct l;[|simpl;rewrite IHl;clear;firstorder subst;auto].
      clear IHl;firstorder;subst;auto.
  Qed.
  
  Lemma sum_iter_incl_sp l m s : l ⊆ m -> sp_terms (Σ l ⁺) s -> sp_terms (Σ m ⁺) s.
  Proof.
    intros h (n&I);exists n;revert s I;induction n;intros s;simpl.
    - repeat rewrite sum_sp_terms.
      intros (e&Ie&Is);exists e;split;[apply h|];auto.
    - intros (s1&s2&->&I1&I2);exists s1,s2;repeat split;[|apply IHn,I2].
      repeat rewrite sum_sp_terms in *.
      destruct I1 as (e&Ie&Is);exists e;split;[apply h|];auto.
  Qed.
  Corollary sum_iter_eq_sp l m s : l ≈ m -> sp_terms (Σ l ⁺) s <-> sp_terms (Σ m ⁺) s.
  Proof. intros E;split;apply sum_iter_incl_sp;rewrite E;reflexivity. Qed.
    
  Lemma reduce_expr_sp_terms A e t :
    sp_terms e t ->
    exists f, f ∈ (reduce_expr A e) /\ sp_terms f t.
  Proof.
    revert t;induction e;simpl;try tauto.
    - intros ? -> ;exists (𝐄_var x);simpl;auto.
    - intros ? (t1&t2&->&I1&I2).
      apply IHe1 in I1 as (f1&If1&It1).
      apply IHe2 in I2 as (f2&If2&It2).
      exists (f1⋅f2);split;simpl.
      + apply in_concat;exists (red_prod A f1 f2);split.
        * unfold red_prod;simpl;case_eq (testA A f1);case_eq(testA A f2);simpl;tauto.
        * apply in_bimap;exists f1,f2;split;auto.
      + exists t1,t2;auto.
    - intros ? (t1&t2&->&I1&I2).
      apply IHe1 in I1 as (f1&If1&It1).
      apply IHe2 in I2 as (f2&If2&It2).
      exists (f1 ∩ f2);split;simpl.
      + apply in_bimap;exists f1,f2;split;auto.
      + exists t1,t2;auto.
    - intros t [I|I];[apply IHe1 in I as (f&If&It)|apply IHe2 in I as (f&If&It)];
        exists f;split;auto;mega_simpl.
    - intros t I;apply IHe in I as (f&If&It);exists (f ¯);split;auto.
      apply in_map_iff;exists f;auto.
    - intros t (n&I);revert t I;induction n.
      + intros t I;apply IHe in I as (f&If&It);exists (Σ[f]⁺);split.
        * apply in_map_iff;exists [f];split;auto.
          apply in_rm;split;[discriminate|].
          revert If;clear;induction (reduce_expr A e);simpl;auto.
          intros [->|I];mega_simpl.
          -- left;apply in_map_iff;exists [];split;auto.
             clear;induction l;mega_simpl;auto.
          -- tauto.
        * simpl;exists O;simpl;auto.
      + intros t (t1&t2&->&I1&I2).
        apply IHe in I1 as (f1&If1&It1).
        apply IHn in I2 as (f2&If2&It2).
        apply in_map_iff in If2 as (E&<-&Ie).
        apply in_rm in Ie as (_&Ie).
        assert (IE : f1::E ⊆ reduce_expr A e)
          by (intros ? [<-|I];[|eapply subsets_In];eauto).
        apply subsets_spec in IE as (F&I__F&Eq).
        exists (Σ F ⁺);split.
        * apply in_map_iff;exists F;split;auto.
          apply in_rm;split;auto.
          intros ->;cut (f1∈[]);[simpl;auto|apply Eq;now left].
        * apply (sum_iter_eq_sp _ Eq).
          assert (I__E: E ⊆ f1 :: E) by (intro;mega_simpl).
          apply (sum_iter_incl_sp I__E) in It2.
          destruct It2 as (m&It2);exists (S m),t1,t2;repeat split;simpl;auto.
          destruct E;simpl;auto.
  Qed.

  Lemma balance_app l m : balance (l++m) = balance l ++ balance m.
  Proof.
    unfold balance;repeat rewrite flat_map_concat_map.
    rewrite <-concat_app,<- map_app;reflexivity.
  Qed.
  
  Lemma sp_terms_vars_incl e t :
    sp_terms e t -> 𝐒𝐏'_variables t ⊆ balance (Vars e).
  Proof.
    revert t;induction e;simpl;try tauto.
    - intros t -> ? [<-|F];simpl in *;tauto.
    - intros t (t1&t2&->&I1&I2);rsimpl;apply IHe1 in I1 as ->;apply IHe2 in I2 as ->.
      rewrite balance_app;reflexivity.
    - intros t (t1&t2&->&I1&I2);rsimpl;apply IHe1 in I1 as ->;apply IHe2 in I2 as ->.
      rewrite balance_app;reflexivity.
    - intros ? [I|I];[apply IHe1 in I as ->|apply IHe2 in I as ->];
        rewrite balance_app;[apply incl_appl|apply incl_appr];reflexivity.
    - intros ? I;apply IHe in I.
      rewrite prime_var in I;assumption.
    - intros t (n&I);revert t I;induction n.
      + apply IHe.
      + intros t (t1&t2&->&I1&I2);rsimpl;apply IHe in I1 as ->;apply IHn in I2 as ->.
        clear;intro;mega_simpl.
  Qed.

  Lemma Vars_sum {A : Set} (a : A) L : a ∈ Vars (Σ L) <-> exists e, e ∈ L /\ a ∈ Vars e.
  Proof. induction L;simpl;[|destruct L;mega_simpl;[|rewrite IHL]];clear;firstorder subst;auto. Qed.
  
  Lemma reduce_epxr_sp_terms_vars_incl A e t :
    sp_terms e t ->
    exists f, f ∈ (reduce_expr A e) /\ sp_terms f t /\ balance (Vars f) ⊆ 𝐒𝐏'_variables t.
  Proof.
    revert t;induction e;simpl;try tauto.
    - intros ? -> ;exists (𝐄_var x);repeat split;simpl;reflexivity||auto.
    - intros ? (t1&t2&->&I1&I2).
      apply IHe1 in I1 as (f1&If1&It1&h1).
      apply IHe2 in I2 as (f2&If2&It2&h2).
      exists (f1⋅f2);repeat split;simpl.
      + apply in_concat;exists (red_prod A f1 f2);split.
        * unfold red_prod;simpl;case_eq (testA A f1);case_eq(testA A f2);simpl;tauto.
        * apply in_bimap;exists f1,f2;split;auto.
      + exists t1,t2;auto.
      + rewrite <- h1, <- h2,balance_app;reflexivity.
    - intros ? (t1&t2&->&I1&I2).
      apply IHe1 in I1 as (f1&If1&It1&h1).
      apply IHe2 in I2 as (f2&If2&It2&h2).
      exists (f1 ∩ f2);repeat split;simpl.
      + apply in_bimap;exists f1,f2;split;auto.
      + exists t1,t2;auto.
      + rewrite <- h1, <- h2,balance_app;reflexivity.
    - intros t [I|I];[apply IHe1 in I as (f&If&It)|apply IHe2 in I as (f&If&It)];
        exists f;split;auto;mega_simpl.
    - intros t I;apply IHe in I as (f&If&It&h);exists (f ¯);repeat split;auto.
      + apply in_map_iff;exists f;auto.
      + simpl;rewrite prime_var in h;assumption.
    - intros t (n&I);revert t I;induction n.
      + intros t I;apply IHe in I as (f&If&It&h);exists (Σ[f]⁺);split.
        * apply in_map_iff;exists [f];split;auto.
          apply in_rm;split;[discriminate|].
          revert If;clear;induction (reduce_expr A e);simpl;auto.
          intros [->|I];mega_simpl.
          -- left;apply in_map_iff;exists [];split;auto.
             clear;induction l;mega_simpl;auto.
          -- tauto.
        * split.
          -- simpl;exists O;simpl;auto.
          -- simpl;assumption.
      + intros t (t1&t2&->&I1&I2).
        apply IHe in I1 as (f1&If1&It1&h1).
        apply IHn in I2 as (f2&If2&It2&h2).
        apply in_map_iff in If2 as (E&<-&Ie).
        apply in_rm in Ie as (_&Ie).
        assert (IE : f1::E ⊆ reduce_expr A e)
          by (intros ? [<-|I];[|eapply subsets_In];eauto).
        apply subsets_spec in IE as (F&I__F&Eq).
        exists (Σ F ⁺);repeat split.
        * apply in_map_iff;exists F;split;auto.
          apply in_rm;split;auto.
          intros ->;cut (f1∈[]);[simpl;auto|apply Eq;now left].
        * apply (sum_iter_eq_sp _ Eq).
          assert (I__E: E ⊆ f1 :: E) by (intro;mega_simpl).
          apply (sum_iter_incl_sp I__E) in It2.
          destruct It2 as (m&It2);exists (S m),t1,t2;repeat split;simpl;auto.
          destruct E;simpl;auto.
        * rsimpl;rewrite <- h1,<-h2,<-balance_app;simpl.
          revert Eq;clear.
          unfold balance;intros Eq a Ia;apply in_flat_map in Ia as (b&Ib&Ia).
          apply in_flat_map;exists b;split;auto.
          clear a Ia;apply Vars_sum in Ib as (e&Ie&Ib);apply Eq in Ie as [<-|Ie];mega_simpl.
          right;apply Vars_sum;exists e;tauto.
  Qed.

  Fixpoint positive {X} (e : 𝐄 X) :=
    match e with
    | 0 | 1 => false
    | e + f | e ⋅ f | e ∩ f => positive e && positive f
    | e ¯ | e ⁺ => positive e
    | _ => true
    end.

  Lemma positive_reduce_expr A e f : f ∈ reduce_expr A e -> positive f = true.
  Proof.
    revert f;induction e;intro f;simpl;auto.
    - intros [<-|F];auto.
    - intro I;apply in_concat in I as (F&I__F&I).
      apply in_bimap in I as (f1&f2&<-&I1&I2).
      apply IHe1 in I1;apply IHe2 in I2.
      revert I__F;unfold red_prod;case_eq (testA A f1);case_eq(testA A f2);simpl;
        intros _ _ E;repeat destruct E as [<-|E];auto;apply andb_true_iff;auto.
    - intro I;apply in_bimap in I as (f1&f2&<-&I1&I2).
      apply IHe1 in I1;apply IHe2 in I2.
      apply andb_true_iff;auto.
    - intro I;apply in_app_iff in I as [I|I];[apply IHe1|apply IHe2];assumption.
    - intro I;apply in_map_iff in I as (f'&<-&I);simpl.
      apply IHe,I.
    - intro I;apply in_map_iff in I as (F&<-&I);simpl.
      apply in_rm in I as (N&I).
      apply subsets_In in I.
      induction F as [|f F];simpl;[tauto|].
      destruct F;[apply IHe|apply andb_true_iff;split;[apply IHe|apply IHF]];
        (apply I;simpl;auto)||discriminate||(rewrite <- I;intro;mega_simpl;tauto).
  Qed.
      
  Lemma testA_vars A e : positive e = true -> Vars e ⊆ A -> testA A e = true.
  Proof.
    induction e;simpl;try discriminate;repeat (rewrite andb_true_iff||rewrite orb_true_iff);auto.
    - intros _ I;apply inb_spec,I;now left.
    - intros (P1&P2) V;rewrite IHe1,IHe2;auto;rewrite <- V;intro;mega_simpl.
    - intros (P1&P2) V;rewrite IHe1,IHe2;auto;rewrite <- V;intro;mega_simpl.
    - intros (P1&P2) V;rewrite IHe1,IHe2;auto;rewrite <- V;intro;mega_simpl.
  Qed.

  Lemma reduce_expr_var A e f : f ∈ reduce_expr A e -> Vars f ⊆ Vars e.
  Proof.
    revert f;induction e;simpl;intro f;try tauto.
    - intros [<-|F];[|tauto];reflexivity.
    - intros If;apply in_concat in If as (L&If&IL).
      apply in_bimap in IL as (f1&f2&<-&I1&I2).
      rewrite <- (IHe1 _ I1),<-(IHe2 _ I2).
      revert If;unfold red_prod;destruct (testA A f1),(testA A f2);intro I;repeat destruct I as [I|I];
        inversion I;subst;rsimpl;
          reflexivity||(apply incl_appr;reflexivity)||(apply incl_appl;reflexivity).
    - intros If;apply in_bimap in If as (f1&f2&<-&I1&I2).
      rewrite <- (IHe1 _ I1),<-(IHe2 _ I2);rsimpl;reflexivity.
    - intros If;apply in_app_iff in If as [I|I];apply IHe1 in I as <- ||apply IHe2 in I as <- ;
        reflexivity||(apply incl_appr;reflexivity)||(apply incl_appl;reflexivity).
    - intro I;apply in_map_iff in I as (f'&<-&I);apply IHe in I;apply I.
    - intro I;apply in_map_iff in I as (L&<-&I);apply in_rm in I as (_&I);simpl.
      apply subsets_In in I.
      intros a Ia;apply Vars_sum in Ia as (f&If&Ia).
      apply I,IHe in If;apply If in Ia;auto.
  Qed.

  Lemma balance_incl_iff l m : l ⊆ m <-> balance l ⊆ balance m.
  Proof.
    split;[intros I a;unfold balance;repeat rewrite in_flat_map;
           intros (x&Ix&hx);apply I in Ix;exists x;auto|].
    intros I a Ia.
    cut ((a,true)∈balance m);unfold balance in *.
    - intro I';apply in_flat_map in I' as (b&Ib&Eb);repeat destruct Eb as [Eb|Eb];inversion Eb;subst;auto.
    - apply I,in_flat_map;exists a;simpl;auto.
  Qed.

  Lemma is_balanced_balance L : is_balanced L -> L ≈ balance (map fst L).
  Proof.
    intros B (a,b);unfold balance;rewrite in_flat_map;setoid_rewrite in_map_iff;simpl.
    split.
    - intros I;exists a;split;[exists (a,b);simpl|destruct b];auto.
    - intros (x&((a',b')&<-&IL)&E).
      repeat destruct E as [E|E];inversion E;subst;destruct b';apply IL||apply B,IL.
  Qed.

  Lemma prime_incl_bal A s : 𝒱 s  ⊆ balance A <-> 𝒱 (s*) ⊆ balance A.
  Proof.
    split;intros I;
      (transitivity (𝐒𝐏'_variables s);[rewrite <- balance_vars;(apply incl_appl;reflexivity)
                                                              ||(apply incl_appr;reflexivity)|]);
      rewrite (is_balanced_balance (is_balanced_𝐒𝐏'_variables _));apply balance_incl_iff;
        [|rewrite <- prime_var];rewrite <- fst_𝐒𝐏_variables,I;
          apply balance_incl_iff;rewrite <- is_balanced_balance by apply balance_balanced;reflexivity.
  Qed.

  Lemma subsets_nil {A : Set} (l : list A) : []∈ subsets l.
  Proof. induction l;mega_simpl;auto. Qed.
  Lemma subsets_singleton {A : Set} (a : A) l : a ∈ l -> [a]∈ subsets l.
  Proof.
    induction l;mega_simpl;simpl;auto.
    subst;left;apply in_map_iff;exists [];split;[|apply subsets_nil];auto.
  Qed.                                                           

  Lemma witness_positive e : positive e = true -> exists t, sp_terms e t.
  Proof.
    induction e;simpl;try discriminate;repeat rewrite andb_true_iff;intros (P1&P2)||intros P.
    - exists (𝐒𝐏_var (x,true));auto.
    - destruct IHe1 as (t1&I1),IHe2 as (t2&I2);auto.
      exists (t1⨾t2),t1,t2;auto.
    - destruct IHe1 as (t1&I1),IHe2 as (t2&I2);auto.
      exists (t1∥t2),t1,t2;auto.
    - destruct IHe1 as (t1&I1);auto.
      exists t1;auto.
    - destruct IHe as (t&I);auto.
      exists (t* );rewrite prime_idem;auto.
    - destruct IHe as (t&I);auto.
      exists t,O;auto.
  Qed.

  (* Lemma reduce_expr_not_nil_positive A e : reduce_expr A e <> [] -> exists t, sp_terms e t. *)
  (* Proof. *)
  (*   induction e;simpl;try tauto. *)
  (*   - intros _;eexists;reflexivity. *)
  (*   - intro h;destruct IHe1 as (t1&I1);[|destruct IHe2 as (t2&I2)]. *)
  (*     + intros E;apply h;rewrite E;reflexivity. *)
  (*     + intros E;apply h;rewrite E;clear;induction (reduce_expr A e1);simpl;auto. *)
  (*     + exists (t1⨾t2). *)
  (*   -  *)

  (* Qed. *)
  
  Lemma sp_terms_reduce_term_project A e :
    (* one_free e = true -> *)
    forall t, (exists t' s, t ≡ t' /\ t' ∈ reduce_term (balance A) s /\ sp_terms e s)
         -> (exists t', t ≡ t' /\ sp_terms (Σ (reduce_expr A e)) t').
  Proof.
    intro t(* ;split *).
    - intros (t'&s&E&I&SP);setoid_rewrite E;clear E t.
      revert s t' SP I;induction e;intros s t;simpl;try tauto.
      + intros -> [<-|F];[|simpl in F;tauto].
        exists (𝐒𝐏_var (x, true));split;[reflexivity|auto].
      + intros (s1&s2&->&Is1&Is2);intro I.
        cut (t ∈ bimap 𝐒𝐏_seq (reduce_term (balance A) s1) (reduce_term (balance A) s2)
             \/ (t ∈ reduce_term (balance A) s1 /\ 𝒱 s2  ⊆ (balance A))
             \/ (t ∈ reduce_term (balance A) s2 /\ 𝒱 s1  ⊆ (balance A)));
          [clear I|
           revert I;simpl;repeat rewrite <- inclb_correct;clear;
           destruct (inclb (𝒱 s1) (balance A)), (inclb (𝒱 s2) (balance A));
           repeat rewrite in_app_iff;tauto].
        rewrite in_bimap.
        intros [(t1&t2&<-&It1&It2)|[(It&I1)|(It&I2)]].
        * destruct (IHe1 _ _ Is1 It1) as (t'1&E1&I'1),(IHe2 _ _ Is2 It2) as (t'2&E2&I'2).
          exists (t'1⨾t'2);split;[rewrite E1,E2;reflexivity|].
          clear t1 t2 E1 E2 It1 It2 s1 s2 Is1 Is2 IHe1 IHe2.
          rewrite sum_sp_terms in *.
          destruct I'1 as (f1&If1&I1),I'2 as (f2 &If2&I2).
          exists (f1⋅f2);split;[|exists t'1,t'2;auto].
          apply in_concat;exists (red_prod A f1 f2);split;[|apply in_bimap;exists f1,f2;auto].
          unfold red_prod.
          destruct (testA A f1),(testA A f2);simpl;auto.
        * destruct (IHe1 _ _ Is1 It) as (t'&E&I');exists t';split;auto.
          clear t E It s1 Is1 IHe1 IHe2.
          rewrite sum_sp_terms in *.
          apply reduce_epxr_sp_terms_vars_incl with (A:=A) in Is2 as (f2&If2&Is2&I2).
          rewrite (is_balanced_balance (is_balanced_𝐒𝐏'_variables s2)),
            <- balance_incl_iff in I2.
          destruct I' as (f1&If1&It').
          exists f1;split;auto.
          apply in_concat;exists (red_prod A f1 f2);split;[|apply in_bimap;exists f1,f2;auto].
          unfold red_prod.
          case_eq (testA A f2);[case_eq (testA A f1);simpl;auto|intro T;exfalso].
          apply not_true_iff_false in T;apply T.
          apply testA_vars.
          -- eapply positive_reduce_expr,If2.
          -- rewrite I2,<- fst_𝐒𝐏_variables.
             rewrite I1. 
             apply balance_incl_iff.
             rewrite <- is_balanced_balance by apply balance_balanced;reflexivity.
        * destruct (IHe2 _ _ Is2 It) as (t'&E&I');exists t';split;auto.
          clear t E It s2 Is2 IHe1 IHe2.
          rewrite sum_sp_terms in *.
          apply reduce_epxr_sp_terms_vars_incl with (A:=A) in Is1 as (f1&If1&Is1&I1).
          rewrite (is_balanced_balance (is_balanced_𝐒𝐏'_variables s1)),
            <- balance_incl_iff in I1.
          destruct I' as (f2&If2&It').
          exists f2;split;auto.
          apply in_concat;exists (red_prod A f1 f2);split;[|apply in_bimap;exists f1,f2;auto].
          unfold red_prod.
          case_eq (testA A f1);[case_eq (testA A f2);simpl;auto|intro T;exfalso].
          apply not_true_iff_false in T;apply T.
          apply testA_vars.
          -- eapply positive_reduce_expr,If1.
          -- rewrite I1,<- fst_𝐒𝐏_variables.
             rewrite I2. 
             apply balance_incl_iff.
             rewrite <- is_balanced_balance by apply balance_balanced;reflexivity.
      + intros (s1&s2&->&Is1&Is2) It.
        simpl in It;apply in_bimap in It as (t1&t2& <- &It1&It2).
        destruct (IHe1 _ _ Is1 It1) as (t1'&E1&I1),(IHe2 _ _ Is2 It2) as (t2'&E2&I2).
        exists (t1'∥ t2');split;[rewrite E1,E2;reflexivity|].
        rewrite sum_sp_terms in *.
        destruct I1 as (f1&If1&I1),I2 as (f2 &If2&I2).
        exists (f1∩f2);split;[|exists t1',t2';auto].
        apply in_bimap;exists f1,f2;auto.
      + intros [Is|Is] It;[destruct (IHe1 _ _ Is It) as (t'&E&I)
                          |destruct (IHe2 _ _ Is It) as (t'&E&I)];
        (exists t';split;[rewrite E;reflexivity|]);
        rewrite sum_sp_terms in *;destruct I as (f&If&I);
          exists f;split;auto;mega_simpl.
      + intros Is It.
        destruct (IHe _ (t* ) Is) as (t'&E&I).
        * revert It;clear;revert t;induction s;simpl;intro t.
          -- intros [<-|F];[|tauto].
             destruct x as (x&[]);simpl;auto.
          -- case_eq (inclb (𝒱 s1) (balance A));case_eq (inclb (𝒱 s2) (balance A));
               repeat rewrite <- not_true_iff_false;repeat rewrite inclb_correct;
                 rewrite (prime_incl_bal _ s1),(prime_incl_bal _ s2);
                 repeat rewrite <- inclb_correct;repeat rewrite not_true_iff_false;
                   intros -> -> ;mega_simpl;intuition.
             ++ repeat right;apply in_bimap.
                exists (u0* ),(u* );repeat split;intuition.
             ++ repeat right;apply in_bimap.
                exists (u0* ),(u* );repeat split;intuition.
             ++ repeat right;apply in_bimap.
                exists (u0* ),(u* );repeat split;intuition.
          -- intro I;apply in_bimap in I as (t1&t2&<-&I1&I2).
             apply in_bimap;exists (t1* ),(t2* );intuition.
        * exists (t'* );split;[rewrite <- E,prime_idem;reflexivity|].
          rewrite sum_sp_terms in *;destruct I as (f&If&I);
            exists (f ¯);split;mega_simpl;auto.
          simpl;rewrite prime_idem;auto.
      + intros (n&In);revert s t In;induction n;intros s t Is It;simpl.
        * apply (IHe s t Is) in It as (t'&E&It').
          exists t';split;[auto|];simpl in Is.
          rewrite sum_sp_terms in *;destruct It' as (f&If&I);
            exists (Σ[f]⁺);split.
          -- apply in_map_iff;exists [f].
             split;[reflexivity|].
             apply in_rm;split;[discriminate|].
             apply subsets_singleton,If.
          -- exists O;simpl;auto.
        * destruct Is as (s1&s2&->&Is1&Is2).
          cut (t ∈ bimap 𝐒𝐏_seq (reduce_term (balance A) s1) (reduce_term (balance A) s2)
               \/ (t ∈ reduce_term (balance A) s1 /\ 𝒱 s2  ⊆ (balance A))
               \/ (t ∈ reduce_term (balance A) s2 /\ 𝒱 s1  ⊆ (balance A)));
            [clear It|
             revert It;simpl;repeat rewrite <- inclb_correct;clear;
             destruct (inclb (𝒱 s1) (balance A)), (inclb (𝒱 s2) (balance A));
             repeat rewrite in_app_iff;tauto].
          rewrite in_bimap.
          intros [(t1&t2&<-&It1&It2)|[(It&I1)|(It&I2)]].
          -- destruct (IHe _ _ Is1 It1) as (t'1&E1&I'1),(IHn _ _ Is2 It2) as (t'2&E2&I'2).
             exists (t'1⨾t'2);split;[rewrite E1,E2;reflexivity|].
             clear t1 t2 E1 E2 It1 It2 s1 s2 Is1 Is2 IHe IHn.
             rewrite sum_sp_terms in *.
             destruct I'1 as (f1&If1&I1),I'2 as (f2 &If2&I2).
             apply in_map_iff in If2 as (L&<-&IL).
             apply in_rm in IL as (_&IL).
             assert (IL' : f1 :: L ⊆ reduce_expr A e)
               by (intros ? [<-|I];[assumption|apply subsets_In in IL;apply IL,I]).
             apply subsets_spec in IL' as (L'&IL'&EL).
             exists (Σ L' ⁺);split.
             ++ apply in_map_iff;exists L';split;auto.
                apply in_rm;split;[intros ->;cut (f1 ∈ []);[|apply EL]|];simpl;auto.
             ++ assert (h : L ⊆ L') by (rewrite <- EL;intro;simpl;auto).
                apply (sum_iter_incl_sp h) in I2.
                destruct I2 as (k&I).
                exists (S k),t'1,t'2;repeat split;auto.
                apply sum_sp_terms.
                exists f1;split;auto.
                apply EL;simpl;auto.
          -- destruct (IHe _ _ Is1 It) as (t'&E&I');exists t';split;auto.
             clear t E It s1 Is1 IHe IHn.
             rewrite sum_sp_terms in *.
             destruct I' as (f&If&It').
             exists (Σ[f]⁺);split;[|exists O;simpl;auto].
             apply in_map_iff;exists [f];split;auto.
             apply in_rm;split;[discriminate|].
             apply subsets_singleton,If.
          -- apply (IHn s2);auto.
    (* - intros (t'&E&I);setoid_rewrite E;clear E t. *)
    (*   apply sum_sp_terms in I as (f&If&I). *)
    (*   revert f t' If I;induction e;intros f t;simpl;try tauto. *)
    (*   + intros [<-|F];[|tauto];simpl. *)
    (*     intros ->. *)
    (*     exists (𝐒𝐏_var (x,true)),(𝐒𝐏_var (x,true));simpl;split;[reflexivity|auto]. *)
    (*   + intros If;apply in_concat in If as (L&If&IL). *)
    (*     apply in_bimap in IL as (f1&f2&<-&If1&If2). *)
    (*     cut (f = f1 ⋅ f2 *)
    (*          \/ (f = f1 /\ testA A f2 = true) *)
    (*          \/ (f = f2 /\ testA A f1 = true)); *)
    (*       [clear If *)
    (*       |revert If;clear;unfold red_prod;destruct (testA A f1),(testA A f2);mega_simpl;auto]. *)
    (*     intros [->|[(->&T)|(->&T)]]. *)
    (*     * simpl;intros (t1&t2&->&It1&It2). *)
    (*       destruct (IHe1 f1 t1 If1 It1) as (t1'&s1&E1&Is1&It1'). *)
    (*       destruct (IHe2 f2 t2 If2 It2) as (t2'&s2&E2&Is2&It2'). *)
    (*       exists (t1'⨾t2'),(s1 ⨾ s2);split;[rewrite E1,E2;reflexivity|]. *)
    (*       split;[|exists s1,s2;tauto]. *)
    (*       simpl;destruct (inclb 𝒱 s1 (balance A)),(inclb 𝒱 s2 (balance A));mega_simpl; *)
    (*         repeat right;auto;apply in_bimap;exists t1',t2';auto. *)
    (*     * intro It1. *)
    (*       destruct (witness_positive (positive_reduce_expr If2)) as (t2&It2). *)
    (*       destruct (IHe1 f1 t If1 It1) as (t1'&s1&E1&Is1&It1'). *)
    (*       destruct (IHe2 f2 t2 If2 It2) as (t2'&s2&E2&Is2&It2'). *)
  Qed.
        
  Lemma reduce_expr_inf A e : test A ⋅ Σ (reduce_expr A e) ≤ e.
  Proof.
    replace (test A) with (Σ[test A]) by reflexivity.
    rewrite seq_distr.
    apply Σ_small;setoid_rewrite in_bimap.
    intros f' (f''&f&<-&[<-|F]&I);[|simpl in F;tauto].
    revert f I;induction e;simpl;intro;try tauto.
    - intros [<-|F];[|tauto].
      rewrite test_sub_id,ax_seq_1;reflexivity.
    - intros If;apply in_concat in If as (L&If&IL).
      apply in_bimap in IL as (f1&f2&<-&If1&If2).
      rewrite <- (IHe1 _ If1),<- (IHe2 _ If2).
      revert If;unfold red_prod;case_eq (testA A f2);case_eq (testA A f1);
        repeat rewrite testA_spec;simpl.
      + intros h1 h2 [<-|[<-|[<-|F]]];try tauto.
        * rewrite <- h2,<-test_dup.
          rewrite test_seq,<-ax_seq_assoc,<-test_dup.
          reflexivity.
        * rewrite <- h1,<-test_dup.
          rewrite ax_seq_assoc,<-test_dup.
          reflexivity.
        * rewrite test_dup at 1.
          rewrite <-ax_seq_assoc,(ax_seq_assoc _ f1),(test_seq A f1) at 1.
          repeat rewrite ax_seq_assoc;reflexivity.
      + intros _ h2 [<-|[<-|F]];try tauto.
        * rewrite <- h2,<-test_dup.
          rewrite test_seq,<-ax_seq_assoc,<-test_dup.
          reflexivity.
        * rewrite test_dup at 1.
          rewrite <-ax_seq_assoc,(ax_seq_assoc _ f1),(test_seq A f1) at 1.
          repeat rewrite ax_seq_assoc;reflexivity.
      + intros h1 _ [<-|[<-|F]];try tauto.
        * rewrite <- h1,<-test_dup.
          rewrite ax_seq_assoc,<-test_dup.
          reflexivity.
        * rewrite test_dup at 1.
          rewrite <-ax_seq_assoc,(ax_seq_assoc _ f1),(test_seq A f1) at 1.
          repeat rewrite ax_seq_assoc;reflexivity.
      + intros _ _  [<-|F];try tauto.
        rewrite test_dup at 1.
        rewrite <-ax_seq_assoc,(ax_seq_assoc _ f1),(test_seq A f1) at 1.
        repeat rewrite ax_seq_assoc;reflexivity.
    - rewrite in_bimap;intros (f1&f2&<-&If1&If2).
      rewrite <- (IHe1 _ If1),<- (IHe2 _ If2).
      rewrite <- test_inter_left,(ax_inter_comm _ (_⋅f2)).
      rewrite <- test_inter_left,ax_seq_assoc,<-test_dup,ax_inter_comm;reflexivity.
    - rewrite in_app_iff;intros [I|I];rewrite (IHe1 _ I)||rewrite (IHe2 _ I);
        (apply plus_left;reflexivity)||apply plus_right;reflexivity.
    - rewrite in_map_iff;intros (g&<-&Ig).
      rewrite <- (IHe _ Ig).
      rewrite ax_conv_seq,test_conv,test_seq;reflexivity.
    - rewrite in_map_iff;intros (g&<-&Ig).
      apply in_rm in Ig as (_&Ig).
      apply subsets_In in Ig.
      transitivity ((test A ⋅ Σ g) ⁺).
      + rewrite ax_iter_left at 1.
        rewrite ax_seq_plus.
        rewrite ax_seq_assoc.
        apply plus_inf.
        * rewrite ax_iter_left;apply plus_left;reflexivity.
        * transitivity ((test A ⋅ Σ g) ⁺ ⋅ Σ g ⁺).
          -- apply seq_smaller;[|reflexivity].
             rewrite ax_iter_left.
             apply plus_left;reflexivity.
          -- apply left_ind.
             rewrite ax_iter_right at 1.
             rewrite ax_plus_seq.
             apply plus_inf.
             ++ rewrite ax_iter_left;apply plus_right.
                rewrite ax_iter_left,ax_seq_plus;apply plus_left.
                rewrite test_dup at 1.
                repeat rewrite <- ax_seq_assoc.
                rewrite (test_seq _ (Σ _⋅_)).
                repeat rewrite <- ax_seq_assoc.
                rewrite <- test_seq;reflexivity.
             ++ rewrite <- iter_seq at 2.
                repeat rewrite <- ax_seq_assoc.
                apply seq_smaller;[reflexivity|].
                rewrite ax_iter_left;apply plus_right.
                rewrite ax_iter_left,ax_seq_plus;apply plus_left.
                rewrite test_dup at 1.
                repeat rewrite <- ax_seq_assoc.
                rewrite (test_seq _ (Σ _⋅_)).
                repeat rewrite <- ax_seq_assoc.
                rewrite <- test_seq;reflexivity.
      + replace (test A) with (Σ[test A]) by reflexivity.
        rewrite seq_distr;simpl;rewrite app_nil_r.
        apply iter_smaller,Σ_small.
        intros f If;apply in_map_iff in If as (f'&<-&If').
        apply IHe,Ig,If'.
  Qed.
            
End terms.
Require Import w_terms.

Section w_terms.
  Context { X : Set } {decX : decidable_set X}.

  Definition 𝐖_var (x : X) : (@𝐖 X') := (Some (𝐒𝐏_var (x,true)),[]).

  Definition 𝐖'_var (x : X) : 𝐖' := exist _ (𝐖_var x) (Logic.eq_refl _).

  Fixpoint iter_w_prod (P : 𝐖' -> Prop) n :=
    match n with
    | O => P
    | S n => fun w => exists w1 w2, w = w1 ⊗' w2 /\ P w1 /\ iter_w_prod P n w2
    end.

  Definition conv𝐖' : 𝐖' -> 𝐖' :=
    fun w => match w with
            exist _ (None,A) p => exist _ (None,A) p
          | exist _ (Some t,A) p => exist _ (Some (prime t),A) p
          end.

  Notation " e ` " := (conv𝐖' e) (at level 30).
    
  Lemma conv𝐖'_idem w : w`` = w.
  Proof.
    apply 𝐖'_dec;destruct w as (([w|]&A)&p);simpl;[repeat f_equal|reflexivity].
    apply prime_idem.
  Qed.

  Lemma conv𝐖'_seq w1 w2 : (w1 ⊗' w2)` ≡ w2` ⊗' w1`.
  Proof.
    unfold equiv,𝐖'_equiv,equiv,𝐖_equiv,smaller,𝐖_inf.
    destruct w1 as (([w1|]&A1)&p1),w2 as (([w2|]&A2)&p2);simpl;split;
      try rewrite prime_idem;try (split;[|reflexivity]);intro;mega_simpl.
  Qed.
  
  Lemma conv𝐖'_par w1 w2 : (w1 ⊕' w2)` ≡ w1` ⊕' w2`.
  Proof.
    unfold equiv,𝐖'_equiv,equiv,𝐖_equiv,smaller,𝐖_inf.
    destruct w1 as (([w1|]&A1)&p1),w2 as (([w2|]&A2)&p2);simpl;split;
      try rewrite prime_idem;try (split;[|reflexivity]);intro;mega_simpl;
        try rewrite prime_var in *;auto.
    - rewrite prime_var in H;auto.
    - rewrite prime_var in H;auto.
  Qed.
    
  Lemma conv𝐖'_unit : 𝟭' ` = 𝟭'.
  Proof. reflexivity. Qed.

  Global Instance conv𝐖'_monotone : Proper(smaller ==> smaller) conv𝐖'.
  Proof.
    intros (([u|]&A)&p)(([v|]&B)&q);unfold smaller,𝐖'_inf,smaller,𝐖_inf;simpl;auto;simpl in *.
    - intros (I&E);split;[assumption|apply prime_monotone,E;apply balanced_spec,p].
    - intros (I&E);split;[assumption|].
      cut (𝐒𝐏'_variables v ⊆ A).
      + intro h;rewrite<- prime_var in h.
        intros (a,b) Ia;apply h,𝐒𝐏_variables_𝐒𝐏'_variables_iff;destruct b;tauto.
      + apply balanced_spec in p.
        intros (a,b) Ia;apply 𝐒𝐏_variables_𝐒𝐏'_variables_iff in Ia as [Ia|Ia];
          destruct b;apply E,Ia||apply p,E,Ia.
  Qed.
  Global Instance conv𝐖'_morph : Proper(equiv ==> equiv) conv𝐖'.
  Proof. intros e1 e2 (h1&h2);split;apply conv𝐖'_monotone;auto. Qed.

  Lemma iter_w_prod_last L w n : (exists w', w ≡ w' /\ iter_w_prod L (S n) w')
                                 <-> exists w1 w2, w ≡ w1 ⊗' w2 /\ iter_w_prod L n w1 /\ L w2.
  Proof.
    revert w;induction n;simpl;intro w;split.
    - intros (w'&E&(w1&w2&->&I1&I2)).
      exists w1,w2;split;[auto|tauto].
    - intros (w1&w2&E&I1&I2).
      exists (w1⊗' w2);split;[auto|].
      exists w1,w2;auto.
    - intros (w'&E1&w1&w2&->&I1&I2).
      assert (I2': exists w', (w2 ≡ w') /\ iter_w_prod L (S n) w') by (exists w2;split;[reflexivity|apply I2]).
      apply IHn in I2' as (w2'&w3&E&I2'&I3).
      exists (w1⊗' w2'),w3;split;[rewrite E1,E;apply 𝐖'_seq_assoc|].
      split;[exists w1,w2'|];auto.
    - intros (w0&w3&E&(w1&w2&->&I1&I2)&I3).
      assert (I2': exists w', (w2⊗'w3 ≡ w') /\ iter_w_prod L (S n) w')
        by (apply IHn;exists w2,w3;split;[reflexivity|auto]).
      destruct I2' as (w'&E'&I2').
      exists (w1⊗'w');split;[|exists w1,w';auto].
      rewrite E,<-E';symmetry;apply 𝐖'_seq_assoc.
  Qed.      
    
  Fixpoint sub_terms (e : 𝐄 X) : 𝐖' -> Prop := 
    match e with
    | 1 => fun w => w = 𝟭'
    | 0 => fun _ => False
    | 𝐄_var x => fun w => w = 𝐖'_var x
    | e ¯ => fun w => sub_terms e (conv𝐖' w)
    | e1 ⋅ e2 => fun w => exists w1 w2, w = w1 ⊗' w2 /\ sub_terms e1 w1 /\ sub_terms e2 w2
    | e1 ∩ e2 => fun w => exists w1 w2, w = w1 ⊕' w2 /\ sub_terms e1 w1 /\ sub_terms e2 w2
    | e1 + e2 => fun w => sub_terms e1 w \/ sub_terms e2 w
    | e ⁺ => fun w => exists n, iter_w_prod (sub_terms e) n w
    end.

  Definition sub_terms_inf e f := (forall w, sub_terms e w -> exists w', sub_terms f w' /\ w ≤ w').
  Definition sub_terms_eq e f := sub_terms_inf e f /\ sub_terms_inf f e. 

  Infix " ≦ " := sub_terms_inf (at level 80).
  Infix " ⩵ " := sub_terms_eq (at level 80).

  Global Instance sub_terms_inf_PreOrder : PreOrder sub_terms_inf.
  Proof.
    split.
    - intros e w Iw;exists w;split;[assumption|reflexivity].
    - intros e f g E1 E2 w1 Iw1;apply E1 in Iw1 as (w2&Iw2&Ew2);apply E2 in Iw2 as (w3&Iw3&Ew3).
      exists w3;split;[assumption|rewrite Ew2,Ew3;reflexivity].
  Qed.

  Global Instance sub_terms_eq_Equivalence : Equivalence sub_terms_eq.
  Proof.
    split.
    - intro e;split;reflexivity.
    - intros e f (E1&E2);split;assumption.
    - intros e f g (E1&E2) (E3&E4);split;etransitivity;eassumption.
  Qed.

  Global Instance sub_terms_inf_PartialOrder : PartialOrder sub_terms_eq sub_terms_inf.
  Proof. unfold sub_terms_eq,sub_terms_inf;split;auto. Qed.

  Global Instance seq_sub_terms_inf :
    Proper (sub_terms_inf ==> sub_terms_inf ==> sub_terms_inf) (@𝐄_seq X).
  Proof.
    intros e1 e2 ih1 f1 f2 ih2 w (w1&w2&->&Iw1&Iw2).
    apply ih1 in Iw1 as (w1'&Iw1&Ew1).
    apply ih2 in Iw2 as (w2'&Iw2&Ew2).
    exists (w1'⊗'w2');split.
    - exists w1',w2';tauto.
    - rewrite Ew1,Ew2;reflexivity.
  Qed.
  
  Global Instance plus_sub_terms_inf :
    Proper (sub_terms_inf ==> sub_terms_inf ==> sub_terms_inf) (@𝐄_plus X).
  Proof.
    intros e1 e2 ih1 f1 f2 ih2 w [Iw1|Iw1].
    - apply ih1 in Iw1 as (w2&Iw2&Ew2).
      exists w2;split;[left|];assumption.
    - apply ih2 in Iw1 as (w2&Iw2&Ew2).
      exists w2;split;[right|];assumption.
  Qed.

  Global Instance inter_sub_terms_inf :
    Proper (sub_terms_inf ==> sub_terms_inf ==> sub_terms_inf) (@𝐄_inter X).
  Proof.
    intros e1 e2 ih1 f1 f2 ih2 w (w1&w2&->&Iw1&Iw2).
    apply ih1 in Iw1 as (w1'&Iw1&Ew1).
    apply ih2 in Iw2 as (w2'&Iw2&Ew2).
    exists (w1'⊕'w2');split.
    - exists w1',w2';tauto.
    - rewrite Ew1,Ew2;reflexivity.
  Qed.

  Global Instance iter_sub_terms_inf :
    Proper (sub_terms_inf ==> sub_terms_inf) (@𝐄_iter X).
  Proof.
    intros e1 e2 ih1 w (n&Iw);simpl;revert w Iw;induction n;simpl;
      intros w (w1&w2&->&Iw1&Iw2)||intros w1 Iw1;
      apply ih1 in Iw1 as (w1'&Iw1&Ew1);
      try (apply IHn in Iw2 as (w2'&(n'&Iw2)&Ew2)).
    - exists w1';split;[exists O|];auto.
    - exists (w1'⊗'w2');split;[exists (S n'),w1',w2'|];auto.
      rewrite Ew1,Ew2;reflexivity.
  Qed.

   Global Instance conv_sub_terms_inf :
    Proper (sub_terms_inf ==> sub_terms_inf) (@𝐄_conv X).
  Proof.
    intros e1 e2 ih1 w Iw.
    apply ih1 in Iw as (w'&Iw&Ew).
    exists (w'`);split.
    - simpl;rewrite conv𝐖'_idem;assumption.
    - rewrite <- (conv𝐖'_idem w),Ew;reflexivity.
  Qed.


  Global Instance seq_sub_terms_eq :
    Proper (sub_terms_eq ==> sub_terms_eq ==> sub_terms_eq) (@𝐄_seq X).
  Proof.
    intros e1 e2 (Ee1&Ee2) f1 f2 (Ef1&Ef2);split;[rewrite Ee1,Ef1|rewrite Ee2,Ef2];reflexivity.
  Qed.
  
  Global Instance plus_sub_terms_eq :
    Proper (sub_terms_eq ==> sub_terms_eq ==> sub_terms_eq) (@𝐄_plus X).
  Proof.
    intros e1 e2 (Ee1&Ee2) f1 f2 (Ef1&Ef2);split;[rewrite Ee1,Ef1|rewrite Ee2,Ef2];reflexivity.
  Qed.

  Global Instance inter_sub_terms_eq :
    Proper (sub_terms_eq ==> sub_terms_eq ==> sub_terms_eq) (@𝐄_inter X).
  Proof.
    intros e1 e2 (Ee1&Ee2) f1 f2 (Ef1&Ef2);split;[rewrite Ee1,Ef1|rewrite Ee2,Ef2];reflexivity.
  Qed.

  Global Instance iter_sub_terms_eq :
    Proper (sub_terms_eq ==> sub_terms_eq) (@𝐄_iter X).
  Proof.
    intros e1 e2 (Ee1&Ee2);split;[rewrite Ee1|rewrite Ee2];reflexivity.
  Qed.

  Global Instance conv_sub_terms_eq :
    Proper (sub_terms_eq ==> sub_terms_eq) (@𝐄_conv X).
  Proof.
    intros e1 e2 (Ee1&Ee2);split;[rewrite Ee1|rewrite Ee2];reflexivity.
  Qed.
     
  Global Instance equiv_sub_terms_eq : subrelation equiv sub_terms_eq.
  Proof.
    intros e f E;induction E.
    - reflexivity.
    - etransitivity;eassumption.
    - symmetry;assumption.
    - rewrite IHE,IHE0;reflexivity.
    - rewrite IHE,IHE0;reflexivity.
    - rewrite IHE,IHE0;reflexivity.
    - rewrite IHE;reflexivity.
    - rewrite IHE;reflexivity.
    - unfold sub_terms_eq,sub_terms_inf.
      destruct H;simpl;split.
      + intros w (w1&?&->&I1&(w2&w3&->&I2&I3)).
        exists ((w1 ⊗' w2) ⊗' w3);split.
        * exists (w1 ⊗' w2), w3;repeat split;auto.
          exists w1,w2;auto.
        * apply 𝐖'_seq_assoc.
      + intros w (?&w3&->&(w1&w2&->&I2&I3)&I1).
        exists (w1 ⊗' (w2 ⊗' w3));split.
        * exists w1,(w2⊗' w3);repeat split;auto.
          exists w2,w3;auto.
        * apply 𝐖'_seq_assoc.
      + intros x (y&w&->&->&I);exists w;split;auto.
        apply 𝐖'_seq_one_l.
      + intros w I;exists (𝟭'⊗'w);split.
        * exists 𝟭',w;auto.
        * apply 𝐖'_seq_one_l.
      + intros x (w&?&->&I&->);exists w;split;auto.
        apply 𝐖'_seq_one_r.
      + intros w I;exists (w⊗'𝟭');split.
        * exists w,𝟭';auto.
        * apply 𝐖'_seq_one_r.
      + intros w [I|I];exists w;split;reflexivity||tauto.
      + intros w [I|I];exists w;split;reflexivity||tauto.
      + intros w [I|I];exists w;split;reflexivity||tauto.
      + intros w I;exists w;split;reflexivity||tauto.
      + intros w I;exists w;split;reflexivity||tauto.
      + intros w I;exists w;split;reflexivity||tauto.
      + intros w I;exists w;split;reflexivity||tauto.
      + intros w I;exists w;split;reflexivity||tauto.
      + intros ? (?&?&?);tauto.
      + tauto.
      + intros ? (?&?&?);tauto.
      + tauto.
      + intros w (w1&w2&->&I1&I2);exists (w1⊗'w2);split;[|reflexivity].
        destruct I1 as [I1|I1];[left|right];exists w1,w2;tauto.
      + intros w [I|I];destruct I as (w1&w2&->&I1&I2);exists (w1⊗'w2);(split;[exists w1,w2;tauto|reflexivity]).
      + intros w (w1&w2&->&I1&I2);exists (w1⊗'w2);split;[|reflexivity].
        destruct I2 as [I2|I2];[left|right];exists w1,w2;tauto.
      + intros w [I|I];destruct I as (w1&w2&->&I1&I2);exists (w1⊗'w2);(split;[exists w1,w2;tauto|reflexivity]).
      + intros w (w1&?&->&I1&(w2&w3&->&I2&I3)).
        exists ((w1 ⊕' w2) ⊕' w3);split.
        * exists (w1 ⊕' w2), w3;repeat split;auto.
          exists w1,w2;auto.
        * apply 𝐖'_par_assoc.
      + intros w (?&w3&->&(w1&w2&->&I2&I3)&I1).
        exists (w1 ⊕' (w2 ⊕' w3));split.
        * exists w1,(w2⊕' w3);repeat split;auto.
          exists w2,w3;auto.
        * apply 𝐖'_par_assoc.
      + intros w (w1&w2&->&I1&I2);exists (w2⊕'w1);split;[exists w2,w1;auto|].
        apply 𝐖'_par_comm.
      + intros w (w1&w2&->&I1&I2);exists (w2⊕'w1);split;[exists w2,w1;auto|].
        apply 𝐖'_par_comm.
      + intros w (w1&w2&->&I1&I2);exists w1;split;auto.
        apply 𝐖'_par_inf.
      + intros w I;exists (w⊕'w);split;[exists w,w;auto|].
        apply 𝐖'_par_idem.
      + intros w (w1&w2&->&I1&I2);exists (w1⊕' w2);split;[|reflexivity].
        destruct I1;[left|right];exists w1,w2;auto.
      + intros w [I|I];destruct I as (w1&w2&->&I1&I2);exists (w1⊕'w2);(split;[exists w1,w2;tauto|reflexivity]).
      + intros w [(w1&w2&->&I1&I2)|Iw].
        * exists w1;split;[assumption|apply 𝐖'_par_inf].
        * exists w;split;[auto|reflexivity].
      + intros w Iw;exists w;split;[auto|reflexivity].
      + intros w (w1&w2&F);tauto.
      + tauto.
      + intro w;rewrite conv𝐖'_idem;intro Iw;exists w;split;[auto|reflexivity].
      + intros w Iw;exists w;rewrite conv𝐖'_idem;split;[auto|reflexivity].
      + intros w E;exists 𝟭';split;auto.
        rewrite <- (conv𝐖'_idem w),E,conv𝐖'_unit;reflexivity.
      + intros w -> ;exists 𝟭';split;[auto|reflexivity].
      + tauto.
      + tauto.
      + intros w I;exists w;split;[auto|reflexivity].
      + intros w I;exists w;split;[auto|reflexivity].
      + intros w (w1&w2&E&I1&I2).
        exists (w2`⊗' w1`);split;[exists (w2`),(w1`);repeat rewrite conv𝐖'_idem; auto|].
        rewrite <- (conv𝐖'_idem w),E,conv𝐖'_seq;reflexivity.
      + intros w (w1&w2&->&I1&I2).
        exists ((w2`⊗' w1`)`);rewrite conv𝐖'_seq;repeat rewrite conv𝐖'_idem;split;[|reflexivity].
        exists (w2`),(w1`);auto.
      + intros w (w1&w2&E&I1&I2).
        exists (w1`⊕' w2`);split;[exists (w1`),(w2`);repeat rewrite conv𝐖'_idem; auto|].
        rewrite <- (conv𝐖'_idem w),E,conv𝐖'_par;reflexivity.
      + intros w (w1&w2&->&I1&I2).
        exists ((w1`⊕' w2`)`);rewrite conv𝐖'_par;repeat rewrite conv𝐖'_idem;split;[|reflexivity].
        exists (w1`),(w2`);auto.
      + intros w (n&I);revert w I;induction n;intro w.
        * intros I;exists w;split;[|reflexivity].
          exists O;auto.
        * intro I;cut (exists w', w` ≡ w' /\ iter_w_prod (sub_terms e) (S n) w');
            [clear I|exists (w`);split;[reflexivity|auto]].
          intros I;apply iter_w_prod_last in I as (w1&w2&E&I1&I2).
          rewrite <- conv𝐖'_idem in I1;apply IHn in I1 as (w1'&(m&I1)&L).
          exists (w2`⊗'w1');split.
          exists (S m),(w2`),w1';rewrite conv𝐖'_idem;auto.
          rewrite <- (conv𝐖'_idem w).
          rewrite E,conv𝐖'_seq,L;reflexivity.
      + intros w (n&I);revert w I;induction n;intro w.
        * intros I;exists w;split;[|reflexivity].
          exists O;auto.
        * intro I;cut (exists w', w ≡ w' /\ iter_w_prod (fun w0 : 𝐖' => sub_terms e (w0 `)) (S n) w');
            [clear I|exists (w);split;[reflexivity|auto]].
          intros I;apply iter_w_prod_last in I as (w1&w2&E&I1&I2).
          rewrite <- conv𝐖'_idem in I1;apply IHn in I1 as (w1'&(m&I1)&L).
          rewrite conv𝐖'_idem in L.
          exists ((w2`⊗'w1'`)`);split.
          -- exists (S m),(w2`),(w1'`);rewrite conv𝐖'_idem;auto.
          -- rewrite E,conv𝐖'_seq,L.
             repeat rewrite conv𝐖'_idem;reflexivity.
      + intros w ([]&In).
        * exists w;split;[left;auto|reflexivity].
        * destruct In as (w1&w2&->&I1&I2).
          exists (w1⊗'w2);split;[right;exists w1,w2;repeat split;[|exists n];auto|reflexivity].
      + intros w [I|(w1&w2&->&I1&n&I2)].
        * exists w;split;[exists O;auto|reflexivity].
        * exists (w1⊗'w2);split;[exists (S n),w1,w2;auto|reflexivity].
      + intros w ([]&In).
        * exists w;split;[left;auto|reflexivity].
        * cut (exists w', w ≡ w' /\ iter_w_prod (sub_terms e) (S n) w');[|exists w;split;[reflexivity|auto]].
          clear In;intro I;apply iter_w_prod_last in I as (w1&w2&E&I1&I2).
          exists (w1⊗'w2);split;[|rewrite E;reflexivity].
          right;exists w1,w2;split;[|split;[exists n|]];auto.
      + intros w [I|(w1&w2&->&(n&I1)&I2)].
        * exists w;split;[exists O;auto|reflexivity].
        * cut (exists w', w1⊗' w2 ≡ w' /\ iter_w_prod (sub_terms e) (S n) w');
            [|apply iter_w_prod_last;exists w1,w2;split;[reflexivity|auto]].
          clear I1 I2;intros (w'&E&w1'&w2'&->&I1&I2).
          exists (w1'⊗' w2');split;[exists (S n),w1',w2';auto|rewrite E;reflexivity].
      + intros w (?&?&->&->&w1&w2&->&I1&I2).
        exists (𝟭' ⊕' (w1⊕'w2));split;[exists 𝟭',(w1⊕'w2);split;[|split;[|exists w1,w2]];auto|].
        apply 𝐖'_par_one_seq_par.
      + intros w (?&?&->&->&w1&w2&->&I1&I2).
        exists (𝟭' ⊕' (w1⊗'w2));split;[exists 𝟭',(w1⊗'w2);split;[|split;[|exists w1,w2]];auto|].
        apply 𝐖'_par_one_seq_par.
      + intros w0 (?&w&->&->&I).
        exists (𝟭' ⊕' w`);split;[exists 𝟭', (w`);auto|].
        destruct w as (([w|]&A)&P);unfold 𝐖'_par, smaller,𝐖'_inf;simpl in *;[|reflexivity].
        unfold smaller,𝐖_inf.
        rewrite prime_var;reflexivity.
      + intros w0 (?&w&->&->&I).
        exists (𝟭' ⊕' w`);split;[exists 𝟭', (w`);rewrite conv𝐖'_idem;auto|].
        destruct w as (([w|]&A)&P);unfold 𝐖'_par, smaller,𝐖'_inf;simpl in *;[|reflexivity].
        unfold smaller,𝐖_inf.
        rewrite prime_var;reflexivity.
      + intros w (?&w2&->&(?&w1&->&->&I1)&I2).
        exists (w2⊗'(𝟭'⊕'w1));split.
        * exists w2,(𝟭'⊕'w1);repeat split;[ |exists 𝟭',w1];auto.
        * apply 𝐖'_par_one_seq.
      + intros w (w1&?&->&I1&(?&w2&->&->&I2)).
        exists ((𝟭'⊕'w2)⊗'w1);split.
        * exists (𝟭'⊕'w2),w1;repeat split;[exists 𝟭',w2|];auto.
        * apply 𝐖'_par_one_seq.
      + intros w (?&w3&->&(?&w2&->&(?&w1&->&->&I1)&I2)&I3).
        exists ((𝟭' ⊕' w1) ⊗' (w2 ⊕' w3));split;[|apply 𝐖'_par_one_par].
        exists (𝟭' ⊕' w1),(w2 ⊕' w3);repeat split.
        * exists 𝟭',w1;auto.
        * exists w2,w3;auto.
      + intros w (?&?&->&(?&w1&->&->&I1)&w2&w3&->&I2&I3).
        exists ((𝟭' ⊕' w1) ⊗' w2 ⊕' w3);split;[|apply 𝐖'_par_one_par].
        exists ((𝟭' ⊕' w1)⊗' w2),w3;repeat split;auto.
        exists (𝟭' ⊕' w1), w2;repeat split;auto.
        exists 𝟭',w1;auto.
      + intros w (n&In);revert w In;induction n;intros w I.
        * destruct I as [I|I].
          -- exists w;split;[|reflexivity].
             left;exists O;auto.
          -- destruct I as (?&w2&->&(?&w1&->&->&I1)&I2).
             exists ((𝟭' ⊕' w1) ⊗' w2);split;[|reflexivity].
             right;exists (𝟭' ⊕' w1),w2;repeat split;auto.
             ++ exists 𝟭',w1;auto.
             ++ exists O;simpl;auto.
        * destruct I as (u&u'&->&I&I').
          apply IHn in I' as (v&Iv&Ev).
          setoid_rewrite Ev;clear u' Ev.
          destruct I as [I|(?&u2&->&(?&u1&->&->&I1)&I2)],
                        Iv as [(m&Iv)|(?&v2&->&(?&v1&->&->&Iv1)&(m&Iv2))].
          -- exists (u⊗'v);split;[|reflexivity].
             left;exists (S m),u,v;auto.
          -- exists ((𝟭' ⊕' v1) ⊗' (u⊗'v2));split;
               [|rewrite 𝐖'_seq_assoc,<-𝐖'_par_one_seq;apply 𝐖'_seq_assoc].
             right;exists (𝟭' ⊕' v1), (u ⊗' v2);repeat split;auto.
             ++ exists 𝟭',v1;auto.
             ++ exists (S m),u, v2;auto.
          -- setoid_rewrite <- 𝐖'_seq_assoc.
             exists ((𝟭' ⊕' u1) ⊗' (u2⊗'v));split;[|reflexivity].
             right;exists (𝟭' ⊕' u1), (u2⊗'v);repeat split.
             ++ exists 𝟭',u1;auto.
             ++ exists (S m),u2, v;repeat split;auto.
                revert v Iv;clear.
                induction m;simpl;auto.
                intros w (w1&w2&->&I1&I2).
                apply IHm in I2.
                exists w1,w2;auto.
          -- exists ((𝟭' ⊕' u1) ⊗' (u2 ⊗' v2));split.
             ++ right;exists (𝟭' ⊕' u1),(u2 ⊗' v2);repeat split.
                ** exists 𝟭', u1;auto.
                ** exists (S m),u2,v2;auto.
             ++ transitivity ((𝟭' ⊕' (u1⊕'v1)) ⊗' (u2 ⊗' v2)).
                ** rewrite <-𝐖'_par_one_seq_par_one.
                   repeat rewrite <- 𝐖'_seq_assoc.
                   repeat rewrite (𝐖'_par_one_seq v1).
                   repeat rewrite <- 𝐖'_seq_assoc.
                   reflexivity.
                ** apply 𝐖'_seq_𝐖'_inf;[|reflexivity].
                   apply 𝐖'_par_𝐖'_inf;[reflexivity|].
                   apply 𝐖'_par_inf.
      + intros w [(n&In)|(?&w2&->&(?&w1&->&->&I1)&I2)].
        * exists w;split;[|reflexivity].
          exists n;revert w In;induction n;intros w I;simpl;auto.
          destruct I as (w1&w2&->&I1&I2).
          apply IHn in I2.
          exists w1,w2;auto.
        * destruct I2 as (n&I2);revert w2 I2;induction n;intros w2 I2.
          -- destruct I2 as [I2|I2].
             ++ exists w2;split;[exists O;left;auto|].
                rewrite 𝐖'_par_inf;apply 𝐖'_seq_one_l.
             ++ exists ((𝟭' ⊕' w1) ⊗' w2);split;[|reflexivity].
                exists O;right.
                exists (𝟭' ⊕' w1), w2;repeat split;auto.
                exists 𝟭',w1;auto.
          -- destruct I2 as (u1&u2&->&Iu1&Iu2).
             apply IHn in Iu2 as (w'&(m&I)&E).
             destruct Iu1 as [Iu1|Iu1].
             ++ exists (u1⊗'w');split.
                ** exists (S m),u1,w';auto.
                ** rewrite 𝐖'_seq_assoc,𝐖'_par_one_seq,<-𝐖'_seq_assoc,E;reflexivity.
             ++ exists (((𝟭' ⊕' w1) ⊗' u1)⊗'w');split.
                ** exists (S m),((𝟭' ⊕' w1) ⊗' u1),w';repeat split;auto.
                   right;exists (𝟭' ⊕' w1),u1;repeat split;auto.
                   exists 𝟭',w1;auto.
                ** rewrite <- E.
                   repeat rewrite <-𝐖'_seq_assoc.
                   repeat rewrite 𝐖'_par_one_seq.
                   repeat rewrite <-𝐖'_seq_assoc.
                   rewrite 𝐖'_par_one_seq_par_one,𝐖'_par_idem.
                   reflexivity.
    - unfold sub_terms_eq,sub_terms_inf.
      destruct H;destruct IHE as (ih&ih');split;simpl in *.
      + intros w [(w1&w2&->&(n&I1)&I2)|Iw];[|exists w;split;[auto|reflexivity]].
        revert w1 w2 I1 I2;induction n.
        * intros w1 w2 I1 I2;simpl in *.
          apply ih;left;exists w1,w2;auto.
        * intros w w3 I I3.
          cut (exists w', w ≡ w' /\ iter_w_prod (sub_terms e) (S n) w');
            [clear I;intros I|exists w;split;[reflexivity|assumption]].
          apply iter_w_prod_last in I as (w1&w2&E&I1&I2).
          destruct (ih (w2⊗'w3)) as (w2'&I2'&E2).
          -- left;exists w2,w3;auto.
          -- setoid_rewrite E;clear w E.
             setoid_rewrite <- 𝐖'_seq_assoc.
             setoid_rewrite E2.
             apply IHn;auto.
      + intros w Iw;exists w;split;[right;auto|reflexivity].
      + intros w [(w1&w2&->&I1&n&I2)|Iw];[|exists w;split;[auto|reflexivity]].
        revert w1 w2 I1 I2;induction n.
        * intros w1 w2 I1 I2;simpl in *.
          apply ih;left;exists w1,w2;auto.
        * intros w1 w I1 (w2&w3&->&I2&I3).
          setoid_rewrite 𝐖'_seq_assoc.
          destruct (ih (w1⊗'w2)) as (w'&I1'&E1).
          -- left;exists w1,w2;auto.
          -- setoid_rewrite E1.
             apply IHn;auto.
      + intros w Iw;exists w;split;[auto|reflexivity].
  Qed.
  
  Global Instance smaller_sub_terms_inf : subrelation smaller sub_terms_inf.
  Proof.
    intros e f E.
    unfold smaller,𝐄_Smaller in E.
    apply equiv_sub_terms_eq in E as (E&_).
    intros w Iw;apply E;left;auto.
  Qed.
    
  Proposition sub_terms_inf_dec e f :
    { e ≦ f } + { exists w, sub_terms e w /\ forall w', sub_terms f w' -> ~ (w ≤ w')}.
  Admitted.
  
End w_terms.
Notation " e ` " := (conv𝐖' e) (at level 30).
Infix " ≦ " := sub_terms_inf (at level 80).
Infix " ⩵ " := sub_terms_eq (at level 80).

Section sub_sp_terms.
  Context {X : Set}`{decX : decidable_set X}.

  Lemma 𝐒𝐏'_variables_fst (t : 𝐒𝐏 (@X' X)) : map fst (𝐒𝐏'_variables t) ≈ map fst (𝒱 t).
  Proof.
    intros x;repeat rewrite in_map_iff;split;intros ((a,b)&<-&I).
    - apply 𝐒𝐏_variables_𝐒𝐏'_variables_iff in I as [I|I];[exists (a,true)|exists (a,false)];auto.
    - exists (a,b);split;auto.
      apply 𝐒𝐏_variables_𝐒𝐏'_variables_iff;destruct b;auto.
  Qed.
  
  Lemma inter_1_sp_terms e t :
    sp_terms e t -> exists C : list X, C ∈ inter_1 e /\ C ≈ map fst (𝒱 t).
  Proof.
    revert t;induction e;simpl;try tauto.
    - intros ? -> ;exists [x];simpl;split;[auto|reflexivity].
    - intros t (t1&t2&->&I1&I2).
      apply IHe1 in I1 as (C1&I1&E1);apply IHe2 in I2 as (C2&I2&E2).
      exists (C1++C2);split;[|rewrite E1,E2,<-map_app;reflexivity].
      apply in_bimap;exists C1,C2;auto.
    - intros t (t1&t2&->&I1&I2).
      apply IHe1 in I1 as (C1&I1&E1);apply IHe2 in I2 as (C2&I2&E2).
      exists (C1++C2);split;[|rewrite E1,E2,<-map_app;reflexivity].
      apply in_bimap;exists C1,C2;auto.
    - intros t [I|I];[apply IHe1 in I as (C&I&E)|apply IHe2 in I as (C&I&E)];
        exists C;split;auto;mega_simpl.
    - intros t I;apply IHe in I as (C&IC&EC).
      exists C;split;auto.
      rewrite EC.
      transitivity (map fst (𝐒𝐏'_variables t)).
      + rewrite <- prime_var;symmetry;apply 𝐒𝐏'_variables_fst.
      + apply 𝐒𝐏'_variables_fst.
    - intros t (n&I);revert t I;induction n.
      + intros t I;apply IHe in I as (C&IC&E).
        exists C;split;auto.
        apply in_map_iff.
        exists [C];split;[simpl;rewrite app_nil_r;reflexivity|].
        apply in_rm;split;[discriminate|].
        revert IC;generalize (inter_1 e);clear;intro l;induction l;simpl;auto.
        intros [<-|I].
        * apply in_app_iff;left;apply in_map_iff;exists [];split;auto.
          clear;induction l;simpl;mega_simpl;auto.
        * mega_simpl;auto.
      + intros t (t1&t2&->&I1&I2).
        apply IHe in I1 as (C1&IC1&E1).
        apply IHn in I2 as (A&I2&E2).
        apply in_map_iff in I2 as (B&<-&I2).
        apply in_rm in I2 as (NB&I2).
        assert (I: C1::B ⊆ inter_1 e)
          by (intros a [<-|Ia];auto;apply subsets_In in I2;apply I2;auto).
        apply subsets_spec in I as (D&ID&ED).
        exists (concat D);split.
        * apply in_map_iff;exists D;split;auto.
          apply in_rm;split;auto.
          intros ->;cut (C1 ∈ []);[simpl;auto|apply ED;now left].
        * intro a;rsimpl. 
          rewrite map_app,<-E2,<-E1,in_app_iff;repeat rewrite in_concat;setoid_rewrite <- ED.
          simpl;clear;firstorder subst;auto.
  Qed.

  Lemma sub_terms_sp_terms_one_free (e : 𝐄 X) w :
    one_free e = true ->
    sub_terms e w -> exists t, sp_terms e t /\ π{w} ≡ (Some t,[]).
  Proof.
    intro O;revert O w;induction e;simpl;
      discriminate||tauto||(rewrite andb_true_iff;intros (O1&O2))||intro O.
    - intros w -> ;exists (𝐒𝐏_var (x,true));split;auto.
      split;simpl;reflexivity.
    - intros w (w1&w2&->&I1&I2).
      apply IHe1 in I1 as (t1&I1&E1);auto;apply IHe2 in I2 as (t2&I2&E2);auto.
      exists (t1⨾t2);split;[exists t1,t2;auto|].
      destruct w1 as (([s1|]&A1)&p1),w2 as (([s2|]&A2)&p2);simpl;simpl in *;
        try (destruct E1 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto)
        || (destruct E2 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto).
      destruct E1 as ((h11&E11)&(h12&E12)),E2 as ((h21&E21)&(h22&E22)).
      split;split;try rewrite <- h11,<-h21||rewrite h12,h22;try reflexivity.
      -- apply 𝐒𝐏_inf_seq;(eapply incl_𝐒𝐏_inf;[|eassumption]).
         ++ apply incl_appl;reflexivity.
         ++ apply incl_appr;reflexivity.
      -- rewrite E12,E22;reflexivity.
    - intros w (w1&w2&->&I1&I2).
      apply IHe1 in I1 as (t1&I1&E1);auto;apply IHe2 in I2 as (t2&I2&E2);auto.
      exists (t1∥t2);split;[exists t1,t2;auto|].
      destruct w1 as (([s1|]&A1)&p1),w2 as (([s2|]&A2)&p2);simpl;simpl in *;
        try (destruct E1 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto)
        || (destruct E2 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto).
      destruct E1 as ((h11&E11)&(h12&E12)),E2 as ((h21&E21)&(h22&E22)).
      split;split;try rewrite <- h11,<-h21||rewrite h12,h22;try reflexivity.
      -- apply 𝐒𝐏_inf_par;(eapply incl_𝐒𝐏_inf;[|eassumption]).
         ++ apply incl_appl;reflexivity.
         ++ apply incl_appr;reflexivity.
      -- rewrite E12,E22;reflexivity.
    - intros w [I|I];[apply IHe1 in I as (t&E)|apply IHe2 in I as (t&E)];auto;exists t;tauto.
    - intros w I;apply IHe in I as (t&I&E);auto.
      exists (prime t);rewrite prime_idem;split;auto.
      destruct w as (([s|]&B)&p);destruct E as (E1&E2);split;unfold smaller,𝐖_inf in *;
        simpl in *;try tauto;destruct E1 as (h1&E1),E2 as (h2&E2);split;auto.
      + rewrite <- (prime_idem s);apply prime_monotone;auto.
        apply balanced_spec;auto.
      + rewrite <- (prime_idem s);apply prime_monotone;auto.
        apply balanced_spec;auto.
    - intros w (n&I);revert w I;induction n.
      + intros w I;apply IHe in I as (t&I&E);auto.
        exists t;split;[exists 0%nat;simpl|];auto.
      + intros w (w1&w2&->&I1&I2).
        apply IHe in I1 as (t1&I1&E1);auto;apply IHn in I2 as (t2&I2&E2);auto.
        exists (t1⨾t2);split;[destruct I2 as (m&I2);exists (S m),t1,t2;auto|].
        destruct w1 as (([s1|]&A1)&p1),w2 as (([s2|]&A2)&p2);simpl;simpl in *;
          try (destruct E1 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto)
          || (destruct E2 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto).
        destruct E1 as ((h11&E11)&(h12&E12)),E2 as ((h21&E21)&(h22&E22)).
        split;split;try rewrite <- h11,<-h21||rewrite h12,h22;try reflexivity.
        -- apply 𝐒𝐏_inf_seq;(eapply incl_𝐒𝐏_inf;[|eassumption]).
           ++ apply incl_appl;reflexivity.
           ++ apply incl_appr;reflexivity.
        -- rewrite E12,E22;reflexivity.
  Qed.
      
  Lemma sp_terms_sub_terms_one_free (e : 𝐄 X) t :
    one_free e = true ->
    sp_terms e t -> exists w, sub_terms e w /\ π{w} ≡ (Some t,[]).
  Proof.
    intro O;revert O t;induction e;simpl;
      discriminate||tauto||(rewrite andb_true_iff;intros (O1&O2))||intro O.
    - intros t ->;exists (𝐖'_var x);split;auto.
       split;simpl;reflexivity.
    - intros w (w1&w2&->&I1&I2).
      apply IHe1 in I1 as (t1&I1&E1);auto;apply IHe2 in I2 as (t2&I2&E2);auto.
      exists (t1⊗'t2);split;[exists t1,t2;auto|].
      destruct t1 as (([s1|]&A1)&p1),t2 as (([s2|]&A2)&p2);simpl;simpl in *;
        try (destruct E1 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto)
        || (destruct E2 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto).
      destruct E1 as ((h11&E11)&(h12&E12)),E2 as ((h21&E21)&(h22&E22)).
      split;split;try rewrite <- h11,<-h21||rewrite h12,h22;try reflexivity.
      -- apply 𝐒𝐏_inf_seq;(eapply incl_𝐒𝐏_inf;[|eassumption]).
         ++ apply incl_appl;reflexivity.
         ++ apply incl_appr;reflexivity.
      -- rewrite E12,E22;reflexivity.
    - intros w (w1&w2&->&I1&I2).
      apply IHe1 in I1 as (t1&I1&E1);auto;apply IHe2 in I2 as (t2&I2&E2);auto.
      exists (t1⊕'t2);split;[exists t1,t2;auto|].
      destruct t1 as (([s1|]&A1)&p1),t2 as (([s2|]&A2)&p2);simpl;simpl in *;
        try (destruct E1 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto)
        || (destruct E2 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto).
      destruct E1 as ((h11&E11)&(h12&E12)),E2 as ((h21&E21)&(h22&E22)).
      split;split;try rewrite <- h11,<-h21||rewrite h12,h22;try reflexivity.
      -- apply 𝐒𝐏_inf_par;(eapply incl_𝐒𝐏_inf;[|eassumption]).
         ++ apply incl_appl;reflexivity.
         ++ apply incl_appr;reflexivity.
      -- rewrite E12,E22;reflexivity.
    - intros w [I|I];[apply IHe1 in I as (t&E)|apply IHe2 in I as (t&E)];auto;exists t;tauto.
    - intros w I;apply IHe in I as (t&I&E);auto.
      exists (t`);rewrite conv𝐖'_idem;split;auto.
      destruct t as (([s|]&B)&p);destruct E as (E1&E2);split;unfold smaller,𝐖_inf in *;
        simpl in *;try tauto;destruct E1 as (h1&E1),E2 as (h2&E2);split;auto.
      + rewrite <- (prime_idem w);apply prime_monotone;auto.
        apply balanced_spec;auto.
      + rewrite <- (prime_idem w);apply prime_monotone;auto.
        apply balanced_spec;auto.
    - intros w (n&I);revert w I;induction n.
      + intros w I;apply IHe in I as (t&I&E);auto.
        exists t;split;[exists 0%nat;simpl|];auto.
      + intros w (w1&w2&->&I1&I2).
        apply IHe in I1 as (t1&I1&E1);auto;apply IHn in I2 as (t2&I2&E2);auto.
        exists (t1⊗'t2);split;[destruct I2 as (m&I2);exists (S m),t1,t2;auto|].
        destruct t1 as (([s1|]&A1)&p1),t2 as (([s2|]&A2)&p2);simpl;simpl in *;
          try (destruct E1 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto)
          || (destruct E2 as (?&?);unfold smaller,𝐖_inf in *;simpl in *;tauto).
        destruct E1 as ((h11&E11)&(h12&E12)),E2 as ((h21&E21)&(h22&E22)).
        split;split;try rewrite <- h11,<-h21||rewrite h12,h22;try reflexivity.
        -- apply 𝐒𝐏_inf_seq;(eapply incl_𝐒𝐏_inf;[|eassumption]).
           ++ apply incl_appl;reflexivity.
           ++ apply incl_appr;reflexivity.
        -- rewrite E12,E22;reflexivity.
  Qed.

                                                          
End sub_sp_terms.
  (* Lemma sub_terms_sp_terms (e : 𝐄 X) w : *)
  (*   sub_terms e w -> *)
  (*   (exists A, (A,None) ∈ expand e /\ π{w} ≡ (None,flat_map (fun a => [(a,true);(a,false)]) A)) *)
  (*   \/ exists f A, (A,Some f) ∈ expand e /\ exists t, sp_terms f t *)
  (*                                         /\ π{w} ≡ (Some t,flat_map (fun a => [(a,true);(a,false)]) A). *)
  (* Proof. *)
  (*   revert w;induction e;simpl. *)
  (*   - intros ? ->. *)
  (*     left;exists [];split;auto. *)
  (*     split;intro;simpl;tauto. *)
  (*   - tauto. *)
  (*   - intros ? ->;simpl. *)
  (*     right;exists (𝐄_var x),[];split;auto. *)
  (*     exists (𝐒𝐏_var (x,true));split;simpl;auto. *)
  (*     split;split;reflexivity. *)
  (*   - intros w (w1&w2&->&I1&I2). *)
  (*     apply IHe1 in I1 as [(A1&I1&E1)|(f1&A1&I1&t1&It1&E1)]; *)
  (*       apply IHe2 in I2 as [(A2&I2&E2)|(f2&A2&I2&t2&It2&E2)]. *)
  (*     + left;exists (A1++A2);split;simpl;auto. *)
  (*       * apply in_bimap;exists (A1,None),(A2,None);split;auto. *)
  (*       * destruct w1 as (([w1|]&B1)&p1), w2 as (([w2|]&B2)&p2);simpl in *; *)
  (*           destruct E1 as (h1&h2),E2 as (h3&h4);simpl in *; *)
  (*             unfold smaller,𝐖_inf in h1,h2, h3, h4;simpl in *;try tauto. *)
  (*         repeat split;unfold smaller,𝐖_inf;simpl. *)
  (*         -- rewrite <- h1,<-h3. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*         -- rewrite h2,h4. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*     + right;exists f2,(A1++A2);split;simpl;auto. *)
  (*       * apply in_bimap;exists (A1,None),(A2,Some f2);split;auto. *)
  (*       * destruct w1 as (([w1|]&B1)&p1), w2 as (([w2|]&B2)&p2);simpl in *; *)
  (*           destruct E1 as (h1&h2),E2 as (h3&h4);simpl in *; *)
  (*             unfold smaller,𝐖_inf in h1,h2, h3, h4;simpl in *;try tauto. *)
  (*         exists t2;split;auto. *)
  (*         destruct h3 as (h3&EE1),h4 as (h4&EE2). *)
  (*         repeat split;unfold smaller,𝐖_inf;simpl. *)
  (*         -- rewrite <- h1,<-h3. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*         -- eapply incl_𝐒𝐏_inf,EE1. *)
  (*            apply incl_appr;reflexivity. *)
  (*         -- rewrite h2,h4. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*         -- eapply incl_𝐒𝐏_inf,EE2. *)
  (*            intro x;repeat rewrite in_flat_map;setoid_rewrite in_app_iff;firstorder. *)
  (*     + right;exists f1,(A1++A2);split;simpl;auto. *)
  (*       * apply in_bimap;exists (A1,Some f1),(A2,None);split;auto. *)
  (*       * destruct w1 as (([w1|]&B1)&p1), w2 as (([w2|]&B2)&p2);simpl in *; *)
  (*           destruct E1 as (h1&h2),E2 as (h3&h4);simpl in *; *)
  (*             unfold smaller,𝐖_inf in h1,h2, h3, h4;simpl in *;try tauto. *)
  (*         exists t1;split;auto. *)
  (*         destruct h1 as (h1&EE1),h2 as (h2&EE2). *)
  (*         repeat split;unfold smaller,𝐖_inf;simpl. *)
  (*         -- rewrite <- h1,<-h3. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*         -- eapply incl_𝐒𝐏_inf,EE1. *)
  (*            apply incl_appl;reflexivity. *)
  (*         -- rewrite h2,h4. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*         -- eapply incl_𝐒𝐏_inf,EE2. *)
  (*            intro x;repeat rewrite in_flat_map;setoid_rewrite in_app_iff;firstorder. *)
  (*     + right;exists (f1⋅f2),(A1++A2);split;simpl;auto. *)
  (*       * apply in_bimap;exists (A1,Some f1),(A2,Some f2);split;auto. *)
  (*       * destruct w1 as (([w1|]&B1)&p1), w2 as (([w2|]&B2)&p2);simpl in *; *)
  (*           destruct E1 as (h1&h2),E2 as (h3&h4);simpl in *; *)
  (*             unfold smaller,𝐖_inf in h1,h2, h3, h4;simpl in *;try tauto. *)
  (*         exists (t1⨾t2);split;[exists t1,t2;auto|]. *)
  (*         destruct h3 as (h3&EE3),h4 as (h4&EE4). *)
  (*         destruct h1 as (h1&EE1),h2 as (h2&EE2). *)
  (*         repeat split;unfold smaller,𝐖_inf;simpl. *)
  (*         -- rewrite <- h1,<-h3. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*         -- apply 𝐒𝐏_inf_seq;eapply incl_𝐒𝐏_inf;eauto. *)
  (*            ++ apply incl_appl;reflexivity. *)
  (*            ++ apply incl_appr;reflexivity. *)
  (*         -- rewrite h2,h4. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*         -- apply 𝐒𝐏_inf_seq;eapply incl_𝐒𝐏_inf;eauto. *)
  (*            ++ intro x;repeat rewrite in_flat_map;setoid_rewrite in_app_iff;firstorder. *)
  (*            ++ intro x;repeat rewrite in_flat_map;setoid_rewrite in_app_iff;firstorder. *)
  (*   - intros w (w1&w2&->&I1&I2). *)
  (*     apply IHe1 in I1 as [(A1&I1&E1)|(f1&A1&I1&t1&It1&E1)]; *)
  (*       apply IHe2 in I2 as [(A2&I2&E2)|(f2&A2&I2&t2&It2&E2)]. *)
  (*     + left;exists (A1++A2);split; *)
  (*         [apply in_concat;exists [(A1++A2,None)];split;simpl;auto;apply in_bimap; *)
  (*          exists (A1,None),(A2,None);auto|]. *)
  (*       destruct w1 as (([w1|]&B1)&p1), w2 as (([w2|]&B2)&p2);simpl in *; *)
  (*           destruct E1 as (h1&h2),E2 as (h3&h4);simpl in *; *)
  (*             unfold smaller,𝐖_inf in h1,h2, h3, h4;simpl in *;try tauto. *)
  (*       repeat split;unfold smaller,𝐖_inf;simpl. *)
  (*       * rewrite <- h1,<-h3. *)
  (*         repeat rewrite flat_map_concat_map. *)
  (*         rewrite map_app,concat_app;reflexivity. *)
  (*       * rewrite h2,h4. *)
  (*         repeat rewrite flat_map_concat_map. *)
  (*         rewrite map_app,concat_app;reflexivity. *)
  (*     + destruct w1 as (([w1|]&B1)&p1), w2 as (([w2|]&B2)&p2);simpl in *; *)
  (*         destruct E1 as (h1&h2),E2 as (h3&h4);simpl in *; *)
  (*           unfold smaller,𝐖_inf in h1,h2, h3, h4;simpl in *;try tauto. *)
  (*       destruct h3 as (h3&EE3),h4 as (h4&EE4). *)
  (*       left. *)
  (*       cut (exists C, C ∈ inter_1 f2 /\ C ≈ map fst 𝒱 t2). *)
  (*       * intros (C&IC&EC). *)
  (*         exists (A1++A2 ++ C);split. *)
  (*         -- clear EC;apply in_concat. *)
  (*            exists (map (fun C : list X => (A1 ++ A2 ++ C, None)) (inter_1 f2));split. *)
  (*            ++ apply in_map_iff;exists C;auto. *)
  (*            ++ apply in_bimap;exists (A1,None),(A2,Some f2);split;auto. *)
  (*         -- pose proof (𝐒𝐏_inf_variables EE3) as P1. *)
  (*            pose proof (𝐒𝐏_inf_variables EE4) as P2. *)
  (*            rewrite h3 in P2. *)
  (*            cut (B2 ++ 𝐒𝐏'_variables w2 ≈ flat_map (fun a : X => [(a, true); (a, false)]) (A2 ++ C)). *)
  (*            ++ clear EC;intro EE;repeat split;unfold smaller,𝐖_inf;simpl. *)
  (*               ** rewrite EE,<- h1.  *)
  (*                  repeat rewrite flat_map_concat_map in *. *)
  (*                  repeat rewrite map_app,concat_app;reflexivity. *)
  (*               ** rewrite EE,h2.  *)
  (*                  repeat rewrite flat_map_concat_map in *. *)
  (*                  repeat rewrite map_app,concat_app;reflexivity. *)
  (*            ++ intros (a,b);simpl. *)
  (*               rewrite in_app_iff. *)
  (*               rewrite (𝐒𝐏_variables_𝐒𝐏'_variables_iff w2 a b). *)
  (*               rewrite flat_map_concat_map, map_app,concat_app. *)
  (*               repeat rewrite <-flat_map_concat_map. *)
  (*               assert (B2 ≈ flat_map (fun a : X => [(a, true); (a, false)]) A2) as <- *)
  (*                 by (intro;split;[apply h4|apply h3]). *)
  (*               rewrite in_app_iff,in_flat_map. *)
  (*               setoid_rewrite EC;simpl. *)
  (*               setoid_rewrite in_map_iff. *)
  (*               apply balanced_spec in p2. *)
  (*               revert p2 P1 P2;clear. *)
  (*               intros Bal P1 P2;split. *)
  (*               ** intros [I|[I|I]];auto. *)
  (*                  --- apply P2,in_app_iff in I as [I|I];auto. *)
  (*                      +++ right;exists a;split;[exists (a,true);auto|destruct b;auto]. *)
  (*                      +++ left;destruct b;apply I||apply Bal,I. *)
  (*                  --- apply P2,in_app_iff in I as [I|I];auto. *)
  (*                      +++ right;exists a;split;[exists (a,false);auto|destruct b;auto]. *)
  (*                      +++ left;destruct b;apply I||apply Bal,I. *)
  (*               ** intros [I|(x&((y&b')&<-&I)&[E|[E|E]])];auto;inversion E;subst; *)
  (*                    apply P1,in_app_iff in I as [I|I]; *)
  (*                    (now destruct b';auto)||(left;destruct b';apply I||apply Bal,I). *)
  (*       * revert It2. *)
  (*         apply inter_1_sp_terms. *)
  (*     + destruct w1 as (([w1|]&B1)&p1), w2 as (([w2|]&B2)&p2);simpl in *; *)
  (*         destruct E1 as (h1&h2),E2 as (h3&h4);simpl in *; *)
  (*           unfold smaller,𝐖_inf in h1,h2, h3, h4;simpl in *;try tauto. *)
  (*       destruct h1 as (h1&EE1),h2 as (h2&EE2). *)
  (*       left. *)
  (*       destruct (inter_1_sp_terms It1) as (C&IC&EC). *)
  (*         exists (A1++A2 ++ C);split. *)
  (*         -- clear EC;apply in_concat. *)
  (*            exists (map (fun C : list X => (A1 ++ A2 ++ C, None)) (inter_1 f1));split. *)
  (*            ++ apply in_map_iff;exists C;auto. *)
  (*            ++ apply in_bimap;exists (A1,Some f1),(A2,None);split;auto. *)
  (*         -- pose proof (𝐒𝐏_inf_variables EE1) as P1. *)
  (*            pose proof (𝐒𝐏_inf_variables EE2) as P2. *)
  (*            rewrite h1 in P2. *)
  (*            cut (B1 ++ 𝐒𝐏'_variables w1 ≈ flat_map (fun a : X => [(a, true); (a, false)]) (A1 ++ C)). *)
  (*            ++ assert (app_comm: forall (T : Set) (l m : list T), l++m ≈ m++l) *)
  (*                    by (intros T l m x;mega_simpl;tauto). *)
  (*               clear EC;intro EE;repeat split;unfold smaller,𝐖_inf;simpl. *)
  (*               ** rewrite <- h3. *)
  (*                  rewrite <- (app_ass B1),(app_comm _ B1),app_ass,EE. *)
  (*                  repeat rewrite flat_map_concat_map in *. *)
  (*                  repeat rewrite map_app,concat_app. *)
  (*                  repeat rewrite <- app_ass. *)
  (*                  rewrite (app_comm _ (_ (_ _ A1))). *)
  (*                  reflexivity. *)
  (*               ** rewrite h4. *)
  (*                  rewrite <- (app_ass B1),(app_comm _ B1),app_ass,EE. *)
  (*                  repeat rewrite flat_map_concat_map in *. *)
  (*                  repeat rewrite map_app,concat_app. *)
  (*                  repeat rewrite <- app_ass. *)
  (*                  rewrite (app_comm _ (_ (_ _ A1))). *)
  (*                  reflexivity. *)
  (*            ++ intros (a,b);simpl. *)
  (*               rewrite in_app_iff. *)
  (*               rewrite (𝐒𝐏_variables_𝐒𝐏'_variables_iff w1 a b). *)
  (*               rewrite flat_map_concat_map, map_app,concat_app. *)
  (*               repeat rewrite <-flat_map_concat_map. *)
  (*               assert (B1 ≈ flat_map (fun a : X => [(a, true); (a, false)]) A1) as <- *)
  (*                 by (intro;split;[apply h2|apply h1]). *)
  (*               rewrite in_app_iff,in_flat_map. *)
  (*               setoid_rewrite EC;simpl. *)
  (*               setoid_rewrite in_map_iff. *)
  (*               apply balanced_spec in p1. *)
  (*               revert p1 P1 P2;clear. *)
  (*               intros Bal P1 P2;split. *)
  (*               ** intros [I|[I|I]];auto. *)
  (*                  --- apply P2,in_app_iff in I as [I|I];auto. *)
  (*                      +++ right;exists a;split;[exists (a,true);auto|destruct b;auto]. *)
  (*                      +++ left;destruct b;apply I||apply Bal,I. *)
  (*                  --- apply P2,in_app_iff in I as [I|I];auto. *)
  (*                      +++ right;exists a;split;[exists (a,false);auto|destruct b;auto]. *)
  (*                      +++ left;destruct b;apply I||apply Bal,I. *)
  (*               ** intros [I|(x&((y&b')&<-&I)&[E|[E|E]])];auto;inversion E;subst; *)
  (*                    apply P1,in_app_iff in I as [I|I]; *)
  (*                    (now destruct b';auto)||(left;destruct b';apply I||apply Bal,I). *)
  (*     + destruct w1 as (([w1|]&B1)&p1), w2 as (([w2|]&B2)&p2);simpl in *; *)
  (*         destruct E1 as (h1&h2),E2 as (h3&h4);simpl in *; *)
  (*           unfold smaller,𝐖_inf in h1,h2, h3, h4;simpl in *;try tauto. *)
  (*       destruct h1 as (h1&EE1),h2 as (h2&EE2). *)
  (*       destruct h3 as (h3&EE3),h4 as (h4&EE4). *)
  (*       right;exists (f1∩f2),(A1++A2);split. *)
  (*       * apply in_concat;exists [(A1 ++ A2, Some (f1 ∩ f2))];split;simpl;auto. *)
  (*         apply in_bimap;exists (A1,Some f1),(A2,Some f2);split;auto. *)
  (*       * exists (t1∥t2);split;[exists t1,t2;auto|]. *)
  (*         repeat split;unfold smaller,𝐖_inf;simpl. *)
  (*         -- rewrite <- h1,<-h3. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*         -- apply 𝐒𝐏_inf_par;eapply incl_𝐒𝐏_inf;eauto. *)
  (*            ++ apply incl_appl;reflexivity. *)
  (*            ++ apply incl_appr;reflexivity. *)
  (*         -- rewrite h2,h4. *)
  (*            repeat rewrite flat_map_concat_map. *)
  (*            rewrite map_app,concat_app;reflexivity. *)
  (*         -- apply 𝐒𝐏_inf_par;eapply incl_𝐒𝐏_inf;eauto. *)
  (*            ++ intro x;repeat rewrite in_flat_map;setoid_rewrite in_app_iff;firstorder. *)
  (*            ++ intro x;repeat rewrite in_flat_map;setoid_rewrite in_app_iff;firstorder. *)
  (*   - intros ? [I|I];[apply IHe1 in I as [(A&IA&E)|(f&A&I&E)] *)
  (*                    |apply IHe2 in I as [(A&IA&E)|(f&A&I&E)]]; *)
  (*     (right;exists f,A;split;[mega_simpl|auto])||(left;exists A;split;[mega_simpl|assumption]). *)
  (*   - intros w I;apply IHe in I as [(A&IA&E)|(f&A&I&E)]. *)
  (*     + left;exists A;split. *)
  (*       * apply in_map_iff;exists (A,None);auto. *)
  (*       * destruct w as (([w|]&B)&p),E as (h1&h2);simpl in *; *)
  (*           split;unfold smaller,𝐖_inf in *;simpl in *;try tauto. *)
  (*     + right;exists (f ¯),A;split;[apply in_map_iff;exists (A,Some f);auto|]. *)
  (*       destruct E as (t&It&Et). *)
  (*       exists (prime t);simpl. *)
  (*       rewrite prime_idem;split;auto. *)
  (*       destruct w as (([w|]&B)&p),Et as (h1&h2);simpl in *; *)
  (*         repeat split;unfold smaller,𝐖_inf in *;simpl in *;try tauto. *)
  (*       * rewrite <- (prime_idem w). *)
  (*         destruct h1 as (_&h1). *)
  (*         apply balanced_spec in p. *)
  (*         apply prime_monotone;auto. *)
  (*       * rewrite <- (prime_idem w). *)
  (*         destruct h2 as (_&h2). *)
  (*         apply prime_monotone;auto. *)
  (*         clear;intros a;repeat rewrite in_flat_map;simpl;split;intros (x&Ix&[E|[E|E]]); *)
  (*           inversion E;subst;exists a;auto. *)
  (*   - intros w (n&I);simpl;revert w I;induction n. *)
  (*     + intros w I;apply IHe in I as [(A&I&E)|(f&A&I&E)]. *)
  (*       * left;exists A;split;auto. *)
  (*         revert I;clear;induction (expand e) as [|(B&f) L];simpl;auto. *)
  (*         intros [E|I];[inversion E;subst;simpl;auto|]. *)
  (*         destruct f as [f|];mega_simpl;auto. *)
  (*       * right. *)

  (*         ;exists f,A;split;auto. *)
  (*         revert I;clear;induction (expand e) as [|(B&g) L];simpl;auto. *)
  (*         intros [E|I];[inversion E;subst;mega_simpl;auto|]. *)
  (*         destruct g as [f|];mega_simpl;auto. *)
      
  (* Qed. *)

Section language.
  (** * Language interpretation *)
  Context { X : Set }.

  (** We interpret expressions as languages in the obvious way: *)
  Global Instance to_lang_𝐄 {Σ}: semantics 𝐄 language X Σ :=
    fix to_lang_𝐄 σ e:=
      match e with
      | 1 => 1%lang
      | 0 => 0%lang
      | 𝐄_var a => (σ a)
      | e + f => ((to_lang_𝐄 σ e) + (to_lang_𝐄 σ f))%lang
      | e ⋅ f => ((to_lang_𝐄 σ e) ⋅ (to_lang_𝐄 σ f))%lang
      | e ∩ f => ((to_lang_𝐄 σ e) ∩ (to_lang_𝐄 σ f))%lang
      | e ¯ => (to_lang_𝐄 σ e)¯%lang
      | e⁺ => (to_lang_𝐄 σ e)⁺%lang
      end.

  (* begin hide *)
  Global Instance semSmaller_𝐄 : SemSmaller (𝐄 X) :=
    (@semantic_containment _ _ _ _ _).
  Global Instance semEquiv_𝐄 : SemEquiv (𝐄 X) :=
    (@semantic_equality _ _ _ _ _).
  Hint Unfold semSmaller_𝐄 semEquiv_𝐄 : semantics. 

  Section rsimpl.
    Context { Σ : Set }{σ : 𝕬[X→Σ] }.
    Lemma 𝐄_union e f : (⟦ e+f ⟧σ) = ((⟦e⟧σ) + ⟦f⟧σ)%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_prod e f :  (⟦ e⋅f ⟧σ) = ((⟦e⟧σ) ⋅ ⟦f⟧σ)%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_intersection e f : (⟦ e∩f ⟧σ) = ((⟦e⟧σ) ∩ ⟦f⟧σ)%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_mirror e :  (⟦ e ¯⟧σ) = (⟦e⟧σ)¯%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_iter_l e :  (⟦ e⁺⟧σ) = (⟦e⟧σ)⁺%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_variable a : (⟦𝐄_var a⟧ σ) = σ a.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_unit : (⟦1⟧σ) = 1%lang.
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐄_empty : (⟦0⟧σ) = 0%lang.
    Proof. unfold interprete;simpl;auto. Qed.
  End rsimpl.
  Hint Rewrite @𝐄_unit @𝐄_empty @𝐄_variable @𝐄_intersection
       @𝐄_prod @𝐄_union @𝐄_mirror @𝐄_iter_l
    : simpl_typeclasses.

  Global Instance sem_incl_𝐄_plus :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐄_plus X).
  Proof.
    intros e f E g h F ? ?;simpl;rsimpl;revert E F;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_incl_𝐄_fois :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐄_seq X).
  Proof.
    intros e f E g h F Σ σ;simpl;revert E F;rsimpl;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_incl_𝐄_inter :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐄_inter X).
  Proof.
    intros e f E g h F Σ σ;simpl;revert E F;rsimpl;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_incl_𝐄_conv :
    Proper (ssmaller ==> ssmaller) (@𝐄_conv X).
  Proof.
    intros e f E Σ σ;simpl;revert E;rsimpl;
      repeat autounfold with semantics;firstorder.
  Qed.
  Global Instance sem_incl_𝐄_iter :
    Proper (ssmaller ==> ssmaller) (@𝐄_iter X).
  Proof.
    intros e f E Σ σ;simpl.
    autorewrite with simpl_typeclasses.
    apply iter_lang_incl,E.
  Qed.
  Global Instance sem_eq_𝐄_plus :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐄_plus X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.
  Global Instance sem_eq_𝐄_seq :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐄_seq X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.
  Global Instance sem_eq_𝐄_inter :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐄_inter X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.
  Global Instance sem_eq_𝐄_conv :
    Proper (sequiv ==> sequiv) (@𝐄_conv X).
  Proof.
    intros e f E Σ σ;simpl;autounfold;rsimpl.
    intro w;rewrite (E Σ σ (rev w));tauto.
  Qed.
  Global Instance sem_eq_𝐄_iter :
    Proper (sequiv ==> sequiv) (@𝐄_iter X).
  Proof.
    intros e f E Σ σ;simpl.
    autorewrite with simpl_typeclasses.
    apply iter_lang_eq,E.
  Qed.
  Global Instance 𝐄_sem_equiv :
    Equivalence (fun e f : 𝐄 X => e ≃ f).
  Proof. once (typeclasses eauto). Qed.
  Global Instance 𝐄_sem_PreOrder :
    PreOrder (fun e f : 𝐄 X => e ≲ f).
  Proof. once (typeclasses eauto). Qed.
  Global Instance 𝐄_sem_PartialOrder :
    PartialOrder (fun e f : 𝐄 X => e ≃ f)
                 (fun e f : 𝐄 X => e ≲ f).
  Proof.
    eapply semantic_containment_PartialOrder;once (typeclasses eauto).
  Qed.

  (* end hide *)
  
  Lemma lang_iter_prod_last {Σ}(l: language Σ) n : (l ^{S n} ≃ l ^{n} ⋅ l)%lang.
  Proof.
    induction n.
    - simpl;intros w;split;intros (u&v&I1&I2&->).
      + rewrite I2,app_nil_r;exists [],u;split;reflexivity||auto.
      + rewrite I1;exists v,[];rewrite app_nil_r;repeat split;auto.
    - intros x;split.
      + intros (u&?&I1&I2&->).
        apply IHn in I2 as (v&w&I2&I3&->).
        exists (u++v),w;rewrite app_ass;repeat split;auto.
        exists u,v;tauto.
      + intros (?&w&(u&v&I1&I2&->)&I3&->).
        cut (l^{S n}%lang (v++w));[|apply IHn;exists v,w;tauto].
        intro I';exists u,(v++w);rewrite app_ass;repeat split;auto.
  Qed.

  (** This interpretation is sound in the sense that axiomatically
  equivalent expressions are semantically equivalent. Differently put,
  the axioms we imposed hold in every algebra of languages. *)
  Theorem 𝐄_eq_equiv_lang : forall e f : 𝐄 X, e ≡ f ->  e ≃ f.
  Proof.
    assert (dumb : forall A (w:list A), rev w = nil <-> w = nil)
      by (intros A w;split;[intro h;rewrite <-(rev_involutive w),h
                           |intros ->];reflexivity).
    intros e f E Σ σ;induction E;rsimpl;try firstorder.
    - apply iter_lang_eq,IHE.
    -  destruct H;rsimpl;repeat autounfold with semantics;try (now firstorder).
       + intro w;firstorder.
         * rewrite H1,H3;clear x0 w H1 H3;rewrite<- app_ass.
           eexists;eexists;split;[eexists;eexists;split;[|split]
                                 |split];eauto.
         * rewrite H1,H3;clear x w H1 H3;rewrite app_ass.
           eexists;eexists;split;
             [|split;[eexists;eexists;split;[|split]|]];eauto.
       + intro w;firstorder subst.
         * simpl in *;auto.
         * exists nil;exists w;simpl;auto.
       + intro w;firstorder subst.
         * rewrite app_nil_r;auto.
         * exists w;exists nil;rewrite app_nil_r;auto.
       + intro w;rewrite rev_involutive;tauto.
       + intro w;firstorder.
         * rewrite <- (rev_involutive w),H1,rev_app_distr;eauto.
           exists (rev x0);exists (rev x).
           setoid_rewrite rev_involutive;auto.
         * rewrite H1,rev_app_distr;eauto.
       + intros w;split.
         * intros (n&Iu);exists n.
           generalize dependent (S n);clear n;intros n;revert w.
           induction n;intro w;[simpl;apply dumb|].
           intro I;apply lang_iter_prod_last in I as (u&v&Iu&Iv&E).
           replace w with (rev v++rev u) in * by (now rewrite <- rev_app_distr,<-E,rev_involutive).
           clear w E.
           exists (rev v),(rev u);rewrite rev_involutive.
           repeat split;auto.
           apply IHn;rewrite rev_involutive;assumption.
         * intros (n&Iu);exists n.
           generalize dependent (S n);clear n;intros n;revert w.
           induction n;intro w;[simpl;apply dumb|].
           intro I;apply lang_iter_prod_last in I as (u&v&Iu&Iv&->).
           exists (rev v),(rev u);rewrite rev_app_distr.
           repeat split;auto.
       + intros w;split.
         * intros ([]&Iu).
           -- left;destruct Iu as (u'&?&Iu&->&->).
              rewrite app_nil_r;assumption.
           -- right;destruct Iu as (u&v&Iu&Iv&->).
              exists u,v;repeat split;auto.
              exists n;assumption.
         * intros [Iu|(u&v&Iu&(m&Iv)&->)].
           -- exists 0%nat,w,[];rewrite app_nil_r;repeat split;assumption.
           -- exists (S m),u,v;tauto.
       + intros w;split.
         * intros ([]&Iu).
           -- left;destruct Iu as (u'&?&Iu&->&->).
              rewrite app_nil_r;assumption.
           -- apply lang_iter_prod_last in Iu.
              right;destruct Iu as (u&v&Iu&Iv&->).
              exists u,v;repeat split;auto.
              exists n;assumption.
         * intros [Iu|(u&v&(m&Iu)&Iv&->)].
           -- exists 0%nat,w,[];rewrite app_nil_r;repeat split;assumption.
           -- exists (S m);apply lang_iter_prod_last.
              exists u,v;tauto.
       + intros w;firstorder subst.
         * destruct x;[|destruct x0];auto; discriminate. 
         * destruct x;destruct x0;try discriminate;auto.
         * exists [];exists [];subst;auto.
       + intro w;firstorder subst;simpl in *;auto.
       + intro w;firstorder subst;simpl in *. 
         * exists x0;exists [];rewrite app_nil_r;auto.
         * exists [];exists x;rewrite app_nil_r;auto. 
       + intro w;firstorder subst;simpl in *;auto.
       + intros w;split.
         * intros (n&In);revert w In;induction n;intros w Iw.
           -- destruct Iw as (u&v&Iu&->&->);rewrite app_nil_r.
              destruct Iu as [Iu|(v&w&(->&Iv)&Iw&->)].
              ++ left;exists 0%nat,u,[];rewrite app_nil_r;repeat split;auto.
              ++ right;exists [],w;repeat split;auto.
                 exists 0%nat,w,[];rewrite app_nil_r;repeat split;auto.
           -- destruct Iw as (u&v&Iu&Iv&->).
              apply IHn in Iv.
              destruct Iv as [(m&Iv)|(v1&v2&(->&Iv1)&(m&Im)&->)];
                destruct Iu as [Iu|(?&u'&(->&I1)&I2&->)];simpl;repeat rewrite app_nil_r. 
              ++ left;exists (S m),u,v;repeat split;auto.
              ++ right;exists [],(u'++v);repeat split;auto.
                 exists (S m),u',v;repeat split;auto.
                 assert (⟦ g ⟧ σ ≲ ⟦g + f⟧ σ) as E
                   by (rsimpl;repeat autounfold with semantics;intuition).
                 eapply (iter_prod_lang_incl E) in Iv;try reflexivity;clear E.
                 apply Iv.
              ++ right;exists [],(u++v2);split;[tauto|split;[|tauto]].
                 exists (S m),u,v2;tauto.
              ++ right;exists [],(u'++v2);split;[tauto|split;[|tauto]].
                 exists (S m),u',v2;tauto.
         * intros [I|I].
           -- assert (g≲ g + (1∩e)⋅f) as E
                 by (intro;intros;rsimpl;repeat autounfold with semantics;intuition).
              pose proof (E _ σ) as E';clear E.
              apply iter_lang_incl in E'.
              apply E',I. 
           -- destruct I as (?&u&(->&Ie)&I&->);simpl.
              assert (⟦g+f⟧ σ≲ ⟦g + (1∩e)⋅f⟧σ) as E.
              ++ intros v [Iv|Iv];[left;apply Iv|right;exists [],v;repeat split;assumption].
              ++ apply iter_lang_incl in E.
                 apply E,I.
    - destruct H.
      + assert (ih : ⟦e⋅f⟧σ≲⟦f⟧σ) by (intros u Iu;apply (IHE u);left;apply Iu);clear IHE.
        cut (⟦e⁺⋅f⟧σ≲⟦f⟧σ);[intros E u;split;[intros [I|I];[apply E|]|intro;right];assumption|].
        intros w (u&v&(n&Iu)&Iv&->).
        revert u v Iu Iv;induction n;intros u v Iu Iv.
        * destruct Iu as (u'&?&Iu&->&->).
          rewrite app_nil_r;apply ih;exists u',v;tauto.
        * apply lang_iter_prod_last in Iu as (u1&u2&Iu1&Iu2&->);rewrite app_ass.
          apply IHn;[assumption|].
          apply ih;exists u2,v;tauto.
      + assert (ih : ⟦f⋅e⟧σ≲⟦f⟧σ) by (intros u Iu;apply (IHE u);left;apply Iu);clear IHE.
        cut (⟦f⋅e⁺⟧σ≲⟦f⟧σ);[intros E u;split;[intros [I|I];[apply E|]|intro;right];assumption|].
        intros w (u&v&Iu&(n&Iv)&->).
        revert u v Iu Iv;induction n;intros u v Iu Iv.
        * destruct Iv as (v'&?&Iv&->&->).
          rewrite app_nil_r;apply ih;exists u,v';tauto.
        * destruct Iv as (v1&v2&Iv1&Iv2&->);rewrite <- app_ass.
          apply IHn;[|assumption].
          apply ih;exists u,v1;tauto.
  Qed.

  (** This extends to ordering as well. *)
  Lemma 𝐄_inf_incl_lang (e f : 𝐄 X) : e ≤ f -> e ≲ f.
  Proof.
    intro E;apply smaller_equiv,𝐄_eq_equiv_lang in E.
    intros Σ σ w I;apply E;rsimpl;left;left;auto.
  Qed.

  (** A word is in the language of a sum if and only if it is in the
  language of one of its components. *)
  Lemma sum_lang l A (σ:𝕬[X→A]) w :
    (w ∊ ⟦Σ l⟧ σ) <-> (exists t : 𝐄 X, t ∈ l /\ (⟦ t ⟧ σ) w).
  Proof.
    rewrite (𝐄_eq_equiv_lang (sum_fold _) _ _).
    induction l;simpl;rsimpl;autounfold with semantics.
    - firstorder.
    - split;[intros [h|IH];[|apply IHl in IH as (t&I&IH)]
            |intros (t&[<-|I]&IH)];auto.
      -- exists a;split;auto.
      -- exists t;split;auto.
      -- right;apply IHl;eauto.
  Qed.

  Global Instance incl_assign A : SemSmaller 𝕬[X → A] :=
    fun σ τ => forall x, σ x ≲ τ x.
  Global Instance eq_assign A : SemEquiv 𝕬[X → A] :=
    fun σ τ => forall x, σ x ≃ τ x.

  Global Instance incl_assign_PreOrder A : PreOrder (@ssmaller 𝕬[X → A] _).
  Proof.
    split.
    - intros σ x;reflexivity.
    - intros σ1 σ2 σ3 E1 E2 x;transitivity (σ2 x);auto.
  Qed.

  Global Instance eq_assign_Equivalence A : Equivalence (@sequiv 𝕬[X → A] _).
  Proof.
    split.
    - intros σ x;reflexivity.
    - intros σ τ E x;symmetry;auto.
    - intros σ1 σ2 σ3 E1 E2 x;transitivity (σ2 x);auto.
  Qed.
  Global Instance incl_assign_PartialOrder A : PartialOrder _ (@ssmaller 𝕬[X → A] _).
  Proof.
    intros σ τ;split;unfold Basics.flip.
    - intro E;split;intro x;rewrite (E x);reflexivity.
    - intros (E1&E2) x;apply lang_incl_PartialOrder;unfold Basics.flip;split;auto.
  Qed.

  Global Instance assign_monotone A :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (fun (σ : 𝕬[X → A]) e => ⟦e⟧σ).
  Proof.
    intros σ τ E e f E'.
    transitivity (⟦e⟧τ).
    - clear f E'.
      induction e;rsimpl;reflexivity||auto.
      + rewrite IHe1,IHe2;reflexivity.
      + rewrite IHe1,IHe2;reflexivity.
      + rewrite IHe1,IHe2;reflexivity.
      + rewrite IHe;reflexivity.
      + rewrite IHe;reflexivity.
    - apply E'.
  Qed.
  
  Global Instance assign_morph A :
    Proper (sequiv ==> sequiv ==> sequiv) (fun (σ : 𝕬[X → A]) e => ⟦e⟧σ).
  Proof.
    intros σ τ E e f E'.
    apply lang_incl_PartialOrder;unfold Basics.flip;split;apply assign_monotone;auto.
    - rewrite E;reflexivity.
    - rewrite E';reflexivity.
    - rewrite E;reflexivity.
    - rewrite E';reflexivity.
  Qed.

  Context {decX : decidable_set X}.
  Lemma 𝐒𝐏_variables_nil A t (σ : 𝕬[X → A]) :
    (⟦ t ⟧ Ξ σ) [] <->
    forall a : X', a ∈ 𝐒𝐏'_variables t -> Ξ σ a [].
  Proof.
    induction t as [a|t1 ih1 t2 ih2|t1 ih1 t2 ih2];simpl;split.
    - destruct a as (a,[]);intros I (b,[]);rsimpl;repeat rewrite mirror_nil;
        intros [E|[E|E]];inversion E;subst;auto.
    - intros I;rsimpl;apply I;destruct a as (a&[]);simpl;auto.
    - intros (u1&u2&I1&I2&E) a.
      symmetry in E;apply app_eq_nil in E as (->&->);mega_simpl.
      + rewrite ih1 in I1;apply (I1 a H).
      + rewrite ih2 in I2;apply (I2 a H).
    - rsimpl;intro I;exists [],[];repeat split.
      + rewrite ih1;intros a Ia;apply I;mega_simpl.
      + rewrite ih2;intros a Ia;apply I;mega_simpl.
    - intros (I1&I2) a;mega_simpl.
      + rewrite ih1 in I1;apply (I1 a H).
      + rewrite ih2 in I2;apply (I2 a H).
    - rsimpl;intro I;repeat split.
      + rewrite ih1;intros a Ia;apply I;mega_simpl.
      + rewrite ih2;intros a Ia;apply I;mega_simpl.
  Qed.
          
  Lemma lang_sub_terms A e (σ : 𝕬[X → A]) :
    ⟦e⟧σ ≃ (fun u => exists w, sub_terms e w /\ u∊ ⟦π{w}⟧ Ξ σ).
  Proof.
    induction e;simpl;rsimpl;split.
    - intros -> ;exists 𝟭';rsimpl;split;[auto|].
      unfold interprete,to_lang_𝐖;simpl.
      unfold T;split;[|reflexivity].
      simpl;intro;simpl;tauto.
    - intros (?&->&_&->);reflexivity.
    - intro F;exfalso;apply F.
    - intros (?&F&_);apply F.
    - intro I;exists (𝐖'_var x);split;[reflexivity|].
      split;simpl;[|apply I].
      intro;simpl;tauto.
    - intros (t&->&h1&h2).
      apply h2.
    - intros (u1&u2&I1&I2&->).
      apply IHe1 in I1 as (w1&I1&h11&h12).
      apply IHe2 in I2 as (w2&I2&h21&h22).
      exists (w1⊗' w2);split;[exists w1,w2;auto|].
      destruct w1 as (([t1|]&A1)&p1),w2 as (([t2|]&A2)&p2);split;simpl in *.
      + intro a;simpl;mega_simpl.
        * apply h11;auto.
        * apply h21;auto.
      + exists u1,u2;auto.
      + intro a;simpl;mega_simpl.
        * apply h11;auto.
        * apply h21;auto.
      + rewrite h22,app_nil_r;auto.
      + intro a;simpl;mega_simpl.
        * apply h11;auto.
        * apply h21;auto.
      + rewrite h12;auto.
      + intro a;simpl;mega_simpl.
        * apply h11;auto.
        * apply h21;auto.
      + rewrite h12,h22;reflexivity.
    - intros (?&(w1&w2&->&I1&I2)&h1&h2).
      destruct w1 as (([t1|]&A1)&p1),w2 as (([t2|]&A2)&p2);simpl in *.
      + destruct h2 as (u1&u2&Iu1&Iu2&->).
        exists u1,u2;repeat split;[apply IHe1|apply IHe2].
        * exists (exist _ (Some t1,A1) p1);repeat split;auto.
          intro;intros;simpl.
          apply h1;mega_simpl.
        * exists (exist _ (Some t2,A2) p2);repeat split;auto.
          intro;intros;simpl.
          apply h1;mega_simpl.
      + exists w,[];rewrite app_nil_r;repeat split;[apply IHe1|apply IHe2].
        * exists (exist _ (Some t1,A1) p1);repeat split;auto.
          intro;intros;simpl.
          apply h1;mega_simpl.
        * exists (exist _ (None,A2) p2);repeat split;auto.
          intro;intros;simpl;apply h1;mega_simpl.
      + exists [],w;repeat split;[apply IHe1|apply IHe2].
        * exists (exist _ (None,A1) p1);repeat split;auto.
          intro;intros;simpl;apply h1;mega_simpl.
        * exists (exist _ (Some t2,A2) p2);repeat split;auto.
          intro;intros;simpl.
          apply h1;mega_simpl.
      + rewrite h2;exists [],[];repeat split;[apply IHe1|apply IHe2].
        * exists (exist _ (None,A1) p1);repeat split;auto.
          intro;intros;simpl;apply h1;mega_simpl.
        * exists (exist _ (None,A2) p2);repeat split;auto.
          intro;intros;simpl;apply h1;mega_simpl.
    - intros (I1&I2).
      apply IHe1 in I1 as (w1&I1&h11&h12).
      apply IHe2 in I2 as (w2&I2&h21&h22).
      exists (w1⊕' w2);split;[exists w1,w2;auto|].
      destruct w1 as (([t1|]&A1)&p1),w2 as (([t2|]&A2)&p2);repeat split;simpl in *;
        try rewrite h12;try rewrite h22;try reflexivity.
      + intro a;simpl;mega_simpl.
        * apply h11;auto.
        * apply h21;auto.
      + apply h12.
      + apply h22.
      + intro a;simpl;mega_simpl.
        * apply h11;auto.
        * apply h21;auto.
        * rewrite h22 in h12.
          eapply 𝐒𝐏_variables_nil,H;auto.
      + intro a;simpl;mega_simpl.
        * apply h11;auto.
        * apply h21;auto.
        * rewrite h12 in h22.
          eapply 𝐒𝐏_variables_nil,H;auto.
      + intro a;simpl;mega_simpl.
        * apply h11;auto.
        * apply h21;auto.
    - intros (?&(w1&w2&->&I1&I2)&Iu);split;[apply IHe1;exists w1|apply IHe2;exists w2];split;auto;
        destruct w1 as (([t1|]&A1)&p1),w2 as (([t2|]&A2)&p2);simpl in *;destruct Iu as (hT&Iu);
          simpl in *.
      + destruct Iu as (Iu1&Iu2);split;simpl;auto.
        intros a Ia;apply hT;mega_simpl.
      + rewrite Iu;split;simpl.
        * intros a Ia;apply hT;mega_simpl.
        * apply 𝐒𝐏_variables_nil;intros a Ia;apply hT;mega_simpl;auto.
      + rewrite Iu;split;simpl.
        * intros a Ia;apply hT;mega_simpl.
        * reflexivity.
      + rewrite Iu;split;simpl.
        * intros a Ia;apply hT;mega_simpl.
        * reflexivity.
      + split;simpl.
        * intros a Ia;apply hT;mega_simpl.
        * destruct Iu as (Iu1&Iu2);apply Iu2.
      + rewrite Iu;split;simpl;[|reflexivity].
        intros a Ia;apply hT;mega_simpl;auto.
      + rewrite Iu;split;simpl.
        * intros a Ia;apply hT;mega_simpl;auto.
        * apply 𝐒𝐏_variables_nil;intros a Ia;apply hT;mega_simpl;auto.
      + rewrite Iu;split;simpl;[|reflexivity].
        intros a Ia;apply hT;mega_simpl;auto.
    - intros [I|I];[apply IHe1 in I|apply IHe2 in I];destruct I as (t&It&Iu);exists t;auto.
    - intros (t&[It|It]&Iu);[left;apply IHe1|right;apply IHe2];exists t;auto.
    - intros I;apply IHe in I as (t&It&Iu).
      exists (t`);rewrite conv𝐖'_idem;split;auto.
      clear IHe.
      destruct t as (([t|]&At)&p);simpl in *.
      + destruct Iu as (hT&Iu);split.
        * intros a Ia;apply hT,Ia.
        * simpl in *.
          clear hT;revert w Iu;clear.
          induction t as [(a,[])| |];intros u;rsimpl.
          -- auto.
          -- unfold mirror;rewrite rev_involutive;auto.
          -- intros (u1&u2&I1&I2&E).
             rewrite <- (rev_involutive u),E,rev_app_distr.
             exists (rev u2),(rev u1);repeat split.
             ++ apply IHt2;rewrite rev_involutive;auto. 
             ++ apply IHt1;rewrite rev_involutive;auto.
          -- intros (I1&I2);split;[apply IHt1|apply IHt2];auto.
      + destruct Iu as (hT&E);simpl in E.
        unfold unit in E.
        rewrite <- (rev_involutive w),E;split;[|reflexivity].
        apply hT.
    - intros I;simpl in *;apply IHe;destruct I as (t&It&hT&Iu);exists (t`);split;auto.
      destruct t as (([t|]&At)&p);(split;[intros a Ia;apply hT,Ia|]);simpl in *;
        [|rewrite <- (rev_involutive w),Iu;reflexivity].
          clear hT;revert w Iu;clear.
          induction t as [(a,[])| |];intros u;rsimpl.
          -- unfold mirror;rewrite rev_involutive;auto.
          -- auto.
          -- intros (u1&u2&I1&I2&->).
             rewrite rev_app_distr.
             exists (rev u2),(rev u1);repeat split.
             ++ apply IHt2;auto. 
             ++ apply IHt1;auto.
          -- intros (I1&I2);split;[apply IHt1|apply IHt2];auto.
    - intros (n&In);revert w In;induction n;intro w.
      + intros (u&?&Iu&->&->).
        apply IHe in Iu as (t&It&Iu).
        exists t;split;[exists O;auto|].
        rewrite app_nil_r;auto.
      + intros (u1&u2&Iu1&Iu2&->).
        apply IHn in Iu2 as (t2&(m&It2)&h21&h22).
        apply IHe in Iu1 as (t1&It1&h11&h12).
        exists (t1⊗' t2);split;[exists (S m),t1,t2;tauto|].
        destruct t1 as (([t1|]&A1)&p1),t2 as (([t2|]&A2)&p2);split;simpl in *.
        * intro a;simpl;mega_simpl.
          -- apply h11;auto.
          -- apply h21;auto.
        * exists u1,u2;auto.
        * intro a;simpl;mega_simpl.
          -- apply h11;auto.
          -- apply h21;auto.
        * rewrite h22,app_nil_r;auto.
        * intro a;simpl;mega_simpl.
          -- apply h11;auto.
          -- apply h21;auto.
        * rewrite h12;auto.
        * intro a;simpl;mega_simpl.
          -- apply h11;auto.
          -- apply h21;auto.
        * rewrite h12,h22;reflexivity. 
    - intros (t&(n&It)&Iu);exists n;revert w t It Iu;induction n;intros w t.
      + simpl;intros It Iu.
        exists w,[];rewrite app_nil_r;repeat split;apply IHe;exists t;auto.
      + intros (t1&t2&->&It1&It2) (h1&h2).
        destruct t1 as (([t1|]&A1)&p1),t2 as (([t2|]&A2)&p2);simpl in *.
        * destruct h2 as (u1&u2&Iu1&Iu2&->).
          exists u1,u2;repeat split;[apply IHe|eapply IHn].
          -- exists (exist _ (Some t1,A1) p1);repeat split;auto.
             intro;intros;simpl.
             apply h1;mega_simpl.
          -- apply It2.
          -- split;auto.
             intro;intros;simpl.
             apply h1;mega_simpl.
        * exists w,[];rewrite app_nil_r;repeat split;[apply IHe|eapply IHn].
          -- exists (exist _ (Some t1,A1) p1);repeat split;auto.
             intro;intros;simpl.
             apply h1;mega_simpl.
          -- apply It2.
          -- repeat split;auto.
             intro;intros;simpl;apply h1;mega_simpl.
             
        * exists [],w;repeat split;[apply IHe|eapply IHn].
          -- exists (exist _ (None,A1) p1);repeat split;auto.
             intro;intros;simpl;apply h1;mega_simpl.
          -- apply It2.
          -- repeat split;auto.
             intro;intros;simpl.
             apply h1;mega_simpl.
        * rewrite h2;exists [],[];repeat split;[apply IHe|eapply IHn].
          -- exists (exist _ (None,A1) p1);repeat split;auto.
             intro;intros;simpl;apply h1;mega_simpl.
          -- apply It2.
          -- repeat split;auto.
             intro;intros;simpl;apply h1;mega_simpl.
  Qed.

  Corollary ssmaller_sub_terms_inf : subrelation sub_terms_inf ssmaller.
  Proof.
    intros e f I A σ u Iu.
    apply lang_sub_terms in Iu as (w&Iw&Iu).
    apply I in Iw as (w'&Iw&Ew).
    apply lang_sub_terms;exists w';split;[assumption|].
    unfold smaller,𝐖'_inf,smaller,𝐖_inf in Ew.
    destruct w as (([t|]&At)&p),w' as (([t'|]&At')&p');unfold interprete,to_lang_𝐖 in *;simpl in *.
    - destruct Ew as (I1&I2),Iu as (hT&Iu).
      split;[intros a Ia;apply hT,I1,Ia|].
      revert I2 p hT Iu;clear.
      intros E Bal hσ Iu.
      apply balanced_spec in Bal.
      unfold T in hσ;simpl in hσ.
      revert u Iu;induction E;intro u.
      + tauto.
      + firstorder.
      + destruct H as [[]|[]];rsimpl;firstorder subst.
        * exists (x++x1),x2;rewrite app_ass;repeat split;auto.
          exists x,x1;auto.
        * exists x1,(x2++x0);rewrite app_ass;repeat split;auto.
          exists x2,x0;auto.
      + destruct H;rsimpl;firstorder subst.
        * exists [],u;repeat split;auto.
          apply 𝐒𝐏_variables_nil.
          intros a Ia;apply hσ;simpl.
          destruct a as (a,b);apply 𝐒𝐏_variables_𝐒𝐏'_variables_iff in Ia.
          destruct b,Ia as [Ia|Ia];apply H,Ia||apply Bal,H,Ia.
        * exists u,[];rewrite app_nil_r;repeat split;auto.
          apply 𝐒𝐏_variables_nil.
          intros a Ia;apply hσ;simpl.
          destruct a as (a,b);apply 𝐒𝐏_variables_𝐒𝐏'_variables_iff in Ia.
          destruct b,Ia as [Ia|Ia];apply H,Ia||apply Bal,H,Ia.
      + rsimpl;intros (u1&u2&I1&I2&->);exists u1,u2;repeat split;firstorder.
      + rsimpl;intros (I1&I2);split;firstorder.  
    - tauto.
    - destruct Ew as (I1&I2),Iu as (hT&->).
      split;[intros a Ia;apply hT,I1,Ia|].
      apply 𝐒𝐏_variables_nil.
      intros a Ia;apply hT;simpl.
      destruct a as (a,b);apply 𝐒𝐏_variables_𝐒𝐏'_variables_iff in Ia.
      apply balanced_spec in p.
      destruct b,Ia as [Ia|Ia];apply I2,Ia||apply p,I2,Ia.
    - destruct Iu as (hT&->).
      split;[intros a Ia;apply hT,Ew,Ia|reflexivity].
  Qed.
    
End language.
(* begin hide *)
Hint Rewrite @𝐄_unit @𝐄_empty @𝐄_variable @𝐄_intersection
     @𝐄_prod @𝐄_union @𝐄_mirror
  : simpl_typeclasses.
(* end hide *)

Require Import to_lang_witness.

Theorem ssmaller_sub_terms_inf_iff {X:Set}`{decidable_set X} (e f : 𝐄 X) : e ≦ f <-> e ≲ f.
Proof.
  split;[apply ssmaller_sub_terms_inf|].
  destruct (sub_terms_inf_dec e f) as [E|E];auto.
  destruct E as (w&Iw&Nw).
  intros E;exfalso.
  destruct (not_incl_witness_Ξ w) as (u&σ&P).
  pose proof (E nat σ) as E.
  assert (F :  𝕲w π{ w} ≲ 𝕲w π{ w}) by (split;reflexivity).
  apply P in F.
  assert (Iu : (⟦ f ⟧ σ) u) by (apply E,lang_sub_terms;exists w;auto).
  apply lang_sub_terms in Iu as (w'&Iw'&Iu).
  apply P in Iu.
  apply Nw in Iw'.
  apply 𝐖_inf_is_weak_graph_inf in Iu;[|apply dec_X';auto].
  apply Iw',Iu.
Qed.


Section diablerie.
  Context {X : Set} {decX : decidable_set X}.

  Lemma sub_terms_sum L t : sub_terms (Σ L) t <-> exists e, e ∈ L /\ sub_terms e t.
  Proof.
    revert t;induction L as [|f L];intro t;simpl;[firstorder|].
    destruct L as [|x L];[|generalize dependent (x::L);clear L;intros L IHL];simpl.
    - split;[intro I;exists f|intros (?&[<-|F]&I)];tauto.
    - rewrite IHL;clear IHL;split;
        [intros [I|(e&Ie&I)];[exists f|exists e]|intros (e&[<-|Ie]&I);[left|right;exists e]];tauto.
  Qed.
  
  Lemma sp_terms_sum L t : sp_terms (Σ L) t <-> exists e : 𝐄 X, e ∈ L /\ sp_terms e t.
  Proof.
    revert t;induction L as [|f L];intro t;simpl;[firstorder|].
    destruct L as [|x L];[|generalize dependent (x::L);clear L;intros L IHL];simpl.
    - split;[intro I;exists f|intros (?&[<-|F]&I)];tauto.
    - rewrite IHL;clear IHL;split;
        [intros [I|(e&Ie&I)];[exists f|exists e]|intros (e&[<-|Ie]&I);[left|right;exists e]];tauto.
  Qed.

  Lemma sub_terms_test A w :
    sub_terms (test A) w -> fst (π{w}) = None /\ snd (π{w}) ≈ balance A.
  Proof.
    revert w;induction A;intro w;simpl.
    - intros ->;split;reflexivity.
    - intros (w1&w2&->&(t1&t2&->&->&E)&I).
      rewrite <- (conv𝐖'_idem t2),E.
      apply IHA in I as (E1&E2);clear t2 E IHA.
      destruct w2 as ((?&B)&?);simpl in *;subst;simpl;split;auto.
      rewrite E2;intro;mega_simpl;simpl;tauto.
  Qed.
  
  Lemma sub_terms_test_member A :
    exists wA, sub_terms (test A) wA /\ fst (π{wA}) = None /\ snd (π{wA}) ≈ balance A.
  Proof.
    induction A;simpl.
    + exists 𝟭';split;[|split];reflexivity.
    + destruct IHA as (wA&IA&E1&E2).
      exists (( 𝐖'_var a ⊕' 𝐖'_var a`)⊕'wA).
      split.
      * exists ( 𝐖'_var a ⊕' 𝐖'_var a`),wA;split;[reflexivity|].
        split;[|assumption].
        exists (𝐖'_var a),(𝐖'_var a`);rewrite conv𝐖'_idem;auto.
      * destruct wA as ((wA&B)&pp);simpl in *;subst;simpl.
        split;[auto|].
        rewrite E2;clear.
        intro;mega_simpl;simpl;tauto.
  Qed.

  Lemma positive_one_free (e : 𝐄 X) : positive e = true -> one_free e = true.
  Proof. induction e;simpl;repeat rewrite andb_true_iff;firstorder. Qed.
  
  Lemma reduction_one_free A (e f : 𝐄 X) :
    one_free e = true -> test A ⋅ e ≲ f ->
    exists g, one_free g = true /\ test A ⋅ g ≤ f /\ e ≲ g.
  Proof.
    intros Oe E.
    exists (Σ (flat_map (fun p => match snd p,inclb (fst p) A with
                          | Some g,true => reduce_expr A g 
                          | _,_ => [] end)
                   (expand f))).
    repeat split.
    - clear;induction (expand f) as [|(B&[g|]) l];[reflexivity| |apply IHl].
      simpl; case_eq (inclb B A);[|intros _;apply IHl].
      rewrite inclb_correct;intro I.
      pose proof (@positive_reduce_expr _ _ A g) as hyp.
      induction (reduce_expr A g);[simpl;apply IHl|].
      destruct l0 as [|x l0];[destruct (flat_map _ l)|].
      * simpl;apply positive_one_free, hyp;now left.
      * apply andb_true_iff;split;[apply positive_one_free, hyp;now left|].
        apply IHl0;simpl;tauto.
      * apply andb_true_iff;split;[apply positive_one_free, hyp;now left|].
        apply IHl0;simpl.
        intros ? [<-|?];apply hyp;simpl;auto.  
    - replace (test A) with (Σ [test A]) by reflexivity.
      rewrite seq_distr;simpl;rewrite app_nil_r.
      apply Σ_small.
      intros f' If'.
      apply in_map_iff in If' as (g'&<-&Ig).
      apply in_flat_map in Ig as ((B&[g|])&Ig&EE);simpl in EE;[|tauto].
      case_eq (inclb B A);[|intro F;rewrite F in EE;simpl in EE;tauto].
      intro I;rewrite I in EE;rewrite inclb_correct in I.
      rewrite (expand_eq decX f).
      transitivity (test B ⋅ g);
        [|eapply Σ_big;reflexivity||auto;
          apply in_map_iff;exists (B,Some g);auto].
      apply test_incl in I;unfold Basics.flip in I.
      rewrite test_dup;rewrite I at 1.
      rewrite <- ax_seq_assoc.
      apply seq_smaller;[reflexivity|].
      etransitivity;[|apply reduce_expr_inf].
      apply seq_smaller;[reflexivity|].
      eapply Σ_big;eauto;reflexivity.
    - rewrite (𝐄_eq_equiv_lang(expand_eq decX f)) in E.
        rewrite <- ssmaller_sub_terms_inf_iff in *.
        intros t It.
        destruct (sub_terms_test_member A) as (wA&IA&EA1&EA2).
        assert (Is : sub_terms (test A ⋅ e) (wA⊗'t)) by (exists wA,t;auto).
        apply E in Is as (s&Is&Es).     
        apply sub_terms_sum in Is as (f'&If'&Iw').
        apply in_map_iff in If' as ((B&g)&<-&Ig);simpl in *.
        cut (exists t', π{t} = (Some t',[]));
          [|apply sub_terms_sp_terms_one_free in It as (?&?&(E1&E2));auto;
            destruct t as (([t|]&C)&pp);[simpl in *|exfalso;apply E2];
            exists t;f_equal;destruct E2 as (E2&_);
            destruct C as [|c];[reflexivity|exfalso;cut (c ∈ []);[|apply E2];simpl;auto]].
        destruct wA as ((wA&C)&pp);simpl in *;subst;clear IA.
        destruct t as (tt&pt);simpl;intros (t&->);simpl in *.
        unfold smaller,𝐖'_inf in *;simpl in *.
        rewrite app_nil_r in Es.
        destruct s as (([s|],D)&ps);simpl in *;clear pp;[|exfalso;apply Es].
        destruct Es as (E__D&Es).
        rewrite EA2 in *.
        apply incl_PartialOrder in EA2 as (EE&_);apply (incl_𝐒𝐏_inf EE) in Es;clear C EE.
        destruct g as [g|];[|exfalso;apply sub_terms_test in Iw' as (FF&_);discriminate].
        destruct Iw' as (t1&t2&EE&IB&Is).
        apply sub_terms_test in IB as (E1&E__B).
        destruct t1 as ((?&C)&pt1);simpl in *;subst.
        destruct t2 as (([t2|]&C')&pt2);[|discriminate].
        inversion EE;subst;clear EE;simpl in *.
        clear ps pt1.
        rewrite E__B in E__D;clear C E__B.
        pose proof Is as Is';apply sub_terms_sp_terms_one_free in Is' as (s&Is'&((h11&E1)&(h21&E2)));
          [|eapply (@one_free_expand _ decX);eauto].
        destruct C' as [|c];[clear h11 h21|exfalso;cut (c∈[]);[|apply h21];simpl;auto].
        rewrite app_nil_r in *.
        apply balance_incl_iff in E__D.
        rewrite (incl_𝐒𝐏_inf (incl_nil _) E1) in Es.
        apply (@reduce_term_spec _ dec_X') in Es as (w'&Ew'&Iw').
        destruct (@sp_terms_reduce_term_project _ _ A g w') as (w&Ew&Iw);
          [exists w',s;split;[reflexivity|auto]|].
        apply sp_terms_sum in Iw as (g'&Ig'&Iw).
        apply sp_terms_sub_terms_one_free in Iw as (W&Iw&EW);
          [|apply positive_one_free;eapply positive_reduce_expr;eauto].
        exists W;split.
      + apply sub_terms_sum.
        exists g';split;auto.
        apply in_flat_map;exists (B,Some g);split;auto.
        simpl;apply inclb_correct in E__D as ->;auto.
      + rewrite EW;split;[reflexivity|].
        rewrite Iw';apply Ew.
  Qed.                           
End diablerie.  



(*  LocalWords:  subunits
 *)


