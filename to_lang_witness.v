(** In most this module, we establish the following lemma: for every
series-parallel term [u], there exists a word [w] and an
interpretation [σ] such that for every other [𝐒𝐏] term [v], the graph
of [v] is larger than the graph of [u] if and only if [w] belongs to
the [σ]-interpretation of [v]. This lemma will allow us to prove
completeness of our axiomatization with respect to language
interpretations.

In the second part of the module, we generalise the result to weak
terms and primed weak terms. *)

(** In this module, we call a list of natural numbers a _sequence_ is
it of the form [[a(0);a(1);...;a(n)]] such that for every [i<n],
[a(i)+1=a(i+1)]. Equivalently, they are those sequences produced using
the function [seq]. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export w_terms_graphs.
Require Import PeanoNat.
Section s.
  (** * Main construction *)
  Variable X : Set.
  Variable dec_X : decidable_set X.
  (** Let us fix a term [u] and a set of variables [𝐀] for this section. *)
  Variable u : 𝐒𝐏 X.
  Variable 𝐀 : list X.
  (** We write the reachability relation in the graph of [u] as [⤏].*)
  Notation " i ⤏ j " := (i -[𝕲 u]→ j) (at level 60).

  (** To make some definitions easier, we define the function [co_nth
  i l]. This function returns the smallest index [j] such that the
  [j]th element of the list [l] is equal to [i]. *)
  Fixpoint co_nth i l :=
    match l with
    | [] => 0
    | a::l =>
      if i =? a
      then 0
      else S (co_nth i l)
    end.

  (** If [i] appears in the list [l], then the element at index
  [co_nth i l] in the list [l] is equal to [i]. *)
  Lemma co_nth_In i l :
    i ∈ l -> nth (co_nth i l) l 0 = i.
  Proof.
    revert i;induction l;intros i;simpl.
    - tauto.
    - destruct_eqb i a.
      + tauto.
      + intros [->|I];[tauto|].
        apply IHl,I.
  Qed.

  (** If [i] is in [l], this function returns an index smaller than
  the length of [l]. *)
  Lemma co_nth_range i l : i ∈ l -> co_nth i l < length l.
  Proof.
    revert i;induction l;intros i;simpl.
    - tauto.
    - destruct_eqb i a;simpl.
      + lia.
      + intros [->|I];[tauto|].
        apply IHl in I;lia.
  Qed.

  (** The following simple observation follows easily from
  [co_nth_In]. *)
  Lemma co_nth_inj i j l :
    i ∈ l -> j ∈ l -> co_nth i l = co_nth j l -> i = j.
  Proof.
    intros Ii Ij E.
    pose proof (co_nth_In Ii) as <-.
    pose proof (co_nth_In Ij) as <-.
    rewrite E;reflexivity.
  Qed.

  (** We will mainly apply this function on lists without
  duplicates. In such lists, the function is injective. *)
  Lemma nth_NoDup {A} i j l (d1 d2 : A) :
    NoDup l -> i < length l -> j < length l ->
    nth i l d1 = nth j l d2 -> i = j.
  Proof.
    intros ND L1 L2.
    rewrite (nth_indep _ _ d1 L2).
    apply NoDup_nth;auto.
  Qed.

  (** In lists with no duplicates, we can prove the dual of lemma
  [co_nth_In]. *)
  Lemma co_nth_nth i l :
    NoDup l -> i < length l -> co_nth (nth i l 0) l = i.
  Proof.
    intros ND L.
    apply (nth_NoDup (l:=l) (d1:= 0) (d2:=0));auto.
    - apply co_nth_range.
      apply nth_In,L.
    - rewrite co_nth_In;auto.
      apply nth_In,L.
  Qed.

  (** [Vert] is a list containing exactly the vertices of the graph of
  [u], without any duplication, and listed in increasing order. *)
  Definition Vert :=
    filter (fun i => inb i (vertices u)) (seq 0 (S (ln u))).

  Lemma Vert_NoDup : NoDup Vert.
  Proof. unfold Vert;apply filter_NoDup,seq_NoDup. Qed.

  Lemma Vert_vertices : Vert ≈ vertices u.
  Proof.
    unfold Vert;intro i.
    rewrite filter_In,inb_spec,in_seq.
    intuition.
    apply vertices_graph_vertices in H;auto.
    apply vertices_bounded in H.
    lia.
  Qed.
  
  (** [N] is the number of vertices in the graph of [u]. *)
  Definition N := length Vert.

  (** The word [w_u] will be the witness we announced earlier. It is
  defined as the sequence of natural numbers from [0] to [2 * N -
  1]. *)
  Definition w_u := seq 0 (2*(N-1)).

  (** The function [γ] finds the index of a vertex in the list
  [Vert]. It provides us with a bijection between the vertices of [u]
  and the numbers from 0 to [N-1]. *)
  Definition γ i := co_nth i Vert.

  Lemma γ_inj i j :
    i ∈ (vertices u) -> j ∈ (vertices u) -> γ i = γ j -> i = j.
  Proof. rewrite <- Vert_vertices;unfold γ;apply co_nth_inj. Qed.

  Lemma γ_bij n : n < N -> exists i, i ∈ (vertices u) /\ γ i = n.
  Proof.
    intro L.
    exists (nth n Vert 0);split.
    - apply Vert_vertices,nth_In,L.
    - apply co_nth_nth;auto.
      apply Vert_NoDup.
  Qed.

  (** [γ] is order preserving. *)  
  Lemma γ_le i j :
    i ∈ (vertices u) -> j ∈ (vertices u) -> i <= j <-> γ i <= γ j.
  Proof.
    rewrite <- Vert_vertices.
    unfold γ,Vert.
    generalize dependent (fun i0 : nat => inb i0 (vertices u)).
    generalize dependent 0.
    generalize dependent (S (ln u)).
    induction n as [|l];intros n f.
    - simpl;tauto.
    - simpl;destruct (f n);simpl.
      + destruct_eqb i n;destruct_eqb j n.
        * intros;split;lia.
        * intros _ [F|I];try lia.
          apply filter_In in I as (I&_);apply in_seq in I;lia.
        * intros [F|I] _;try lia.
          apply filter_In in I as (I&_);apply in_seq in I;lia.
        * intros [F|Ii] [F'|Ij];try lia.
          rewrite (IHl _ _ Ii Ij);lia.
      + intros Ii Ij;rewrite (IHl _ _ Ii Ij);lia.
  Qed.

  (** Moreover, we know that the ordering of natural numbers contains
  the path ordering of vertices. Therefore using the previous lemma we
  get the following monotonicity property. *)
  Lemma γ_path i j : i ⤏ j -> γ i <= γ j.
  Proof.
    intro P.
    destruct_eqb i j;[reflexivity|].
    pose proof (path_𝕲 P) as L.
    rewrite <- γ_le;auto.
    - eapply vertices_graph_vertices,path_in_vertices;eauto.
    - eapply vertices_graph_vertices,path_in_vertices;
        try (right;apply P);auto.
  Qed.

  (** The following two observations are fairly self evident.*)
  Lemma γ0 : γ 0 = 0.
  Proof.
    unfold γ,Vert; pose proof (vertex_0 u) as I;destruct u;simpl;auto.
    - apply inb_spec in I;simpl in I.
      rewrite I;auto.
    - apply inb_spec in I;simpl in I.
      rewrite I;auto.
  Qed.

  Lemma N_non_zero : 1 < N.
  Proof.
    unfold N.
    pose proof (vertex_0 u) as I0.
    pose proof (vertex_ln u) as Iln.
    apply Vert_vertices in I0.
    apply Vert_vertices in Iln.
    pose proof (ln_non_zero u) as P.
    destruct Vert as [|v0 V];simpl in *;[tauto|].
    destruct V;simpl in *;lia.
  Qed.

  (** The next few lemmas are simple observations on sequences and on
  the function [co_nth]. *)
  Lemma seq_app i k l :
    seq i (k+l) = seq i k ++ seq (i+k) l.
  Proof.
    revert i;induction k;simpl;auto.
    intros i;simpl.
    rewrite IHk.
    f_equal;f_equal;f_equal.
    lia.
  Qed.
    
  Lemma seq_last i l : seq i (S l) = seq i l ++ [i+l].
  Proof.
    replace (S l) with (l + 1) by lia.
    rewrite seq_app.
    f_equal.
  Qed.

  Lemma rev_seq n1 l1 n2 l2 : seq n1 l1 = rev (seq n2 l2)
                              <-> l1 = l2 /\ (n1 = n2 /\ l1 = 1 \/ l1 = 0).
  Proof.
    split.
    - intros E.
      assert (F:length (seq n1 l1) = length (seq n1 l1)) by reflexivity.
      rewrite E,rev_length in F at 1;
        repeat rewrite seq_length in F.
      rewrite F in *;clear F l2;split;auto.
      destruct l1 as [|[|l1]];auto.
      + simpl in E;inversion E;auto.
      + exfalso.
        symmetry in E;rewrite seq_last,seq_last in E at 1.
        repeat rewrite rev_app_distr in E;simpl in E.
        inversion E;lia.
    - intros (<-&[(->&->) | ->]);reflexivity.
  Qed.

  Lemma firstn_seq n m l :
    m <= l -> firstn m (seq n l) = seq n m.
  Proof.
    revert n l;induction m;simpl;auto.
    intros n [|l] L;[lia|].
    simpl;f_equal;apply IHm;lia.
  Qed.
  
  Lemma skipn_seq n m l :
    m <= l -> skipn m (seq n l) = seq (n+m) (l-m).
  Proof.
    revert n l;induction m;simpl;auto.
    - intros n l _;f_equal;lia.
    - intros n [|l] L;[lia|].
      simpl;f_equal.
      rewrite IHm;[f_equal|];lia.
  Qed.

  Lemma co_nth_app_right i l m :
    ~ i ∈ l -> i ∈ m -> co_nth i (l++m) = length l + co_nth i m.
  Proof.
    induction l;simpl;auto.
    intros N I.
    destruct_eqb i a;[tauto|].
    rewrite IHl;auto.
  Qed.

  Lemma co_nth_app_left i l m :
    i ∈ l -> co_nth i (l++m) = co_nth i l.
  Proof.
    induction l;simpl.
    - tauto.
    - destruct_eqb i a;[tauto|].
      intros [->|I];[tauto|].
      apply IHl in I as ->;reflexivity.
  Qed.

  (** These observations allow us to prove that the image of the
  output of the graph of [u] by [γ] is [N-1]. *)
  Lemma γN : γ (ln u) = N - 1.
  Proof.
    pose proof N_non_zero as h;revert h.
    unfold N,γ;intros h.
    apply Nat.le_antisymm.
    - cut (co_nth (ln u) Vert < length Vert);[lia|].
      pose proof (vertex_ln u) as V.
      apply Vert_vertices,co_nth_range in V.
      lia.
    - unfold Vert.
      rewrite seq_last;simpl.
      rewrite filter_app.
      rewrite co_nth_app_right.
      + rewrite app_length;simpl. 
        pose proof (vertex_ln u) as V.
        apply inb_spec in V as ->;simpl.
        lia.
      + intro I;apply filter_In in I as (E&_).
        apply in_seq in E;lia.
      + apply filter_In;split;simpl;auto.
        apply inb_spec,vertex_ln.
  Qed.

  (** [ω i j] is the sequence of numbers from [2*γ i] to [2*γ j - 1]. *)
  Definition ω i j :=
    seq (2 * γ i) (2 * (γ j - γ i)).

  (** In particular we can use [ω] to define [w_u]. *)
  Lemma ω_word : w_u = ω 0 (ln u).
  Proof.
    unfold w_u,ω.
    rewrite γ0,γN.
    repeat lia||f_equal.
  Qed.

  (** This lemma is almost tautological, but it turns out to come in
  handy several times. *)
  Lemma ω_nil i j : ω i j = [] <-> γ j <= γ i.
  Proof. unfold ω;rewrite <- length_zero_iff_nil,seq_length;lia. Qed.  

  (** We now define the witnessing interpretation. For every edge
  [(i,a,j)] in the graph of [u], the word [ω i j] is in [σ a].
  Furthermore, if [a] belongs to the set [𝐀], then the empty word is
  in [σ a]. *)
  Definition σ : 𝕬[X→nat] :=
    fun a w =>
      (a ∈ 𝐀 /\ w = [])
      \/ (exists i j, (i,a,j) ∈ (edges (𝕲 u)) /\ w = ω i j).

  (** The only way for the empty word to appear in [σ a] is when [a]
  belongs to the set [𝐀]. *)
  Lemma σ_nil a : σ a [] <-> a ∈ 𝐀.
  Proof.
    unfold σ;split;auto.
    intros [(I&_)|(i&j&Ie&E)];auto.
    exfalso.
    symmetry in E;apply ω_nil in E.
    pose proof (In_vertices _ Ie) as (V1 & V2).
    rewrite <- (γ_le V2 V1) in E.
    apply 𝕲_bounded in Ie;lia.
  Qed.
  
  (** A simple observation we can make is that for any letter [a] the
  length of any word in [σ a] is even. This can be neatly extended to
  the language of any series-parallel term according to [σ]. *)
  Lemma lang_even (v : 𝐒𝐏 X) w :
    w ∊ (⟦v⟧ σ) -> Nat.Even (# w).
  Proof.
    revert w;induction v;intros w;rsimpl.
    - unfold σ;intros [(_&->)|(i&j&_&->)].
      + simpl;exists 0;reflexivity.
      + simpl;exists (γ j - γ i);unfold ω;rewrite seq_length;reflexivity.
    - intros (w1&w2&L1&L2&->);rewrite app_length.
      apply IHv1 in L1 as (n1&->).
      apply IHv2 in L2 as (n2&->).
      exists (n1+n2);lia.
    - now intros (L1&L2);auto.
  Qed.

  (** This result mainly relies on the fact that the [seq] function is
  essentially bijective. *)
  Lemma ω_seq n l i j :
    seq n (S l) = ω i j -> 2 * γ j = S (n + l) /\ 2 * γ i = n.
  Proof.
    unfold ω.
    intros E.
    assert (El: 2 * (γ j - γ i) = S l)
      by (rewrite <- (seq_length _ (2 * γ i)),<- E,seq_length;
          reflexivity).
    assert (n = 2 * γ i)
      by (rewrite El in E;simpl in E;inversion E;reflexivity).
    lia.
  Qed.

  (** If a word in the [σ]-interpretation of some term [v] is a
  sequence, then it can be expressed as [ω i j], for some vertices
  [i,j] such that [i⤏j]. *)
  Lemma seq_lang_vertices (v : 𝐒𝐏 X) n l :
    seq n l ∊ (⟦v⟧ σ) ->
    exists i j, i ∈ (vertices u) /\ j ∈ (vertices u) /\ i ⤏ j
           /\ seq n l = ω i j.
  Proof.
    revert n l;induction v;intros n l;rsimpl.
    - intros [(_&->)|(i&j&Ie&->)].
      + exists 0;exists 0;repeat split;try reflexivity||apply vertex_0.
        unfold ω;rewrite Nat.sub_diag;simpl;auto.
      + exists i;exists j;repeat split;
          reflexivity||(eapply path_edge,Ie)||is_vertex.
    - intros (w1&w2&L1&L2&E).
      replace l with (length w1 + length w2) in *
        by (rewrite<- app_length,<- E,seq_length;reflexivity).
      rewrite seq_app in *.
      apply length_app in E as (E1&E2);[|now apply seq_length].
      rewrite <- E1 in L1;rewrite<- E2 in  L2;clear E1 E2.
      apply IHv1 in L1 as (i&k1&Vi&Vk1&P1&E1).
      apply IHv2 in L2 as (k2&j&Vk2&Vj&P2&E2).
      destruct (length w1);[|destruct (length w2)].
      + simpl_nat;simpl.
        exists k2;exists j;repeat split;auto.
      + simpl;exists i;exists k1;repeat split;auto.
        rewrite app_nil_r;auto.
      + replace k2 with k1 in *.
        * exists i;exists j;repeat split;auto.
          -- etransitivity;eauto.
          -- rewrite E1,E2;unfold ω.
             pose proof (γ_path P1) as Γ1;pose proof (γ_path P2) as Γ2.
             replace (2 * (γ j - γ i))
             with (2 * (γ k1 - γ i) + 2 * (γ j - γ k1)) by lia.
             rewrite seq_app;f_equal.
             f_equal;lia.
        * apply ω_seq in E1;apply ω_seq in E2.
          apply γ_inj;auto;lia.
    - intros (L1&L2);apply IHv1 in L1 as (i&j&Vi&Vj&P&->).
      exists i;exists j;repeat split;auto.
  Qed.

  (** For every pair of vertices [i<=j], if [ω i j] belongs to the
  [σ]-interpretation of a sequential product [v1⨾v2], then there
  exists a vertex [k] between [i] and [j], such that [ω i k] belongs
  to the language of [v1] and [ω k j] to that of [v2]. This lemma is
  critical in the proof of the main lemma to make the induction case
  for products go through. *)
  Lemma seq_lang_prod (v1 v2 : 𝐒𝐏 X) i j :
    i ∈ (vertices u) -> j ∈ (vertices u) -> i <= j ->
    ω i j ∊ (⟦v1⨾v2⟧ σ) ->
    exists k, i <= k <= j /\ k ∈ (vertices u) /\ ω i k ∊ (⟦v1⟧ σ) /\ ω k j ∊ (⟦v2⟧ σ).
  Proof.
    rsimpl;intros Vi Vj P (w1&w2&L1&L2&E).
    unfold ω in *.
    assert (E' : 2 * (γ j - γ i) = length w1 + length w2)
      by (rewrite<- app_length,<- E,seq_length;reflexivity).
    rewrite E',seq_app in E.
    apply length_app in E as (E1&E2);[|now apply seq_length].
    rewrite <- E1 in L1;rewrite <- E2 in L2.
    destruct (length w1);[|destruct (length w2)].
    + simpl_nat;exists i;rewrite Nat.sub_diag.
      repeat split;auto.
      rewrite E';simpl in *;tauto.
    + simpl_nat;exists j;rewrite Nat.sub_diag.
      repeat split;auto.
      rewrite E';simpl in *;tauto.
    + pose proof L1 as H1;pose proof L2 as H2.
      apply seq_lang_vertices in H1 as (i1&k&Vi1&Vk1&P1&Ek1).
      apply seq_lang_vertices in H2 as (k2&j2&Vk2&Vj2&P2&Ek2).
      apply ω_seq in Ek1 as (Ei1&Ek1).
      apply ω_seq in Ek2 as (Ek2&Ej2).
      replace i1 with i in * by (apply γ_inj;auto;lia).
      replace j2 with j in * by (apply γ_inj;auto;lia).
      replace k2 with k in * by (apply γ_inj;auto;lia).
      exists k;repeat split;auto.
      * apply path_𝕲 in P1;auto.
      * apply path_𝕲 in P2;auto.
      * replace (S n) with (2 * (γ k - γ i)) in L1 by lia;auto.
      * replace (2 * γ i + S n) with (2 * γ k) in L2 by lia.
        replace (S n0) with (2 * (γ j - γ k)) in L2 by lia;auto.
  Qed.

  (** By a simple induction on [v], one can check that the empty word
  belongs to the language of [v] exactly if all of the variables in
  [v] belong to the set [𝐀]. *)
  Lemma lang_nil (v : 𝐒𝐏 X) : [] ∊ (⟦v⟧ σ) <-> 𝒱 v ⊆ 𝐀.
  Proof.
    induction v;rsimpl.
    - unfold σ;split.
      + intros [(I&_)|(i&j&F&E)].
        * unfold 𝒱;simpl;intros ? [->|F];simpl in *;tauto.
        * exfalso.
          symmetry in E;apply ω_nil in E.
          rewrite <- γ_le in E;[apply 𝕲_bounded in F;lia| |];
          apply In_vertices in F;tauto.
      + intro I;left;split;auto;apply I;now left.
    - unfold 𝒱 in *;simpl;split.
      + intros (w1&w2&L1&L2&E).
        symmetry in E;apply app_eq_nil in E as (->&->).
        apply incl_app;tauto.
      + intro E;apply incl_app_split in E as (L1&L2);
          exists [];exists [];repeat split;tauto.
    - unfold 𝒱 in *;simpl;split.
      + intros (L1&L2).
        apply incl_app;tauto.
      + intro E;apply incl_app_split in E as (L1&L2);
          split;tauto.
  Qed.

  (** To prove that from the fact that [w_u] is in the language [⟦v⟧
      σ] we can build a morphism from [v] to [u], we need to
      strengthen the statement slightly to make the induction work
      better. The statement that turns out to be sufficient is the
      following: if [ω i j] belongs to [⟦v⟧ σ], then there is an
      internal premorphism from [v] to [u] that must in addition map
      [0] to [i] and [ln v] to [j]. *)
  Lemma lang_ω_morph (v :𝐒𝐏 X) i j :
    i ∈ (vertices u) -> j ∈ (vertices u) -> i <= j ->
    ω i j ∊ (⟦v⟧ σ) ->
    exists φ, φ 0 = i /\ φ (ln v) = j /\ 𝐀 ⊨ (𝕲 v) -{φ}⇀ (𝕲 u) /\ internal_map φ (𝕲 v) (𝕲 u).
  Proof.
    revert i j;induction v;intros i j Vi Vj P.
    - rsimpl;unfold σ;
        pose proof P as Γ;rewrite γ_le in Γ;auto.
      rewrite ω_nil.
      intros [(Ia&E)|(i0&j0&Ie&E)].
      + replace j with i in * by (apply γ_inj;auto;lia).
        exists (fun _ => i);repeat split;auto.
        * intros p a q [Ee|F];[|simpl in F;tauto].
          symmetry in Ee;inversion Ee;tauto.
        * intros _ _;apply vertices_graph_vertices;auto.
      + cut (i = i0 /\ j = j0);[intros (<-&<-)|].
        * exists (fun k => if k =? 0 then i else j).
          repeat split;auto.
          -- intros p a q [Ee|F];[|simpl in F;tauto].
             symmetry in Ee;inversion Ee;tauto.
          -- intros [|k] _;apply vertices_graph_vertices;auto.
        * pose proof (In_vertices _ Ie) as (Vi0&Vj0).
          apply 𝕲_bounded in Ie as (Γ0&_).
          assert (L : γ i0 < γ j0).
          -- apply Nat.le_neq;split.
             ++ rewrite <-  γ_le;auto;lia.
             ++ intro N;apply γ_inj in N as ->;auto;lia.
          -- unfold ω at 2 in E.
             replace (2 * (γ j0 - γ i0))
             with (S (2 * (γ j0 - γ i0) - 1)) in E by lia.
             symmetry in E;apply ω_seq in E as (E1&E2).
             split;apply γ_inj;auto;lia.
    - intro h;apply seq_lang_prod in h as (k&(Pik&Pkj)&Vk&L1&L2);auto.
        apply IHv1 in L1 as (φ1&h1&h2&h3&h4);auto.
        apply IHv2 in L2 as (φ2&g1&g2&g3&g4);auto.
        pose proof (@inverse_add_left (ln v1) (⌊𝕲 v2⌋)) as Inv.
        exists (φ1 ⧼⌊𝕲 v1⌋⧽ φ2∘⨪(ln v1));
          split;[|split;[|split]].
      + rewrite seq_morph_in;auto.
        apply vertices_graph_vertices,vertex_0;auto.
      + rewrite seq_morph_not_in;auto.
        * unfold Basics.compose,decr;simpl;simpl_nat;auto.
        * simpl;intro I;apply vertices_bounded in I;
            pose proof (ln_non_zero v2);lia.
      + intros p a q Ie.
        simpl in Ie;rewrite edges_graph_union in Ie;
          apply in_app_iff in Ie as [Ie|Ie].
        * repeat rewrite seq_morph_in;
            try (apply in_graph_vertices;right;right;eauto).
          apply h3 in Ie;tauto.
        * rewrite edges_graph_map in Ie.
          apply graph.in_map in Ie as (x&y&Ie&<-&<-).
          replace (seq_morph _ _ _ (⨥(ln v1)x))
          with (φ2 x).
          -- rewrite seq_morph_not_in.
             ++ unfold Basics.compose,incr,decr;simpl_nat.
                apply g3 in Ie;tauto.
             ++ intro I;integers.
          -- destruct x.
             ++ unfold incr,decr,Basics.compose;simpl;simpl_nat.
                simpl;rewrite g1,seq_morph_in,h2;auto.
                apply vertices_graph_vertices,vertex_ln;auto.
             ++ rewrite seq_morph_not_in.
                ** unfold Basics.compose,incr,decr;f_equal;lia.
                ** intro I;integers.
      + intros k0 Vk0;unfold seq_morph;
          destruct (inb_dec k0 ⌊𝕲 v1⌋) as [(->&I)|(->&I)].
        * apply h4,I.
        * unfold Basics.compose;apply g4.
          rewrite <- vertices_graph_vertices;auto.
          rewrite <- vertices_graph_vertices in Vk0;auto.
          simpl in Vk0;rewrite in_app_iff,in_map_iff in Vk0.
          destruct Vk0 as [V|(k1&<-&V)];
            [|unfold incr,decr;simpl_nat;auto].
          apply vertices_graph_vertices in V;tauto.
    - intro L; rsimpl in L;destruct L as (L1&L2).
      apply IHv1 in L1 as (φ1&h1&h2&h3&h4);auto.
      apply IHv2 in L2 as (φ2&g1&g2&g3&g4);auto.
      pose proof (@inverse_par_map_left _ v1 v2) as Inv1.
      pose proof (@inverse_par_map_right _ v1 v2) as Inv2.
      exists (φ1 ∘ replace (ln v1 + ln v2) (ln v1)
            ⧼vertices v1⧽ φ2 ∘((fun n : nat => n - ln v1)
                                 ∘ replace 0 (ln v1))).
      split;[|split;[|split]].
      + rewrite seq_morph_in;auto.
        * unfold Basics.compose,replace.
          destruct_eqX 0 (ln v1+ln v2);auto.
          pose proof (ln_non_zero v1);lia.
        * apply vertex_0.
      + rewrite seq_morph_not_in.
        * unfold Basics.compose,replace;simpl.
          destruct_eqb (ln v1+ln v2) 0;simpl.
          -- pose proof (ln_non_zero v1);lia.
          -- simpl_nat;auto.
        * intro h;apply vertices_graph_vertices,vertices_bounded in h;
            auto.
          simpl in h;pose proof (ln_non_zero v2);lia.
      + intros p a q Ie.
        simpl in Ie;rewrite edges_graph_union in Ie;
          repeat rewrite edges_graph_map in Ie;
          apply in_app_iff in Ie as [Ie|Ie];
          apply graph.in_map in Ie as (x&y&Ie&<-&<-).
        -- replace (seq_morph _ _ _ ( _ x)) with (φ1 x);
             [replace (seq_morph _ _ _ ( _ y)) with (φ1 y)|].
           ++ apply h3 in Ie;tauto.
           ++ unfold Basics.compose,replace,seq_morph.
              pose proof (ln_non_zero v2) as hx.
              destruct_eqX y (ln v1);auto.
              ** destruct_eqX (ln v1+ln v2) 0;[lia|].
                 simpl_nat.
                 destruct (inb_dec (ln v1 + ln v2) (vertices v1))
                   as [(->&V)|(->&V)];auto.
                 rewrite h2,g2;auto.
              ** destruct (inb_dec y (vertices v1))
                  as [(->&V)|(->&V)];auto.
                 --- destruct_eqX y (ln v1+ln v2);auto.
                     apply 𝕲_bounded in Ie;lia.
                 --- apply In_vertices in Ie;tauto.
           ++ unfold Basics.compose,replace,seq_morph.
              pose proof (𝕲_bounded Ie) as hx.
              destruct_eqX x (ln v1);[lia|].
              pose proof (In_vertices _ Ie) as (V&_).
              apply inb_spec in V as ->.
              destruct_eqX x (ln v1+ln v2);[lia|auto].
        -- replace (seq_morph _ _ _ ( _ x)) with (φ2 x);
             [replace (seq_morph _ _ _ ( _ y)) with (φ2 y)|].
           ++ apply g3 in Ie;tauto.
           ++ unfold Basics.compose,replace,seq_morph,incr.
              pose proof (𝕲_bounded Ie) as hx.
              pose proof (In_vertices _ Ie) as (_&V).
              destruct_eqX (ln v1 + y) (ln v1);[lia|].
              destruct_eqX (ln v1 + y) 0;[lia|].
              destruct (inb_dec (ln v1 + y) (vertices v1))
                as [(->&V1)|(->&V1)];auto.
              ** apply vertices_graph_vertices,vertices_bounded in V1;
                   auto;lia.
              ** simpl_nat;auto.
           ++ unfold Basics.compose,replace,seq_morph,incr.
              pose proof (𝕲_bounded Ie) as hx.
              pose proof (In_vertices _ Ie) as (V&_).
              pose proof (ln_non_zero v1) as Pos.
              destruct_eqX (ln v1 + x) (ln v1).
              ** pose proof (vertex_0 v1) as I1;
                   apply inb_spec in I1 as ->.
                 destruct_eqX 0 (ln v1 + ln v2);[lia|].
                 replace x with 0 by lia;lia.
              ** destruct_eqX (ln v1 + x) 0;[lia|];simpl_nat.
                 destruct (inb_dec (ln v1+x) (vertices v1))
                   as [(->&V')|(->&V')];auto.
                 apply vertices_graph_vertices,vertices_bounded in V';
                   auto;lia.
      + intros k Vk ;apply vertices_graph_vertices in Vk;simpl in Vk;
        auto.
        apply in_rm in Vk as (Ek&Vk).
        rewrite in_app_iff,in_map_iff in Vk;destruct Vk
          as [Vk|(x&<-&Vx)].
        * rewrite seq_morph_in;auto.
          unfold Basics.compose,replace.
          pose proof Vk as V'.
          apply vertices_graph_vertices,vertices_bounded in V';auto.
          destruct_eqX k (ln v1+ln v2);[lia|].
          apply h4,vertices_graph_vertices,Vk;auto.
        * rewrite seq_morph_not_in.
          -- unfold Basics.compose,replace,incr in *.
             pose proof Vx as V'.
             apply vertices_graph_vertices,vertices_bounded in V';auto.
             destruct_eqX (ln v1+x) 0;[lia|].
             simpl_nat;apply g4,vertices_graph_vertices,Vx;auto.
          -- intros V';apply vertices_graph_vertices,
                       vertices_bounded in V';auto.
             integers.
  Qed.

  (** For the converse direction, we use the following lemma, also
  proved by induction on the term [v]. *)
  Lemma premorphism_ω (v : 𝐒𝐏 X) φ :
    𝐀 ⊨ 𝕲 v -{φ}⇀ 𝕲 u -> internal_map φ (𝕲 v) (𝕲 u) ->
    ω (φ 0) (φ (ln v)) ∊ (⟦v⟧ σ).
  Proof.
    revert φ;induction v;intros φ Mφ Iφ;autorewrite with simpl_typeclasses.
    - unfold σ;simpl.
      assert (I: (0,x,1) ∈ (edges (𝕲 (𝐒𝐏_var x)))) by now left.
      rewrite ω_nil.
      pose proof (Mφ _ _ _ I) as [(Ia&->)|Ie];auto.
      right;eauto.
    - set (φ1 := φ∘id).
      set (φ2 := φ∘(id∘⨥(ln v1))).
      assert (is_premorphism 𝐀 φ1 (𝕲 v1) (𝕲 u)
              /\ internal_map φ1 (𝕲 v1) (𝕲 u)
              /\ is_premorphism 𝐀 φ2 (𝕲 v2) (𝕲 u)
              /\ internal_map φ2 (𝕲 v2) (𝕲 u))
             as (M1&I1&M2&I2).
      + split;[|split;[|split]].
        * rewrite <- (app_nil_l 𝐀).
          eapply is_premorphism_compose;eauto.
          apply seq_graph_union_decomp;apply good_for_seq_𝕲.
        * intros i V;unfold φ1,id,Basics.compose.
          apply Iφ,vertices_graph_vertices;simpl;auto.
          apply in_app_iff;left;apply vertices_graph_vertices;auto.
        * rewrite <- (app_nil_l 𝐀).
          eapply is_premorphism_compose;eauto.
          eapply is_premorphism_compose_weak.
          -- pose proof (graph_map_morphism (⨥ (ln v1)) (𝕲 v2))
              as (_&_&M);eauto.
             apply M.
          -- apply seq_graph_union_decomp;apply good_for_seq_𝕲.
        * intros i V;unfold φ2,id,Basics.compose.
          apply Iφ,vertices_graph_vertices;simpl;auto.
          apply in_app_iff;right.
          apply in_map_iff;rewrite <-vertices_graph_vertices in V;eauto.
      + exists (ω (φ 0) (φ (ln v1)));exists (ω (φ (ln v1)) (φ (ln (v1 ⨾ v2)))).
        split;[|split].
        * apply IHv1;auto.
        * replace (φ (ln v1)) with (φ2 0)
            by (unfold φ2,incr,id,Basics.compose;simpl_nat;reflexivity).
          replace (φ (ln (v1⨾v2))) with (φ2 (ln v2))
            by (unfold φ2,incr,id,Basics.compose;simpl_nat;reflexivity).
          apply IHv2;auto.
        * simpl;unfold ω.
          cut (γ(φ (ln v1)) <= γ(φ (ln (v1⨾v2)))
               /\ γ (φ 0) <= γ (φ (ln v1))).
          -- intro L;simpl in L;replace (2 * γ (φ (ln v1)))
                     with (2 * γ (φ 0) + 2 * (γ (φ (ln v1)) - γ (φ 0)))
               by lia.
             rewrite<- seq_app;f_equal;lia.
          -- split;rewrite<-γ_le.
             ++ eapply path_𝕲.
                eapply (morphism_is_order_preserving Mφ).
                rewrite <- (graph_output (_⨾_)).
                apply 𝕲_connected,vertices_graph_vertices;auto.
                simpl;apply in_app_iff;left;apply vertex_ln.
             ++ apply vertices_graph_vertices,Iφ,
                vertices_graph_vertices;auto.
                simpl;apply in_app_iff;left;apply vertex_ln.
             ++ apply vertices_graph_vertices,Iφ,
                vertices_graph_vertices;auto.
                apply vertex_ln.
             ++ eapply path_𝕲.
                eapply (morphism_is_order_preserving Mφ).
                rewrite <- (graph_input (v1⨾v2)).
                apply 𝕲_connected,vertices_graph_vertices;auto.
                simpl;apply in_app_iff;left;apply vertex_ln.
             ++ apply vertices_graph_vertices,Iφ,
                vertices_graph_vertices;auto.
                apply vertex_0.
             ++ apply vertices_graph_vertices,Iφ,
                vertices_graph_vertices;auto.
                simpl;apply in_app_iff;left;apply vertex_ln.
    - simpl in Mφ.
      set (φ1 := φ∘(id∘{|ln v1\ln (v1∥v2)|})).
      set (φ2 := φ∘(id∘({|ln v1\0|} ∘ ⨥ (ln v1)))).
      assert (is_premorphism 𝐀 φ1 (𝕲 v1) (𝕲 u)
              /\ internal_map φ1 (𝕲 v1) (𝕲 u)
              /\ is_premorphism 𝐀 φ2 (𝕲 v2) (𝕲 u)
              /\ internal_map φ2 (𝕲 v2) (𝕲 u))
             as (M1&I1&M2&I2);[|split].
      + split;[|split;[|split]].
        * rewrite <- (app_nil_l 𝐀).
          eapply is_premorphism_compose;eauto.
          eapply is_premorphism_compose_weak.
          -- pose proof (graph_map_morphism
                           {|ln v1\ln v1 + ln v2|} (𝕲 v1))
              as (_&_&M&_);eauto.
          -- apply par_graph_union_decomp;apply good_for_par_𝕲.
        * intros i V;unfold φ1,id,Basics.compose.
          apply Iφ,vertices_graph_vertices;auto.
          unfold replace;destruct_eqX i (ln v1);auto.
          -- apply vertex_ln.
          -- simpl;apply in_rm;split;auto.
             apply in_app_iff;left;apply vertices_graph_vertices;auto.
        * rewrite <- (app_nil_l 𝐀).
          eapply is_premorphism_compose;eauto.
          eapply is_premorphism_compose_weak.
          -- pose proof (graph_map_morphism
                           ({|ln v1\0|} ∘ ⨥(ln v1)) (𝕲 v2))
              as (_&_&M&_);eauto.
          -- apply par_graph_union_decomp;apply good_for_par_𝕲.
        * intros i V;unfold φ2,id,Basics.compose.
          apply Iφ,vertices_graph_vertices;auto.
          unfold replace;destruct_eqX (⨥(ln v1)i) (ln v1);auto.
          -- apply vertex_0.
          -- simpl;apply in_rm;split;auto.
             apply in_app_iff;right;apply in_map_iff.
             rewrite <-vertices_graph_vertices in V;auto.
             exists i;split;auto;lia.
      + replace (φ 0) with (φ1 0);
          [replace (φ (ln (v1 ∥ v2))) with (φ1 (ln v1)) |].
        * apply IHv1;auto.
        * unfold φ1,id,incr,Basics.compose,replace.
          rewrite eqX_refl;auto.
        * unfold φ1,id,Basics.compose,replace.
          destruct_eqX 0 (ln v1);auto.
          pose proof (ln_non_zero v1);lia.
      +  replace (φ 0) with (φ2 0);
          [replace (φ (ln (v1 ∥ v2))) with (φ2 (ln v2)) |].
        * apply IHv2;auto.
        * unfold φ2,id,Basics.compose,replace.
          destruct_eqX (⨥(ln v1)(ln v2)) (ln v1);auto.
          integers.
        * unfold φ2,id,Basics.compose,replace.
          simpl_nat; rewrite eqX_refl;auto.
  Qed.

  (** With the two technical lemmas we just proved, the proof of the
  main result of this module becomes very simple. *)
  Theorem σ_spec (v : 𝐒𝐏 X) :
    w_u ∊ (⟦v⟧ σ) <-> 𝐀 ⊨ 𝕲 u ⊲ 𝕲 v.
  Proof.
    rewrite ω_word;split.
    - intro L.
      apply lang_ω_morph in L as (φ&h1&h2&h3&h4);auto.
      + exists φ;split;[|split;[|split]];auto.
        * repeat rewrite graph_input;auto.
        * repeat rewrite graph_output;auto.
      + apply vertex_0.
      + apply vertex_ln.
      + lia.
    - intros (ψ&ψ0&ψN&Mψ&Iψ).
      repeat rewrite graph_input in *||rewrite graph_output in *.
      rewrite <- ψ0,<-ψN.
      apply premorphism_ω;auto.
  Qed.
  
End s.

Section w.
  (** * Weak terms *)
  (** Nothing deep happens here, we just apply the results and
  constructions above to get a similar theorem. *)
  Variable X : Set.
  Variable dec_X : decidable_set X.
  
  Definition word (u : 𝐖 X) :=
    match u with
    | (None,_) => []
    | (Some u,_) => w_u u
    end.

  Definition σ' (u : 𝐖 X) : 𝕬[X→nat] :=
    match u with
    | (None,A) => fun a w => a ∈ A /\ w = []
    | (Some u,A) => σ u A
    end.

  Lemma σ'_nil u a : σ' u a [] <-> a ∈ (snd u).
  Proof.
    destruct u as ([u|],A);unfold σ'.
    - rewrite σ_nil;simpl;tauto.
    - simpl;split;tauto.
  Qed.

  Lemma word_of_term_correct u v :
    word u ∊ (⟦v⟧(σ' u)) <-> 𝕲w u ≲ 𝕲w v.
  Proof.
    unfold interprete,to_lang_𝐖.
    destruct v as ([v|]&A);destruct u as ([u|]&B);
      simpl;autounfold with semantics;unfold ssmaller,weak_graph_inf,T;simpl.
    - rewrite σ_spec;auto.
      split;intros (h1&h2);split;auto.
      + intros a I;eapply σ_nil;auto.
      + intros a I;eapply σ_nil;auto.
    - rewrite <- supid_hom_order,<- test_compatible_nil; split.
      + intros (h1&h2);split;intros a I.
        * now apply h1.
        * now apply h2.
      + intros (h1&h2);split;intros a I;split;auto.
    - split;intros (_&F);exfalso.
      + unfold w_u in F.
        pose proof (N_non_zero dec_X u).
        cut (length (@nil nat) = length (@nil nat));[|reflexivity].
        rewrite <- F,seq_length at 1;simpl;lia.
      + destruct F as (φ&h1&h2&_).
        repeat rewrite graph_input in * || rewrite graph_output in *.
        unfold input,output in *;simpl in *.
        integers.
    - split;intros (h&_);(split;[|reflexivity]);intros i I;
        [|split;auto];apply h,I.  Qed.
  
  Corollary not_incl_witness u :
    exists w (σ : 𝕬[X→nat]), forall v, 𝕲w u ≲ 𝕲w v <-> w ∊ (⟦v⟧σ).
  Proof.
    exists (word u);exists (σ' u);intro v.
    now rewrite word_of_term_correct.
  Qed.
End w.

Section t.
  (** * Primed weak terms *)
  (** For primed weak terms, another problem arises. Because of the
      way we have set up the translation form terms and expressions
      into weak terms, it is not sufficient to have a lemma of the
      shape

      [exists w (σ: 𝕬[X'→nat]), ∀ v : 𝐖', 𝕲w (π{u}) ≲ 𝕲w (π{v}) <-> w ∊
      ⟦π{v}⟧σ.]

      We need our witnessing interpretation [σ] to arise as [Ξ σ'] for
      some [σ' : 𝕬[X→nat]]. Thus we massage the construction we proved
      correct earlier, to put it into the appropriate shape. *)
  Variable X : Set.
  Variable dec_X : decidable_set X.
  Hint Resolve dec_X' dec_X.

  (** This is the witnessing interpretation. It works mostly like the
  one we built for series-parallel terms, but we make use of the
  boolean in the letters from the duplicated alphabet: if [(i,a,j)] is
  an edge, then [ω i j] is in the language [σΞ (a,true)], and its
  mirror image is in the language of [σΞ (a,false)]. *)
  Definition σΞ (t:𝐖') : 𝕬[X→nat] :=
    fun a w =>
      ((a,true) ∈ (snd (π{t})) /\ w = [])
      \/ (exists u, fst (π{t}) = Some u
                    /\ (exists i j,
                          ((i,(a,true),j) ∈ (edges (𝕲 u))
                            /\ w = ω u i j)
                           \/
                           ((i,(a,false),j) ∈ (edges (𝕲 u))
                            /\ w = rev (ω u i j)))).

  (** First, we make some observations about [σΞ]. *)
  Lemma σΞ_nil t a : σΞ t a [] <-> (a,true) ∈ (snd (π{t})).
  Proof.
    destruct t as ((t,A)&p);simpl.
    unfold σΞ;simpl.
    split;[|tauto].
    intros [(I&_)|(u&->&i&j&[(Ie&E)|(Ie&F)])];try tauto.
    - exfalso.
      symmetry in E;apply ω_nil in E.
      pose proof (In_vertices _ Ie) as (V1&V2).
      rewrite<- (γ_le _ V2 V1) in E.
      apply 𝕲_bounded in Ie;lia.
    - exfalso.
      assert (E : rev (@nil nat) = []) by reflexivity.
      rewrite F,rev_involutive in E at 1.
      apply ω_nil in E.
      pose proof (In_vertices _ Ie) as (V1&V2).
      rewrite<- (γ_le _ V2 V1) in E.
      apply 𝕲_bounded in Ie;lia.
  Qed.

  Lemma Ξ_σΞ_nil t a :
    Ξ (σΞ t) a [] <-> a ∈ (snd (π{t})).
  Proof.
    destruct a as (a&[|]);simpl.
    - apply σΞ_nil.
    - rewrite mirror_nil,σΞ_nil.
      destruct t as ((?&A),p);simpl.
      apply balanced_spec in p;apply p.
  Qed.
  
  Lemma lang_σΞ_even A u (v : 𝐒𝐏 X') w (p : balanced A = true):
    w∊(⟦v⟧(Ξ (σΞ (exist _ (Some u,A) p)))) ->
    exists k, length w = 2 * k.
  Proof.
    revert w;induction v;intros w;autorewrite with simpl_typeclasses.
    - destruct x as (x&[|]);autounfold;intro L;
      simpl in L;[|unfold mirror in L].
      -- destruct L as [(IA&->)|(s&E&i&j&[(I&->)|(I&->)])];
        try (revert I;symmetry in E;inversion E;clear s E H0;intro I).
         --- exists 0%nat;simpl;lia.
         --- unfold ω;rewrite seq_length;eauto.
         --- unfold ω;rewrite rev_length,seq_length;eauto.
      -- destruct L as [(IA&Eq)|(s&E&i&j&[(I&Eq)|(I&Eq)])].
         --- rewrite <- (rev_involutive w),Eq; exists 0%nat;simpl;lia.
         --- rewrite <- (rev_involutive w),Eq;
           unfold ω;rewrite rev_length,seq_length;eauto.
         ---rewrite <- (rev_involutive w),Eq,rev_involutive.
            unfold ω;rewrite seq_length;eauto.
    - intros (u1&u2&hu1&hu2&->).
      rewrite app_length; destruct (IHv1 u1) as (k1&->);
      [|destruct (IHv2 u2) as (k2&->)];auto.
      exists (k1+k2)%nat;lia.
    - intros (L1&L2);destruct (IHv1 w) as (k1&->);tauto||eauto.
  Qed.

  (** When looking for sequences, we can relate [Ξ σΞ] and [σ']. *)
  Lemma σΞ_σ_subword t A x i j  (p: balanced A = true) :
    Ξ (σΞ (exist _ (Some t,A) p)) x (ω t i j) <-> σ' (Some t,A) x (ω t i j).
  Proof.
    unfold is_balanced,Ξ,σ',σ,σΞ in *;destruct x as (x&[|]);simpl;
      unfold mirror;split.
    - intros [(I&E)|(u&S&i0&j0&I)];auto.
      (replace u with t in * by (now inversion S)).
      destruct I as [(I&E)|(I&E)].
      + right;eauto.
      + unfold ω in E;apply rev_seq in E as (E1&[(_&F)|E2]).
        * lia.
        * pose proof (In_vertices _ I) as (Vi0&Vj0).
          apply 𝕲_bounded in I.
          assert (L0: γ t j0 <= γ t i0) by lia.
          apply γ_le in L0;auto.
          lia.
    - intros [(I&E)|(i0&j0&I&E)];auto.
      right;eexists;split;eauto.
    - intros [(I&E)|(u&S&i0&j0&I)];auto.
      + left;split.
        * apply balanced_spec in p;apply p;auto.
        * rewrite <- rev_involutive,E at 1;reflexivity.
      + replace u with t in * by (now inversion S).
        destruct I as [(I&E)|(I&E)].
        * symmetry in E;unfold ω in E;
            apply rev_seq in E as (E1&[(_&F)|E2]).
          -- lia.
          -- pose proof (In_vertices _ I) as (Vi0&Vj0).
             apply 𝕲_bounded in I.
             assert (L0: γ t j0 <= γ t i0) by lia.
             apply γ_le in L0;auto.
             lia.
        * right;eexists;eexists;split;eauto.
          rewrite <- rev_involutive,<-E,rev_involutive;reflexivity.
    - intros [(I&->)|(i0&j0&I&->)];auto.
      + left;split;auto.
        apply balanced_spec in p;apply p,I.
      + right;exists t;split;auto.
        exists i0;exists j0;right;split;auto.
  Qed.

  (** Hence we can transfer the presence of a witness in [⟦v⟧(Ξ (σΞ
  u))] to the presence of the witness in the interpretation we set up
  for weak terms. *)
  Lemma witness_Ξ (u v : 𝐖') :
    (word (π{u})) ∊ (⟦π{v}⟧ (Ξ (σΞ u)))
    <->
    (word (π{u})) ∊ (⟦π{v}⟧ (σ' (π{u}))).
  Proof.
    revert v;cut (forall v : 𝐒𝐏 X',
                     (word (π{u})) ∊ (⟦v⟧(Ξ (σΞ u)))
                     <->(word (π{u})) ∊ (⟦v⟧ (σ' (π{u}))));
    [intros hyp (([v|],B),p);
     unfold interprete,to_lang_𝐖,intersection;simpl
    |intro v; destruct u as (([u|],A),p);simpl].
    - setoid_rewrite hyp.
      simpl;unfold T,test_compatible;simpl.
      setoid_rewrite Ξ_σΞ_nil.
      setoid_rewrite σ'_nil;auto;tauto.
    - simpl.
      unfold T,test_compatible;simpl.
      setoid_rewrite Ξ_σΞ_nil.
      setoid_rewrite σ'_nil;auto;tauto.
    - rewrite ω_word;auto.
      pose proof (vertex_0 u) as V1;pose proof (vertex_ln u) as V2.
      assert (L:0 <= ln u) by (pose proof (ln_non_zero u);lia).
      revert V1 V2 L;generalize dependent (ln u);
        generalize dependent 0%nat;induction v.
      + intros;rsimpl;apply σΞ_σ_subword;auto.
      + intros i j Vi Vj Lij;split.
        * rsimpl;intros (w1&w2&L1&L2&E).
          destruct (lang_σΞ_even L1) as (k1&E1).
          destruct (lang_σΞ_even L2) as (k2&E2).
          set (k := nth (k1 + γ u i) (Vert u) 0).
          assert (LE : 2*k1+2*k2 = 2 * (γ u j - γ u i))
            by (rewrite <- E1,<-E2,<-app_length,<-E;
                unfold ω;apply seq_length).
          assert (L : k1 + γ u i < length (Vert u)).
          -- assert (Lj:γ u j <= N u - 1)
              by (rewrite <- γN;auto;apply γ_le;auto using vertex_ln;
                  apply vertices_bounded,vertices_graph_vertices;
                  auto).
             pose proof (N_non_zero _ u) as hN.
             rewrite (γ_le _ Vi Vj) in Lij.
             unfold N in *; lia.
          -- assert (Vk: k ∈ (vertices u))
              by (apply Vert_vertices, nth_In,L;auto).
             assert (Γ: γ u k = k1 + γ u i)
               by (unfold γ,k;rewrite co_nth_nth;auto using Vert_NoDup).
             exists w1;exists w2;repeat split;auto.
             ++ replace w1 with (ω u i k) in *.
                ** apply IHv1;auto.
                   rewrite (γ_le _ Vi Vk);lia.
                ** transitivity (firstn (2*k1) (ω u i j)).
                   --- unfold ω;rewrite firstn_seq.
                       +++ f_equal;lia.
                       +++ lia.
                   --- rewrite E.
                       rewrite firstn_app,E1,Nat.sub_diag,<-E1.
                       simpl;rewrite app_nil_r.
                       apply firstn_all.
             ++ replace w2 with (ω u k j) in *.
                ** apply IHv2;auto.
                   rewrite (γ_le _ Vk Vj).
                   rewrite (γ_le _ Vi Vj) in Lij.
                   lia.
                ** transitivity (skipn (2*k1) (ω u i j)).
                   --- unfold ω;rewrite skipn_seq.
                       +++ f_equal;lia.
                       +++ lia.
                   --- rewrite E.
                       cut (w1++w2 = w1++w2);[|reflexivity].
                       rewrite <- firstn_skipn at 1.
                       intros E';apply length_app in E' as (_&E');eauto.
                       rewrite <- E1.
                       apply firstn_length_le.
                       rewrite app_length;lia.
        * intros L;eapply seq_lang_prod in L as (k&(R1&R2)&Vk&L1&L2);
            auto.
          apply IHv1 in L1;auto.
          apply IHv2 in L2;auto.
          rsimpl;eexists;eexists;repeat split;eauto.
          unfold ω.
          apply (γ_le _ Vi Vk) in R1.
          apply (γ_le _ Vk Vj) in R2.
          replace (2* γ u k) with (2 * γ u i + (2 * (γ u k - γ u i)))
            by lia.
          rewrite <- seq_app;f_equal;lia.
      + intros;rsimpl;split;intros (h1&h2);split.
        * apply IHv1;eauto.
        * apply IHv2;eauto.
        * apply IHv1;eauto.
        * apply IHv2;eauto.
    - induction v;simpl;rsimpl.
      + destruct x as (x&[|]);unfold Ξ,σΞ,σ,mirror;simpl.
        * split.
          -- intros [(I&_)|h];auto.
             destruct h as (?&F&_);discriminate.
          -- tauto.
        * split.
          -- intros [(I&_)|h];auto.
             ++ split;auto;apply balanced_spec in p;apply p;auto.
             ++ destruct h as (?&F&_);discriminate.
          -- intros (I&_);left;split;auto.
             apply balanced_spec in p;apply p;auto.
      + split;intros (w1&w2&I1&I2&E);(destruct w1;[|discriminate]);
          (destruct w2;[|discriminate]);exists [];exists[];repeat split;
            try (apply IHv1 || apply IHv2);  eauto.
      + split;intros (h1&h2);split;try(apply IHv1||apply IHv2);auto.
  Qed.

  (** We now prove our witnessing lemma by putting together lemmas
  [witness_Ξ] and [word_of_term_correct]. *)
  Corollary not_incl_witness_Ξ (u : 𝐖') :
    exists w (σ : 𝕬[X→nat]),
      forall v : 𝐖', 𝕲w (π{u}) ≲ 𝕲w (π{v}) <-> w ∊ (⟦π{v}⟧(Ξ σ)).
  Proof.
    exists (word (π{u}));exists (σΞ u);intro v.
    now rewrite witness_Ξ,word_of_term_correct;auto using dec_X'.
  Qed.

End t.
