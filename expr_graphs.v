(** Stringing together the various translations we've defined so far, we
may associate with every expression a set of weak graphs. This implies
decidability of the axiomatic equivalence for expressions. *)
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export w_terms_graphs terms_graphs expr_terms.

Section s.
  Variable X : Set.
  Variable dec_X : decidable_set X.

  (** Because the containment of terms is decidable, we have the
  following property. *)
  Lemma 𝐄_neq_𝐓s_neq (e f : 𝐄 X) :
    ~ (e ≤ f) -> exists t, t ∈ (𝐄_to_𝐓s e) /\ forall s, s ∈ (𝐄_to_𝐓s f) -> ~ (t≤s).
  Proof.
    rewrite 𝐄_to_𝐓s_inf_iff;auto;unfold smaller at 1;unfold 𝐓s_inf.
    setoid_rewrite 𝐓_inf_is_weak_graph_inf.
    setoid_rewrite <- (weak_graph_infb_correct dec_X').
    setoid_rewrite <- existsb_exists.
    intro h;apply forall_existsb in h as (t&It&ht).
    apply not_true_iff_false in ht.
    rewrite existsb_exists in ht.
    exists t;split;auto.
    intros s Is Es;apply ht;eauto.
  Qed.

  
  Definition 𝕲e (e : 𝐄 X) := List.map 𝕲t (𝐄_to_𝐓s e).

  Global Instance graphs_smaller :
    SemSmaller (list (weak_graph (@X' X))) :=
    fun l m => forall g, g ∈ l -> exists h, h ∈ m /\ g ≲ h.
  
  Global Instance graphs_equiv : SemEquiv (list (weak_graph (@X' X))) :=
    fun l m => l ≲ m /\ m ≲ l.

  Theorem smaller_𝕲e e f : e ≤ f <-> 𝕲e e ≲ 𝕲e f.
  Proof.
    rewrite 𝐄_to_𝐓s_inf_iff;auto.
    unfold 𝕲e, ssmaller,smaller,𝐓s_inf,graphs_smaller.
    setoid_rewrite 𝐓_inf_is_weak_graph_inf.
    repeat setoid_rewrite in_map_iff.
    split.
    - intros h1 g (? & <- & x & <- & I).
      destruct (h1 (𝐄_to_𝐓 x)) as (?&((y&<-&J)&L));[eauto|].
      mega_simpl;eauto.
    - intros h1 u (x & <- & I).
      destruct (h1 (𝕲t (𝐄_to_𝐓 x)))
        as (?&((?&<-&y&<-&E)&L));[eauto|].
      mega_simpl;eauto.
  Qed.

  Theorem equiv_𝕲e e f : e ≡ f <-> 𝕲e e ≃ 𝕲e f.
  Proof.
    unfold sequiv,graphs_equiv.
    rewrite <-smaller_𝕲e, <-smaller_𝕲e.
    split;[intros ->;auto|].
    apply smaller_PartialOrder;auto.
  Qed.

  Definition graphs_smallerb l m :=
    forallb (fun g => existsb
                        (@weak_graph_infb
                           (@X' X)
                           (@dec_X' _ dec_X) g) m) l.

  Lemma graphs_smallerb_correct l m :
    graphs_smallerb l m = true <-> l ≲ m.
  Proof.
    unfold graphs_smallerb,graphs_smaller.
    rewrite forallb_forall.
    setoid_rewrite existsb_exists.
    setoid_rewrite weak_graph_infb_correct.
    firstorder.
  Qed.
  
  Lemma graphs_equivb_correct l m :
    graphs_smallerb l m && graphs_smallerb m l = true <-> l ≃ m.
  Proof.
    unfold sequiv,graphs_equiv;rewrite andb_true_iff.
    repeat rewrite graphs_smallerb_correct;tauto.
  Qed.

  Corollary decidable_𝐄_inf (e f : 𝐄 X) : {e ≤ f} + {~ (e ≤ f)}.
  Proof.
    case_eq (graphs_smallerb (𝕲e e) (𝕲e f)).
    - intro h;apply graphs_smallerb_correct,smaller_𝕲e in h.
      now left.
    - intro h;rewrite <-not_true_iff_false,graphs_smallerb_correct, <-smaller_𝕲e in h.
      now right.
  Qed.
  
  Corollary decidable_𝐄_equiv (e f : 𝐄 X) : {e ≡ f} + {~ (e ≡ f)}.
  Proof.
    case_eq (graphs_smallerb (𝕲e e) (𝕲e f) && graphs_smallerb (𝕲e f) (𝕲e e)).
    - intro h;apply graphs_equivb_correct,equiv_𝕲e in h.
      now left.
    - intro h;rewrite <-not_true_iff_false,graphs_equivb_correct, <-equiv_𝕲e in h.
      now right.
  Qed. 

End s.