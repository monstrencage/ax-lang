(** We prove here that there is a finite number of functions between
 two finite decidable sets. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Export tools.

(** We will consider here maps modulo function extensionality. *)
Definition eqf { A B } : relation (A -> B) :=
  fun f g => forall n, f n = g n.
Infix " ⩵ " := eqf (at level 80).

(** [eqf] is an equivalence relation. *)
Global Instance eqf_Equivalence {A B}:
  Equivalence (@eqf A B).
Proof.
  split.
  - intros f n;auto.
  - intros f g E n;symmetry;apply E.
  - intros f g h E1 E2 n;rewrite E1;auto.
Qed.

Section s.
  
  (** We assume the source and target of our functions are decidable. *)
  Variables src trg : Set.
  Variable D : decidable_set src.
  Variable R : decidable_set trg.

  (** [hom] is the type of partial functions. *)
  Notation hom := (src -> option trg).

  (** The domain of [f] is the list [l] if the elements of [l] are
      exactly the values where [f] is defined. *)
  Definition domain (f : hom) l := forall x , f x <> None <-> x ∈ l.

  (** We use a somewhat relaxed notion of range here: we say that [m] 
      is a range of [f] if every value produced by [f] appears in the 
      list [m]. Note that [m] could contain more values. *)
  Definition range (f : hom) m := forall x y, f x = Some y -> y ∈ m.

  (** The map [f] is a finite function from [l] to [m] if [l] is its
      domain and [m] is its range. *)
  Definition finite_function f l m := domain f l /\ range f m.

  (** [finite_functions l m] generates a list of all finite functions 
      from [l] to [m], modulo [eqf]. *)
  Fixpoint finite_functions (l :list src) (m : list trg) :=
    match l with
    | [] => [fun _ => None]
    | a::l =>
      bimap (fun f b n => if eqX n a then Some b else f n)
            (finite_functions l m) m
    end.

  (** If [f] is a finite function from [l] to [m], there is an
      extensionally equivalent function [g] in the list
      [finite_functions l m]. *)
  Lemma finite_function_is_finite l m f :
    finite_function f l m ->
    exists g, g ∈ (finite_functions l m) /\ f ⩵ g.
  Proof.
    revert m f;induction l;intro m.
    - simpl;intros f (d&r);exists (fun _ => None);split;simpl;auto.
      intro n;pose proof (d n) as hyp;simpl in hyp.
      destruct (f n);auto; firstorder discriminate.
    - intros f (d&r);destruct (In_dec a l).
      -- destruct (IHl m f) as (g&I&E);[split|];auto;clear IHl.
         --- intro n;rewrite (d n);simpl;split;auto.
             intros [->|I];auto.
         --- simpl; assert (exists v, g a = Some v) as (v&eq)
                 by (case_eq (g a);[eauto|];intro N;
                     pose proof (d a) as hyp;simpl in hyp;exfalso;
                     rewrite E,N in hyp; firstorder discriminate).
             setoid_rewrite in_flat_map;setoid_rewrite in_map_iff.
             eexists;split;[exists g;split;[|exists v;split;
                                             [reflexivity|]]|];auto.
             ---- now apply (r a);rewrite E.
             ---- intro x;rewrite E,<-eq;destruct_eqX x a;reflexivity.
      --  destruct (IHl m (fun n => if eqX n a then None else f n))
        as (g&Ig&E);[split|];clear IHl.
          --- intros x; destruct_eqX x a;[tauto|].
              rewrite (d x);simpl;firstorder congruence.
          --- intros x y I;eapply r.
              destruct_eqX x a;[discriminate|eauto].
          --- exists (fun n => if eqX n a then (f a) else g n);split;
              [|intro x;rewrite<- E;destruct_eqX x a;auto].
              simpl; apply in_flat_map.
              assert (exists v, f a = Some v) as (v&eq)
                  by (case_eq (f a);[eauto|];intro N;
                      pose proof (d a) as hyp;simpl in hyp;exfalso;
                      rewrite N in hyp; firstorder discriminate).
              rewrite eq;exists g;split;
              [eauto|apply in_map_iff;exists v;split];auto.
              now apply (r a).
  Qed.

  (** If we're given a default value, we can complete a partial
      function as a total function. *)
  Definition make_total y0 (f : hom) x :=
    match f x with None => y0 | Some v => v end.

  (** Given a list [l] and a total function [f], we can compute a
   partial function whose domain is [l] and whose values are those of
   [f].*)
  Definition restriction l (f : src -> trg) : hom :=
    fun x => if inb x l then Some (f x) else None.

  (** We define finite support for total function using the default
  value [y0] instead of the undefined value [None] we used for partial
  functions. *)
  Definition finite_support (f : src -> trg) l m y0 :=
    forall x, (f x <> y0 -> x ∈ l) /\ f x ∈ m.

  (** If [f] has finite support, then its restriction to its domain is
  a finite function, and [f] is extensionally equivalent to the
  completion of its restriction. *)
  Lemma finite_support_finite_function f l m y0 :
    finite_support f l m y0 ->
    finite_function (restriction l f) l m
    /\ f ⩵ make_total y0 (restriction l f).
  Proof.
    unfold finite_support;intros r;split;[split|];auto.
    - intro x;unfold restriction.
      destruct (inb_dec x l) as [(->&I)|(->&I)];
        firstorder discriminate.
    - intros x y;unfold restriction.
      destruct (inb_dec x l) as [(->&I)|(->&I)];
        [|discriminate].
      intros h;inversion h;apply r.
    - intro x;unfold make_total,restriction.
      destruct (inb_dec x l) as [(->&I)|(->&I)];auto.
      destruct_eqX (f x) y0;[|apply r in N];tauto.
  Qed.

  (** Translates the list of finite partial functions into a list of
  finitely supported total functions. *)
  Definition finite_support_functions l m y0 :=
    map (make_total y0) (finite_functions l m).

  (** This list is exhaustive. *)
  Lemma get_finite_support_function f l m y0 :
    finite_support f l m y0 ->
    exists g, g ∈ (finite_support_functions l m y0) /\ f ⩵ g.
  Proof.
    intro d;apply finite_support_finite_function in d as (fn&E1).
    apply finite_function_is_finite in fn as (g&I&E2).
    unfold finite_support_functions;setoid_rewrite in_map_iff.
    eexists;split;[eauto|].
    intro;unfold make_total in *;rewrite E1,E2;auto.
  Qed.
End s.