(** This module is devoted to the reductions between expressions and
terms. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export terms expr.
Open Scope expr_scope.
Open Scope term_scope.
Section s.
  (** * Definitions *)
  Variable X : Set.
  Variable dec_X : decidable_set X.
  
  (** Translation of a term as an expression. *)
  Fixpoint 𝐓_to_𝐄 (t : 𝐓 X) : 𝐄 X :=
    match t with
    | 1 => 1%expr
    | 𝐓_var a => 𝐄_var a
    | 𝐓_cvar a => 𝐄_var a ¯%expr
    | u ⋅ v => (𝐓_to_𝐄 u ⋅ 𝐓_to_𝐄 v)%expr
    | u ∩ v => (𝐓_to_𝐄 u ∩ 𝐓_to_𝐄 v)%expr
    end.

  (** Translation of an expression into a term. Note that this
  function only makes sense for expressions that are already terms
  according to [is_term] (that is expressions that avoid [+] and [0],
  and where mirror can only be applied to variables). *)
  Fixpoint 𝐄_to_𝐓 (e : 𝐄 X) : 𝐓 X:=
    match e with
    | 1%expr => 1
    | 𝐄_var a => 𝐓_var a
    | 𝐄_var a ¯%expr => 𝐓_cvar a
    | (e ⋅ f)%expr => (𝐄_to_𝐓 e)⋅(𝐄_to_𝐓 f)
    | (e ∩ f)%expr => (𝐄_to_𝐓 e)∩(𝐄_to_𝐓 f)
    | _ => 1
    end.

  (** We transform an expression into a list of terms by first
  combing it (thus producing a list of term-like expressions), and
  then translating each of those expressions into terms. *)
  Definition 𝐄_to_𝐓s e := map 𝐄_to_𝐓 (comb e true).

  (** Given two lists of terms [l] and [m], we write [l≤m] when every
  term in [l] is axiomatically smaller than some term in [m]. *)
  Global Instance 𝐓s_inf : Smaller (list (𝐓 X)) :=
    fun l m => (forall u, u ∈ l -> exists v, v ∈ m /\ u ≤ v).

  (** We write [l≡m] when both [l≤m] and [m≤l] hold. *)
  Global Instance 𝐓s_eq : Equiv (list (𝐓 X)) :=
    fun l m => l ≤ m /\ m ≤ l.

  (** * Results *)

  (** Translating a term as an expression and then translating back
  does not change the term. *)
  Lemma 𝐓_to_𝐄_to_𝐓 t : t = 𝐄_to_𝐓 (𝐓_to_𝐄 t).
  Proof. induction t;simpl;firstorder congruence. Qed.

  (** The other direction also holds for term-like expressions. *)
  Lemma 𝐄_to_𝐓_to_𝐄 e : is_term e = true -> e = 𝐓_to_𝐄 (𝐄_to_𝐓 e).
  Proof.
    induction e;simpl;repeat rewrite andb_true_iff in *;
    try discriminate; try firstorder congruence.
    destruct e;try discriminate;intros;eexists;eauto.
  Qed.

  (** [𝐓_to_𝐄] is monotone. *)
  Lemma 𝐓_to_𝐄_inf u v : u ≤ v -> 𝐓_to_𝐄 u ≤ 𝐓_to_𝐄 v.
  Proof.
    intro E;induction E;simpl;auto.
    - eauto.
    - destruct H;simpl;auto.
      apply inf_ax_inter_l.
    - destruct H as [[]|[]];simpl;auto.
  Qed.

  (** The following lemma is critical to relate the contents of [comb
  e true] and [comb e false]. *)
  Lemma 𝐄_to_𝐓_comb e d :
    forall u, u ∈ map 𝐄_to_𝐓 (comb e d) ->
         exists v, v ∈ map 𝐄_to_𝐓 (comb e (negb d)) /\ u = v °.
  Proof.
    revert d;induction e.
    - intros d u [<-|F];simpl in *;exists 1;split;tauto||auto.
    - intros d u;simpl;tauto.
    - intros [|] u [<-|F];simpl in *;try tauto.
      + exists (𝐓_var x °);split;auto.
      + exists (𝐓_var x);split;auto.
    - intros [|] u;simpl;rewrite map_bimap,in_bimap;
        intros (a&b&<-&Ia&Ib);
        apply (in_map 𝐄_to_𝐓) in Ia;apply (in_map 𝐄_to_𝐓) in Ib.
      + simpl;apply IHe1 in Ia as (va&Ia&->);apply IHe2 in Ib as (vb&Ib&->).
        clear a b; apply in_map_iff in Ia as (a&<-&Ia); apply in_map_iff in Ib as (b&<-&Ib).
        rewrite map_bimap;setoid_rewrite in_bimap.
        exists (𝐄_to_𝐓 (b ⋅ a)%expr);split;simpl;eauto.
      + simpl;apply IHe2 in Ia as (va&Ia&->);apply IHe1 in Ib as (vb&Ib&->).
        clear a b; apply in_map_iff in Ia as (a&<-&Ia); apply in_map_iff in Ib as (b&<-&Ib).
        rewrite map_bimap;setoid_rewrite in_bimap.
        exists (𝐄_to_𝐓 (b ⋅ a)%expr);split;simpl;eauto.
    - intros d u;simpl;rewrite map_bimap,map_bimap,in_bimap;
        setoid_rewrite in_bimap;intros (a&b&<-&Ia&Ib);simpl.
      apply (in_map 𝐄_to_𝐓) in Ia;apply (in_map 𝐄_to_𝐓) in Ib.
        apply IHe1 in Ia as (va&Ia&->);apply IHe2 in Ib as (vb&Ib&->).
        clear a b; apply in_map_iff in Ia as (a&<-&Ia); apply in_map_iff in Ib as (b&<-&Ib).
        exists (𝐄_to_𝐓 (a ∩ b)%expr);split;simpl;eauto.
    - intro d;simpl;repeat rewrite map_app;setoid_rewrite in_app_iff.
      firstorder.
    - simpl;firstorder.
  Qed.

  (** We now prove that for every axiom of the algebra of expressions,
  the images by [𝐄_to_𝐓s] of the left and right side are
  equivalent. Although the proof is quite routine, it involves a large
  case analysis, due partly to the number of axioms we have given for
  expressions, and partly because of the combinatorial nature of the
  equivalence of lists of terms. *)  
  Lemma ax_𝐄_to_𝐓s_incl e f :
    ax e f -> (𝐄_to_𝐓s e) ≡ (𝐄_to_𝐓s f).
  Proof.
    assert (innil : forall A (u : A) , u ∈ [] <-> False )
      by (intros A u;simpl;reflexivity).
    Ltac unravel :=
        (simpl in *;
         match goal with
         | h : 𝐄_to_𝐓 _ = ?x |- _ => rewrite <- h in *;clear x h
         | h : _ = ?x |- _ => rewrite <- h in *;clear x h
         | |- _ /\ _ => split
         | |- _ \/ False => left
         | |- False \/ _ => right
         | |- _ ∈ _ => eassumption
         | |- _ = _ => reflexivity
         end).
    unfold equiv,𝐓s_eq,smaller,𝐓s_inf, 𝐄_to_𝐓s;intro E;repeat split; destruct E; simpl;
      repeat
        match goal with
        | |- context[_ ∈ bimap _ _ _] => setoid_rewrite in_bimap
        | |- context[_ ∈ map _ _] => setoid_rewrite in_map_iff
        | |- context[_ ∈ (_ ++ _)] => setoid_rewrite in_app_iff
        | |- context[ _ ∈ []] => setoid_rewrite innil
        end;try firstorder; try now (repeat (eexists || unravel;simpl);firstorder eauto).
    - rewrite<- H0 in *;clear x H0.
      rewrite<- H in *;clear u H.
      assert (dec_X : decidable_set X) by (now exists eqX).
      apply (in_map 𝐄_to_𝐓),𝐄_to_𝐓_comb in H1; simpl in *.
      setoid_rewrite in_map_iff in H1;destruct H1 as (v&(x&<-&I)&->).
      exists (1 ∩ 𝐄_to_𝐓 x);rewrite 𝐓_conv_one;split;auto.
      repeat (eexists||unravel);simpl;auto.
    - rewrite<- H0 in *;clear x H0.
      rewrite<- H in *;clear u H.
      assert (dec_X : decidable_set X) by (now exists eqX).
      apply (in_map 𝐄_to_𝐓),𝐄_to_𝐓_comb in H1; simpl in *.
      setoid_rewrite in_map_iff in H1;destruct H1 as (v&(x&<-&I)&->).
      exists (1 ∩ 𝐄_to_𝐓 x);rewrite 𝐓_conv_one;split;auto.
      repeat (eexists||unravel);simpl;auto.
  Qed.

  (** With the previous lemma, it is quite easy to show that [𝐄_to_𝐓s]
  is monotone. *)
  Lemma 𝐄_to_𝐓s_inf e f : e ≤ f -> (𝐄_to_𝐓s e) ≤ (𝐄_to_𝐓s f).
  Proof.
    intro E.
    unfold 𝐄_to_𝐓s;induction E;intros u I.
    - firstorder.
    - apply IHE in I as (v&Iv&Lv).
      apply IHE0 in Iv as (w&Iw&Lw).
      exists w;split;eauto.
    - simpl in *;rewrite map_app in *.
      revert I;setoid_rewrite in_app_iff;firstorder.
    - revert I;simpl;repeat rewrite map_bimap.
      setoid_rewrite in_bimap; intros (u1&u2&<-&Iu1&Iu2).
      apply (in_map 𝐄_to_𝐓),IHE in Iu1 as (v1&I1&L1).
      apply (in_map 𝐄_to_𝐓),IHE0 in Iu2  as (v2&I2&L2).
      apply in_map_iff in I1 as (w1&<-&I1).
      apply in_map_iff in I2 as (w2&<-&I2).
      repeat eexists||split;eauto.
      simpl;eauto.
    - revert I;simpl;repeat rewrite map_bimap.
      setoid_rewrite in_bimap; intros (u1&u2&<-&Iu1&Iu2).
      apply (in_map 𝐄_to_𝐓),IHE in Iu1 as (v1&I1&L1).
      apply (in_map 𝐄_to_𝐓),IHE0 in Iu2  as (v2&I2&L2).
      apply in_map_iff in I1 as (w1&<-&I1).
      apply in_map_iff in I2 as (w2&<-&I2).
      repeat eexists||split;eauto.
      simpl;eauto.
    - simpl in *.
      apply 𝐄_to_𝐓_comb in I as (u'&I&->).
      apply IHE in I as (v'&I&L).
      apply 𝐄_to_𝐓_comb in I as (v&I&->).
      exists v;split;auto.
      rewrite -> L,𝐓_conv_idem;auto.
    - destruct H as [H|H];apply ax_𝐄_to_𝐓s_incl in H; apply H in I;auto.
    - simpl in I;tauto.
  Qed.

  (** [𝐄_to_𝐓s] is in fact order preserving. *)
  Theorem 𝐄_to_𝐓s_inf_iff e f : e ≤ f <-> (𝐄_to_𝐓s e) ≤ (𝐄_to_𝐓s f).
  Proof.
    split;[apply 𝐄_to_𝐓s_inf|].
    intros h;rewrite comb_equiv,(comb_equiv f).
    cut (forall l m, (forall t, t ∈ (l++m) -> is_term t = true)
                     ->
                     (forall u : 𝐓 X,
                         u ∈ (map 𝐄_to_𝐓 m) ->
                         exists v : 𝐓 X,
                           v ∈ (map 𝐄_to_𝐓 l) /\ u ≤ v)
                     ->  (⋃ m)  ≤ ⋃ l );
      [intro hyp;apply hyp;auto;intro t;rewrite in_app_iff;intros [I|I];
      eapply is_term_comb;eauto|clear h e f].
    intros m l;revert m;induction l;simpl;intros m T L ;auto.
    rewrite (IHl m).
    -- assert (I: exists v : 𝐓 X, v ∈ (map 𝐄_to_𝐓 m)
                                   /\ 𝐄_to_𝐓 a ≤ v)
        by (now apply L;left).
       assert (Tm : forall t, t ∈ m  -> is_term t = true)
         by (now intros;apply T;rewrite in_app_iff;left).
       assert (Ta : is_term a = true)
         by (now apply T;rewrite in_app_iff;right;left).
       clear L l T IHl.
       induction m;simpl in *;[destruct I as (_&F&_);tauto|].
       destruct I as (v&[<-|I]&L);auto.
       --- transitivity (a0 + a0 + ⋃m);auto.
           apply 𝐓_to_𝐄_inf in L.
           rewrite <- 𝐄_to_𝐓_to_𝐄 in L;auto.
           rewrite <- 𝐄_to_𝐓_to_𝐄 in L;auto.
           eauto.
       --- rewrite <- IHm at 2;auto.
           ---- transitivity (a + a0 + ⋃m); eauto.
                transitivity (a0 + a + ⋃m); eauto.
           ---- exists v;auto.
    -- intro t;rewrite in_app_iff;intros [I|I];apply T,in_app_iff;
       simpl;auto.
    -- now intros u I;apply L;right.
  Qed.
End s.

(** The function [𝐓_to_𝐄] is language preserving. *)
Lemma 𝐓_to_𝐄_lang X (t : 𝐓 X) Σ (σ : 𝕬[X→Σ]) :
  ⟦t⟧σ ≃ ⟦𝐓_to_𝐄 t⟧σ.
Proof.
  induction t;intro w;simpl;rsimpl;autounfold with semantics;rsimpl;try tauto.
  - setoid_rewrite (IHt1 _);setoid_rewrite (IHt2 _);tauto.
  - rewrite (IHt1 _),(IHt2 _);tauto.
Qed.

(** Combining this fact with the soundness result we have for
expressions and the monotonicity of [𝐓_to_𝐄] we get the soundness of
the axiomatization of terms. *)
Corollary 𝐓_inf_incl_lang (X : Set) {D: decidable_set X} (e f : 𝐓 X) :
  e ≤ f -> e ≲ f.
Proof.
  intro E;apply 𝐓_to_𝐄_inf,𝐄_inf_incl_lang in E;auto.
  intros Σ σ;rewrite 𝐓_to_𝐄_lang,𝐓_to_𝐄_lang;intuition.
Qed.

Lemma 𝐄_to_𝐓_lang (X : Set) e :
  is_term e = true -> forall Σ (σ:𝕬[X→Σ]), ⟦e⟧σ ≃ ⟦𝐄_to_𝐓 e⟧σ.
Proof.
  induction e;simpl;try discriminate || rewrite andb_true_iff.
  - intros;rsimpl;intuition.
  - intros;rsimpl;intuition.
  - intros (T1&T2) Σ σ w;rsimpl;autounfold with semantics.
    setoid_rewrite (IHe1 T1 Σ σ _).
    setoid_rewrite (IHe2 T2 Σ σ _).
    intuition.
  - intros (T1&T2) Σ σ w;rsimpl;autounfold with semantics.
    rewrite (IHe1 T1 _ _ _),(IHe2 T2 _ _ _ ); intuition.
  - destruct e;try discriminate;intros T Σ σ.
    rsimpl;reflexivity.
Qed.
    
Lemma 𝐄_to_𝐓s_lang (X : Set) {D: decidable_set X} e :
  forall Σ (σ:𝕬[X→Σ]) w, w ∊ (⟦e⟧σ) <-> exists t, t ∈ (𝐄_to_𝐓s e) /\ w ∊ (⟦t⟧σ).
Proof.
  intros;rewrite (comb_lang e σ w). 
  unfold 𝐄_to_𝐓s;setoid_rewrite in_map_iff;split;
    [intros (t&I&L)|intros (t&(s&E&I)&L)].
  - exists (𝐄_to_𝐓 t);split;eauto;
      apply (𝐄_to_𝐓_lang (is_term_comb _ I));auto.
  -  rewrite<- E in L;
       apply (𝐄_to_𝐓_lang (is_term_comb _ I)) in L;eauto.
Qed.



Close Scope term_scope.
Close Scope expr_scope.
