(* -*- company-coq-initial-fold-state: bullets; -*- *)

(** We define in this module 2-pointed labelled directed graphs, and
some operations on them.*)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export tools.
Require Import finite_functions.

(** * Main definitions *)
(** An edge over vertices of type [A] and labels of type [B] is a
triple, containing its source, its label, and its target. *) 
Definition edge {A B : Set} : Set :=(A * B * A).

(** A graph is a triple, with an input vertex, a list of edges,
    and an output vertex. *)
Definition graph (A B : Set) : Set := A * list (@edge A B) * A.

Section g.

  (** We fix a set of labels. *)
  Variable L : Set.

  Definition input {A} (g : graph A L) : A := fst (fst g).
  Definition output {A} (g : graph A L) : A := snd g.
  Definition edges {A} (g : graph A L) : list edge := snd (fst g).

  Hint Unfold input output edges.

  (** It is often useful to map a function to a list of edges,
  applying it to the source and target of every edge (but not on their
  labels). *)
  Definition map {A B : Set} (f : A -> B) l : list (@edge B L) :=
    List.map (fun (e : edge)  => match e with (i,α,j) => (f i,α,f j) end) l.

  (** This [map] satisfies the usual properties of maps. *)
  Lemma map_id {A : Set} l : map (@id A) l = l.
  Proof.
    unfold map;rewrite <- (map_id l) at 2;apply map_ext.
    intros ((?,?),?);reflexivity.
  Qed.
  
  Lemma in_map {A B : Set} (f : A -> B) l i α j :
    (i,α,j) ∈ (map f l) <-> exists x y, (x,α,y) ∈ l /\ f x = i /\ f y = j.
  Proof.
    unfold map;rewrite in_map_iff;split;
      [intros (((x&β)&y)&h&I);inversion h|intros (x&y&I&<-&<-)];
      repeat eexists;eauto;firstorder subst;auto.
  Qed.

  Lemma map_map {A B C : Set} (g : B -> C) (f : A -> B) l :
    map g (map f l) = map (g ∘ f) l.
  Proof.
    now unfold map;rewrite map_map;apply map_ext;intros ((?&?)&?).
  Qed.

  Lemma map_app  {A B : Set} (f : A -> B) l m :
    map f (l++m) = map f l ++ map f m.
  Proof. now unfold map;rewrite map_app. Qed.

  (** [graph_map] applies a function on the vertices of a graph. *)
  Definition graph_map {X Y: Set} (φ : X -> Y) g : graph Y L :=
    (φ (input g),map φ (edges g),φ (output g)).

  Lemma input_graph_map {X Y: Set} (φ : X -> Y) g :
    input (graph_map φ g) = φ (input g).
  Proof. reflexivity. Qed.

  Lemma output_graph_map {X Y: Set} (φ : X -> Y) g :
    output (graph_map φ g) = φ (output g).
  Proof. reflexivity. Qed.

  Lemma edges_graph_map {X Y: Set} (φ : X -> Y) g :
    edges (graph_map φ g) = map φ (edges g).
  Proof. reflexivity. Qed.

  Lemma graph_map_map {X Y Z : Set} (φ : X -> Y) (ψ : Y -> Z) g :
    graph_map ψ (graph_map φ g) = graph_map (ψ ∘ φ) g.
  Proof.
    destruct g as ((ι&e)&o);unfold graph_map;autounfold;simpl.
    rewrite map_map;reflexivity.
  Qed.

  Lemma graph_map_id {X: Set} (g : graph X L) :
    graph_map id g = g.
  Proof.
    destruct g as ((ι&e)&o);unfold graph_map;autounfold;simpl.
    rewrite map_id;reflexivity.
  Qed.
  
  (** We can extract from a graph its list of vertices, by taking the
  input vertex, the output vertex and every vertex appear either as
  the source or the target of some edge.*)
  Global Instance graph_vertices {A} : Support graph A L :=
    fun g =>
      (input g)::(output g)
               ::(flat_map (fun e => [fst (fst e);snd e]) (edges g)).

  Lemma in_graph_vertices {A : Set} (i : A) (g : graph A L) :
    i ∈ ⌊ g ⌋ <-> input g=i \/ output g=i
                \/ (exists α j, (i,α,j) ∈ (edges g))
                \/ (exists α j, (j,α,i) ∈ (edges g)).
  Proof.
    unfold graph_vertices;simpl;rewrite in_flat_map;split.
    - intros [I|[I|(((?&?)&?)&I&[<-|[<-|F]])]];simpl in *;try tauto.
      -- right;right;left;eauto.
      -- right;right;right;eauto.
    - intros [I|[I|[(?&?&I)|(?&?&I)]]];try tauto;right;right;eexists;
        (split;[eauto|simpl;tauto]).
  Qed.

  Lemma edges_vertices {A : Set} i a j (g : graph A L) :
    (i,a,j) ∈ edges g -> i ∈ ⌊g⌋ /\ j ∈ ⌊g⌋.
  Proof.
    destruct g as ((x&e)&y).
    simpl;unfold edges,input,output,fst,snd.
    repeat rewrite in_flat_map.
    intros Ie;simpl;split;right;right; exists (i,a,j);tauto.
  Qed.

  (** Set of labels of a graph. *)
  Global Instance labels {A : Set} : Alphabet (graph A) L :=
    fun g => List.map (snd∘fst) (edges g).

  (** The set of labels is invariant under morphism. *)
  Lemma variables_graph_map {A B : Set} (f : A -> B) (g : graph A L) :
    𝒱 (graph_map f g) = 𝒱 g.
  Proof.
    destruct g as ((?&e)&?).
    unfold graph_map,𝒱,labels,edges,snd,fst.
    unfold map;rewrite List.map_map;apply map_ext.
    intros ((?&x)&?);simpl.
    reflexivity.
  Qed.
  
  (** If two functions [φ] and [ψ] coincide on the vertices of a graph
  [g], then the images of [g] by [φ] and [ψ] are equal. *)
  Lemma graph_map_ext_in {X Y : Set} (φ ψ : X -> Y) g :
    (forall x, x ∈ ⌊g⌋ -> φ x = ψ x) -> graph_map φ g = graph_map ψ g.
  Proof.
    intro E;destruct g as ((ι&e)&o);unfold graph_map;autounfold;simpl.
    f_equal;[f_equal|].
    - rewrite E;[reflexivity|now left].
    - unfold map;apply map_ext_in;intros ((i,α),j) I;f_equal;[f_equal|].
      -- apply E,in_graph_vertices;right;right;eauto.
      -- apply E,in_graph_vertices;right;right;eauto.
    - rewrite E;[reflexivity|now right;left].
  Qed.

  (** We can obtain the vertices of the image of [g] by [φ] by simply
      applying [φ] to the vertices of [g]. *)
  Lemma graph_vertices_map {X Y: Set} g (φ : X -> Y) :
    ⌊graph_map φ g⌋ = List.map φ ⌊g⌋.
  Proof.
    unfold graph_vertices,graph_map;autounfold;unfold map,support;simpl.
    f_equal;f_equal.
    rewrite flat_map_concat_map,flat_map_concat_map,concat_map,
    List.map_map,List.map_map;simpl.
    induction (snd(fst g)) as [|((i,α),j) l IH];simpl;auto.
    rewrite IH;reflexivity.
  Qed.

  (** A map is called internal from [g] to [h] whenever every vertex
  in [g] is sent to a vertex in [h]. *)
  Definition internal_map {X Y: Set} (φ : X -> Y) g h :=
    forall x, x ∈ ⌊g⌋ -> (φ x) ∈ ⌊h⌋.

  (** A map [φ] is always internal from [g] to [graph_map φ g]. *)
  Lemma internal_map_graph_map {X Y: Set} (φ : X -> Y) g :
    internal_map φ g (graph_map φ g).
  Proof.
    unfold internal_map;rewrite graph_vertices_map.
    intros x I;apply List.in_map,I.
  Qed.
  
  (** [φ] is an [A]-premorphism from [g] to [h] if for every edge
  [(i,α,j)] in [g], either
  - [α] is in [A] and [φ i] is equal to [φ j],
  - there is an edge [(φ i,α,φ j)] in [h]. *)
  Definition is_premorphism {X Y} A φ (g : graph X L) (h : graph Y L):=
    forall i α j, (i,α,j) ∈ (edges g) ->
             (α ∈ A /\ φ i = φ j) \/ (φ i,α,φ j) ∈ (edges h).

  Notation " A ⊨ g -{ φ }⇀ h" := (is_premorphism A φ g h)
                                   (at level 80).

  (** If there is an [A]-premorphism from [g] to [h], then the labels
  of [g] are either labels of [h] or chosen from [A]. *)
  Lemma variables_premorphism {X Y : Set} A (φ : X -> Y) g h :
    A ⊨ g -{φ}⇀ h -> 𝒱 g ⊆ 𝒱 h ++ A.
  Proof.
    destruct g as ((?&e)&?);destruct h as ((?&e')&?);
      unfold is_premorphism,𝒱,labels,edges,Basics.compose;simpl.
    intros h a;rewrite in_app_iff;repeat rewrite in_map_iff.
    intros (((i,α),j)&<-&Ie);apply h in Ie as [(I&_)|Ie].
    - now right.
    - left;exists (φ i,α,φ j);split;tauto||reflexivity.
  Qed.
  
  (** Premorphisms are closed under composition. *)
  Lemma is_premorphism_compose {X Y Z : Set} (φ1 : X -> Y) (φ2 : Y -> Z)
        g1 g2 g3 (A B : list L) :
    A ⊨ g1 -{φ1}⇀ g2 -> B ⊨ g2 -{φ2}⇀ g3 -> A++B ⊨ g1 -{φ2∘φ1}⇀ g3.
  Proof.
    intros h1 h2 i α j I;apply h1 in I as [(I&E)|I].
    - left;split.
      -- apply in_app_iff;tauto.
      -- autounfold;rewrite E;reflexivity.
    - apply h2 in I as [(I&E)|I];auto.
      left;split;auto.
      apply in_app_iff;tauto.
  Qed.

  (** If [A] is contained in [B], every [A]-premorphism is also a
  [B]-premorphism. *)  
  Lemma is_premorphism_incl {X Y : Set} A B g h (φ : X -> Y) :
    A ⊆ B ->  A ⊨ g -{ φ }⇀ h -> B ⊨ g -{ φ }⇀ h.
  Proof.
    destruct g as ((ι,e),o);destruct h as ((ι',e'),o').
    intros I he;intros i α j Ie;autounfold in *;simpl in *.
    apply he in Ie as [(Ie&E)|Ie];[apply I in Ie|];tauto.
  Qed.

  (** We can compose [A]-premorphisms and obtain another
  [A]-premorphism. *)
  Lemma is_premorphism_compose_weak {X Y Z : Set} φ1 φ2
        (g1 : graph X L) (g2: graph Y L) (g3: graph Z L) A :
    A ⊨ g1 -{φ1}⇀ g2 -> A ⊨ g2 -{φ2}⇀ g3 -> A ⊨ g1 -{φ2∘φ1}⇀ g3.
  Proof.
    intros h1 h2;eapply is_premorphism_incl,is_premorphism_compose;
      eauto;intro x;rewrite in_app_iff;tauto.
  Qed.

  (** We we choose [A] to be the empty list, then the third condition
  can be reformulated as the containment of [map φ (edges g)] inside
  [edges h]. *)  
  Lemma proper_morphism_diff {X Y : Set} (φ : X -> Y) g h :
    [] ⊨ g -{φ}⇀ h <-> (map φ (edges g)) ⊆ (edges h).
  Proof.
    unfold is_premorphism,incl;simpl;split;intro hyp. 
    - intros ((i,α),j) I;apply in_map in I as (?&?&I&<-&<-).
      apply hyp in I as [(F&_)|I];tauto.
    - intros i α j I;right;apply hyp,in_map;firstorder.
  Qed.
  
  (** [φ] is an [A]-morphism from [g] to [h] if 
      - the image of the input of [g] is the input of [h],
      - the image of the output of [g] is the output of [h],
      - it is an [A]-premorphism;
      - it is internal from [g] to [h].
   *)
  Definition is_morphism {X Y} A φ (g : graph X L) (h : graph Y L) :=
    φ (input g) = input h /\ φ (output g) = output h
    /\ A ⊨ g -{φ}⇀ h /\ internal_map φ g h.

  Notation " A ⊨ g -{ φ }→ h " := (is_morphism A φ g h) (at level 80).

  (** Because a morphism is in particular a premorphism, we have the
  same containment between sets of labels. *)
  Lemma variables_morphism {X Y : Set} A (φ : X -> Y) g h :
    A ⊨ g -{φ}→ h -> 𝒱 g ⊆ 𝒱 h ++ A.
  Proof. intros (_&_&M&_);eapply variables_premorphism;eauto. Qed.
  
  (** If [A] is contained in [B], every [A]-morphism is also a
  [B]-morphism. *)  
  Lemma is_morphism_incl {V1 V2 : Set} A B g h (φ : V1 -> V2) :
    A ⊆ B ->  A ⊨ g -{φ}→ h -> B ⊨ g -{φ}→ h.
  Proof.
    destruct g as ((ι,e),o);destruct h as ((ι',e'),o').
    intros I (hι&ho&he&Int);split;[|split;[|split]];
      unfold input,output,edges in *;simpl in *;auto.
    eapply is_premorphism_incl;eauto.
  Qed.
  
  (** The identity is an ø-morphism from [g] to itself. *)
  Lemma is_morphism_id {V : Set} (g : graph V L) : [] ⊨ g -{id}→ g.
  Proof. unfold is_morphism,is_premorphism;firstorder. Qed.

  (** We can compose morphisms. *)
  Lemma is_morphism_compose {X Y Z} φ1 φ2
        (g1 : graph X L) (g2: graph Y L) (g3: graph Z L) A B :
    A ⊨ g1 -{φ1}→ g2 -> B ⊨ g2 -{φ2}→ g3 -> A++B ⊨ g1 -{φ2∘φ1}→ g3.
  Proof.
    intros (E1&E2&h1&Int1) (E3&E4&h2&Int2);split;[|split;[|split]].
    - autounfold in *;rewrite E1,E3;reflexivity.
    - autounfold in *;rewrite E2,E4;reflexivity.
    - eapply is_premorphism_compose;eauto.
    - intros x I;apply Int2,Int1,I.
  Qed.

  (** We can compose [A]-morphisms. With the lemmas [is_morphism_id]
  and [is_morphism_incl], this means that graphs and [A]-morphisms
  form a category. *)
  Lemma is_morphism_compose_weak {X Y Z} φ1 φ2
        (g1 : graph X L) (g2: graph Y L) (g3: graph Z L) A :
    A ⊨ g1 -{φ1}→ g2 -> A ⊨ g2 -{φ2}→ g3 -> A ⊨ g1 -{φ2∘φ1}→ g3.
  Proof.
    intros (hι&ho&he&Int)(hι'&ho'&he'&Int');split;[|split;[|split]];
      eauto using is_premorphism_compose_weak; autounfold in *.
    - rewrite hι,hι';reflexivity.
    - rewrite ho,ho';reflexivity.
    - intros x I;apply Int',Int,I.
  Qed.

  (** [φ] is an ø-morphism from [g] to [graph_map φ g]. *)
  Lemma graph_map_morphism {X Y: Set} (φ : X -> Y) g :
    [] ⊨ g-{φ}→(graph_map φ g).
  Proof.
    split;[|split;[|split]];autounfold;simpl;auto.
    - unfold graph_map;intros i α j;autounfold;simpl;auto.
      intro I;right;apply in_map;eauto.
    - apply internal_map_graph_map.
  Qed.
  
  (** We can use [A]-morphisms to define a preorder on graphs. *)
  Definition hom_order {V} A (g h : graph V L) := exists φ,  A ⊨ h -{φ}→ g.

  Notation " A ⊨ g ⊲ h " := (hom_order A g h) (at level 80).
  
  Global Instance hom_order_PreOrder {V:Set} A :
    PreOrder (@hom_order V A).
  Proof.
    split.
    - intro g;exists (fun x => x);split;[|split;[|split]];auto.
      + intros i α j I;auto.
      + intros x I;tauto.
    - intros g1 g2 g3 (φ&hφ) (ψ&hψ).
      eexists;eapply is_morphism_compose_weak;eauto.
  Qed.

  (** Containment between sets of labels. *)
  Lemma variables_hom_order {X : Set} A (g h : graph X L) :
    A ⊨ g ⊲ h -> 𝒱 h ⊆ 𝒱 g ++ A.
  Proof. intros (φ&M);eapply variables_morphism;eauto. Qed.


  (** The ordering induced by [A]-morphisms is included in the
  ordering induced by [B]-morphisms. *)
  Lemma hom_order_incl {V : Set} A B (g h : graph V L) :
    A ⊆ B -> A ⊨ g ⊲ h -> B ⊨ g ⊲ h.
  Proof. intros I (φ,hφ);exists φ;eapply is_morphism_incl;eauto. Qed.


  (** If two functions coincide on the vertices of a graph [g], then
  one is an [A]-morphism from [g] to [h] if and only if the other
  is. *)
  Lemma morphism_ext_vertices {X Y: Set} (φ ψ : X -> Y) A g h :
    (forall i, i ∈ ⌊ g ⌋ -> φ i = ψ i) -> A ⊨ g -{φ}→ h <-> A ⊨ g -{ψ}→ h.
  Proof.
    revert φ ψ;cut (forall φ ψ,
                       (forall i, i ∈ ⌊g⌋ -> φ i = ψ i)
                       -> A ⊨ g -{φ}→ h -> A ⊨ g -{ψ}→ h).
    - intros hyp φ ψ E;split;intro M;eapply hyp;try apply M;auto.
      intros;rewrite E;auto.
    - intros φ ψ E (M1&M2&M3&M4);split;[|split;[|split]].
      -- rewrite <-E,M1;[|left];auto.
      -- rewrite <-E,M2;[|right;left];auto.
      -- intros i α j I;destruct (M3 _ _ _ I) as [(I'&E')|I'].
         --- left;split;auto;rewrite <-E,E',E;auto;
               apply in_graph_vertices;right;right;eauto.
         --- right;rewrite<-E,<-E;auto;
               apply in_graph_vertices;right;right;eauto.
      -- intros x I;rewrite <- (E _ I);apply M4,I.
  Qed.

  (** if [ψ] is the inverse of [φ] on the vertices of [g], then [ψ]
  is an ø-morphism from the image of [g] by [φ] to [g]. *)
  Lemma inverse_graph_map_morphism {X Y : Set} g (φ : X -> Y) ψ :
    inverse φ ψ ⌊g⌋ -> [] ⊨ (graph_map φ g) -{ψ}→ g.
  Proof.
    intros hψ; replace g with (graph_map (ψ ∘ φ) g) at 2.
    -- rewrite<-(graph_map_map φ ψ);apply graph_map_morphism.
    -- rewrite <- graph_map_id;apply graph_map_ext_in;auto.
  Qed.

  (** If a graph [g] is greater than the point graph, then its
  variables are all contained in [A]. *)
  Lemma contractible_graph A g :
    𝒱 g ⊆ A <-> A ⊨ (0, [], 0) ⊲ g.
  Proof.
    destruct g as ((?&E)&?);split.
    - intro C;exists (fun _ => 0);split;[|split;[|split]];auto.
      + intros ? ? ? I;left;split;auto;apply C,in_map_iff.
        eexists;split;eauto;auto.
      + intros x _;now left.
    - intro h;apply variables_hom_order in h;simpl in h;tauto.
  Qed.

  (** We define the path ordering on a graph. *)
  Inductive path {V}(g : graph V L) : V -> V -> Prop :=
  | path_re i : path g i i
  | path_trans j i k : path g i j -> path g j k -> path g i k
  | path_edge i j α : (i,α,j) ∈ (edges g) -> path g i j.

  Notation " u -[ G ]→ v " := (path G u v) (at level 80).

  (** This is a preorder. *)
  Global Instance path_PreOrder {V} (g : graph V L) : PreOrder (path g).
  Proof.
    split.
    - intro i;apply path_re.
    - intros i j k;apply path_trans.
  Qed.

  (** If [φ] is an [A]-premorphism from [g] to [h], then it preserves
  the path ordering. *)
  Lemma morphism_is_order_preserving {V1 V2 : Set} A (φ :V1 -> V2) g h:
    A ⊨ g -{φ}⇀ h -> forall i j, i -[g]→j ->  φ i -[h]→ φ j.
  Proof.
    intros hyp i j G.
    induction G;auto.
    - reflexivity.
    - etransitivity;eauto.
    - apply hyp in H as [(I&->)|I].
      -- reflexivity.
      -- eapply path_edge;eauto.
  Qed.

  (** If [y] is reachable from [x] in [g], then [φ y] is reachable
      from [φ x] in the image of [g] by [φ]. *)
  Lemma graph_map_path {X Y : Set} (φ : X -> Y) (g : graph X L) x y :
    x -[g]→ y -> φ x -[graph_map φ g]→ φ y.
  Proof.
    intro P;eapply morphism_is_order_preserving;
      [apply graph_map_morphism|];auto.
  Qed.

  (** The reachability order is non-trivial only between vertices. *)
  Lemma path_non_trivial_only_for_vertices {X:Set} g (i j : X) :
    i-[g]→j -> i = j \/ [i;j] ⊆ ⌊g⌋.
  Proof.
    intro P;induction P;auto.
    - destruct IHP1 as [->|IH1];destruct IHP2 as [->|IH2];auto;right.
      intros ? [<-|[<-|F]];[| |simpl in F;tauto].
      --- apply IH1;now left.
      --- apply IH2;now right;left.
    - right;intros ? [<-|[<-|F]];[| |simpl in F;tauto];
        apply in_graph_vertices;right;right;eauto.
  Qed.

  (** Equivalently, if [i] and [j] are different elements such that
      one is reachable from the other, then [i] is a vertex (and by
      symmetry, so is [j].  *)
  Lemma path_in_vertices {X:Set} g (i j : X) :
    i <> j -> i-[g]→j \/ j-[g]→i -> i ∈ ⌊g⌋.
  Proof.
    intros N [I|I];
      apply path_non_trivial_only_for_vertices in I as [F|I];
      try (rewrite F in *;tauto);apply I;simpl;auto.
  Qed.

  (** A third equivalent formulation of the same lemma. *)
  Lemma path_graph_vertices {X : Set} g (i j : X) :
    i-[g]→ j -> i ∈ ⌊g⌋ <-> j ∈ ⌊g⌋.
  Proof.
    intro P;destruct (path_non_trivial_only_for_vertices P) as [->|v].
    - tauto.
    - assert (i ∈ [i;j] /\ j ∈ [i;j]) as (Ii&Ij) by (simpl;tauto).
      apply v in Ii;apply v in Ij;tauto.
  Qed.

  (** If [ψ] is the inverse of [φ] and [y] is reachable from [x] in
  the image of [g] by [φ], then [ψ y] was reachable from [ψ x] in
  [g]. *)
  Lemma injective_graph_map_path {X Y : Set} (φ : X -> Y) ψ g x y :
    inverse φ ψ ⌊g⌋ -> x -[graph_map φ g]→ y -> ψ x -[g]→ ψ y.
  Proof.
    intros hψ P.
    destruct (path_non_trivial_only_for_vertices P) as [->|I];auto.
    - reflexivity.
    - rewrite graph_vertices_map in I.
      assert (hi: x ∈ (List.map φ ⌊g⌋))
        by (apply I;now left).
      assert (hj: y ∈ (List.map φ ⌊g⌋))
        by (apply I;now right;left).
      apply in_map_iff in hi as (i'&<-&hi).
      apply in_map_iff in hj as (j'&<-&hj);f_equal.
      destruct (inverse_graph_map_morphism hψ) as (_&_&F&_).
      eapply morphism_is_order_preserving;eauto.
  Qed.

  (** The relation [hom_eq] relates two graphs if there is a pair
  of ø-morphisms, going both directions between the two graphs. Note
  that despite the name, it does not mean the two graphs are
  isomorphic: indeed this pair of morphisms need not be inverses. *)
  Definition hom_eq {X : Set} (g h : graph X L) :=
    ([] ⊨ g ⊲ h) /\ ([] ⊨ h ⊲ g).

  Infix "⋈" := hom_eq (at level 80).

  (** It is an equivalence relation. *)
  Global Instance hom_eq_Equivalence {X : Set} :
    Equivalence (fun g h : graph X L => g ⋈ h).
  Proof.
    split.
    - split;reflexivity. 
    - intros g h (h1&h2);split;auto.
    - intros f g h (h1&h2) (h3&h4);split;etransitivity;eauto.
  Qed.

  (** Equivalence of sets of labels. *)
  Lemma variables_hom_eq {X : Set} (g h : graph X L) :
    g ⋈ h -> 𝒱 g ≈ 𝒱 h.
  Proof.
    intros (h1&h2).
    eapply variables_hom_order in h1;rewrite app_nil_r in h1.
    eapply variables_hom_order in h2;rewrite app_nil_r in h2.
    apply incl_PartialOrder;split;tauto.
  Qed.

  (** If [φ] is injective, then [g] and its [φ]-image are
  equivalent. *)
  Lemma injective_map_hom_eq {X : Set} g (φ : X -> X) :
    injective φ ⌊g⌋ -> g ⋈ (graph_map φ g).
  Proof.
    intros (ψ & hψ);split;[exists ψ|exists φ];auto.
    - apply inverse_graph_map_morphism,hψ.
    - apply graph_map_morphism.
  Qed.

  (** We call a graph connected when any node is reachable from its
  input and co-reachable from the output. *)
  Definition connected {X} (g : graph X L) :=
    forall i, i ∈ ⌊g⌋ -> input g -[g]→ i /\ i-[g]→output g.
  
  (** If [g] and [h] are isomorphic (i.e. there are ø-morphisms from
  [g] to [h] and [h] to [g] that are inverses) then if one of then is
  connected so is the other. *)
  Lemma injective_morphism_connected {X Y:Set} g h
        (φ : X -> Y) (ψ : Y -> X) :
    inverse φ ψ ⌊g⌋ -> inverse ψ φ ⌊h⌋ ->
    [] ⊨ g -{φ}→ h ->  [] ⊨ h-{ψ}→g -> connected g -> connected h.
  Proof.
    intros inv1 inv2 (M1&M2&M3&In1) (M4&M5&M6&In2) C x I.
    rewrite <- (inv2 x I).
    rewrite <- (inv2 (input h));[|now left].
    rewrite <- (inv2 (output h));[|now right;left].
    split;eapply morphism_is_order_preserving;eauto;
      [rewrite M4|rewrite M5];apply C;apply In2,I.
  Qed.

  (** If [φ] is injective and [g] is connected then so is the image of
  [g]. *)
  Lemma graph_map_injective_connected {X Y: Set} (φ : X -> Y) g:
    injective φ ⌊g⌋ -> connected g -> connected (graph_map φ g).
  Proof.
    intros (ψ&inj) C;eapply injective_morphism_connected in C;eauto.
    - intros x I;rewrite graph_vertices_map in I;
        apply in_map_iff in I as (y&<-&I);rewrite inj;auto.
    - apply graph_map_morphism.
    - replace g with (graph_map (fun x => ψ (φ x)) g) at 2.
      -- rewrite<-(graph_map_map φ ψ);apply graph_map_morphism.
      -- rewrite <- graph_map_id;apply graph_map_ext_in;auto.
  Qed.

  (** The union of two graphs is a graph such that:
      - its input is the input of the first graph;
      - its output is that of the second graph;
      - its edges or the union of the edges of both graphs. 

   The union of [g] and [h] is written [g⋄h]. *)
  Definition graph_union {X} (g h : graph X L) : graph X L :=
    (input g,edges g++edges h,output h).

  Infix " ⋄ " := graph_union (at level 40).
  
  Lemma input_graph_union {X} (g h : graph X L):
    input (g ⋄ h) = input g.
  Proof. unfold graph_union;autounfold;simpl;auto. Qed.

  Lemma output_graph_union {X} (g h : graph X L):
    output (g ⋄ h) = output h.
  Proof. unfold graph_union;autounfold;simpl;auto. Qed.

  Lemma edges_graph_union {X} (g h : graph X L):
    edges (g ⋄ h) = edges g ++ edges h.
  Proof. unfold graph_union;autounfold;simpl;auto. Qed.

  (** The labels of [g⋄h] are obtained by concatenating the labels of
  [g] and those of [h]. *)
  Lemma variables_graph_union {X} (g h : graph X L):
    𝒱 (g ⋄ h) = 𝒱 g ++ 𝒱 h.
  Proof.
    unfold 𝒱,labels.
    rewrite edges_graph_union.
    apply List.map_app.
  Qed.
  
  (** The vertices of [g⋄h] are contained in the union of the vertices
      of [g] and those of [h]. In general this inclusion is strict,
      as the output of [g] and the input of [h] might be missing. *)
  Lemma vertices_graph_union {X} (g h : graph X L) : ⌊g ⋄ h⌋ ⊆ ⌊g⌋++⌊h⌋.
  Proof.
    destruct g as ((ig&eg)&og); destruct h as ((ih&eh)&oh).
    unfold graph_union,graph_vertices in *;
      autounfold in *;simpl in *.
    intros x;simpl;rewrite in_flat_map,in_app_iff,in_flat_map;simpl;
      rewrite in_flat_map;setoid_rewrite in_app_iff.
    intros [<-|[<-|(((?&?)&?)&[I|I]&E)]];try tauto.
    - right;right;left.
      destruct E as [E|[E|E]];simpl in E;try rewrite E in *;eauto.
    - right;right;right;right;right.
      destruct E as [E|[E|E]];simpl in E;try rewrite E in *;eauto.
      -- eexists;split;[eauto|simpl;auto].
      -- eexists;split;[eauto|simpl;auto].
      -- tauto.
  Qed.

  (** [⋄] commutes with [graph_map]. *)
  Lemma graph_map_union {V Y : Set} (φ : V -> Y) (g h : graph V L) :
    graph_map φ (g ⋄ h) = (graph_map φ g) ⋄ (graph_map φ h).
  Proof.
    unfold graph_map.
    rewrite edges_graph_union,input_graph_union,output_graph_union.
    unfold graph_union;autounfold;simpl;f_equal;f_equal.
    apply map_app.
  Qed.

  (** [⋄] is associative. *)
  Lemma graph_union_assoc {V : Set} (f g h : graph V L) :
    f ⋄ (g ⋄ h) = (f ⋄ g) ⋄ h.
  Proof.
    destruct f as ((fi&fe)&fo);destruct g as ((gi&ge)&go);
      destruct h as ((hi&he)&ho);unfold graph_union;autounfold;simpl.
    rewrite app_assoc;reflexivity.
  Qed.
  
  (** If the output of [g] is the input of [h], and if this node is the
  only vertex appearing in both graphs, then [g ⋄ h] is the sequential 
  product of [g] and [h]. *)
  Definition good_for_seq {X} (g h : graph X L) :=
    output g = input h /\ (forall x, x ∈ ⌊g⌋ -> x ∈ ⌊h⌋ -> x = input h).

  (** The identity is a premorphism from both [g] and [h] to their
  sequential product. *) 
  Lemma seq_graph_union_decomp {X} (g h : graph X L):
    good_for_seq g h ->
    id (input g) = input (g ⋄ h)
    /\ id (input h) = output g
    /\ [] ⊨ g -{id}⇀ (g ⋄ h)
    /\ [] ⊨ h -{id}⇀ (g ⋄ h).
  Proof.
    unfold graph_union, good_for_seq;autounfold;simpl;intros (->&N).
    split;[|split;[|split]];simpl;auto.
    - apply proper_morphism_diff;autounfold;simpl;
        rewrite map_id;intro;rewrite in_app_iff;tauto.
    - apply proper_morphism_diff;autounfold;simpl;
        rewrite map_id;intro;rewrite in_app_iff;tauto.
  Qed.

  (** If [g] is connected, then the set vertices of the sequential
      product of [g] and [h] is the union of the vertices of [g] and
      those of [h]. *)
  Lemma seq_vertices_connected_graph_union {X} (g h : graph X L) :
    connected g -> good_for_seq g h -> ⌊g ⋄ h⌋ ≈⌊g⌋++⌊h⌋.
  Proof.
    intros C G;apply incl_PartialOrder;split;
      [apply vertices_graph_union|].
    intros x I;apply in_app_iff in I as [I|I].
    - apply in_graph_vertices in I as [<-|[<-|[(?&?&I)|(?&?&I)]]].
      -- now left.
      -- pose proof (C (output g)) as (I&_);[now right;left|].
         eapply morphism_is_order_preserving in I;
           [|apply (seq_graph_union_decomp G)].
         apply path_non_trivial_only_for_vertices in I as [E|I].
         --- unfold id in E;rewrite <- E; now left.
         --- apply I;now right;left.
      -- apply in_graph_vertices;
           rewrite input_graph_union,edges_graph_union.
         setoid_rewrite in_app_iff.
         right;right;left;eexists;eexists;left;eauto.
      -- apply in_graph_vertices;
           rewrite input_graph_union,edges_graph_union.
         setoid_rewrite in_app_iff.
         right;right;right;eexists;eexists;left;eauto.
    - apply in_graph_vertices in I as [<-|[<-|[(?&?&I)|(?&?&I)]]].
      -- pose proof G as (<-&_).
         pose proof (C (output g)) as (I&_);[now right;left|].
         eapply morphism_is_order_preserving in I;
           [|apply (seq_graph_union_decomp G)].
         apply path_non_trivial_only_for_vertices in I as [E|I].
         --- unfold id in E;rewrite <- E; now left.
         --- apply I;now right;left.
      -- now right;left.
      -- apply in_graph_vertices;
           rewrite input_graph_union,edges_graph_union.
         setoid_rewrite in_app_iff.
         right;right;left;eexists;eexists;right;eauto.
      -- apply in_graph_vertices;
           rewrite input_graph_union,edges_graph_union.
         setoid_rewrite in_app_iff.
         right;right;right;eexists;eexists;right;eauto.
  Qed.
  
  (** If [g] and [h] are connected, so is their sequential product. *)
  Lemma seq_graph_union_connected {X} (g h : graph X L) :
    good_for_seq g h -> connected g -> connected h -> connected (g ⋄ h).
  Proof.
    intros G C1 C2 x I.
    apply vertices_graph_union,in_app_iff in I as [I|I];auto.
    - apply C1 in I as (P1&P2).
      pose proof (seq_graph_union_decomp G) as (_&_&P1'&P2').
      rewrite input_graph_union,output_graph_union;split;
        [|etransitivity].
      -- apply (morphism_is_order_preserving P1') in P1;auto.
      -- apply (morphism_is_order_preserving P1') in P2;eauto.
      -- destruct G as (->&G).
         replace (output h) with (id (output h)) by reflexivity.
         apply (morphism_is_order_preserving P2').
         apply C2;now left.
    - apply C2 in I as (P1&P2).
      pose proof (seq_graph_union_decomp G) as (_&_&P1'&P2').
      rewrite input_graph_union,output_graph_union;split;
        [etransitivity|].
      -- replace (input g) with (id (input g)) by reflexivity.
         apply (morphism_is_order_preserving P1').
         apply C1;now left.
      -- destruct G as (->&G).
         apply (morphism_is_order_preserving P2') in P1;eauto.
      -- apply (morphism_is_order_preserving P2') in P2;eauto.
  Qed.

  (** If [g] and [h] share the same input, the same output, and
  otherwise have distinct vertices then [g⋄h] is their parallel
  product. *)
  Definition good_for_par {X} (g h : graph X L) :=
    input g = input h 
    /\ output g = output h 
    /\ (forall x, x ∈ ⌊g⌋ -> x ∈ ⌊h⌋ -> x = input h \/ x = output h).

  (** The vertices of the parallel product is the union of the
  vertices. *)
  Lemma par_vertices_graph_union {X : Set} (g h : graph X L) :
    good_for_par g h -> ⌊g ⋄ h⌋≈⌊g⌋++⌊h⌋.
  Proof.
    intros G;
      apply incl_PartialOrder;split;[apply vertices_graph_union|].
    destruct G as (E1&E2&G); intros x I;apply in_app_iff in I as [I|I];
      apply in_graph_vertices in I as [<-|[<-|[(?&?&I)|(?&?&I)]]].
    - now left.
    - now rewrite E2;right;left.
    - apply in_graph_vertices;right;right;rewrite edges_graph_union;
        setoid_rewrite in_app_iff;eauto.
    - apply in_graph_vertices;right;right;rewrite edges_graph_union;
        setoid_rewrite in_app_iff;eauto.
    - now rewrite <- E1;left.
    - now right;left.
    - apply in_graph_vertices;right;right;rewrite edges_graph_union;
        setoid_rewrite in_app_iff;eauto.
    - apply in_graph_vertices;right;right;rewrite edges_graph_union;
        setoid_rewrite in_app_iff;eauto.
  Qed.

  (** The identity is a morphism from both [g] and [h] to their
  parallel product. *)
  Lemma par_graph_union_decomp {X : Set} (g h : graph X L) :
    good_for_par g h -> [] ⊨ g -{id}→ (g ⋄ h) /\ [] ⊨ h -{id}→ (g ⋄ h).
  Proof.
    intro G;pose proof (par_vertices_graph_union G) as Int.
    destruct G as (E1&E2&G);split;(split;[|split;[|split]]);simpl;auto.
    - intros i α j I;rewrite edges_graph_union,in_app_iff;tauto.
    - intros x I;apply Int,in_app_iff;auto.
    - intros i α j I;rewrite edges_graph_union,in_app_iff;tauto.
    - intros x I;apply Int,in_app_iff;auto.
  Qed.
  
  Lemma par_graph_union_decomp_left {X : Set} (g h : graph X L) :
    good_for_par g h -> [] ⊨ g -{id}→ (g ⋄ h).
  Proof. intro G;apply par_graph_union_decomp in G as (G&_);auto. Qed.
  
  Lemma par_graph_union_decomp_right {X : Set} (g h : graph X L) :
    good_for_par g h -> [] ⊨ h -{id}→ (g ⋄ h).
  Proof. intro G;apply par_graph_union_decomp in G as (_&G);auto. Qed.

  (** The parallel product of two connected graphs is connected. *)
  Lemma par_graph_union_connected {X : Set} (g h : graph X L) :
    good_for_par g h -> connected g -> connected h -> connected (g ⋄ h).
  Proof.
    intros G C1 C2 x I;
      apply par_vertices_graph_union,in_app_iff in I as[I|I];auto.
    -- apply C1 in I as (I1&I2);
         destruct (par_graph_union_decomp G) as ((_&_&P&_)&_);split.
       --- apply (morphism_is_order_preserving P) in I1;auto.
       --- apply (morphism_is_order_preserving P) in I2;
             destruct G as (_&E&_);rewrite E in I2;auto.
    -- apply C2 in I as (I1&I2);
         destruct (par_graph_union_decomp G) as (_&(_&_&P&_));split.
       --- apply (morphism_is_order_preserving P) in I1;auto.
           destruct G as (E&_&_);rewrite <-E in I1;auto.
       --- apply (morphism_is_order_preserving P) in I2;auto.
  Qed.

  (** A graph is acyclic if its reachability relation is a partial
  order. *)  
  Definition acyclic {V : Set} (g: graph V L):=
    PartialOrder eq (path g).

  (** We call a graph [good] if it is both connected and acyclic. *)
  Definition good {V : Set} (g: graph V L):= connected g /\ acyclic g.

  (** The image of an acyclic graph by an injective function is
  acyclic. *)
  Lemma graph_map_injective_acyclic {X Y : Set} (φ : X -> Y) g :
    injective φ ⌊g⌋ -> acyclic g -> acyclic (graph_map φ g).
  Proof.
    intros (ψ&hψ) A i j;split.
    - intros ->;split;reflexivity.
    - intros (h1&h2).
      destruct (path_non_trivial_only_for_vertices h1) as [->|I];auto.
      apply (injective_graph_map_path hψ) in h1.
      apply (injective_graph_map_path hψ) in h2.
      rewrite graph_vertices_map in I.
      assert (hi: i ∈ (List.map φ ⌊g⌋))
        by (apply I;now left).
      assert (hj: j ∈ (List.map φ ⌊g⌋))
        by (apply I;now right;left).
      apply in_map_iff in hi as (i'&<-&hi).
      apply in_map_iff in hj as (j'&<-&hj);f_equal.
      apply A;split;rewrite <-(hψ i'),<-(hψ j');auto.
  Qed.

  (** The image of a good graph by an injective function is good. *)
  Lemma graph_map_injective_good {X Y : Set} (φ : X -> Y) g :
    injective φ ⌊g⌋ -> good g -> good (graph_map φ g).
  Proof.
    intros i (c&a);split;
      [apply graph_map_injective_connected
      |apply graph_map_injective_acyclic];auto.
  Qed.

  (** We may filter a graph according to two boolean predicates. *)
  Definition filter_graph {A} fs ft (g : graph A L) : graph A L:=
    (input g,filter (fun e => match e with
                           | (i,a,j) => fs i || ft j
                           end) (edges g),
     output g).

  Lemma input_filter_graph {A : Set} (fs ft : A -> bool) (g : graph A L) :
    input (filter_graph fs ft g) = input g.
  Proof. reflexivity. Qed.

  Lemma output_filter_graph {A : Set} (fs ft : A -> bool) (g : graph A L) :
    output (filter_graph fs ft g) = output g.
  Proof. reflexivity. Qed.

  Lemma edges_filter_graph {A : Set} (fs ft : A -> bool) (g : graph A L) :
    edges (filter_graph fs ft g) =
    filter (fun e => match e with
                  | (i,a,j) => fs i || ft j
                  end) (edges g).
  Proof. reflexivity. Qed.
  
  (** We may lift paths in the filtered graph to the original graph. *)
  Lemma filter_graph_path {A : Set} (fs ft : A -> bool) g i j:
    i -[ filter_graph fs ft g ]→ j -> i -[g]→ j.
  Proof.
    intro P;induction P.
    - reflexivity.
    - etransitivity;eauto.
    - unfold filter_graph in H.
      unfold edges in H at 1;simpl in H.
      apply filter_In in H as (I&_).
      apply path_edge in I;auto.
  Qed.

  (** The identity is a morphism from [filter_graph fs ft g] to [g]. *)
  Lemma filter_graph_morphism {A : Set} (fs ft : A -> bool) g :
    [] ⊨ filter_graph fs ft g -{id}→ g.
  Proof.
    repeat split;auto.
    - intros i α j Ie.
      rewrite edges_filter_graph,filter_In in Ie.
      unfold id;tauto.
    - intros x I.
      apply in_graph_vertices;rewrite in_graph_vertices in I.
      rewrite input_filter_graph,output_filter_graph in I.
      repeat setoid_rewrite edges_filter_graph in I.
      repeat setoid_rewrite filter_In in I.
      unfold id;firstorder.
  Qed.

  (** If every vertex satisfies the predicate, then filtering doesn't
change the graph. *)
  Lemma filter_graph_true {A : Set} (fs ft : A -> bool) g :
    (forall i, i ∈ ⌊g⌋ -> fs i && ft i = true) -> filter_graph fs ft g = g.
  Proof.
    destruct g as ((x,e),y);intros hyp.
    setoid_rewrite in_graph_vertices in hyp.
    unfold filter_graph,input,output,edges in *;simpl in *;
      f_equal;f_equal.
    induction e as [|((i,a),j) e];simpl;auto.
    replace (fs i||ft j) with true.
    - f_equal;apply IHe.
      intros i0 [<-|[<-|[(α&j0&I)|(α&j0&I)]]];apply hyp;auto.
      + right;right;left;exists α;exists j0;now right.
      + right;right;right;exists α;exists j0;now right.
    - symmetry;apply orb_true_iff;left.
      cut (fs i && ft i = true);[rewrite andb_true_iff;tauto|].
      apply hyp;right;right.
      left;exists a;exists j;now left.
  Qed.

  (** [filter_graph] commutes with [graph_union]. *)
  Lemma filter_graph_union {A : Set} (fs ft : A -> bool) g h :
    filter_graph fs ft (g⋄h) =
    filter_graph fs ft g ⋄ filter_graph fs ft h.
  Proof.
    destruct g as ((i1&e1)&o1);destruct h as ((i2&e2)&o2);
      unfold filter_graph,graph_union,input,output,edges;simpl.
    f_equal;f_equal.
    rewrite filter_app;reflexivity.
  Qed.

  (** [filter_graph] commutes with [graph_map]. *)
  Lemma filter_graph_map {A B : Set} (fs ft : A -> bool) (φ : B -> A) g :
    filter_graph fs ft (graph_map φ g)
    = graph_map φ (filter_graph (fs∘φ) (ft∘φ) g).
  Proof.
    destruct g as ((i&e)&o);
      unfold filter_graph,graph_map,input,output,edges;simpl.
    f_equal;f_equal.
    unfold map;rewrite filter_map;unfold Basics.compose.
    f_equal.
    apply filter_ext.
    intros ((x&a)&y);reflexivity.
  Qed.
  
End g.
(* begin hide *)
Notation " A ⊨ g -{ φ }→ h " := (is_morphism A φ g h) (at level 80).
Notation " A ⊨ g -{ φ }⇀ h" := (is_premorphism A φ g h) (at level 80).
Notation " A ⊨ g ⊲ h " := (hom_order A g h) (at level 80).
Infix "⋈" := hom_eq (at level 80).
Notation " u -[ G ]→ v " := (path G u v) (at level 80).
Infix " ⋄ " := graph_union (at level 40).
Hint Unfold input output edges.
(* end hide *)

(** * Graphs with decidable labels and vertices *)
Section dec_g.
  Variables vert lbl : Set.
  Variable V : decidable_set vert.
  Variable L : decidable_set lbl.

  (** In this case the edges form a decidable set. *)
  Global Instance decidable_edges : decidable_set (@edge vert lbl).
  Proof. apply dec_prod;[apply dec_prod|];auto. Qed.

  (** [swap a b l] replaces every occurrence of the vertex [a] with the
  vertex [b] in the list of edges [l]. *)
  Definition swap (a b : vert) (l : list (@edge vert lbl)) :=
    map {|a \ b|} l.

  (** If [φ1] is an internal map from [g1] to [h1] and [φ2] is an
      internal map from [g2] to [h2], then [φ1⧼⌊g1⌋⧽φ2] is internal
      between [g1⋄g2] and parallel product of [h1] and [h2].  *)
  Lemma par_internal_graph_union {X} (g1 g2 : graph vert lbl)
        (h1 h2 : graph X lbl) φ1 φ2 :
    good_for_par h1 h2 -> internal_map φ1 g1 h1 -> internal_map φ2 g2 h2 ->
    internal_map (φ1 ⧼⌊g1⌋⧽ φ2) (g1 ⋄ g2) (h1 ⋄ h2).
  Proof.
    intros Gh Inj1 Inj2 x G.
    apply par_vertices_graph_union,in_app_iff;auto.
    apply vertices_graph_union,in_app_iff in G as [I1|I2].
    - rewrite seq_morph_in;auto.
    - destruct (In_dec x ⌊g1⌋) as [I1|N1].
      -- rewrite seq_morph_in;auto.
      -- rewrite seq_morph_not_in;auto.
  Qed.

  (** For the sequential product, we additionally need that [h1] to be
  connected. *)
  Lemma seq_internal_graph_union {X} (g1 g2 : graph vert lbl)
        (h1 h2 : graph X lbl) φ1 φ2 :
    connected h1 ->
    good_for_seq h1 h2 -> 
    internal_map φ1 g1 h1 ->
    internal_map φ2 g2 h2 ->
    internal_map (φ1⧼⌊g1⌋⧽φ2) (g1 ⋄ g2) (h1 ⋄ h2).
  Proof.
    intros C1 Gh Inj1 Inj2 x G.
    apply seq_vertices_connected_graph_union,in_app_iff;auto.
    apply vertices_graph_union,in_app_iff in G as [I1|I2].
    - rewrite seq_morph_in;auto.
    - destruct (In_dec x ⌊g1⌋) as [I1|N1].
      -- rewrite seq_morph_in;auto.
      -- rewrite seq_morph_not_in;auto.
  Qed.
  
  (** We can combine a morphism from [g1] to [h1] and a morphism from
  [g2] to [h2] as a morphism from the sequential product of [g1] and
  [g2] to the sequential product of [h1] and [h2]. *)
  Lemma seq_morphism_graph_union {X}
        (g1 g2 : graph vert lbl) (h1 h2 : graph X lbl) φ1 φ2 A :
    connected h1 ->
    good_for_seq g1 g2 ->
    good_for_seq h1 h2 ->
    A ⊨ g1 -{φ1}→ h1 -> 
    A ⊨ g2 -{φ2}→ h2 ->
    A ⊨ g1 ⋄ g2 -{φ1⧼⌊g1⌋⧽φ2}→ h1 ⋄ h2.
  Proof.
    intros Ch1 (hio1&D1) G2 (φ1i&φ1o&Mφ1&M1) (φ2i&φ2o&Mφ2&M2).
    assert (φg2: forall x,x ∈ ⌊g2⌋ ->
                     seq_morph φ1 φ2 ⌊g1⌋ x = φ2 x).
    - destruct G2 as (hio2&D2).
      intros x I2;destruct (In_dec x ⌊g1⌋) as [I1|I1].
      -- rewrite seq_morph_in;auto.
         pose proof (D1 _ I1 I2) as E;rewrite E.
         rewrite φ2i,<- hio1,φ1o,hio2;reflexivity.
      -- rewrite seq_morph_not_in;auto.      
    - split;[|split;[|split]].
      -- destruct g1 as((i1,e1),o1);unfold seq_morph, graph_union;simpl;
           autounfold in *; simpl in *.
         rewrite eqX_refl,φ1i;reflexivity.
      -- repeat rewrite output_graph_union. 
         rewrite φg2;simpl;auto.
      -- intros i α j I;
           repeat rewrite edges_graph_union,in_app_iff in *;
           destruct I as [I|I].
         --- repeat (rewrite seq_morph_in;
                     [|apply in_graph_vertices;right;right;eauto]).
             apply Mφ1 in I;tauto.
         --- repeat (rewrite φg2;
                     [|apply in_graph_vertices;right;right;eauto]).
             apply Mφ2 in I;tauto.
      -- intros x;repeat rewrite vertices_graph_union.
         rewrite seq_vertices_connected_graph_union;auto.
         repeat rewrite in_app_iff.
         intros [I|I].
         ++ rewrite seq_morph_in;auto.
         ++ destruct (In_dec x ⌊g1⌋) as [I1|I1].
            +++ rewrite seq_morph_in;auto.
            +++ rewrite seq_morph_not_in;auto.
  Qed.

  (** We can combine a morphism from [g1] to [h] and a morphism from
  [g2] to [h] as a morphism from the parallel product of [g1] and
  [g2] to [h]. *)
  Lemma par_morphism_graph_union {X}
        (g1 g2 : graph vert lbl) (h : graph X lbl) φ1 φ2 A :
    good_for_par g1 g2 ->
    A ⊨ g1 -{φ1}→ h -> 
    A ⊨ g2 -{φ2}→ h ->
    A ⊨ g1 ⋄ g2 -{φ1⧼⌊g1⌋⧽φ2}→ h.
  Proof.
    intro G;pose proof (par_vertices_graph_union G) as Vert.
    revert G;intros (hi1&ho1&D1) (φ1i&φ1o&Mφ1&M1) (φ2i&φ2o&Mφ2&M2).
    assert (φg2: forall x,x ∈ ⌊g2⌋ ->
                     seq_morph φ1 φ2 ⌊g1⌋ x = φ2 x).
    - intros x I2;destruct (In_dec x ⌊g1⌋) as [I1|I1].
      -- rewrite seq_morph_in;auto.
         pose proof (D1 _ I1 I2) as [E|E];rewrite E.
         --- rewrite φ2i,<-hi1,φ1i;auto.
         --- rewrite φ2o,<-ho1,φ1o;auto.
      -- rewrite seq_morph_not_in;auto.      
    - split;[|split;[|split]].
      -- destruct g1 as((i1,e1),o1);unfold seq_morph, graph_union;simpl;
           autounfold in *; simpl in *.
         rewrite eqX_refl,φ1i;reflexivity.
      -- repeat rewrite output_graph_union. 
         rewrite φg2;simpl;auto.
      -- intros i α j I;
           repeat rewrite edges_graph_union,in_app_iff in *;
           destruct I as [I|I].
         --- repeat (rewrite seq_morph_in;
                     [|apply in_graph_vertices;right;right;eauto]).
             apply Mφ1 in I;tauto.
         --- repeat (rewrite φg2;
                     [|apply in_graph_vertices;right;right;eauto]).
             apply Mφ2 in I;tauto.
      -- intros x;rewrite Vert,in_app_iff.
         intros [I|I].
         ++ rewrite seq_morph_in;auto.
         ++ destruct (In_dec x ⌊g1⌋) as [I1|I1].
            +++ rewrite seq_morph_in;auto.
            +++ rewrite seq_morph_not_in;auto.
  Qed.
  
  (** We can combine a morphism from [g1] to [h1] and a morphism from
  [g2] to [h2] as a morphism from the parallel product of [g1] and
  [g2] to the parallel product of [h1] and [h2]. *)
  Lemma par_par_morphism_graph_union {X}
        (g1 g2 : graph vert lbl) (h1 h2 : graph X lbl) φ1 φ2 A :
    good_for_par g1 g2 ->
    good_for_par h1 h2 ->
    A ⊨ g1 -{φ1}→ h1 -> 
    A ⊨ g2 -{φ2}→ h2 ->
    A ⊨ g1 ⋄ g2 -{φ1⧼⌊g1⌋⧽φ2}→ h1 ⋄ h2.
  Proof.
    intros G1 G2 M1 M2.
    apply par_morphism_graph_union;auto.
    - apply (@morphism_ext_vertices _ _ _ (id∘φ1));[now intros;auto|].
      rewrite <- (app_nil_r A); eapply is_morphism_compose;eauto.
      apply par_graph_union_decomp,G2.
    - apply (@morphism_ext_vertices _ _ _ (id∘φ2));[now intros;auto|].
      rewrite <- (app_nil_r A); eapply is_morphism_compose;eauto.
      apply par_graph_union_decomp,G2.
  Qed.

  (** The parallel product is smaller than both its arguments. *)
  Lemma par_hom_order_weak_left (g1 g2 : graph vert lbl) A :
    good_for_par g1 g2 -> A ⊨ g1 ⋄ g2 ⊲ g1.
  Proof.
    intros;eapply hom_order_incl;[apply incl_nil|];
      exists id;apply par_graph_union_decomp;auto.
  Qed.
  
  Lemma par_hom_order_weak_right (g1 g2 : graph vert lbl) A :
    good_for_par g1 g2 -> A ⊨ g1 ⋄ g2 ⊲ g2.
  Proof.
    intros;eapply hom_order_incl;[apply incl_nil|];
      exists id;apply par_graph_union_decomp;auto.
  Qed.

  (** If both [g1] and [g2] are greater than [h] so is their parallel
  product. *)
  Lemma par_smaller_hom_order (g1 g2 h : graph vert lbl) A :
    good_for_par g1 g2 -> A ⊨ h ⊲ g1 -> A ⊨ h ⊲ g2 -> A ⊨ h ⊲ g1 ⋄ g2.
  Proof.
    intros G1 (φ1&h1) (φ2&h2);eexists;apply par_morphism_graph_union;
      eauto.
  Qed.
  
  (** If the parallel product of [g1] and [g2] is greater than a graph
  [h], then both [g1] and [g2] are greater than [h]. *)
  Lemma smaller_par_hom_order (g1 g2 h : graph vert lbl) A :
    good_for_par g1 g2 -> A ⊨  h ⊲ g1 ⋄ g2 -> A ⊨  h ⊲ g1 /\ A ⊨ h ⊲ g2.
  Proof.
    intros G I;split.
    - rewrite I;apply par_hom_order_weak_left,G.
    - rewrite I;apply par_hom_order_weak_right,G.
  Qed.
  
  (** The next two lemma state that the homomorphism order on graphs
  is a congruence for the parallel and sequential products.  *)
  Lemma seq_hom_order (g1 g2 h1 h2 : graph vert lbl) A :
    connected g1 -> good_for_seq g1 g2 -> good_for_seq h1 h2 ->
    A ⊨ g1 ⊲ h1 -> A ⊨ g2 ⊲ h2 -> A ⊨ g1 ⋄ g2 ⊲ h1 ⋄ h2.
  Proof.
    intros Ch1 G1 G2 (φ1&hφ1) (φ2&hφ2);eexists;
      apply seq_morphism_graph_union;eauto.
  Qed.
  
  Lemma par_hom_order (g1 g2 h1 h2 : graph vert lbl) A :
    good_for_par g1 g2 -> good_for_par h1 h2 ->
    A ⊨ g1 ⊲ h1 -> A ⊨ g2 ⊲ h2 -> A ⊨ g1 ⋄ g2 ⊲ h1 ⋄ h2.
  Proof.
    intros G1 G2 I1 I2;apply par_smaller_hom_order;auto.
    - transitivity g1;auto.
      apply par_hom_order_weak_left,G1.
    - transitivity g2;auto.
      apply par_hom_order_weak_right,G1.
  Qed.

  (** If [φ] is a morphism from the sequential product of [g1] and
  [g2] to [h], then it is a premorphism from both [g1] and [g2] to
  [h].  *)
  Lemma seq_morphism_graph_union_left {X} (g1 g2 : graph vert lbl)
        (h : graph X lbl) φ A :
    good_for_seq g1 g2 ->  A ⊨ g1 ⋄ g2 -{φ}→ h ->
    A ⊨ g1 -{φ}⇀ h
    /\ A ⊨ g2 -{φ}⇀ h
    /\ φ (input g1) = input h
    /\ φ (output g2) = output h.
  Proof.
    intros (hio&D) (φi&φo&M).
    rewrite input_graph_union in φi;rewrite output_graph_union in φo.
    split;[|split;[|split]];auto;intros i α j I.
    - apply M,in_app_iff;tauto.
    - apply M,in_app_iff;tauto.
  Qed.

  (** If [φ] is a morphism from the parallel product of [g1] and [g2]
  to [h], then it is a morphism from both [g1] and [g2] to [h].  *)
  Lemma par_morphism_graph_union_left {X} (g1 g2 : graph vert lbl)
        (h : graph X lbl) φ A :
    good_for_par g1 g2 ->  A ⊨ g1 ⋄ g2 -{φ}→ h ->
    A ⊨ g1 -{φ}→ h /\ A ⊨ g2 -{φ}→ h.
  Proof.
    intros G M;destruct (par_graph_union_decomp G) as (P1&P2).
    split;(rewrite (@morphism_ext_vertices _ _ _ φ (φ∘id));
           [|intros;simpl;auto]);eapply is_morphism_compose_weak;eauto;
      eapply is_morphism_incl;eauto;apply incl_nil.
  Qed.

  (** Conversely, if [φ] is a morphism from both [g1] and [g2] to [h],
      then it is a morphism from the parallel product of [g1] and [g2]
      to [h]. *)
  Lemma par_morphism_graph_union_right {X} (g1 g2 : graph vert lbl)
        (h : graph X lbl) φ A :
    good_for_par g1 g2 -> A ⊨ g1 -{φ}→ h ->  A ⊨ g2 -{φ}→ h ->
    A ⊨ g1 ⋄ g2 -{φ}→ h.
  Proof.
    intros G M1 M2;split;[|split;[|split]].
    - rewrite input_graph_union;apply M1.
    - rewrite output_graph_union;apply M2.
    - intros i α j I;rewrite edges_graph_union in I.
      apply in_app_iff in I as [I|I];[apply M1|apply M2];auto.
    - intros x;rewrite par_vertices_graph_union;auto.
      rewrite in_app_iff;intros [I|I];apply M1,I || apply M2,I.
  Qed.

  (** If [g] and [h] are good and there is a path from [i] to [k] in
  their parallel product, then there this path can be found either in
  [g] or in [h]. *)
  Lemma path_par_union (g h: graph vert lbl) i k :
    good_for_par g h -> good g -> good h -> i -[g ⋄ h]→ k ->
    i -[g]→ k \/ i -[h]→ k.
  Proof.
    intros G (C1&A1) (C2&A2) P;induction P.
    - left;reflexivity.
    - destruct IHP1 as [P1'|P1'];destruct IHP2 as [P2'|P2'].
      -- left;etransitivity;eauto.
      -- destruct G as (G1&G2&D).
         destruct (path_non_trivial_only_for_vertices P1') as [->|N1];
           auto.
         destruct (path_non_trivial_only_for_vertices P2') as [->|N2];
           auto.
         destruct (D j) as [-> | ->].
         --- now apply N1;right;left.
         --- now apply N2;left.
         --- replace i with (input h)
             by (rewrite <- G1 in *; apply A1;split;auto;
                 apply C1,N1;now left).
             now right;apply C2,N2;right;left.
         --- replace k with (output g)
             by (rewrite G2 in *; apply A2;split;auto;
                 apply C2,N2;now right;left).
             now left;apply C1,N1;left.
      -- destruct G as (G1&G2&D).
         destruct (path_non_trivial_only_for_vertices P1') as [->|N1];
           auto.
         destruct (path_non_trivial_only_for_vertices P2') as [->|N2];
           auto.
         destruct (D j) as [-> | ->].
         --- now apply N2;left.
         --- now apply N1;right;left.
         --- replace i with (input h)
             by (apply A2;split;auto;apply C2,N1;now left);auto.
         --- replace k with (output h)
             by (rewrite <-G2 in *; apply A1;split;auto;
                 apply C1,N2;now right;left);auto.
      -- right;etransitivity;eauto.
    - rewrite edges_graph_union in H;apply in_app_iff in H as [I|I];
        [left|right];eapply path_edge;eauto.
  Qed.

  (** If [g1] and [g2] are good, and there is a path inside their
  parallel product from [i], a node from [g1], to [k], a node from
  [g2], then either [i] is the input of the graph, or [k] is its
  output. *)  
  Lemma path_par_union_cross
      (g1 g2 : graph vert lbl) i k :
    good_for_par g1 g2 -> good g1 -> good g2 ->
    i ∈ ⌊g1⌋ -> k ∈ ⌊g2⌋ ->
    i -[g1⋄g2]→ k -> i = input (g1⋄g2) \/ k = output (g1⋄g2).
  Proof.
    intros G G1 G2 V1 V2 P.
    rewrite input_graph_union,output_graph_union.
    apply path_par_union in P as [P|P];auto;
    destruct G as (E1&E2&G);destruct G1 as (C1&A1);
    destruct G2 as (C2&A2);
    destruct (path_non_trivial_only_for_vertices P) as [->|I];auto.
    - rewrite E1;apply G;auto.
    - assert (V3:k ∈ [i;k]) by (now right;left);apply I in V3.
      destruct (G k V3 V2) as [-> | ->];auto.
      rewrite <- E1 in *;left;apply A1;split;auto;apply C1;auto.
    - rewrite E1;apply G;auto.
    - assert (V3:i ∈ [i;k]) by (now left);apply I in V3.
      destruct (G i V1 V3) as [-> | ->];auto.
      right;apply A2;split;auto;apply C2;auto.
  Qed.

  (** In the next two lemma, we show that a path in a parallel product
whose two ends belong to the same component can be lifted to that
component. *)
  Lemma path_par_union_left (g1 g2 : graph vert lbl) i k :
    good_for_par g1 g2 -> good g1 -> good g2 ->
    i ∈ ⌊g1⌋ -> k ∈ ⌊g1⌋ ->
    i -[g1⋄g2]→ k -> i -[g1]→ k.
  Proof.
    intros G G1 G2 V1 V2 P'.
    assert (Cr:forall i k,
               i ∈ ⌊g1⌋ -> k ∈ ⌊g2⌋ ->
               i -[g1⋄g2]→ k -> i = input (g1⋄g2) \/ k = output (g1⋄g2))
      by (intros;apply path_par_union_cross;auto).
    destruct (path_par_union G G1 G2 P') as [P|P];auto;
    destruct G as (E1&E2&G);destruct G1 as (C1&A1);
    destruct G2 as (C2&A2);
    destruct (path_non_trivial_only_for_vertices P) as [->|I];auto.
    - reflexivity.
    - assert (V3:k ∈ [i;k]) by (now right;left);apply I in V3.
      destruct (Cr _ _ V1 V3 P') as [-> | ->];auto.
      -- rewrite input_graph_union;apply C1;auto.
      -- rewrite output_graph_union,<-E2;apply C1;auto.
  Qed.
  
  Lemma path_par_union_right (g1 g2 : graph vert lbl) i k :
    good_for_par g1 g2 -> good g1 -> good g2 ->
    i ∈ ⌊g2⌋ -> k ∈ ⌊g2⌋ -> i -[g1⋄g2]→ k -> i -[g2]→ k.
  Proof.
    intros G G1 G2 V1 V2 P'.
    destruct (path_par_union G G1 G2 P') as [P|P];auto;
    destruct G as (E1&E2&G);destruct G1 as (C1&A1);
    destruct G2 as (C2&A2);
    destruct (path_non_trivial_only_for_vertices P) as [->|I];auto.
    - reflexivity.
    - assert (V3:k ∈ [i;k]) by (now right;left);apply I in V3.
      destruct (G k V3 V2) as [-> | ->];auto.
      -- replace i with (input g2);[reflexivity|].
         destruct (path_non_trivial_only_for_vertices P) as [->|Vi];
           auto.
         apply A1;split;auto;rewrite<- E1;apply C1;auto.
         apply Vi;simpl;auto.
      -- apply C2;auto.
  Qed.

  (** For any two graph [g1, g2] if there is a path from [i] to [k] in
  either of the graphs, there is one in [g1⋄g2]. *)
  Lemma path_graph_union (g1 g2 : graph vert lbl) i k :
    i -[g1]→ k \/ i -[g2]→ k -> i -[g1⋄g2]→ k.
  Proof.
    intros [P|P];induction P.
    - reflexivity.
    - etransitivity;eauto.
    - eapply path_edge;rewrite edges_graph_union.
      apply in_app_iff;left;eauto.
    - reflexivity.
    - etransitivity;eauto.
    - eapply path_edge;rewrite edges_graph_union.
      apply in_app_iff;right;eauto.
  Qed.

  (** If [g1] and [g2] are good, then a path in their sequential
      product exists from [i] to [k] exactly if either [i] and [k] are
      equal, or they are different and one of the following conditions
      hold:

      - they both come from the same graph, and are linked in that
        graph
      - or [i] is a vertex of [g1] and [k] is a vertex of [g2]. *)
  Lemma path_seq (g1 g2 : graph vert lbl) i k :
    good_for_seq g1 g2 -> good g1 -> good g2 ->
    i -[g1⋄g2]→ k <->
    i = k
    \/ (i<> k /\
       (( i ∈ ⌊g1⌋ /\ k ∈ ⌊g1⌋ /\ i -[g1]→ k)
        \/ ( i ∈ ⌊g2⌋ /\ k ∈ ⌊g2⌋ /\ i -[g2]→ k)
        \/ ( i ∈ ⌊g1⌋ /\ k ∈ ⌊g2⌋))).
  Proof.                             
    intros (E&D) G1 G2;split.
    - intro P;induction P;auto.
      + destruct_eqX i k;[tauto|right;split;auto].
        destruct IHP1 as [->|(Nij&[(Ii&Ij&P1')|[(Ii&Ij&P1')|(Ii&Ij)]])];
          destruct IHP2 as [->|(Njk&[(Ij'&Ik&P2')
                                    |[(Ij'&Ik&P2')|(Ij'&Ik)]])];
          try tauto.
        * left;repeat split;auto;transitivity j;auto.
        * exfalso;cut (j = input g2);[|apply D;tauto].
          intros ->;apply Nij,G2;split;auto.
          apply G2 in Ii;tauto.
        * right;left;repeat split;auto;transitivity j;auto.
        * exfalso;cut (j = input g2);[|apply D;tauto].
          intros ->;apply Nij,G2;split;auto.
          apply G2 in Ii;tauto.
        * exfalso;cut (j = input g2);[|apply D;tauto].
          rewrite <- E;intros ->;apply Njk,G1;split;auto.
          apply G1 in Ik;tauto.
      + destruct_eqX i j;[tauto|right;split;auto].
        rewrite edges_graph_union in H;apply in_app_iff in H as [Ie|Ie];
          [left|right;left];pose proof (edges_vertices Ie) as (Ii&Ij);
            apply path_edge in Ie;tauto.
    - intros [->|(N&[(Ii&Ik&P)|[(Ii&Ik&P)|(Ii&Ik)]])].
      + reflexivity.
      + apply path_graph_union;tauto.
      + apply path_graph_union;tauto.
      + transitivity (input g2).
        * apply path_graph_union;left;rewrite <- E;apply G1 in Ii;tauto.
        * apply path_graph_union;right;apply G2 in Ik;tauto.
  Qed.

  (** If two graphs are good then their sequential and parallel
  compositions are also good. *)
  Lemma good_seq_graph_union (g1 g2 : graph vert lbl) :
    good_for_seq g1 g2 -> good g1 -> good g2 -> good (g1⋄g2).
  Proof.
    intros G G1 G2;split.
    - destruct G1 as (C1&_);destruct G2 as (C2&_);
        now apply seq_graph_union_connected.
    - intros i j;split.
      + intros ->;split;reflexivity.
      + intros (I1&I2);unfold Basics.flip in I2.
        rewrite path_seq in I1,I2;auto.
        destruct G as (E&D);destruct G1 as (C1&A1);
          destruct G2 as (C2&A2);intuition.
        * apply A1;split;tauto.
        * transitivity (input g2).
          -- apply D;auto.
          -- symmetry;apply D;auto.
        * apply A1;split;auto.
          replace i with (output g1).
          -- apply C1;tauto.
          -- symmetry;rewrite E;apply D;auto.
        * transitivity (input g2).
          -- apply D;auto.
          -- symmetry;apply D;auto.
        * apply A1;split;auto.
          replace j with (output g1).
          -- apply C1;tauto.
          -- symmetry;rewrite E;apply D;auto.
        * apply A2;split;auto.
        * apply A2;split;auto.
          unfold Basics.flip;replace j with (input g2).
          -- apply C2;tauto.
          -- symmetry;apply D;auto.
        * apply A2;split;auto.
          unfold Basics.flip;replace i with (input g2).
          -- apply C2;tauto.
          -- symmetry;apply D;auto.
        * transitivity (input g2).
          -- apply D;auto.
          -- symmetry;apply D;auto.
  Qed.
  
  Lemma good_par_graph_union (g1 g2 : graph vert lbl) :
    good_for_par g1 g2 -> good g1 -> good g2 -> good (g1⋄g2).
  Proof.
    intros G G1 G2;split.
    - destruct G1;destruct G2;apply par_graph_union_connected;tauto.
    - intros i j;split.
      + intros ->;split;reflexivity.
      + intros (I1&I2);unfold Basics.flip in I2.
        destruct_eqX i j;[auto|].
        pose proof (path_par_union G G1 G2 I1) as [P1|P1];
          pose proof (path_par_union G G1 G2 I2) as [P2|P2].
        * apply G1;split;auto.
        * assert (i ∈ ⌊g1⌋ /\ j∈⌊g1⌋) as (Ii1&Ij1)
              by (apply path_non_trivial_only_for_vertices in P1
                   as [->|I];[tauto|];split;apply I;simpl;tauto).
          assert (i ∈ ⌊g2⌋ /\ j∈⌊g2⌋) as (Ii2&Ij2)
              by (apply path_non_trivial_only_for_vertices in P2
                   as [->|I];[tauto|];split;apply I;simpl;tauto).
          destruct G as (E1&E2&D).
          destruct (D _ Ii1 Ii2) as [-> | ->];
            destruct (D _ Ij1 Ij2) as [-> | ->];auto.
          -- apply G2;split;auto.
             apply G2 in Ii2;tauto.
          -- rewrite <- E1, <- E2 in *;apply G1;split;auto.
             apply G1 in Ij1;tauto.
        * assert (i ∈ ⌊g1⌋ /\ j∈⌊g1⌋) as (Ii1&Ij1)
              by (apply path_non_trivial_only_for_vertices in P2
                   as [->|I];[tauto|];split;apply I;simpl;tauto).
          assert (i ∈ ⌊g2⌋ /\ j∈⌊g2⌋) as (Ii2&Ij2)
              by (apply path_non_trivial_only_for_vertices in P1
                   as [->|I];[tauto|];split;apply I;simpl;tauto).
          destruct G as (E1&E2&D).
          destruct (D _ Ii1 Ii2) as [-> | ->];
            destruct (D _ Ij1 Ij2) as [-> | ->];auto.
          -- rewrite <- E1, <- E2 in *;apply G1;split;auto.
             apply G1 in Ij1;tauto.
          -- apply G2;split;auto.
             apply G2 in Ii2;tauto.
        * apply G2;split;auto.
  Qed.
End dec_g.
(* begin hide *)
Hint Unfold swap.
(* end hide *)
Section p.
  
  Variables vert lbl : Set.
  Variable V : decidable_set vert.
  Variable L : decidable_set lbl.
  Context { g : graph vert lbl }.
  (** * Boolean path predicate *)

  Notation " i → j " := (i -[g]→ j) (at level 80).
  
  (** [ispath g l i j] states that [l] is a valid path from [i] to
  [j] in [g]. *) 
  Fixpoint ispath l i j :=
    match l with
    | [] => i = j
    | k::l => (exists α, (i,α,k) ∈ (edges g))
             /\ ispath l k j
    end.

  (** Hence we can prove that whenever there is a path according to
  [ispath] from [i] to [j], they are related by the [path g]
  relation. *)
  Lemma ispath_path l i j :
    ispath l i j -> i → j.
  Proof.
    revert i j;induction l as [|k l].
    - intros i j -> ;reflexivity.
    - intros i j ((α&I)&P).
      eapply path_trans.
      + eapply path_edge;eauto.
      + apply IHl;eauto.
  Qed.
  (** We can combine paths by concatenating them. *)
  Lemma ispath_app i j k p1 p2 :
    ispath p1 i j -> ispath p2 j k -> ispath (p1 ++ p2) i k.
  Proof.
    revert i j k;induction p1 as [|x p1];simpl.
    - intros i j k <- P2; tauto.
    - intros i j k ((α&I)&P1) P2.
      split;[eauto|].
      eapply IHp1;eauto.
  Qed.

  (** Furthermore, if [j] is reachable from [i], then there is a path
  linking them. *)
  Lemma path_ispath i j :
    i → j -> exists l, ispath l i j.
  Proof.
    intros P;induction P.
    - exists [];simpl;auto.
    - clear P1 P2;destruct IHP1 as (p1&P1);destruct IHP2 as (p2&P2).
      exists (p1++p2);eapply ispath_app;eauto.
    - exists [j];simpl;split;eauto.
  Qed.

  (** [neighbour g i k] tests if there is an edge in [g] whose source
  is [i] and target is [k]. *)
  Definition neighbour i k :=
    existsb (fun e => match e with
                   | ((x,_),y) => eqX x i && eqX y k end)
            (edges g).

  Lemma neighbour_spec i k :
    neighbour i k = true <-> exists α, (i,α,k) ∈ (edges g).
  Proof.
    unfold neighbour;rewrite existsb_exists;split.
    - intros (((x&α)&y)&I&E).
      rewrite andb_true_iff in E;repeat rewrite eqX_correct in E.
      destruct E as (->&->); eauto.
    - intros (α&I);exists (i,α,k);repeat rewrite eqX_refl;split;auto.
  Qed.

  (** [is_pathb] is the boolean version of [is_path]. *)
  Fixpoint ispathb l i j :=
    match l with
    | [] => eqX i j
    | k::l => neighbour i k && ispathb l k j
    end.
  
  Lemma ispathb_ispath l i j :
    ispathb l i j = true <-> ispath l i j.
  Proof.
    revert i j;induction l as [|k l].
    - intros i j;simpl.
      repeat rewrite eqX_correct;tauto.
    - intros i j; simpl.
      rewrite <- IHl,<- neighbour_spec,andb_true_iff;tauto.
  Qed.

  (** We can decompose paths using the intermediary vertices. *)
  Lemma split_path i j k l m :
    ispath (l++(k::m)) i j -> ispath m k j.
  Proof.
    revert i;induction l as [|v l];intro i;simpl;try tauto.
    intros ((α&I)&P).
    eapply IHl;eauto.
  Qed.

  (** If [l] is a path from [i] to [j], then either [i] doesn't appear
  in [l], or we can split [l] into [m++i::n] such that [i] doesn't
  appear in [n] and [n] is a path from [i] to [j]. *)
  Lemma path_ispath_short i j l : 
    ispath l i j ->
    ~ i ∈ l \/ exists m n, l = m++(i::n) /\ ~ i ∈ n /\ ispath n i j.
  Proof.
    destruct (In_dec i l);eauto.
    apply in_split_strict in i0 as (m&n&->&N).
    intro P; apply split_path in P.
    right;eauto.
  Qed.


  (** Iterating the previous lemma, we can remove every duplicate from
  a path. Thus is [j] is reachable from [i], there is a path without
  duplication linking them. *)
  Lemma short_path i j : 
    i → j -> exists l, NoDup (i::l) /\ ispath l i j.
  Proof.
    intro P;apply path_ispath in P as (l&P).
    cut (exists l, NoDup l /\ ispath l i j).
    - clear l P;intros (l&N&P).
      setoid_rewrite NoDup_cons_iff.
      destruct (path_ispath_short P) as [Nk|(m&l2&->&Ik&P')].
      + eauto.
      + eapply NoDup_remove_3 in N;eauto.
        exists l2;repeat split;tauto.
    - revert i j P;induction l as [|k l];intros i j;simpl.
      + intros -> ; exists [] ;split;simpl;auto.
        apply NoDup_nil.
      + intros ((α&I)&P).
        apply IHl in P as (l1&N1&P).
        destruct (path_ispath_short P)
          as [Nk|(m&l2&->&Ik&P')].
        * exists (k::l1);split;simpl;eauto.
          apply NoDup_cons;auto.
        * exists (k::l2);split;simpl;eauto.
          apply NoDup_cons;auto.
          eapply NoDup_remove_3;eauto.
  Qed.

  (** A path can only use vertices. *)
  Lemma path_is_vertices i j l :
    ispath l i j -> l ⊆ ⌊g⌋.
  Proof.
    revert i j;induction l;simpl;auto.
    - intros _ _ _ ?;simpl;tauto.
    - intros i j ((α&Ie)&P) x [->|I].
      + apply in_graph_vertices;repeat right;eauto.
      + eapply IHl;eauto.
  Qed.

  (** [ex_path g i j l n] tests whether there is a path of length
  smaller or equal to [n] using elements from [l] linking [i] and
  [j]. *)  
  Fixpoint ex_path i j l n :=
    match n with
    | O => eqX i j
    | S n =>
      eqX i j || existsb (fun k => neighbour i k && ex_path k j l n) l
    end.

  Lemma ex_path_spec i j l n :
    ex_path i j l n = true <->
    exists p, ispath p i j /\ p ⊆ l /\ # p <= n.
  Proof.
    revert i j;induction n;intros i j;simpl.
    - rewrite eqX_correct;split.
      + intros -> ;exists [];repeat split;auto.
        intro;simpl;tauto.
      + intros ([|? ?]&P&_&I);simpl in *;[tauto|lia].
    - rewrite orb_true_iff,eqX_correct,existsb_exists.
      setoid_rewrite andb_true_iff.
      setoid_rewrite neighbour_spec.
      setoid_rewrite IHn;clear IHn.
      split.
      + intros [->|(x&Ix&(α&Ie)&(p&P&I&hn))].
        * exists [];repeat split;simpl.
          -- intro;simpl;tauto.
          -- lia.
        * exists (x::p);repeat split;auto.
          -- eauto.
          -- intros y [<-|Iy];eauto.
          -- simpl;lia.
      + intros ([|y p]&P&I&hn);simpl in P.
        * rewrite P in *;auto.
        * destruct P as ((α&Ie)&P).
          right;exists y;repeat split;auto.
          -- apply I;now left.
          -- eauto.
          -- exists p;repeat split;simpl in *;auto.
             ++ intros ? ?;apply I;now right.
             ++ lia.
  Qed.

  (** We may now define [exists_path], a boolean function
  corresponding to the predicate [path]. *)
  Definition exists_path i j := ex_path i j ⌊g⌋ (# ⌊g⌋).

  Notation " i -?→ j " := (exists_path i j) (at level 50).
  
  Lemma exists_path_spec i j :
    i -?→ j = true <-> i → j.
  Proof.  
    unfold exists_path;rewrite ex_path_spec;split.
    - intros (p & P & _ & _) .
      eapply ispath_path;eauto.
    - intro P;apply short_path in P as (p&N&P).
      pose proof (path_is_vertices P) as Vert.
      exists p;repeat split;auto.
      apply NoDup_cons_iff in N as (_&N).
      clear i j P; generalize dependent ⌊g⌋;clear g.
      induction p;intro l;simpl;auto.
      + lia.
      + intros I.
        apply NoDup_cons_iff in N as (nI&N).
        assert (I' : p ⊆ rm a l).
        * intros x Ix;apply in_rm;split.
          -- intros ->;tauto.
          -- apply I;now right.
        * apply IHp in I';auto.
          assert (h:a ∈ l) by now (apply I;left).
          apply rm_length_in in h.
          lia.
  Qed.

  (** The visible set of vertices of [g] from [k] is the set of
  vertices of [g] that are either reachable or co-reachable from
  [k]. *)
  Definition visible k :=
    filter (fun i => i -?→ k || (k -?→ i)) ⌊g⌋.

  (** This is also the set of vertices of the filtered graph of [g]
  where the source predicate is "is reachable from [k]" and the target
  predicate is "can reach [k]", granted that [g] is reachable. *)
  Lemma filter_graph_visible k :
    connected g -> k ∈ ⌊g⌋ ->
    ⌊filter_graph (fun i => k -?→ i) (fun j => j -?→ k) g⌋
     ≈ visible k.
  Proof.
    intros C Vk.
    assert (input g ∈ visible k /\ output g ∈ visible k) as (V0&Vn).
    - unfold visible;repeat rewrite filter_In,orb_true_iff;
        repeat rewrite exists_path_spec.
      apply C in Vk;intuition.
      + now left.
      + now right;left.
    - apply incl_PartialOrder;split.
      + intros i [<-|[<-|E]].
        * rewrite input_filter_graph;auto.
        * rewrite output_filter_graph;auto.
        * apply in_flat_map in E as (((x&a)&y)&E&I).
          rewrite edges_filter_graph,filter_In in E.
          rewrite orb_true_iff in E;
            repeat rewrite exists_path_spec in E.
          unfold visible;apply filter_In.
          rewrite orb_true_iff;repeat rewrite exists_path_spec.
          destruct E as (Ie&[P|P]);
            pose proof (edges_vertices Ie) as (Vx&Vy);
            (destruct I as [<-|[<-|F]];[| |simpl in F;tauto]);
            unfold fst,snd;split;auto.
          -- right;transitivity x;auto.
             apply path_edge in Ie;auto.
          -- left;transitivity y;auto.
             apply path_edge in Ie;auto.
      + unfold Basics.flip,visible;intros i I.
        apply filter_In in I as (I&F).
        rewrite orb_true_iff in F.
        repeat rewrite exists_path_spec in F.
        destruct F as [P|P].
        * assert (i = input g \/ exists j α, (j,α,i) ∈ edges g)
            as [->|(j&α&Ie)].
          -- apply C in I as (I&_);clear k V0 Vn Vk P C.
             induction I;auto.
             ++ destruct IHI2 as [->|(?&?&I)];auto.
                right;exists x;exists x0;apply I.
             ++ right;exists i;exists α;apply H.
          -- left;apply input_filter_graph.
          -- apply in_graph_vertices;right;right;right.
             exists α;exists j;rewrite edges_filter_graph;apply filter_In;
               split;auto.
             apply orb_true_iff;repeat rewrite exists_path_spec.
             tauto.
        * assert (i = output g \/ exists j α, (i,α,j) ∈ edges g)
            as [->|(j&α&Ie)].
          -- apply C in I as (_&I);clear k Vk V0 Vn P C.
             induction I;auto.
             ++ destruct IHI1 as [->|(?&?&I)];auto.
                right;exists x;exists x0;apply I.
             ++ right;exists j;exists α;apply H.
          -- right;left;apply output_filter_graph.
          -- apply in_graph_vertices;right;right;left.
             exists α;exists j;rewrite edges_filter_graph;apply filter_In;
               split;auto.
             apply orb_true_iff;repeat rewrite exists_path_spec.
             tauto.
  Qed.
End p.
Arguments exists_path {vert lbl V} g i j.
Arguments visible {vert lbl V} g k.
Notation " i -[ g ]?→ j " := (exists_path g i j) (at level 80).

(** * Morphisms between graphs with decidable vertices *)
Section dec_hom.
  Variables vert1 vert2 lbl : Set.
  Variable V1 : decidable_set vert1.
  Variable V2 : decidable_set vert2.
  Variable L : decidable_set lbl.
  Notation grph A := (graph A lbl).

  (** First, we notice that if [φ] is a morphism between [g] and [h],
      then there must be a morphism [ψ] that has finite support. *)
  Lemma is_morphism_finite_function A φ
        (g : grph vert1) (h : grph vert2):
    A ⊨ g -{φ}→ h ->
    exists ψ, finite_support ψ ⌊g⌋ ⌊h⌋ (input h) /\ A ⊨ g -{ψ}→ h.
  Proof.
    intros M.
    exists (fun n => if (inb n ⌊g⌋)
                  &&(inb (φ n) ⌊h⌋)
             then φ n
             else (input h)) ;split;[split|].
    - destruct (inb_dec x ⌊g⌋) as [(->&I)|(->&I)];
        [destruct (inb_dec (φ x) ⌊h⌋)as[(->&J)|(->&j)]|];
        tauto.
    - destruct (inb_dec x ⌊g⌋) as [(->&I)|(->&I)];
        [destruct (inb_dec (φ x)⌊h⌋)as[(->&J)|(->&j)]|];
        simpl in *;tauto.
    - eapply morphism_ext_vertices;eauto.
      intros i I; pose proof I as Iφ.
      apply inb_spec in I as ->.
      apply M,inb_spec in Iφ as ->;auto.
  Qed.

  (** [internal_map_b φ g h] is a boolean testing whether [φ] is
  internal from [g] to [h]. *)
  Definition internal_map_b φ (g : grph vert1) (h : grph vert2) :=
    inclb (List.map φ ⌊g⌋) ⌊h⌋.

  Lemma internal_map_b_spec φ g h :
    internal_map_b φ g h = true <-> internal_map φ g h.
  Proof.
    unfold internal_map_b.
    rewrite inclb_correct.
    split;intros hyp x I.
    - apply hyp,List.in_map,I.
    - apply in_map_iff in I as (y&<-&I).
      apply hyp,I.
  Qed.
  
  
  
  (** [is_morphismb A φ g h] is a boolean testing whether [φ] is
  actually an [A]-morphism between [g] and [h]. *)
  Definition is_morphismb A φ (g : grph vert1) (h : grph vert2) :=
    (eqX (φ (input g)) (input h))
      &&
      ((eqX (φ (output g)) (output h))
         &&
         ((forallb (fun e =>
                      match e with
                      | ((i,a),j) =>
                        ((inb a A) && (eqX (φ i) (φ j)))
                        || (inb ((φ i,a),φ j) (edges h))
                      end)
                   (edges g))
            && internal_map_b φ g h)).

  
  Lemma is_morphismb_correct A φ g h :
    is_morphismb A φ g h = true <-> A ⊨ g -{ φ }→ h.
  Proof.
    assert (prod_destr : forall A B  (P : A * B -> Prop),
               (forall p : A * B, P p) <->
               (forall (a : A) (b : B), P (a, b)))
      by (firstorder; destruct p;auto).
    assert (prod_destr_had_hoc :
              forall A B C D  (P : A -> B * C -> D -> Prop),
                (forall a p d, P a p d) <->
                (forall a b c d, P a (b, c) d))
      by (firstorder; destruct p;auto).
    unfold is_morphism, is_morphismb.
    simpl;repeat rewrite andb_true_iff.
    rewrite internal_map_b_spec.
    rewrite forallb_forall.
    repeat rewrite prod_destr.
    setoid_rewrite orb_true_iff.
    setoid_rewrite andb_true_iff.
    repeat setoid_rewrite eqX_correct.
    setoid_rewrite inb_spec.
    tauto.
  Qed.

  (** [ex_is_morphismb A g h] is a boolean testing whether there
  exists an [A]-morphism from [g] to [h]. *)
  Definition ex_is_morphismb A (g : grph vert1) (h : grph vert2) :=
    existsb (fun φ => is_morphismb A φ g h)
            (finite_support_functions _ ⌊g⌋ ⌊h⌋ (input h)).

  Lemma ex_is_morphismb_correct A g h :
    ex_is_morphismb A g h = true <-> exists φ, A⊨ g -{φ}→ h.
  Proof.
    unfold ex_is_morphismb;rewrite existsb_exists.
    setoid_rewrite is_morphismb_correct.
    split.
    - intros (φ & F & M);eexists;eauto.
    - intros (φ & M).
      apply is_morphism_finite_function in M as (ψ & F & hi&ho&he&Int);
        auto.
      eapply get_finite_support_function in F as (ζ&F&E);auto.
      exists ζ;split;auto;split;[|split;[|split]];unfold is_premorphism in *;
        repeat setoid_rewrite <- (E _); auto.
      intros x I;rewrite <- E;apply Int,I.
  Qed.
  
End dec_hom.
(* begin hide *)

Ltac list_edges_simpl :=
  autounfold in *;simpl in *;
  repeat match goal with
         | [ h : In _ (map _ (map _ _)) |- _ ] =>
           rewrite map_map in h
         | [ h : In _ (_ ++ _) |- _ ] =>
           apply in_app_iff in h as [h|h]
         | [ h : In _ (rm ?e _ _) |- _ ] =>
           let N := fresh "N" in
           apply (in_rm PeanoNat.Nat.eqb_eq) in h as (N&h)
         | [ h : In _ (bimap _ _ _ ) |- _ ] => 
           let u1 := fresh "u" in
           let u2 := fresh "u" in
           let h1 := fresh "h" in
           let E := fresh "E" in
           apply in_bimap in h as (u1&u2&E&h&h1);
           try rewrite <- E in *
         | [ h : In _ (map _ _) |- _ ] =>
           let x := fresh "x" in
           let y := fresh "y" in
           let Ex := fresh "Ex" in
           let Ey := fresh "Ey" in
           apply in_map in h as (x&y&h&Ex&Ey);
           try rewrite<- Ex in *;try rewrite<- Ey in *
         | [ h : In _ (List.map _ _) |- _ ] =>
           let x := fresh "x" in
           let Ex := fresh "E" in
           apply in_map_iff in h as (x&Ex&h);
           try rewrite<- Ex in *
         end;
  repeat ((setoid_rewrite map_map)
          || (setoid_rewrite in_app_iff)
          || (setoid_rewrite in_map)
          || (setoid_rewrite in_map_iff)
          || (setoid_rewrite (in_rm PeanoNat.Nat.eqb_eq))).

(* end hide *)

(*  LocalWords:  premorphism Premorphisms premorphisms
 *)
