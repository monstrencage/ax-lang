Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Import tools rklm lklc language completeness.
Open Scope expr.
Section language_algebra.
  Hypothesis A : Type.
  Hypothesis join : A -> A -> A.
  Hypothesis prod : A -> A -> A.
  Hypothesis meet : A -> A -> A.
  Hypothesis inv : A -> A.
  Hypothesis iter : A -> A.
  Hypothesis bottom : A.
  Hypothesis unit : A.
  Hypothesis eqA : A -> A -> Prop.

  Class lang_sig (eqA : A -> A -> Prop) join prod meet inv iter :=
    { eqA_Equivalence :> Equivalence eqA;
      join_eqA :> Proper (eqA ==> eqA ==> eqA) join;
      meet_eqA :> Proper (eqA ==> eqA ==> eqA) meet;
      prod_eqA :> Proper (eqA ==> eqA ==> eqA) prod;
      inv_eqA :> Proper (eqA ==> eqA) inv;
      iter_eqA :> Proper (eqA ==> eqA) iter }.
  Hint Constructors lang_sig.
  Hypothesis lang_sig : lang_sig eqA join prod meet inv iter.
  Notation " e ⌣ " := (inv e) (at level 10).
  Notation " e † " := (iter e) (at level 15).
  Infix " • " := prod (at level 40).
  Infix " ∧ " := meet (at level 50).
  Infix " ∨ " := join (at level 60).
  Infix " == " := eqA (at level 80).
  Class lang_alg :=
    { axiom_conv_1 : (unit ⌣) == unit;
      axiom_conv_0 : (bottom ⌣) == bottom;
      axiom_conv_plus e f: ((e ∨ f)⌣) == (e⌣ ∨ f⌣);
      axiom_conv_fois e f: ((e • f)⌣) == (f⌣ • e⌣);
      axiom_conv_inter e f: ((e∧f)⌣) == (e⌣ ∧ f⌣);
      axiom_conv_conv e : (e ⌣ ⌣) == e;
      axiom_conv_iter e : e †⌣ == e⌣†; 
      axiom_plus_com e f : (e∨f) == (f∨e);
      axiom_plus_idem e : (e∨e) == e;
      axiom_plus_ass e f g : (e∨(f∨g)) == ((e∨f)∨g);
      axiom_plus_0 e : (e∨bottom) == e;
      axiom_fois_0 e : (e•bottom) == bottom;
      axiom_0_fois e : (bottom•e) == bottom;
      axiom_plus_fois e f g: ((e ∨ f)•g) == (e•g ∨ f•g);
      axiom_fois_plus e f g: (e•(f ∨ g)) == (e•f ∨ e•g);
      axiom_inter_0 e : (e∧bottom) == bottom ;
      axiom_plus_inter e f g: ((e ∨ f)∧g) == (e∧g ∨ f∧g);
      axiom_inter_plus e f : ((e∧f)∨e) == e;
      axiom_fois_assoc e f g : (e•(f • g)) == ((e•f)•g);
      axiom_fois_1 e : (unit•e) == e;
      axiom_1_fois e : (e•unit) == e;
      axiom_inter_assoc e f g : (e∧(f ∧ g)) == ((e∧f)∧g);
      axiom_inter_comm e f : (e∧f) == (f∧e);
      axiom_inter_idem e : (e ∧ e) == e;
      axiom_iter_left e : (e†) == (e ∨ e • e†);
      axiom_iter_right e :  (e†) == (e ∨ e† • e);
      axiom_inter_1_fois e f : (unit ∧ (e•f)) == (unit ∧ (e ∧ f));
      axiom_inter_1_conv e : (unit ∧ (e⌣)) == (unit ∧ e);
      axiom_inter_1_comm_fois e f : ((unit ∧ e)•f) == (f•(unit ∧ e));
      axiom_inter_1_inter e f g : (((unit ∧ e)•f) ∧ g) == ((unit ∧ e)•(f ∧ g));
      axiom_inter_1_iter e f g :  ((g ∨ (unit ∧ e) • f)†) == (g† ∨ (unit ∧ e) • (g ∨ f)†);
      axiom_right_ind e f : e•f ∨ f == f -> e† • f ∨ f == f;
      axiom_left_ind e f : f • e ∨ f == f -> (f • e† ∨ f == f)
    }.
  Hint Constructors lang_alg.
  Reserved Notation " σ ⎣ e ⎦ " (at level 10).
  
  Fixpoint interpretation (σ : nat -> A) e :=
    match e with
    | 1 => unit
    | 0 => bottom
    | 𝐄_var a => σ a
    | e + f => σ ⎣e⎦ ∨ σ ⎣f⎦
    | e ⋅ f => σ ⎣e⎦ • σ ⎣f⎦
    | e ∩ f => σ ⎣e⎦ ∧ σ ⎣f⎦
    | e ¯ => σ⎣e⎦⌣
    | e ⁺ => σ⎣e⎦†
    end
  where " σ ⎣ e ⎦ " := (interpretation σ e).

  Definition is_lang_alg :=
    forall σ e f, e ≡ f -> σ ⎣ e ⎦ == σ ⎣ f ⎦.

  Lemma is_lang_alg_ax :
    is_lang_alg <-> 
    ((forall σ e f, ax e f -> σ ⎣ e ⎦ == σ ⎣ f ⎦)
     /\ (forall σ e f g h, ax_impl e f g h -> e ≡ f -> σ ⎣ g ⎦ == σ ⎣ h ⎦)).
  Proof.
    split;[intros hyp;split;intros σ e f;[|intros g h];intros hax;
           [apply hyp,eq_ax,hax|intros H;eapply hyp,eq_ax_impl;eauto]|].
    destruct lang_sig.
    intros hyp σ e f E;revert σ;induction E;simpl;intro σ;auto.
    - reflexivity.
    - etransitivity;eauto.
    - symmetry;auto.
    - rewrite IHE,IHE0;reflexivity.
    - rewrite IHE,IHE0;reflexivity.
    - rewrite IHE,IHE0;reflexivity.
    - apply hyp,H.
    - destruct hyp as (hyp1 & hyp2).
      eapply hyp2,H0;assumption.
  Qed.

  Definition is_free_lang_alg :=
    is_lang_alg /\
    (forall e f, (forall σ, σ ⎣ e ⎦ == σ ⎣ f ⎦) -> e ≃ f).
    
  Lemma axiom_is_lang_alg :
    lang_alg -> is_lang_alg.
  Proof.
    destruct lang_sig.
    intros ax σ e f E;induction E;simpl.
    - reflexivity.
    - etransitivity;eauto.
    - symmetry;auto.
    - rewrite IHE,IHE0;reflexivity.
    - rewrite IHE,IHE0;reflexivity.
    - rewrite IHE,IHE0;reflexivity.
    - rewrite IHE;reflexivity.
    - rewrite IHE;reflexivity.
    - destruct H;simpl;apply ax.
    - destruct H;simpl in *;apply ax,IHE.
  Qed.

  Lemma is_free_lang_alg_spec :
    is_free_lang_alg <-> (forall e f, e ≃ f <-> (forall σ, σ ⎣ e ⎦ == σ ⎣ f ⎦)).
  Proof.
    split.
    - intros (h1&h2) e f;split;auto.
      rewrite <- Completeness_of_Reversible_Kleene_Lattices;auto using NatNum.
    - intros h;split.
      + intros e f σ E;apply h,Completeness_of_Reversible_Kleene_Lattices;
          auto using NatNum.
      + intros e f E;rewrite h;auto.
  Qed.

  (* begin hide *)
  
  Lemma unit_subst σ : unit = σ ⎣1⎦.
  Proof. auto. Qed.
  Lemma bottom_subst σ : bottom = σ ⎣0⎦.
  Proof. auto. Qed.
  Lemma union_subst σ e f : σ ⎣e⎦ ∨ σ ⎣f⎦ = σ ⎣e+f⎦.
  Proof. auto. Qed.
  Lemma prod_subst σ e f : σ ⎣e⎦ • σ ⎣f⎦ = σ ⎣e⋅f⎦.
  Proof. auto. Qed.
  Lemma inter_subst σ e f : σ ⎣e⎦ ∧ σ ⎣f⎦ = σ ⎣e∩f⎦.
  Proof. auto. Qed.
  Lemma conv_subst σ e : σ ⎣e⎦ ⌣  = σ ⎣ e ¯⎦.
  Proof. auto. Qed.
  Lemma iter_subst σ e : σ ⎣e⎦ †  = σ ⎣ e ⁺⎦.
  Proof. auto. Qed.

  Ltac factor_subst σ :=
    repeat rewrite (unit_subst σ)
    || rewrite (bottom_subst σ)
    || rewrite union_subst
    || rewrite prod_subst
    || rewrite inter_subst
    || rewrite conv_subst
    || rewrite iter_subst.
  
  Ltac build_subst n M σ1 σ2 e :=
    match e with
    | ?e ⌣ => build_subst n M σ1 σ2 e
    | ?e † => build_subst n M σ1 σ2 e
    | ?e • ?f =>
      let M' := fresh "m" in
      let σ3 := fresh "σ" in
      pose (σ3:= 0%nat); pose (M':= 0%nat);
      build_subst n M' σ1 σ3 e;build_subst M' M σ3 σ2 f
    | ?e ∨ ?f =>
      let M' := fresh "m" in
      let σ3 := fresh "σ" in
      pose (σ3:= 0%nat); pose (M':= 0%nat);
      build_subst n M' σ1 σ3 e;build_subst M' M σ3 σ2 f
    | ?e ∧ ?f =>
      let M' := fresh "m" in
      let σ3 := fresh "σ" in
      pose (σ3:= 0%nat); pose (M':= 0%nat);
      build_subst n M' σ1 σ3 e;build_subst M' M σ3 σ2 f
    | unit => clear M σ2; set (M:=n);set (σ2:=σ1)
    | bottom => clear M σ2; set (M:=n);set (σ2:=σ1)
    | ?e =>
      clear M σ2; set (M:=S n);
      set (σ2:= fun k => if Nat.eqb k n
                         then e
                         else σ1 k)
    end.

  Ltac rm_vars :=
    repeat
      match goal with
      | m := ?x |- _ => unfold m in *;clear m
      end.
  Ltac do_subst σ n m e :=
    match e with
    | ?e ⌣ => do_subst σ n m e
    | ?e † => do_subst σ n m e
    | ?e • ?f =>
      let m' := fresh "m" in
      pose (m':= 0%nat);do_subst σ n m' e;do_subst σ m' m f
    | ?e ∨ ?f =>
      let m' := fresh "m" in
      pose (m':= 0%nat);do_subst σ n m' e;do_subst σ m' m f
    | ?e ∧ ?f =>
      let m' := fresh "m" in
      pose (m':= 0%nat);do_subst σ n m' e;do_subst σ m' m f
    | unit => clear m;pose (m:=n)
    | bottom => clear m;pose (m:=n)
    | ?e =>
      clear m;pose (m:=S n);
        try replace e with (σ ⎣𝐄_var n⎦) by (rm_vars;reflexivity)
    end.
  Ltac reify :=
    match goal with
    | [ |- ?e == ?f ] =>
      let M1 := fresh "m" in
      let σ := fresh "σ" in
      pose (M1:=0%nat);pose (σ:=0%nat);
      build_subst 0%nat M1 (fun k:nat => bottom) σ (e∨f);
      let M3 := fresh "m" in
      pose (M3:=0%nat);do_subst σ 0%nat M3 (e∨f);
      factor_subst σ;
      match goal with
      | m := ?x |- _ =>
        (let h := fresh "h" in assert (σ = x) as h by
               auto;revert h)
      end;rm_vars;match goal with
                  | |- ?x = _ -> _ =>
                    set (σ:=x);intros _
                  end 
    end.

  (* end hide *)

  
  (* Lemma is_lang_alg_axiom : *)
  (*   is_lang_alg -> lang_alg. *)
  (* Proof. *)
  (*   intros h;split;intros;try (now reify;apply h,eq_ax;auto). *)
  (*   - assert (exists (σ : nat -> A) E F, σ⎣E⎦ == e /\ σ⎣F⎦ == f /\ E⋅F ≤ F) *)
  (*       as (σ&E&F&he&hf&Eq) by admit. *)
  (*     destruct lang_sig. *)
  (*     eapply eq_ax_impl in Eq;[|apply ax_right_ind]. *)
  (*     apply (h σ ) in Eq;simpl in Eq;repeat rewrite he in Eq;repeat rewrite hf in Eq. *)
  (*     assumption. *)
  (*   - assert (exists (σ : nat -> A) E F, σ⎣E⎦ == e /\ σ⎣F⎦ == f /\ F⋅E ≤ F) *)
  (*       as (σ&E&F&he&hf&Eq) by admit. *)
  (*     destruct lang_sig. *)
  (*     eapply eq_ax_impl in Eq;[|apply ax_left_ind]. *)
  (*     apply (h σ ) in Eq;simpl in Eq;repeat rewrite he in Eq;repeat rewrite hf in Eq. *)
  (*     assumption. *)
  (* Qed. *)
  
End language_algebra.
(* Lemma is_lang_alg_equiv A e j p m i s (b u:A) : *)
(*   lang_sig e j p m i s -> *)
(*   is_lang_alg j p m i s b u e <-> lang_alg j p m i s b u e. *)
(* Proof. *)
(*   intro h;split. *)
(*   - apply is_lang_alg_axiom;auto. *)
(*   - apply axiom_is_lang_alg;auto. *)
(* Qed. *)
Lemma lang_sig_𝐄 (X :Set) (d:decidable_set X) :
  lang_sig
    (fun e f => e ≡ f)
    (@𝐄_plus X) (@𝐄_seq X) (@𝐄_inter X)
    (@𝐄_conv X) (@𝐄_iter X).
Proof. split;once (typeclasses eauto). Qed.

Lemma lang_alg_𝐄 (X :Set) (d:decidable_set X) :
  lang_alg
    (@𝐄_plus X) (@𝐄_seq X) (@𝐄_inter X)
    (@𝐄_conv X) (@𝐄_iter X) (@𝐄_zero X) (@𝐄_one X) (fun e f => e ≡ f).
Proof.
  split;auto.
  - intros;eapply eq_ax_impl,H.
    apply ax_right_ind.
  - intros;eapply eq_ax_impl,H.
    apply ax_left_ind.
Qed.
    
Lemma is_lang_alg_equiv E F :
  E ≡ F <-> forall A e j p m i s (b u:A), lang_sig e j p m i s -> lang_alg j p m i s b u e ->
                                   forall σ, e (interpretation j p m i s b u σ E)
                                          (interpretation j p m i s b u σ F).
Proof.
  split.
  - intros Eq;intros A e j p m i s b u sig ax σ.
    apply axiom_is_lang_alg;auto.
  - intros h.
    pose proof (h _ _ _ _ _ _ _ _ _ (lang_sig_𝐄 NatNum) (lang_alg_𝐄 NatNum)) as hyp.
    
    set (σ0 := fun x:nat => 𝐄_var x).
    assert (σ0_spec : forall E, (interpretation (𝐄_plus (X:=nat)) (𝐄_seq (X:=nat))
                                           (𝐄_inter (X:=nat)) 
                                           (𝐄_conv (X:=nat)) (𝐄_iter (X:=nat))
                                           0 1 σ0 E) = E).
    + clear;intro E;induction E;simpl;congruence||reflexivity.
    + rewrite <- (σ0_spec E), <- (σ0_spec F).
      apply hyp.
Qed.

Section languages.
  Hypothesis Σ : Set.
  Notation interpr :=
    (interpretation
       (@language.union Σ) (@language.prod Σ) (@language.intersection Σ)
       (@language.mirror Σ) (@language.iter Σ) (@language.empty Σ) (@language.unit Σ)).
  Definition eqLang (l1 l2 : language Σ) := forall w, l1 w <-> l2 w.
  Lemma eqLang_Equivalence : Equivalence eqLang.
  Proof.
    split;intros;auto.
    - intros e w;tauto.
    - intros e f E w;rewrite (E w);tauto.
    - intros e f g E1 E2 w;rewrite (E1 w),(E2 w);tauto.
  Qed.
  Lemma join_eqLang : Proper (eqLang ==> eqLang ==> eqLang) (@union Σ).
  Proof.
    intros e f E1 g h E2 w;autounfold with *;rewrite (E1 w),(E2 w);tauto.
  Qed.
  Lemma meet_eqLang : Proper (eqLang ==> eqLang ==> eqLang)
                             (@intersection Σ).
  Proof.
    intros e f E1 g h E2 w;autounfold with *;rewrite (E1 w),(E2 w);tauto.
  Qed.
  Lemma prod_eqLang : Proper (eqLang ==> eqLang ==> eqLang)
                             (@language.prod Σ).
  Proof.
    intros e f E1 g h E2 w;autounfold.
    split;intros (u&v&hu&hv&->);exists u;exists v;
    rewrite (E1 u),(E2 v) in *;tauto.
  Qed.
  Lemma inv_eqLang : Proper (eqLang ==> eqLang) (@mirror Σ).
  Proof. intros e f E w;autounfold with *;rewrite (E _);tauto. Qed.

  
  Lemma to_lang_𝐄_interpretation (σ : assignment nat Σ) e :
    ⟦e⟧σ ≃ interpr σ e.
  Proof.
    induction e;simpl;rsimpl;try reflexivity.
  Qed.

  Lemma is_lang_alg_languages :
    is_lang_alg
      (@language.union Σ) (@language.prod Σ) (@language.intersection Σ)
      (@mirror Σ) (@iter Σ) (@language.empty Σ) (@language.unit Σ)
      eqLang.
  Proof.
    unfold is_lang_alg.
    setoid_rewrite Completeness_of_Reversible_Kleene_Lattices;auto using NatNum.
    unfold sequiv,semantic_equality.
    setoid_rewrite <- to_lang_𝐄_interpretation;unfold eqLang.
    intros σ e f hyp w;rewrite (hyp _ _ w);tauto.
  Qed.
  
End languages.
Lemma is_lang_alg_𝐄 (X :Set) (d:decidable_set X) :
  is_lang_alg
    (@𝐄_plus X) (@𝐄_seq X) (@𝐄_inter X)
    (@𝐄_conv X) (@𝐄_iter X) (@𝐄_zero X) (@𝐄_one X) (fun e f => e ≡ f).
Proof.
  apply axiom_is_lang_alg;eauto;split;auto; once (typeclasses eauto)||intros;eauto.
Qed.
    
  
Section funky_alg.
  Hypothesis A : Type.
  Hypothesis B : Type.
  Hypothesis eqA : A -> A -> Prop.
  Hypothesis join : A -> A -> A.
  Hypothesis prod : A -> A -> A.
  Hypothesis meet : A -> A -> A.
  Hypothesis inv : A -> A.
  Hypothesis itr : A -> A.
  Hypothesis bottom : A.
  Hypothesis unit : A.
  Hypothesis eqB : B -> B -> Prop.
  Hypothesis π : A -> B.
  Hypothesis ρ : B -> A.

  Class funky_alg_sig
        (eqA : A -> A -> Prop) (join prod meet : A -> A -> A)
        (inv itr : A -> A) (bottom unit : A)
        (eqB : B -> B -> Prop) (π : A -> B) (ρ : B -> A)
    := { funky_lang_sig :> lang_sig eqA join prod meet inv itr;
         eqB_Equivalence : Equivalence eqB}.
  
  Hypothesis funky : funky_alg_sig eqA join prod meet inv itr
                                   bottom unit eqB π ρ.
  
  Infix " == " := eqA (at level 80).
  Notation " e ⌣ " := (inv e) (at level 10).
  Notation " e † " := (itr e) (at level 10).
  Infix " • " := prod (at level 40).
  Infix " ∧ " := meet (at level 50).
  Infix " ∨ " := join (at level 60).
  Notation " ⊤ " := unit.
  Notation " ⊥ " := bottom.

  Infix " ==' " := eqB (at level 80).

  Class funky_alg :=
    { ρ_morph : Proper (eqB ==> eqA) ρ;
      ρπ : forall a, ρ (π a) == ⊤ ∧ a;
      ρ_itr a : (ρ a)† == ρ a ;
      π_prod : forall a b, π (a • b) =='  π (a ∧ b);
      π_inv : forall a, π (a⌣) ==' π a;
      funky_ax_conv_1 : ⊤ ⌣ == ⊤ ;
      funky_ax_conv_0 : ⊥ ⌣ == ⊥ ;
      funky_ax_conv_plus e f: (e ∨ f)⌣ == e⌣ ∨ f⌣ ;
      funky_ax_conv_fois e f: ((e • f)⌣) == (f⌣ • e⌣) ;
      funky_ax_conv_inter e f: ((e∧f)⌣) == (e⌣ ∧ f⌣) ;
      funky_ax_conv_conv e : (e ⌣ ⌣) == e ;
      funky_ax_conv_itr e : (e † ⌣) == e ⌣ †;
      funky_ax_plus_com e f : (e∨f) == (f∨e) ;
      funky_ax_plus_idem e : (e∨e) == e ;
      funky_ax_plus_ass e f g : (e∨(f∨g)) == ((e∨f)∨g) ;
      funky_ax_plus_0 e : (e∨⊥) == e ;
      funky_ax_fois_0 e : (e•⊥) == ⊥ ;
      funky_ax_0_fois e : (⊥•e) == ⊥ ;
      funky_ax_plus_fois e f g: ((e ∨ f)•g) == (e•g ∨ f•g) ;
      funky_ax_fois_plus e f g: (e•(f ∨ g)) == (e•f ∨ e•g) ;
      funky_ax_inter_0 e : (e∧⊥) == ⊥ ;
      funky_ax_plus_inter e f g: ((e ∨ f)∧g) == (e∧g ∨ f∧g) ;
      funky_ax_inter_plus e f : ((e∧f)∨e) == e ;
      funky_ax_fois_assoc e f g : (e•(f • g)) == ((e•f)•g) ;
      funky_ax_fois_1 e : (⊤•e) == e ;
      funky_ax_1_fois e : (e•⊤) == e ;
      funky_ax_inter_assoc e f g : (e∧(f ∧ g)) == ((e∧f)∧g) ;
      funky_ax_inter_comm e f : (e∧f) == (f∧e) ;
      funky_ax_inter_idem e : (e ∧ e) == e ;
      funky_ax_ρ_prod a b : (a • (ρ b)) == ((ρ b) • a) ;
      funky_ax_ρ_prod_meet a b c : (((ρ b) • a) ∧ c) == ((ρ b) • (a ∧ c)) ;

      funky_ax_inter_1_iter e f g : ((g ∨ (ρ e) • f)†) == (g† ∨ (ρ e) • (g ∨ f)†);

      funky_ax_iter_left e : (e†) == (e ∨ e • e†);
      funky_ax_iter_right e :  (e†) == (e ∨ e† • e);
      funky_ax_right_ind e f : e•f ∨ f == f -> e† • f ∨ f == f;
      funky_ax_left_ind e f : f • e ∨ f == f -> (f • e† ∨ f == f)
    }.

  Lemma funky_is_lang_alg :
    funky_alg ->
    is_lang_alg join prod meet inv itr bottom unit eqA.
  Proof.
    intro funky_alg;
    apply axiom_is_lang_alg;auto;try apply funky.
    split;try (now apply funky_alg;auto);
    destruct funky ;destruct funky_lang_sig0;
    pose proof ρ_morph;intros. 
    - now rewrite <-ρπ,π_prod,ρπ.
    - now rewrite <-ρπ,π_inv,ρπ.
    - rewrite <- ρπ;symmetry;apply funky_alg.
    - rewrite <- ρπ;apply funky_alg.
    - rewrite <- ρπ;apply funky_alg.
  Qed.
End funky_alg.

Lemma funky_𝐄 (X : Set) :
  funky_alg_sig (fun e f => e≡f) (@𝐄_plus X) (@𝐄_seq X)
                (@𝐄_inter X) (@𝐄_conv X) (@𝐄_iter X) (@𝐄_zero X) (@𝐄_one X)
                (fun e f => 1 ∩ e ≡ 1 ∩ f)
                (fun e => e) (fun e => 1 ∩ e).
Proof.
  split;[split;once (typeclasses eauto)|];auto.
  split;auto;intros e f g -> ->;reflexivity.
Qed.

Lemma funky_alg_𝐄 (X : Set) :
  funky_alg (fun e f => e ≡ f) (@𝐄_plus X) (@𝐄_seq X) (@𝐄_inter X)
            (@𝐄_conv X) (@𝐄_iter X) (@𝐄_zero X) (@𝐄_one X)
            (fun e f => 1 ∩ e ≡ 1 ∩ f)
            (fun e => e) (fun e => 1 ∩ e).
Proof.
  split;auto.
  - intros e f E;tauto.
  - intros;apply smaller_PartialOrder;split;unfold Basics.flip;[|apply iter_incr].
    rewrite ax_iter_left.
    apply plus_inf;[reflexivity|].
    apply left_ind.
    rewrite inter_1_abs,ax_inter_idem;reflexivity.
  - intros;eauto.
  - intros;eauto.
Qed.

Section lang_to_funky.
  Hypothesis A : Type.
  Hypothesis join : A -> A -> A.
  Hypothesis prod : A -> A -> A.
  Hypothesis meet : A -> A -> A.
  Hypothesis inv : A -> A.
  Hypothesis itr : A -> A.
  Hypothesis bottom : A.
  Hypothesis unit : A.
  Hypothesis eqA : A -> A -> Prop.

  Notation " e ⌣ " := (inv e) (at level 10).
  Notation " e † " := (itr e) (at level 10).
  Infix " • " := prod (at level 40).
  Infix " ∧ " := meet (at level 50).
  Infix " ∨ " := join (at level 60).
  Infix " == " := eqA (at level 80).

  Hypothesis lang_sig : lang_sig eqA join prod meet inv itr.
  Hypothesis lang_alg : lang_alg join prod meet inv itr bottom unit eqA.

  Lemma lang_alg_is_funky_alg_sig :
  funky_alg_sig eqA join prod meet inv itr bottom unit 
                (fun e f => unit ∧ e == unit ∧ f)
                (fun e => e) (fun e => unit ∧ e).
  Proof.
    destruct lang_sig;split;split;auto.
    - intros e;reflexivity.
    - intros e f ->;reflexivity.
    - intros e f g -> -> ;reflexivity.
  Qed.
  
  Lemma lang_alg_is_funky_alg :
    funky_alg eqA join prod meet inv itr bottom unit 
              (fun e f => unit ∧ e == unit ∧ f)
              (fun e => e) (fun e => unit ∧ e).
  Proof.
    destruct lang_sig;split;intros;
    try (reflexivity || now apply lang_alg).
    - intros e f;tauto.
    - transitivity (((unit ∧ a) • (unit ∧ a) †) ∨ (unit ∧ a)).
      + transitivity ((unit ∧ a) ∨ ((unit ∧ a) • (unit ∧ a) †));apply lang_alg.
      + eapply axiom_left_ind;[eassumption|].
        transitivity ((unit ∧ a) ∨ (unit ∧ a));[|apply lang_alg].
        apply join_eqA0;[|reflexivity].
        transitivity (((unit ∧ a)•unit)∧ a);[symmetry;apply lang_alg|].
        transitivity ((unit ∧ a)∧ a);[apply meet_eqA0;[apply lang_alg|reflexivity]|].
        transitivity (unit ∧ (a∧ a));[symmetry;apply lang_alg|].
        apply meet_eqA0;[reflexivity|apply lang_alg].
    - symmetry;apply lang_alg.
  Qed.

End lang_to_funky.
Section powermonoid.
  Hypothesis M : Type.
  Hypothesis prod : M -> M -> M.
  Infix " • " := prod (at level 40).
  Hypothesis ε : M.
  Hypothesis inv : M -> M.
  Notation " e ⌣ " := (inv e) (at level 10).
  Hypothesis left_neutrality : forall x, ε • x = x.
  Hypothesis right_neutrality : forall x, x • ε = x.
  Hypothesis associativity : forall x y z, x • (y • z) = (x • y) • z.
  Hypothesis inv_ε : ε⌣=ε.
  Hypothesis inv_inv : forall x, x⌣⌣=x.
  Hypothesis inv_prod : forall x y, (x•y)⌣=y⌣•x⌣.
  Hypothesis ε_not_prod : forall x y, x•y = ε -> x = ε /\ y = ε.

  Definition 𝒜 := M -> Prop.
  
  Definition Sngl x : 𝒜 := fun y => y = x.
  Notation " {{ x }} " := (Sngl x).
  
  Definition Unit := {{ ε }}.
  Notation " 𝟣 " := Unit.

  Definition ø : 𝒜 := fun _ => False.

  Definition Meet : 𝒜 -> 𝒜 -> 𝒜 := (fun a b x => a x /\ b x).
  Infix " ∧ " := Meet (at level 50).

  Definition Join : 𝒜 -> 𝒜 -> 𝒜 := (fun a b x => a x \/ b x).
  Infix " ∨ " := Join (at level 60).

  Definition Prod : 𝒜 -> 𝒜 -> 𝒜 := (fun a b x => exists y z, a y /\ b z /\ x = y • z).
  Infix " @ " := Prod (at level 60).
             
  Definition Flip : 𝒜 -> 𝒜 := (fun a y => exists x, a x /\ y = x ⌣).
  Notation " a ♯ " := (Flip a) (at level 20).

  Fixpoint Exp (a : 𝒜) (n : nat) : 𝒜 :=
    match n with
    | O => Unit
    | S n => a @ (a ^ n)
    end
  where " a ^ n " := (Exp a n).

  Definition Iter : 𝒜 -> 𝒜 := fun a x => exists n, (a ^ (S n)) x.
  Notation " e † " := (Iter e) (at level 30).

  Definition Incl : relation 𝒜 := (fun a b : M -> Prop  => forall x, a x -> b x).
  Infix " ⊑ " := Incl (at level 80).

  Definition Eq : relation 𝒜 := (fun a b : M -> Prop  => forall x, a x <-> b x).
  Infix " == " := Eq (at level 80).

  
  Ltac booya :=
    unfold Unit,Sngl,ø,Meet,Join,Prod,Flip,Eq;firstorder.
    
  Lemma emptyset_inverse : ø == (ø ♯).
  Proof. booya. Qed.
  
  Lemma add_inverse x X :  X♯ ∨ {{ x⌣ }} == (X ∨ {{x}}) ♯.
  Proof. booya; subst;eauto. Qed.

  Instance Proper_Exp : Proper (Eq ==> eq ==> Eq) Exp.
  Proof.
    clear;intros a b E n m <-;induction n;simpl.
    - intro;tauto.
    - revert E IHn;booya.
  Qed.

  Global Instance powermon_lang_sig : lang_sig Eq Join Prod Meet Flip Iter.
  Proof.
    split;try booya.
    unfold Iter;intros x y E w;split;intros (n&In);exists n;eapply Proper_Exp;
      try reflexivity||eassumption.
    intro k;rewrite (E k);reflexivity.
  Qed.

  Lemma inv_bij x y : x ⌣ = y ⌣ -> x = y.
  Proof. intro E;rewrite <- inv_inv,<-E,inv_inv;reflexivity. Qed.

  Lemma powermon_prod_ass e f g : (e@(f @ g)) == ((e@f)@g).
  Proof.
    unfold Prod,Eq.
    intros x;split.
    - intros (x1&w'&I1&(x2&x3&I2&I3&->)&->).
      exists (x1•x2),x3;split;[exists x1,x2|];repeat split;auto.
    - intros (w'&x3&(x1&x2&I1&I2&->)&I3&->);exists x1,(x2•x3);split;[|split;[exists x2,x3|]];auto.
  Qed.

  Lemma powermon_fois_1 e : (𝟣 @ e) == e.
  Proof. booya;subst;rewrite left_neutrality;auto. Qed.

  Lemma powermon_1_fois e : (e @ 𝟣) == e.
  Proof. booya;subst;rewrite right_neutrality;auto. Qed.

  Lemma powermon_plus_com e f : (e∨f) == (f∨e).
  Proof. booya. Qed.

  Lemma powermon_plus_idem e : (e∨e) == e.
  Proof. booya. Qed.

  Lemma powermon_plus_ass e f g : (e∨(f∨g)) == ((e∨f)∨g).
  Proof. booya. Qed.

  Lemma powermon_plus_0 e : (e∨ø) == e.
  Proof. booya. Qed.
  
  Lemma powermon_inter_assoc e f g : (e∧(f ∧ g)) == ((e∧f)∧g).
  Proof. booya. Qed.
  
  Lemma powermon_inter_comm e f : (e∧f) == (f∧e).
  Proof. booya. Qed.
  
  Lemma powermon_inter_idem e : (e ∧ e) == e.
  Proof. booya. Qed.
  
  Lemma powermon_fois_0 e : (e@ø) == ø.
  Proof. booya. Qed.
  
  Lemma powermon_0_fois e : (ø@e) == ø.
  Proof. booya. Qed.
  
  Lemma powermon_plus_fois e f g: ((e ∨ f)@g) == (e@g) ∨ (f@g).
  Proof. booya. Qed.
  
  Lemma powermon_fois_plus e f g: (e@(f ∨ g)) == (e@f) ∨ (e@g).
  Proof. booya. Qed.
  
  Lemma powermon_inter_0 e : (e∧ø) == ø.
  Proof. booya. Qed.
  
  Lemma powermon_plus_inter e f g: ((e ∨ f)∧g) == (e∧g) ∨ (f∧g).
  Proof. booya. Qed.
  
  Lemma powermon_inter_plus e f : ((e∧f)∨e) == e.
  Proof. booya. Qed.
  
  Lemma powermon_conv_1 : (𝟣 ♯) == 𝟣.
  Proof.
    intros x;split.
    - intros (y&->&->).
      rewrite inv_ε;reflexivity.
    - intros -> ;exists ε;split;[reflexivity|symmetry;apply inv_ε].
  Qed.
  
  Lemma powermon_conv_0 : (ø ♯) == ø.
  Proof. booya. Qed.
  
  Lemma powermon_conv_plus e f: ((e ∨ f)♯) == (e♯ ∨ f♯).
  Proof. booya. Qed.
  
  Lemma powermon_conv_fois e f: ((e @ f)♯) == (f♯ @ e♯).
  Proof.
    intros x;split.
    - intros (x'&(y&z&Iy&Iz&->)&->).
      rewrite inv_prod.
      repeat split||eexists;auto.
    - intros (y'&z'&(y&Iy&->)&(z&Iz&->)&->).
      repeat split||eexists;eauto.
  Qed.
  
  Lemma powermon_conv_inter e f: ((e∧f)♯) == (e♯ ∧ f♯).
  Proof.
    booya.
    rewrite H1 in H2;apply inv_bij in H2;subst;firstorder.
  Qed.
  
  Lemma powermon_conv_conv e : (e ♯ ♯) == e.
  Proof.
    booya.
    - subst;eauto.
      rewrite inv_inv;auto.
    - exists (x⌣);rewrite inv_inv;firstorder.
  Qed.
  
  Lemma Exp_last e n : e ^ (S n) == e ^ n @ e.
  Proof.
    induction n;simpl.
    - rewrite powermon_1_fois,powermon_fois_1;reflexivity.
    - rewrite IHn at 1;apply powermon_prod_ass.
  Qed.  

  Lemma powermon_conv_Exp e n : (e ^ n) ♯ == (e ♯)^n.
  Proof.
    induction n;intros;simpl.
    - apply powermon_conv_1.
    - rewrite (Exp_last e n),powermon_conv_fois,IHn at 1;reflexivity.
  Qed.

  Lemma powermon_conv_iter e : e †♯ == e♯†.
  Proof.
    unfold Iter;intro x;split.
    - intros (y&(n&I)&->);exists n;apply powermon_conv_Exp;exists y;auto.
    - intros (n&I);apply powermon_conv_Exp in I as (y&I&->);exists y;eauto.
  Qed.

  Lemma powermon_iter_left e : e† == e ∨ (e @ e†).
  Proof.
    intro x;split.
    - intros ([|n]&In).
      + left;destruct In as (?&?&I&->&->).
        rewrite right_neutrality;auto.
      + right.
        destruct In as (x1&x2&I1&I2&->).
        exists x1,x2;repeat split;auto.
        exists n;auto.
    - intros [I|(x1&x2&I1&(n&I2)&->)].
      + exists O,x,ε;rewrite right_neutrality;repeat split;auto.
      + exists (S n),x1,x2;tauto.
  Qed.

  Lemma powermon_iter_right e :  e† == e ∨ (e† @ e).
  Proof.
    intro x;split.
    - intros ([|n]&In).
      + left;destruct In as (?&?&I&->&->).
        rewrite right_neutrality;auto.
      + right.
        apply Exp_last in In.
        destruct In as (x1&x2&I1&I2&->).
        exists x1,x2;repeat split;auto.
        exists n;auto.
    - intros [I|(x1&x2&(n&I1)&I2&->)].
      + exists O,x,ε;rewrite right_neutrality;repeat split;auto.
      + exists (S n).
        apply Exp_last.
        exists x1,x2;tauto.
  Qed.

  Lemma powermon_right_ind e f : (e@f) ∨ f == f -> (e† @ f) ∨ f == f.
  Proof.
    intros hyp x;split;[|intro;right;tauto].
    intros [(x1&x2&(n&I1)&I2&->)|I];[|tauto].
    revert x1 x2 I1 I2;induction n.
    - intros x1 x2 (?&?&I1&->&->) I2.
      rewrite right_neutrality.
      apply hyp;left;exists x,x2;auto.
    - intros w x3 I I3.
      apply Exp_last in I as (x1&x2&I1&I2&->).
      rewrite <- associativity.
      apply IHn;auto.
      apply hyp;left;exists x2,x3;auto.
  Qed.

  Lemma powermon_left_ind e f : (f @ e) ∨ f == f -> (f @ e†) ∨ f == f.
  Proof.
    intros hyp x;split;[|intro;right;tauto].
    intros [(x1&x2&I1&(n&I2)&->)|I];[|tauto].
    revert x1 x2 I1 I2;induction n.
    - intros x1 x I1 (x2&?&I2&->&->).
      rewrite right_neutrality.
      apply hyp;left;exists x1,x2;auto.
    - intros x1 w I1 (x2&x3&I2&I3&->).
      rewrite associativity.
      apply IHn;auto.
      apply hyp;left;exists x1,x2;auto.
  Qed.
    
  Lemma powermon_inter_1_fois e f : 𝟣 ∧ (e@f) == 𝟣 ∧ (e ∧ f).
  Proof.
    intros x;split.
    - intros (->&x1&x2&I1&I2&E).
      symmetry in E;apply ε_not_prod in E as (->&->).
      repeat split;auto.
    - intros (->&I1&I2);split;[reflexivity|].
      exists ε,ε;rewrite left_neutrality;auto.
  Qed.

  Lemma powermon_inter_1_conv e : 𝟣 ∧ (e♯) == 𝟣 ∧ e.
  Proof.
    intro x;split.
    - intros (->&y&I&E).
      rewrite <- inv_ε in E;apply inv_bij in E as <-.
      repeat split;auto.
    - intros (->&I);repeat split.
      exists ε;repeat split;auto.
  Qed.

  Lemma powermon_inter_1_comm_fois e f : (𝟣 ∧ e)@f == f@(𝟣 ∧ e).
  Proof.
    intros x;split.
    - intros (?&w&(->&Ie)&If&->).
      exists w,ε;repeat split;auto.
      rewrite left_neutrality,right_neutrality;reflexivity.
    - intros (w&?&If&(->&Ie)&->).
      exists ε,w;repeat split;auto.
      rewrite left_neutrality,right_neutrality;reflexivity.
  Qed.

  Lemma powermon_inter_1_inter e f g : (((𝟣 ∧ e)@f) ∧ g) == ((𝟣 ∧ e)@(f ∧ g)).
  Proof.
    intro x;split.
    - intros ((?&w&(->&Ie)&If&->)&Ig).
      rewrite left_neutrality in *.
      exists ε,w;rewrite left_neutrality;repeat split;auto.
    - intros (?&w&(->&Ie)&(If&Ig)&->).
      repeat split||rewrite left_neutrality in *||exists ε,w||assumption.
  Qed.

  Lemma Exp_incr_left e f n : e ^ n ⊑ (e ∨ f) ^ n.
  Proof.
    induction n;intros x;simpl;auto.
    intros (x1&x2&I1&I2&->).
    apply IHn in I2;exists x1,x2;repeat split;auto.
    now left.
  Qed.
  
  Lemma Exp_incr_right e f n : f ^ n ⊑ (e ∨ f) ^ n.
  Proof.
    induction n;intros x;simpl;auto.
    intros (x1&x2&I1&I2&->).
    apply IHn in I2;exists x1,x2;repeat split;auto.
    now right.
  Qed.
  
  Lemma powermon_inter_1_iter e f g :
    (g ∨ ((𝟣 ∧ e) @ f)) † ==  g† ∨ ((𝟣 ∧ e) @ (g ∨ f)†).
  Proof.
    intro x;split.
    - intros (n&In).
      revert x In;induction n;intro x.
      + intros (x'&?&I&->&->);rewrite right_neutrality.
        destruct I as [I|(?&y&(->&Ie)&If&->)].
        * left;exists O,x',ε;rewrite right_neutrality;repeat split;auto.
        * right;exists ε,y;repeat split;auto.
          exists O,y,ε;rewrite right_neutrality;repeat split.
          right;assumption.
      + intros (x1&x2&I1&I2&->).
        apply IHn in I2.
        destruct I1 as [I1|(?&y1&(->&Ie1)&If1&->)],
                       I2 as [I2|I2].
        * destruct I2 as (m&Im);left.
          exists (S m),x1,x2;auto.
        * destruct I2 as (?&y&(->&Ie2)&If2&->).
          rewrite left_neutrality.
          right.
          exists ε,(x1•y).
          rewrite left_neutrality.
          repeat split;auto.
          destruct If2 as (m&Im).
          exists (S m),x1,y;repeat split;auto.
          left;auto.
        * rewrite left_neutrality.
          right.
          exists ε,(y1•x2).
          rewrite left_neutrality.
          repeat split;auto.
          destruct I2 as (m&Im).
          exists (S m),y1,x2;repeat split;auto.
          -- right;auto.
          -- apply Exp_incr_left,Im.
        * rewrite left_neutrality.
          right.
          exists ε,(y1•x2).
          rewrite left_neutrality.
          repeat split;auto.
          destruct I2 as (?&y2&(->&Ie2)&(m&If2)&->).
          rewrite left_neutrality.
          exists (S m),y1,y2;repeat split;auto.
          now right.
    - intros [(n&I)|I].
      + exists n;apply Exp_incr_left,I.
      + destruct I as (?&y&(->&Ie)&I&->).
        rewrite left_neutrality.
        destruct I as (n&I);revert y I;induction n;simpl;auto.
        * intros y (w&?&[I|I]&->&->);rewrite right_neutrality.
          -- exists O,w,ε;repeat split;auto.
             now left.
          -- exists O,w,ε;repeat split;auto.
             right;exists ε,w;rewrite left_neutrality;repeat split;auto.
        * intros y (y1&y2&I1&I2&->).
          apply IHn in I2.
          apply powermon_iter_left.
          right.
          exists y1,y2;repeat split;auto.
          destruct I1 as [I1|I1].
          -- left;auto.
          -- right;exists ε,y1;rewrite left_neutrality;repeat split;auto.
  Qed.
  
  Lemma powermon_lang_alg :
    lang_alg Join Prod Meet Flip Iter ø 𝟣 Eq.
  Proof.
    split.
    - apply powermon_conv_1.
    - apply powermon_conv_0.
    - apply powermon_conv_plus.
    - apply powermon_conv_fois.
    - apply powermon_conv_inter.
    - apply powermon_conv_conv.
    - apply powermon_conv_iter.
    - apply powermon_plus_com.
    - apply powermon_plus_idem.
    - apply powermon_plus_ass.
    - apply powermon_plus_0.
    - apply powermon_fois_0.
    - apply powermon_0_fois.
    - apply powermon_plus_fois.
    - apply powermon_fois_plus.
    - apply powermon_inter_0.
    - apply powermon_plus_inter.
    - apply powermon_inter_plus.
    - apply powermon_prod_ass.
    - apply powermon_fois_1.
    - apply powermon_1_fois.
    - apply powermon_inter_assoc.
    - apply powermon_inter_comm.
    - apply powermon_inter_idem.
    - apply powermon_iter_left.
    - apply powermon_iter_right.
    - apply powermon_inter_1_fois.
    - apply powermon_inter_1_conv.
    - apply powermon_inter_1_comm_fois.
    - apply powermon_inter_1_inter.
    - apply powermon_inter_1_iter.
    - apply powermon_right_ind.
    - apply powermon_left_ind.
  Qed.
End powermonoid.

Close Scope expr.