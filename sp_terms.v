(** This module deals with series-parallel terms. *)

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Export tools language.

(** * Terms *)
Section s0.
  Variable X : Set.

  (** Unsurprisingly, here terms are built out of variables and two
  binary operations. *)
  Inductive 𝐒𝐏 : Set :=
  | 𝐒𝐏_var : X -> 𝐒𝐏
  | 𝐒𝐏_seq : 𝐒𝐏 -> 𝐒𝐏 -> 𝐒𝐏
  | 𝐒𝐏_par : 𝐒𝐏 -> 𝐒𝐏 -> 𝐒𝐏.

End s0.
(** In the following, [⨾] denotes the sequential composition, and [∥]
the parallel composition.*)
Infix " ⨾ " := 𝐒𝐏_seq (at level 40).
Infix " ∥ " := 𝐒𝐏_par (at level 50).
Section s.
  Context {X : Set}.

  (** Set of variables of a term. Being associated with the class
  [Alphabet], we can write the set of variables of a term [e] as
  [𝒱 e].*)

  Global Instance 𝐒𝐏_variables : Alphabet 𝐒𝐏 X :=
    fix 𝐒𝐏_variables e :=
      match e with
      | 𝐒𝐏_var a => [a]
      | e ⨾ f | e ∥ f => 𝐒𝐏_variables e ++ 𝐒𝐏_variables f
      end.
  (* begin hide *)
  Lemma 𝐒𝐏_variables_var a : 𝒱 (𝐒𝐏_var a) = [a].
  Proof. reflexivity. Qed.
  Lemma 𝐒𝐏_variables_seq e f : 𝒱 (e⨾f) = 𝒱 e++𝒱 f.
  Proof. reflexivity. Qed.
  Lemma 𝐒𝐏_variables_par e f : 𝒱 (e∥f) = 𝒱 e ++𝒱 f .
  Proof. reflexivity. Qed.
  Hint Rewrite 𝐒𝐏_variables_var 𝐒𝐏_variables_seq 𝐒𝐏_variables_par
    : simpl_typeclasses.
  (* end hide *)

  (** We define the size of a term as the number of nodes in its
  syntax tree. *)
  Global Instance 𝐒𝐏_size : Size (𝐒𝐏 X) :=
    fix 𝐒𝐏_size (u : 𝐒𝐏 X) : nat :=
      match u with
      | 𝐒𝐏_var _  => S O
      | u ⨾ v | u ∥ v => S (𝐒𝐏_size u + 𝐒𝐏_size v)
      end.
  (* begin hide *)
  Lemma 𝐒𝐏_size_var a : | 𝐒𝐏_var a | = S O.
  Proof. reflexivity. Qed.
  Lemma 𝐒𝐏_size_seq u v : | u ⨾ v | = S (|u| + |v|).
  Proof. reflexivity. Qed.
  Lemma 𝐒𝐏_size_par u v : | u ∥ v | = S (|u| + |v|).
  Proof. reflexivity. Qed.
  Hint Rewrite 𝐒𝐏_size_var 𝐒𝐏_size_seq 𝐒𝐏_size_par
    : simpl_typeclasses.
  (* end hide *)
  
  (** The length of the list of variables of a term is smaller than
  its size. *)
  Lemma 𝐒𝐏_size_𝐒𝐏_variables (u : 𝐒𝐏 X) :
    # (𝒱 u)  <= | u |.
  Proof. induction u;rsimpl;auto;repeat rewrite app_length;lia. Qed.

  (** In fact, because the length of the list of variables is the
  number of leaves of the syntax tree of the term, and because that
  tree is binary, we have the following identity: *)
  Lemma 𝐒𝐏_size_𝐒𝐏_variables_eq (u : 𝐒𝐏 X) :
    | u | = 2 * # (𝒱 u)  - 1.
  Proof.
    assert (P : forall u, # (𝒱 u)  > 0)
      by (clear u;induction u;rsimpl;repeat rewrite app_length;lia).
    induction u;rsimpl;auto;repeat rewrite app_length.
    - rewrite IHu1,IHu2.
      pose proof (P u1);pose proof (P u2);lia.
    - rewrite IHu1,IHu2.
      pose proof (P u1);pose proof (P u2);lia.
  Qed.

  (** * Parametric axiomatization *)

  (** For series parallel terms, we don't axiomatize equality but
  instead consider containment. Furthermore, this axiomatization will
  be parametric in a set of variables. This parameter will contain
  the variables that are _super-units_ for the sequential product:
  that is variables [a] such that for any term [u] we have [u≤u⨾a] and
  [u≤a⨾u]. In the following, we call the variables from the parameter
  _tests_. *)
  
  (** The axioms are split into two groups: the equality axioms, which
  can be used in both directions, and the containment axioms which
  must be read from left to right.*)

  (** The equality axioms simply state that [⨾] is associative, and
  that [∥] is commutative and idempotent. *)
  Inductive 𝐒𝐏_ax : 𝐒𝐏 X -> 𝐒𝐏 X -> Prop :=
  | 𝐒𝐏_seq_assoc e f g : 𝐒𝐏_ax (e⨾(f ⨾ g)) ((e⨾f)⨾g)
  | 𝐒𝐏_par_comm e f : 𝐒𝐏_ax (e∥f) (f∥e)
  | 𝐒𝐏_par_idem e : 𝐒𝐏_ax (e ∥ e) e.

  (** The containment axioms state that [∥] is a deceasing operation
  (it is in fact the greatest lower bound), and that is a term is only
  composed of tests, then it is a super-unit. *)
  Inductive 𝐒𝐏_axinf (A : list X) : 𝐒𝐏 X -> 𝐒𝐏 X -> Prop :=
  | 𝐒𝐏_axinf_weak e f : 𝐒𝐏_axinf A (e ∥ f) e
  | 𝐒𝐏_axinf_1_seql e f : 𝒱 e  ⊆ A -> 𝐒𝐏_axinf A f (e ⨾ f)
  | 𝐒𝐏_axinf_1_seqr e f : 𝒱 e  ⊆ A -> 𝐒𝐏_axinf A f (f ⨾ e).

  
  Reserved Notation " A ⊨ e << f " (at level 80).

  (** The relation [A⊨ e << f] is then the smallest order relation
  (that is reflexive and transitive) containing both sets of axioms,
  and such that [⨾] and [∥] are monotone operations. *)
  Inductive 𝐒𝐏_inf (A : list X) : 𝐒𝐏 X -> 𝐒𝐏 X -> Prop :=
  | 𝐒𝐏_inf_re e :  A ⊨ e << e
  | 𝐒𝐏_inf_trans e f g : A ⊨ e << f -> A ⊨ f << g -> A ⊨ e << g
  | 𝐒𝐏_inf_ax e f : 𝐒𝐏_ax e f \/ 𝐒𝐏_ax f e -> A ⊨ e << f
  | 𝐒𝐏_inf_axinf e f: 𝐒𝐏_axinf A e f -> A ⊨ e << f

  | 𝐒𝐏_inf_seq e f g h :
      A ⊨ e << g -> A ⊨ f << h -> A ⊨ e ⨾ f << g ⨾ h
  | 𝐒𝐏_inf_par e f g h :
      A ⊨ e << g -> A ⊨ f << h -> A ⊨ e ∥ f << g ∥ h
  where " A ⊨ e << f " := (𝐒𝐏_inf A e f).
  
  Hint Constructors 𝐒𝐏_inf 𝐒𝐏_ax 𝐒𝐏_axinf.

  Global Instance 𝐒𝐏_inf_PreOrder A : PreOrder (𝐒𝐏_inf A).
  Proof.
    split.
    - now intro e;eauto.
    - now intros e f g;eauto.
  Qed.

  Global Instance par_𝐒𝐏_inf A :
    Proper ((𝐒𝐏_inf A) ==> (𝐒𝐏_inf A) ==> (𝐒𝐏_inf A))
           (@𝐒𝐏_par X).
  Proof. now intros e f hef g h hgh;apply 𝐒𝐏_inf_par. Qed.
  Global Instance seq_𝐒𝐏_inf A :
    Proper ((𝐒𝐏_inf A) ==> (𝐒𝐏_inf A) ==> (𝐒𝐏_inf A))
           (@𝐒𝐏_seq X).
  Proof. now intros e f hef g h hgh;apply 𝐒𝐏_inf_seq. Qed.

  (** [∥] is associative. *)
  Lemma 𝐒𝐏_par_assoc_left A e f g : A ⊨ e∥(f ∥ g) << (e∥f)∥g.
  Proof.
    transitivity ((e∥(f∥g))∥(e∥(f ∥ g)));auto.
    apply 𝐒𝐏_inf_par;auto.
    transitivity ((f∥g)∥e);auto.
    transitivity (f∥g);auto.
    transitivity (g∥f);auto.
  Qed.
    
  Lemma 𝐒𝐏_par_assoc_right A e f g : A ⊨ (e∥f)∥g << e∥(f ∥ g).
  Proof.
    transitivity (((e∥f)∥g)∥((e∥f) ∥ g));auto.
    apply 𝐒𝐏_inf_par;auto.
    - transitivity (e∥f);auto.
    - apply 𝐒𝐏_inf_par;auto.
      transitivity (f∥e);auto.
  Qed.
  
  (** Adding tests only makes the relation grow. *)
  Lemma incl_𝐒𝐏_inf A B e f :
    A ⊆ B -> A ⊨ e << f -> B ⊨ e << f.
  Proof.
    intros I E;induction E;auto.
    - etransitivity;eauto.
    - destruct H;[eauto| |];apply 𝐒𝐏_inf_axinf.
      + apply 𝐒𝐏_axinf_1_seql;rewrite H;auto.
      + apply 𝐒𝐏_axinf_1_seqr;rewrite H;auto.
  Qed.

  (** This relation has a very useful property: if [e] is smaller than
  [f] with tests [A], then the variables in [f] all appear either as
  variables of [e] of as tests. *)
  Lemma 𝐒𝐏_inf_variables (A : list X) e f :
    A ⊨ e << f -> 𝒱 f  ⊆ 𝒱 e  ++ A.
  Proof.
    intro E;induction E;intuition.
    -- rewrite IHE2,IHE1;apply incl_app;[|apply incl_appr];reflexivity.
    -- destruct H0;rsimpl.
       --- apply incl_appl;rewrite app_assoc;reflexivity.
       --- apply incl_appl;intro x;repeat rewrite in_app_iff;tauto.
       --- intro x;repeat rewrite in_app_iff;tauto.
    -- destruct H0;rsimpl.
       --- apply incl_appl;rewrite app_assoc;reflexivity.
       --- apply incl_appl;intro x;repeat rewrite in_app_iff;tauto.
       --- intro x;repeat rewrite in_app_iff;tauto.
    -- destruct H.
       ++ rsimpl;intro x;repeat rewrite in_app_iff;tauto.
       ++ rsimpl;rewrite H.
          simpl;intro x;repeat rewrite in_app_iff;tauto.
       ++ rsimpl;rewrite H.
          simpl;intro x;repeat rewrite in_app_iff;tauto.
    -- rsimpl;rewrite IHE1,IHE2.
       intro x;repeat rewrite in_app_iff;tauto.
    -- rsimpl;rewrite IHE1,IHE2.
       intro x;repeat rewrite in_app_iff;tauto.
  Qed.
                             
End s.
(* begin hide *)
Hint Rewrite @𝐒𝐏_variables_var @𝐒𝐏_variables_seq @𝐒𝐏_variables_par
     @𝐒𝐏_size_var @𝐒𝐏_size_seq @𝐒𝐏_size_par
  : simpl_typeclasses.

Arguments 𝐒𝐏_seq {X}.
Arguments 𝐒𝐏_par {X}.
Notation " A ⊨ e << f " := (𝐒𝐏_inf A e f) (at level 80).
Hint Constructors 𝐒𝐏_inf 𝐒𝐏_ax 𝐒𝐏_axinf.
Hint Resolve 𝐒𝐏_par_assoc_left 𝐒𝐏_par_assoc_right.
(* end hide *)

(** * Terms over a duplicated alphabet *)
Section primed.
  Context { X : Set } {decX : decidable_set X }.

  (** We define the primed variables of a term [e] as follows: *)
  Fixpoint 𝐒𝐏'_variables (e : 𝐒𝐏 X') : list (@X' X) :=
    match e with
    | 𝐒𝐏_var (a,_) => [(a,true);(a,false)]
    | e ⨾ f | e ∥ f => 𝐒𝐏'_variables e ++ 𝐒𝐏'_variables f
    end.

  (** By construction this function generates balanced sets. *)
  Lemma is_balanced_𝐒𝐏'_variables u : is_balanced (𝐒𝐏'_variables u).
  Proof.
    induction u;simpl;unfold is_balanced;intros a.
    - destruct x;simpl;split;intros [E|[E|F]];try inversion E;tauto.
    - rewrite in_app_iff,in_app_iff,(IHu1 _),(IHu2 _);tauto.
    - rewrite in_app_iff,in_app_iff,(IHu1 _),(IHu2 _);tauto.
  Qed.

  Lemma balanced_𝐒𝐏'_variables u : balanced (𝐒𝐏'_variables u) = true.
  Proof. apply balanced_spec,is_balanced_𝐒𝐏'_variables. Qed.

  (** The result of this function is always a superset of the set of
  variables of the term. *)
  Lemma 𝐒𝐏_variables_𝐒𝐏'_variables e : 𝒱 e  ⊆ 𝐒𝐏'_variables e.
  Proof.
    induction e;simpl.
    - destruct x as (a,[|]);intros b [<-|F];simpl;auto.
    - rsimpl;rewrite IHe1,IHe2;reflexivity.
    - rsimpl;rewrite IHe1,IHe2;reflexivity.
  Qed.

  (** The primed variable [(a,b)] is in [𝐒𝐏'_variables u] exactly when
  either [(a,true)] or [(a,false)] appears in [𝒱 u ]. *)
  Lemma 𝐒𝐏_variables_𝐒𝐏'_variables_iff u  a b :
    (a,b) ∈ (𝐒𝐏'_variables u) <-> (a,true) ∈ 𝒱 u  \/ (a,false) ∈ 𝒱 u .
  Proof.
    revert a b;induction u;intros a;simpl.
    - intros [|];destruct x as (b&[|]);simpl;split;intros E;
      repeat tauto || destruct E as [E|E] || inversion E;auto.
    - intro;rsimpl;repeat rewrite in_app_iff;
      rewrite IHu1,IHu2; tauto.
    - intro;rsimpl;repeat rewrite in_app_iff;
      rewrite IHu1,IHu2; tauto.
  Qed.

  (** This is another formulation of the same result. *)
  Lemma fst_𝐒𝐏_variables (u : 𝐒𝐏 (@X' X)) : map fst (𝒱 u)  ≈ map fst (𝐒𝐏'_variables u).
  Proof.
    unfold equivalent;intros x;repeat rewrite in_map_iff.
    setoid_rewrite surjective_pairing.
    setoid_rewrite 𝐒𝐏_variables_𝐒𝐏'_variables_iff.
    split.
    - intros ((a,[|])&<-&h);simpl in *;[exists(a,true)|exists (a,false)];auto.
    - setoid_rewrite <- surjective_pairing.
      intros ((a,b)&<-&[h|h]);eexists;split;try apply h;auto.
  Qed.

  (** The length of the result of this function is the size of the
  term plus one. *)  
  Lemma 𝐒𝐏_size_𝐒𝐏'_variables u :
    # (𝐒𝐏'_variables u) = S (|u|).
  Proof.
    induction u as [(a,b)|u1 u2|u1 u2];rsimpl;auto;
      repeat rewrite app_length;lia.
  Qed.
End primed.

Section reduce.
  Context { X : Set }{ decX : decidable_set X }.
  Context {A : list X}.
  
  Fixpoint reduce_term  t :=
    match t with
    | 𝐒𝐏_var x => [𝐒𝐏_var x]
    | t1 ⨾ t2 =>
      match inclb (𝒱 t1) A,inclb (𝒱 t2) A with
      | true,true => reduce_term t1 ++ reduce_term t2 ++ bimap 𝐒𝐏_seq (reduce_term t1) (reduce_term t2)
      | true,false => reduce_term t2 ++ bimap 𝐒𝐏_seq (reduce_term t1) (reduce_term t2)
      | false,true => reduce_term t1 ++ bimap 𝐒𝐏_seq (reduce_term t1) (reduce_term t2)
      | false,false  => bimap 𝐒𝐏_seq (reduce_term t1) (reduce_term t2)
      end
    | t1 ∥ t2 => bimap 𝐒𝐏_par (reduce_term t1) (reduce_term t2)
    end.

  Lemma reduce_term_inf s t :
    s ∈ reduce_term t -> A ⊨ s << t.
  Proof.
    revert s;induction t;intro s;simpl.
    - intros [<-|F];tauto||auto.
    - case_eq (inclb (𝒱 t1) A);case_eq (inclb (𝒱 t2) A);
        intros T1 T2;rewrite inclb_correct in T1||clear T1;rewrite inclb_correct in T2||clear T2;
          repeat rewrite in_app_iff;intros I;repeat destruct I as [I|I];
            try apply in_bimap in I as (s1&s2&<-&I1&I2);auto.
      + rewrite (IHt1 s I);auto.
      + rewrite (IHt2 s I);auto.
      + rewrite (IHt2 s I);auto.
      + rewrite (IHt1 s I);auto.
    - intro I;apply in_bimap in I as (s1&s2&<-&I1&I2);auto.
  Qed.

  Lemma reduce_term_In t : t ∈ reduce_term t.
  Proof.
    induction t;simpl;auto.
    - destruct (inclb (𝒱 t1) A),(inclb (𝒱 t2) A);
        mega_simpl;repeat right;auto;apply in_bimap;exists t1,t2;auto.
    - apply in_bimap;exists t1,t2;auto.
  Qed.
        
  
End reduce.
Arguments reduce_term {X decX} A.

Section language.
  (** * Language interpretation *)
  Context { X : Set }.

  Open Scope lang.

  (** We interpret the sequential product by concatenation and the
  parallel product by intersection. *)
  Global Instance to_lang_𝐒𝐏 {Σ} : semantics 𝐒𝐏 language X Σ :=
    fix to_lang_𝐒𝐏 σ e:=
      match e with
      | 𝐒𝐏_var a => (σ a)
      | e ⨾ f => (to_lang_𝐒𝐏 σ e) ⋅ (to_lang_𝐒𝐏 σ f)
      | e ∥ f => (to_lang_𝐒𝐏 σ e) ∩ (to_lang_𝐒𝐏 σ f)
      end.

  (** We define semantic equality and containment of series parallel
  from those of languages. *)
  Global Instance semSmaller_𝐒𝐏 : SemSmaller (𝐒𝐏 X) :=
    (@semantic_containment _ _ _ _ _).

  Global Instance 𝐒𝐏_sem_PreOrder :  PreOrder (fun e f : 𝐒𝐏 X => e ≲ f).
  Proof. once (typeclasses eauto). Qed.
  
  Global Instance semEquiv_𝐒𝐏 : SemEquiv (𝐒𝐏 X) :=
    (@semantic_equality _ _ _ _ _).
  
  Global Instance 𝐒𝐏_sem_equiv : Equivalence (fun e f : 𝐒𝐏 X => e ≃ f).
  Proof. once (typeclasses eauto). Qed.

  Global Instance 𝐒𝐏_sem_PartialOrder :
    PartialOrder (fun e f : 𝐒𝐏 X => e ≃ f) (fun e f : 𝐒𝐏 X => e ≲ f).
  Proof.
    eapply semantic_containment_PartialOrder;once (typeclasses eauto).
  Qed.
  
  (* begin hide *)
  Hint Unfold semSmaller_𝐒𝐏 semEquiv_𝐒𝐏 : semantics.
  Section invert.
    Context { Σ : Set } { σ : 𝕬[X→Σ] }.
    Lemma 𝐒𝐏_prod e f : (⟦e ⨾ f⟧σ) = (⟦e⟧σ) ⋅ (⟦f⟧σ).
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐒𝐏_intersection e f : (⟦e∥f⟧σ) = (⟦e⟧σ) ∩ (⟦f⟧σ).
    Proof. unfold interprete;simpl;auto. Qed.
    Lemma 𝐒𝐏_variable a : (⟦𝐒𝐏_var a⟧ σ) = σ a. 
    Proof. unfold interprete;simpl;auto. Qed.
  End invert.
  Hint Rewrite @𝐒𝐏_variable @𝐒𝐏_intersection @𝐒𝐏_prod : simpl_typeclasses.
  (* end hide *)
  
  (** Both products are monotone. *)
  Global Instance sem_incl_𝐒𝐏_seq :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐒𝐏_seq X).
  Proof.
    intros e f E g h F Σ σ;simpl;rsimpl;revert E F;
      repeat autounfold with semantics;firstorder.
  Qed.

  Global Instance sem_incl_𝐒𝐏_inter :
    Proper (ssmaller ==> ssmaller ==> ssmaller) (@𝐒𝐏_par X).
  Proof.
    intros e f E g h F Σ σ;simpl;rsimpl;revert E F;
      repeat autounfold with semantics;firstorder.
  Qed.
  
  Global Instance sem_eq_𝐒𝐏_seq :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐒𝐏_seq X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.

  Global Instance sem_eq_𝐒𝐏_inter :
    Proper (sequiv ==> sequiv ==> sequiv) (@𝐒𝐏_par X).
  Proof. eapply (@sem_eq_op _ _ ssmaller);once (typeclasses eauto). Qed.

  (** An assignment [σ] is test-compatible with the set of variables
  of [u] if and only if the [σ]-interpretation of [u] contains the
  empty list.*)
  Lemma test_compatible_nil (Σ : Set) (u : 𝐒𝐏 X) (σ : 𝕬[X→Σ]) :
    𝒱 u  ⊢ σ <-> (⟦u⟧ σ) [].
  Proof.
    induction u;simpl;split;rsimpl.
    - intros h;apply h;simpl;auto.
    - simpl;intros I ? [->|F]; simpl in *;tauto.
    - intro h;apply test_compatible_app in h as (h1&h2).
      exists [];exists [];repeat split;
      [apply IHu1|apply IHu2];auto.
    - intros (w1&w2&h1&h2&E);destruct w1;[|discriminate].
      destruct w2;[|discriminate].
      apply test_compatible_app;split;tauto.
    - intro h;apply test_compatible_app in h as (h1&h2).
      split;[apply IHu1|apply IHu2];auto.
    - intros (h1&h2); apply test_compatible_app;split;tauto.
  Qed.

  Close Scope lang.
End language.
(* begin hide *)
Hint Rewrite @𝐒𝐏_variable @𝐒𝐏_intersection @𝐒𝐏_prod : simpl_typeclasses.
Hint Unfold semSmaller_𝐒𝐏 semEquiv_𝐒𝐏 : semantics. 
(* end hide *)